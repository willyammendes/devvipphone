const config = {
    apiDomain:
      process.env.NODE_ENV === "development"
        ? "https://api-new.monitchat.com/"
        : "https://api.monitchat.com/",
    apiRootUrl:
      process.env.NODE_ENV === "development"
        ? "https://api-new.monitchat.com/api/v1"
        : "https://api.monitchat.com/api/v1",
    appDomain:
      process.env.NODE_ENV === "development"
        ? "https://monitchat.ddns.net"
        : "https://monitchat.com"
  };
  
  export default config;