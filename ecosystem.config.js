module.exports = {
    apps : [{
      name: 'App Monitchat',
      script: 'npm',
      args: 'run start:production',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env_production: {
        NODE_ENV: 'production'
      }
    }],
  
    deploy : {
      production : {
        user : 'ubuntu',
        host : '189.50.15.226',
        ref  : 'origin/master',
        repo : 'git@bitbucket.org:willyammendes/devvipphone.git',
        path : '/var/www/html/app.monitchat.com',
        'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
      }
    }
  };