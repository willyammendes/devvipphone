import axios from "axios";
import MonitchatConfig from "../config";

axios.defaults.baseURL = MonitchatConfig.apiRootUrl;

axios.interceptors.request.use(request => {
  request.headers.Authorization = `Bearer ${localStorage.token}`;
  return request;
});

export default axios;
