import Echo from "laravel-echo";
import config from "../config.js";

window.Pusher = require("pusher-js");

const socket = new Echo({
  broadcaster: "pusher",
  key:
    process.env.NODE_ENV === "development"
      ? "5a5bf5c2ec524bfd9d77"
      : "83bfc2b3c9ff978c217c",
  authEndpoint: `${config.apiDomain}broadcasting/auth`,
  cluster: "mt1",
  encrypted: true,
  httpHost:
    process.env.NODE_ENV === "development"
      ? "ws-test.monitchat.com"
      : "websockets.monitchat.com",
  wsHost:
    process.env.NODE_ENV === "development"
      ? "ws-test.monitchat.com"
      : "websockets.monitchat.com",
  wsPort: 6001,
  httpPort: 6001,
  httpsHost:
    process.env.NODE_ENV === "development"
      ? "ws-test.monitchat.com"
      : "websockets.monitchat.com",
  wssHost:
    process.env.NODE_ENV === "development"
      ? "ws-test.monitchat.com"
      : "websockets.monitchat.com",
  wssPort: 6001,
  httpsPort: 6001,
  enabledTransports: ["ws", "wss"]
});

socket.connector.pusher.config.auth.headers.Authorization = `Bearer ${
  localStorage.token
}`;

export default socket;
