const EVENTS = [
    { type: 'MessageReceived', channel: 'message-received-{company_id}-{account_number}'}
]

export default EVENTS;