import React from "react";
import ReactDOM from "react-dom";
import api from "./services/api";
import { BrowserRouter, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import { Provider } from "react-redux";
import jwt from "jsonwebtoken";
import App from "./App";
import { Creators as AuthActions } from "./store/ducks/auth";
import store from "./store/store";

if (localStorage.token) {
  try {
    store.dispatch(AuthActions.setCurrentUser(jwt.decode(localStorage.token)));
  } catch (err) {
    localStorage.removeItem("token");
    store.dispatch(AuthActions.userLoggedOut());
    window.location.reload();
  }
} else {
  localStorage.removeItem("token");
  store.dispatch(AuthActions.userLoggedOut());
}

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Route component={App} />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);
