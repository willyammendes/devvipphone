import React from "react";
import PropTypes from "prop-types";
import HomePage from "./components/pages/HomePage";
import ConfigPage from "./components/pages/ConfigPage";
import ClientPage from "./components/pages/ClientPage";
import TicketCategoryPage from "./components/pages/TicketCategoryPage";
import QuickMessagePage from "./components/pages/QuickMessagePage";
import SocialMediaPage from "./components/pages/SocialMediaPage";
import TicketStatusPage from "./components/pages/TicketStatusPage";
import DepartmentPage from "./components/pages/DepartmentPage";
import WebHookPage from "./components/pages/WebHookPage";
import ServerPage from "./components/pages/ServerPage";
import UsersPage from "./components/pages/UsersPage";
import BadWordPage from "./components/pages/BadWordPage";
import AlertWordPage from "./components/pages/AlertWordPage";
import ParametersPage from "./components/pages/ParametersPage";
import PlansPage from "./components/pages/PlansPage";
import ContactsGroupsPages from "./components/pages/ContactsGroupsPages";
import ContactPage from "./components/pages/ContactPage";
import RestrictedContactsPages from "./components/pages/RestrictedContactsPages";
import RelationshipPages from "./components/pages/RelationshipPages";
import ProfessionalsPages from "./components/pages/ProfessionalsPages";
import PlacesPages from "./components/pages/PlacesPages";
import SpecialitiesPages from "./components/pages/SpecialitiesPages";
import SchedulesPages from "./components/pages/SchedulesPages";
import TriggersPages from "./components/pages/TriggersPages";
import TicketsPages from "./components/pages/TicketsPages";
import CampaignPages from "./components/pages/CampaignPages";
import BotContextPages from "./components/pages/BotContextPages";
import BotVarsPages from "./components/pages/BotVarsPages";
import BotSubstitutionsPages from "./components/pages/BotSubstitutionsPages";
import EventsPages from "./components/pages/EventsPages";
import CompaniesPages from "./components/pages/CompaniesPages";
import EmailPages from "./components/pages/EmailPages";
import SMSPages from "./components/pages/SMSPages";
import CallPages from "./components/pages/CallPages";
import SummaryPages from "./components/pages/SummaryPages";
import ProfilePages from "./components/pages/ProfilePages";
import ReportPages from "./components/pages/ReportPages";
import CompanyPages from "./components/pages/CompanyPages";
import MonitoringPages from "./components/pages/MonitoringPages";
import InterruptionPages from "./components/pages/InterruptionPages";
import LoginPage from "./components/pages/LoginPage";
import RegisterPage from "./components/pages/RegisterPage";
import ForgotPage from "./components/pages/ForgotPage";
import ResetPage from "./components/pages/ResetPage";
import RolesPages from "./components/pages/RolesPages";
import PermissionsPages from "./components/pages/PermissionsPages";
import AuthorizedRoute from "./components/routes/AuthorizedRoute";
import GuestRoute from "./components/routes/GuestRoute";
import "./stylesheets/style.css";
import ExtraInputPage from "./components/pages/ExtraInputPage";
import Page from "./components/pages/Page";
import CustomPage from "./components/pages/CustomPage";
import InvoicePage from "./components/pages/InvoicePage";
import WhatsappMonitorPage from "./components/pages/WhatsappMonitorPage";
import UserMonitorPage from "./components/pages/UserMonitorPage";

const App = ({ location }) => (
  <div className="App">
    <AuthorizedRoute
      location={location}
      path="/"
      exact
      component={SummaryPages}
    />
    <AuthorizedRoute
      location={location}
      path="/conversation/:conversationId/ticket/:ticketId"
      exact
      requiredPermission="list-conversation"
      component={HomePage}
    />
    <AuthorizedRoute
      location={location}
      path="/conversation"
      requiredPermission="list-conversation"
      exact
      component={HomePage}
    />
    <AuthorizedRoute
      location={location}
      path="/whatsapp-monitor"
      requiredPermission="delete-permission"
      exact
      component={WhatsappMonitorPage}
    />
    <AuthorizedRoute
      location={location}
      path="/user-monitor"
      requiredPermission="delete-permission"
      exact
      component={UserMonitorPage}
    />
    <AuthorizedRoute
      location={location}
      path="/configurations"
      requiredPermission="list-configuration"
      exact
      component={ConfigPage}
    />
    <AuthorizedRoute
      location={location}
      path="/invoices"
      requiredPermission="edit-company-pricing-plan"
      exact
      component={InvoicePage}
    />
    <AuthorizedRoute
      location={location}
      path="/client"
      requiredPermission="edit-client"
      exact
      component={ClientPage}
    />
    <AuthorizedRoute
      location={location}
      path="/users"
      requiredPermission="edit-user"
      exact
      component={UsersPage}
    />
    <AuthorizedRoute
      location={location}
      path="/department"
      requiredPermission="edit-department"
      exact
      component={DepartmentPage}
    />
    <AuthorizedRoute
      location={location}
      path="/extra-input"
      requiredPermission="edit-extra-input"
      exact
      component={ExtraInputPage}
    />
    <AuthorizedRoute
      location={location}
      path="/page"
      exact
      component={Page}
      requiredPermission="edit-page"
    />
    <AuthorizedRoute
      location={location}
      path="/custom-page/:id"
      exact
      component={CustomPage}
    />
    <AuthorizedRoute
      location={location}
      path="/ticket-status"
      exact
      requiredPermission="list-ticket-status"
      component={TicketStatusPage}
    />
    <AuthorizedRoute
      location={location}
      path="/ticket-category"
      requiredPermission="list-ticket-category"
      exact
      component={TicketCategoryPage}
    />
    <AuthorizedRoute
      location={location}
      path="/quick-message"
      requiredPermission="edit-fast-message"
      exact
      component={QuickMessagePage}
    />
    <AuthorizedRoute
      location={location}
      path="/social-media"
      requiredPermission="edit-social-account"
      exact
      component={SocialMediaPage}
    />
    <AuthorizedRoute
      location={location}
      path="/webhook"
      requiredPermission="list-webhook"
      exact
      component={WebHookPage}
    />
    <AuthorizedRoute
      location={location}
      path="/server"
      requiredPermission="list-server"
      exact
      component={ServerPage}
    />
    <AuthorizedRoute
      location={location}
      path="/bad-word"
      requiredPermission="list-bad-word"
      exact
      component={BadWordPage}
    />
    <AuthorizedRoute
      location={location}
      path="/alert-word"
      requiredPermission="list-alert-word"
      exact
      component={AlertWordPage}
    />
    <AuthorizedRoute
      location={location}
      path="/parameters"
      requiredPermission="list-parameter"
      exact
      component={ParametersPage}
    />
    <AuthorizedRoute
      location={location}
      path="/plans"
      requiredPermission="edit-pricing-plan"
      exact
      component={PlansPage}
    />
    <AuthorizedRoute
      location={location}
      path="/contacts"
      requiredPermission="list-contact"
      exact
      component={ContactPage}
    />
    <AuthorizedRoute
      location={location}
      path="/contacts/groups"
      requiredPermission="list-contact-group"
      exact
      component={ContactsGroupsPages}
    />
    <AuthorizedRoute
      location={location}
      path="/contacts/restricted-contacts"
      exact
      component={RestrictedContactsPages}
    />
    <AuthorizedRoute
      location={location}
      path="/relationship"
      requiredPermission="list-relationship"
      exact
      component={RelationshipPages}
    />
    <AuthorizedRoute
      location={location}
      path="/professionals"
      requiredPermission="list-professional"
      exact
      component={ProfessionalsPages}
    />
    <AuthorizedRoute
      location={location}
      path="/specialities"
      requiredPermission="list-speciality"
      exact
      component={SpecialitiesPages}
    />
    <AuthorizedRoute
      location={location}
      path="/schedules"
      requiredPermission="list-schedule"
      exact
      component={SchedulesPages}
    />
    <AuthorizedRoute
      location={location}
      path="/triggers"
      requiredPermission="list-bot-trigger"
      exact
      component={TriggersPages}
    />
    <AuthorizedRoute
      location={location}
      path="/places"
      requiredPermission="list-place"
      exact
      component={PlacesPages}
    />
    <AuthorizedRoute
      location={location}
      path="/tickets"
      requiredPermission="list-ticket"
      exact
      component={TicketsPages}
    />
    <AuthorizedRoute
      location={location}
      path="/campaign"
      requiredPermission="list-campaing"
      exact
      component={CampaignPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-bot-context"
      path="/bot-context"
      exact
      component={BotContextPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-bot-var"
      path="/bot-vars"
      exact
      component={BotVarsPages}
    />
    <AuthorizedRoute
      location={location}
      path="/bot-substitutions"
      requiredPermission="list-bot-substitution"
      exact
      component={BotSubstitutionsPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-company"
      path="/company"
      exact
      component={CompaniesPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-event"
      path="/events"
      exact
      component={EventsPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-email"
      path="/email"
      exact
      component={EmailPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-sms"
      path="/sms"
      exact
      component={SMSPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-call"
      path="/call"
      exact
      component={CallPages}
    />

    <AuthorizedRoute
      location={location}
      path="/profile"
      exact
      component={ProfilePages}
    />
    <AuthorizedRoute
      location={location}
      path="/report"
      requiredPermission="list-report"
      exact
      component={ReportPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-company"
      path="/companies"
      exact
      component={CompanyPages}
    />

    <AuthorizedRoute
      location={location}
      path="/monitoring"
      exact
      component={MonitoringPages}
    />
    <AuthorizedRoute
      location={location}
      path="/interruption-type"
      requiredPermission="list-interruption-type"
      exact
      component={InterruptionPages}
    />
    <AuthorizedRoute
      location={location}
      requiredPermission="list-role"
      path="/roles"
      exact
      component={RolesPages}
    />

    <AuthorizedRoute
      location={location}
      requiredPermission="list-permission"
      path="/permissions"
      exact
      component={PermissionsPages}
    />

    <GuestRoute location={location} path="/login" exact component={LoginPage} />
    <GuestRoute
      location={location}
      path="/register"
      exact
      component={RegisterPage}
    />
    <GuestRoute
      location={location}
      path="/forgot"
      exact
      component={ForgotPage}
    />
    <GuestRoute location={location} path="/reset" component={ResetPage} />
  </div>
);

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};
export default App;
