import React from "react";
import PropTypes from "prop-types";
import api from "../../api/ticket";
import apiconversation from "../../api/conversation";
import apitask from "../../api/tasks";
import apiactivities from "../../api/activities";
import apiclient from "../../api/client";
import apicontact from "../../api/contact";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import Icon from "@mdi/react";
import { mdiChevronRight } from "@mdi/js";
import TicketClientInfo from "./TicketClientInfo";
import OldTickets from "./OldTickets";
import InfoPersonalizado from "./InfoPersonalizadaCliente";

function changeClass() {
  const element = document.getElementsByName("sideinfo");
  for (let i = 0; i < element.length; i += 1) {
    element[i].classList.toggle("ativo");
  }
}

class TicketInfo extends React.Component {
  state = {
    tasks: [],
    activities: [],
    records: [],
    conversations: [],
    columns: {},
    clients: [],
    order: {},
    errors: "",
    conversation: [],
    loading: false,
    tableTickets: true,
    loadingContacts: false,
    loadingStats: false,
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    conversationModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false,
    conversationActive: [],
    oldTickets: [],
    comments: [],
    contact: [],
    client: {},
    hideArrow: true,
    activeConversation: {},
    copySuccess: false,
    search: "",
    options: false,
    isSearch: true,
  };

  componentWillMount() {
    this.fetchRecords();
    this.fetchConversations();
    this.fetchTasks();
    this.fetchActivities();
    this.fetchClient();
    this.onInit(this.props);
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.activeConversation !== prevProps.activeConversation) {
      this.fetchClient();
    }
  }

  callbackClient = (client) => {
    this.setState({ client });
  };

  componentWillReceiveProps(prevProps) {
    if (prevProps.activeConversation.contact.client.length === 0) {
      this.setState({ client: {} });
    }
  }

  copy = () => {
    this.setState(
      {
        copySuccess: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            copySuccess: false,
          });
        }, 4000);
      }
    );
  };

  onAddItem = (e, { value }) => {
    const { options } = this.state;
    if (options) {
      this.setState((prevState) => ({
        options: [{ text: value, value }, ...prevState.options],
        isSearch: true,
      }));
    } else {
      this.setState({
        options: [{ text: value, value }],
        isSearch: true,
      });
    }
  };

  onSearch = async () => {
    const { search } = this.state;
    try {
      const optionsSearch = await apiclient.client.fetchAll({ search });

      if (optionsSearch.data.length > 0) {
        this.setState({
          options: [
            ...optionsSearch.data.map((data) => {
              return { key: data.id, text: data.name, value: data };
            }),
          ],
        });
      } else {
        this.setState({
          isSearch: false,
        });
      }
    } catch (error) {
      this.setState({ options: [] });
    }
  };

  handleSearch = (e, { searchQuery }) => {
    const { isSearch } = this.state;
    if (isSearch) {
      clearTimeout(this.timer);
      this.setState({ search: searchQuery });
      this.timer = setTimeout(this.onSearch, 300);
    }
  };

  handleChange = async (e, { name, value }) => {
    const { client } = this.state;
    if (name === "name") {
      if (typeof value === "object") {
        this.setState({
          client: {
            ...client,
            ...value,
          },
          isSearch: true,
        });
      } else {
        !value && this.cleanSearch();
        this.setState({
          client: {
            ...client,
            name: value,
          },
          isSearch: true,
        });
      }
    } else {
      this.setState({
        client: {
          ...client,
          [name]: value,
        },
      });
    }
  };

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { client } = this.state;

    const extraFieldIndex = client.extra_fields.findIndex(
      (c) => c.id === extraFieldId
    );

    this.setState({
      client: {
        ...client,
        extra_fields: [
          ...client.extra_fields.slice(0, extraFieldIndex),
          {
            ...client.extra_fields[extraFieldIndex],
            value: rating !== undefined ? rating : value,
          },
          ...client.extra_fields.slice(extraFieldIndex + 1),
        ],
      },
    });

    clearTimeout(this.timer);
    this.timer = setTimeout(this.saveChanges, 800);
  };

  saveChanges = () => {
    const { client } = this.state;
    this.setState({ loading: true });
    apiclient.client.update(client.id, client).then((res) => {
      this.setState({
        client: res.data,
        loading: false,
      });
    });
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.ticket.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  fetchConversations = async (params) => {
    this.setState({ loading: true });

    return await apiconversation.conversations.fetchAll(params).then((res) => {
      this.setState({
        conversations: res,
        loading: false,
      });
    });
  };

  fetchClient = async (params) => {
    const { activeConversation } = this.props;
    if (activeConversation.contact.client.id) {
      this.setState({ loading: true });
      return await apiclient.client
        .fetchClient(activeConversation.contact.client.id)
        .then((res) => {
          this.setState({
            client: res.data,
            loading: false,
          });
        });
    }
    await apiclient.client.fetchAll().then((data) => {
      this.setState({
        client: {
          extra_fields: data.extra_fields,
        },
        loading: false,
      });
    });
  };

  fetchTasks = async (params) => {
    this.setState({ loading: true });

    return await apitask.task.fetchAll(params).then((res) => {
      this.setState({
        tasks: res.data,
        loading: false,
      });
    });
  };
  fetchActivities = async (params) => {
    this.setState({ loading: true });

    return await apiactivities.activities.fetchAll(params).then((res) => {
      this.setState({
        activities: res.data,
        loading: false,
      });
    });
  };
  cleanSearch = async (e) => {
    const { activeConversation } = this.props;
    if (activeConversation.contact.client.id) {
      await apicontact.contact.update(activeConversation.contact.id, {
        ...activeConversation.contact,
        client_id: "",
        client: {},
      });
      this.props.updateContact();
    }
    const extraFields = await apiclient.client.fetchAll();
    this.setState({
      client: {
        name: "",
        city: "",
        address: "",
        additional_info: "",
        code: "",
        document: "",
        email: "",
        neighborhood: "",
        number: "",
        phone_number: "",
        state: "",
        zip_code: "",
        extra_fields: extraFields.extra_fields,
      },
    });
  };

  selectRow = (selectedDataId) => {
    const { activeConversation } = this.props;
    const dataIndex = activeConversation.tickets;

    const conversationActive = activeConversation;

    const oldTickets = activeConversation.tickets;

    this.onConversationSelected(activeConversation.id);

    this.setState({
      selectedDataIndex: dataIndex,
      conversationModalOpen: true,
      conversationActive,
      oldTickets,
      loading: false,
    });
  };
  onInit = (props) =>
    props.conversations.length === 0 && props.fetchAll().then(() => {});
  onConversationSelected = (id) =>
    this.props.conversationSelected(
      this.props.conversation.data.find((c) => c.id === id)
    );

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      conversationModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      activeConversation,
      currentCompany,
      user,
      copySuccess,
    } = this.props;
    const { client } = this.state;

    const activeTicketId = activeConversation.active_ticket_id;
    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );

    return (
      <div
        className="side-info-ticket topoinfo"
        name="sideinfo"
        onMouseOver={changeClass}
        onMouseOut={changeClass}
      >
        <div className="openMenu">
          <div className="iconSeta">
            <Icon path={mdiChevronRight} size={0.8} />
          </div>
        </div>
        <TicketClientInfo
          contact={activeConversation.contact}
          callbackClient={this.callbackClient}
          company={currentCompany}
          user={user}
          copySuccess={copySuccess}
          copy={this.copy}
        />
        <InfoPersonalizado
          contact={client}
          loading={this.state.loading}
          handleChange={this.handleChange}
          saveChanges={this.saveChanges}
          handleExtraFields={this.handleExtraFields}
          fetchUserRecords={this.fetchRecords}
          activeConversation={activeConversation}
          reload={(e) => this.props.updateContact()}
          refresh={(e) => {}}
          cleanSearch={this.cleanSearch}
          options={this.state.options}
          handleSearch={this.handleSearch}
          onAddItem={this.onAddItem}
        />
        <OldTickets
          activeTicket={activeTicket}
          tickets={activeConversation.tickets}
          selectRow={(ticket) => this.selectRow(ticket.id)}
        />
      </div>
    );
  }
}

TicketInfo.propTypes = {
  activeConversation: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
  conversation: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        tickets: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            user: PropTypes.any.isRequired,
          })
        ).isRequired,
      }).isRequired
    ),
  }).isRequired,

  activeConversationId: PropTypes.number.isRequired,
  conversationSelected: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

const mapStateToProps = ({ conversation, user }) => {
  return {
    user: user.user,
    currentCompany: user.user.companies.find(
      (c) => c.id === user.user.company_id
    ),
    conversation,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter ? conversation.filter : "",
    conversations: !conversation.data ? [] : Object.values(conversation.data),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TicketInfo);
