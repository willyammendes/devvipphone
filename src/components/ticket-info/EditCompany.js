import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../../api/contact";
import apigroup from "../../api/group";
import apiclient from "../../api/client";
import DataTable from "../table/DataTable";
import ClientModal from "../client/modal/ClientModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { Button } from "semantic-ui-react";
import ButtonAdd from "./ButtonAdd.js";

class EditCompany extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    groups: [],
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingGroups: false,
    loadingClients: false,
    save_alert: false,
    clients_dropdown: true
  };

  select = selectedDataId => {
    const { groups, clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (groups.length === 0) this.fetchGroups();
    if (
      !!data.client_id &&
      clients.filter(c => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  newDataClick = () => {
    this.setState({
      editModalOpen: true
    });
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  onSearchClientChange = e => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.client.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then(clients => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map(c => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false
        });
      });
    }
  };

  onSelectGroup = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  addGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingGroups: true });

    apigroup.group
      .submit({ name: value })
      .then(data => {
        this.setState({
          groups: [...this.state.groups].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              groups: [...records[dataIndex].groups].concat(data.data.id)
            },
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .then(() => {
        this.setState({ loadingGroups: false });
      });
  };

  componentWillMount() {
    this.fetchGroups();
    this.fetchClients();
  }

  fetchClients = async () => {
    this.setState({ loadingClients: true });
    await apiclient.client.fetchAll().then(clients => {
      this.setState({
        clients: clients.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingClients: false
      });
    });
  };

  fetchGroups = async () => {
    this.setState({ loadingGroups: true });
    await apigroup.group.fetchAll().then(groups => {
      this.setState({
        groups: groups.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingGroups: false
      });
    });
  };

  getClient = async id => {
    this.setState({ loadingClients: true });
    await apiclient.client.get(id).then(client => {
      this.setState({
        clients: [...this.state.clients].concat({
          key: client.id,
          value: client.id,
          text: client.name
        }),
        loadingClients: false
      });
    });
  };

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.contact
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = contact => {
    const { selectedDataIndex, records } = this.state;
    this.setState({ loading: true });

    api.contact
      .update(contact.id, contact)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.contact.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };
  render() {
    const {
      loading,
      editModalOpen,
      groups,
      clients,
      loadingGroups,
      loadingClients,
      clients_dropdown
    } = this.state;
    const { data: conversations } = this.props.conversation;
    const { activeConversationId } = this.props.conversation;
    const activeConversation = conversations.find(
      c => c.id === activeConversationId
    );

    return (
      <div className="icone-add">
        <ButtonAdd
          onAddClick={this.newDataClick}
          icon="edit outline"
          className="editar-card"
          title="Adicionar Empresa"
        />
        {activeConversation ? (
          <ClientModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={() => this.update(activeConversation.contact)}
            onClickAdd={this.submit}
            onSelectGroup={this.onSelectGroup}
            onSelectClient={this.onSelectClient}
            onSearchClientChange={this.onSearchClientChange}
            handleGroupAddition={this.addGroup}
            contact={activeConversation.contact}
            modalHeader={
              activeConversation.contact.client.id
                ? `Edição do ${activeConversation.contact.client.name}`
                : "Novo Cliente"
            }
            open={editModalOpen}
            previousButtonEnabled={false}
            nextButtonEnabled={false}
            groups={groups}
            clients={clients}
            client={activeConversation.contact.client}
            loading={loading}
            loadingGroups={loadingGroups}
            loadingClients={loadingClients}
            showNavigation={false}
            clients_dropdown={clients_dropdown}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ conversation }) => ({ conversation });

export default connect(
  mapStateToProps,
  null
)(EditCompany);
