import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import PerfectScrollbar from "react-perfect-scrollbar";
import LoaderComponent from "../semantic/Loading";

const OldTickets = ({
  tickets,
  loadingTickets,
  setActiveTicket,
  conversation,
}) => {
  const navigateToElement = (ticket, e) => {
    const elmnt = document.getElementById(`ticket-${ticket.id}`);
    //elmnt.scrollIntoView();
    ticket.conversation_id = conversation.id;
    setActiveTicket(ticket, e);
  };

  return (
    <div className="previous-ticket">
      <PerfectScrollbar>
        <span>Tickets Anteriores</span>
        {loadingTickets ? (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        ) : (
          <div className="tickets">
            {tickets.map((ticket) => (
              <p
                title={
                  ticket.current_progress >= 100 ? "Ticket Finalizado" : ""
                }
                className={
                  ticket.current_progress >= 100
                    ? "closed-ticket"
                    : "active-ticket"
                }
                key={`ticket-item-${ticket.id}`}
              >
                <a href onClick={(e) => navigateToElement(ticket, e)}>
                  #{ticket.ticket_number}
                </a>{" "}
                - {ticket.created_at}
              </p>
            ))}
          </div>
        )}
      </PerfectScrollbar>
    </div>
  );
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => {
  return {
    loadingTickets: state.conversation.loadingTickets,
    activeTicketIndex: state.conversation.data.find(
      (c) => c.id === state.conversation.activeConversationId
    ).activeTicketIndex,
    activeConversationId: state.conversation.activeConversationId,
    conversation: state.conversation.data.find(
      (c) => c.id === state.conversation.activeConversationId
    ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OldTickets);
