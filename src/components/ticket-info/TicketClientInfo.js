import React, { useState } from "react";
import Icon from "@mdi/react";
import { mdiPhone, mdiEmail, mdiWhatsapp } from "@mdi/js";
import EditClient from "./EditClient";
import { Modal, Button } from "semantic-ui-react";
import api from "../../services/api";

const TicketClientInfo = ({
  contact,
  company,
  user,
  copySuccess,
  copy,
  callbackClient,
}) => {
  const [showModal, setModal] = useState(false);
  const [copyteste, setCopyteste] = useState(false);

  const clickToCall = () => {
    const url = `${
      company.parameters.monitcall_base_url
    }/apis/v2/ligar.php?chave=${
      company.parameters.monitcall_client_key
    }&ramal=${user.branch_line}&destino=${contact.phone_number.substring(
      2
    )}&controle=${Math.floor(Math.random() * Math.floor(100000))}`;
    setModal(!showModal);

    let monitcall = window.open(
      url,
      "",
      "width=" + window.screen.width / 2 + ",height=100"
    );

    setTimeout(() => {
      monitcall.close();
    }, 1500);
  };

  const syncWhatsappMessages = (id) => {
    api.get("/contact/getContactMessagesFromWhatsapp/" + id);
  };

  const teste = (e) => {
    e.preventDefault();
    var $body = document.getElementsByTagName("body")[0];

    var secretInfo = document.getElementById("secretInfo").innerHTML;

    var copyToClipboard = function(secretInfo) {
      var $tempInput = document.createElement("INPUT");
      $body.appendChild($tempInput);
      $tempInput.setAttribute("value", secretInfo);
      $tempInput.select();
      document.execCommand("copy");
      $body.removeChild($tempInput);
    };

    copyToClipboard(secretInfo);
    copy();

    setTimeout(() => {
      setCopyteste(false);
    }, 4000);
  };
  return (
    <div className="info-cliente">
      <img
        src={
          contact.avatar.indexOf("whatsapp") !== -1
            ? contact.avatar
            : `${contact.avatar}`
        }
        alt=""
      />
      <h2>{contact.name}</h2>
      <span>{contact.client.name}</span>
      <p id="secretInfo" className="invisible">
        {contact.phone_number}
      </p>
      <p>{contact.additional_info}</p>
      <div className="links-empresa">
        {company.parameters.click_to_call ? (
          <a
            onClick={clickToCall}
            title={contact.phone_number}
            href
            style={{ backgroundColor: "#27bf5c" }}
            className={copyteste ? "tele copySuccess" : "tele"}
            onContextMenu={(e) => teste(e)}
          >
            <Icon path={mdiPhone} size={0.8} />
          </a>
        ) : (
          <a
            href={`tel:${contact.phone_number}`}
            title={contact.phone_number}
            className={copyteste ? "tele copySuccess" : "tele"}
            onContextMenu={(e) => teste(e)}
          >
            <Icon path={mdiPhone} size={0.8} />
          </a>
        )}
        <a href={`mailto:${contact.email}`} className="tele">
          <Icon path={mdiEmail} size={0.8} />
        </a>
        <a
          onClick={() => syncWhatsappMessages(contact.id)}
          className="tele"
          style={{ cursor: "pointer" }}
          title="Sincronizar com Whatsapp"
        >
          <Icon path={mdiWhatsapp} size={0.8} />
        </a>
        <EditClient contact={contact} callbackClient={callbackClient} />

        <Modal
          size={"mini"}
          open={showModal}
          onClose={() => setModal(!showModal)}
        >
          <Modal.Header>Click to Call</Modal.Header>
          <Modal.Content>
            <p>Atenda o ramal {user.branch_line} para completar a ligação!</p>
          </Modal.Content>
          <Modal.Actions>
            <Button
              positive
              icon="checkmark"
              labelPosition="right"
              content="Fechar"
              onClick={() => setModal(!showModal)}
            />
          </Modal.Actions>
        </Modal>
      </div>
    </div>
  );
};

export default TicketClientInfo;
