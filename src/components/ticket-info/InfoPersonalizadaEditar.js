import React from "react";
import { Form, Input } from "semantic-ui-react";
import ExtraFieldForm from "../extra-input/ExtraFieldForm";

const InfoPersonalizadaEditar = ({
  contact,
  handleChange,
  saveChanges,
  handleExtraFields
}) => (
  <div className="infos-editar">
    {contact.id && contact.extra_fields && (
      <ExtraFieldForm
        className="input-client"
        fluid
        columns={1}
        onBlur={saveChanges}
        extra_fields={contact.extra_fields}
        onChange={handleExtraFields}
      />
    )}
  </div>
);

export default InfoPersonalizadaEditar;
