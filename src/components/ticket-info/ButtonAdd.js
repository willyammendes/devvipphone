import React, { Component } from "react";
import Icon from "@mdi/react";
import { mdiOfficeBuilding } from "@mdi/js";

class ButtonAdd extends Component {
  render() {
    const { onAddClick, icon, title } = this.props;
    return (
      <span
        name={icon}
        title={title}
        onClick={onAddClick}
        className="editar-card"
        style={{ cursor: "pointer" }}
      >
        <Icon path={mdiOfficeBuilding} size={0.8} />
      </span>
    );
  }
}

export default ButtonAdd;
