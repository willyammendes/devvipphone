import React, { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import InfoEditar from "./InfoPersonalizadaEditar";
import { Button } from "semantic-ui-react";
import api from "../../api/client";
import apiContact from "../../api/contact";
import ClientModal from "../client/modal/ClientModal";
import AlertRed from "../alerts/AlertRed";
import AlertSuccess from "../alerts/AlertSuccess";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";

const InfoPersonalizadaCliente = ({
  contact,
  activeConversation,
  handleChange,
  saveChanges,
  handleExtraFields,
  loading,
  user,
  reload,
  refresh,
  options,
  cleanSearch,
  handleSearch,
  onAddItem,
}) => {
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [client, setClient] = useState(contact);
  const [contactCopy, setContactCopy] = useState(contact);
  const [error, setError] = useState(false);
  const [contactConversation, setContactConversation] = useState("");
  const [errorName, setErrorName] = useState(false);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState("");
  const [completSave, setCompletSave] = useState({
    id: "",
    company_id: "",
    additional_info: "",
    address: "",
    avatar: "",
    city: "",
    code: "",
    created_at: "",
    document: "",
    email: "",
    name: "",
    neighborhood: "",
    phone_number: "",
    zip_code: "",
    state: "",
    number: "",
    tickets: [],
    groups: [],
  });

  const fetchContactConversation = async () => {
    try {
      const getAgent = await apiContact.contact.fetchId(
        activeConversation.contact.id
      );
      setContactConversation(getAgent.data);
    } catch (error) {}
  };

  useEffect(() => {
    setClient(contact);
    setContactCopy(contact);
  }, [contact]);

  useEffect(() => {
    fetchContactConversation();
  }, []);

  const handleClose = () => {
    setEditModalOpen(!editModalOpen);
  };

  const update = async () => {
    try {
      const updated = await api.client.update(client.id, client);
      if (contactConversation.id) {
        await apiContact.contact.update(contactConversation.id, {
          ...contactConversation,
          client_id: client.id,
        });
        reload();
        refresh();
      }
      setMessage(updated.title);
      setSuccess(true);
      setEditModalOpen(!editModalOpen);
      setTimeout(() => {
        setSuccess(false);
      }, 2500);
    } catch (error) {
      setMessage("Operação não realizada!");
      setError(true);
      setEditModalOpen(!editModalOpen);
      setTimeout(() => {
        setError(false);
      }, 2500);
    }
  };

  const submit = async () => {
    try {
      if (client.name) {
        const saved = { ...completSave, ...client };
        const created = await api.client.submit(saved);
        setMessage(created.title);
        const updated = await apiContact.contact.update(
          contactConversation.id,
          { ...contactConversation, client_id: created.data.id }
        );
        setSuccess(true);
        setEditModalOpen(!editModalOpen);
        setTimeout(() => {
          setSuccess(false);
        }, 2500);
        reload();
        refresh();
      } else {
        setErrorName(true);
        setTimeout(() => {
          setErrorName(false);
        }, 2500);
      }
    } catch (error) {
      setMessage("Operação não realizada!");
      setError(true);
      setEditModalOpen(!editModalOpen);
      setTimeout(() => {
        setError(false);
      }, 2500);
    }
  };

  return (
    <div className="infos-personalizados">
      {error && <AlertRed message={message} />}
      {success && <AlertSuccess message={message} />}
      <PerfectScrollbar>
        {contact.id ? (
          <>
            <span style={{ marginTop: 0 }}>
              Informações da Empresa
              <Button
                style={{ background: "#fff", padding: 8 }}
                icon="edit"
                size="medium"
                onClick={handleClose}
                loading={loading}
              />
            </span>
            <InfoEditar
              contact={contact}
              handleChange={handleChange}
              saveChanges={saveChanges}
              handleExtraFields={handleExtraFields}
            />
          </>
        ) : (
          <span>
            Novo Cliente
            <Button
              style={{ background: "#fff" }}
              icon="plus square"
              onClick={handleClose}
              loading={loading}
            />
          </span>
        )}
        <ClientModal
          onChange={handleChange}
          handleExtraFields={handleExtraFields}
          client={client}
          handleClose={handleClose}
          open={editModalOpen}
          previousButtonEnabled
          nextButtonEnabled
          error={errorName}
          modalHeader={
            client.id ? `Edição do Cliente ${client.name}` : "Novo Cliente"
          }
          onClickSave={update}
          onClickAdd={submit}
          options={options}
          cleanSearch={cleanSearch}
          handleSearch={handleSearch}
          onAddItem={onAddItem}
        />
      </PerfectScrollbar>
    </div>
  );
};

const mapStateToProps = ({ conversation }) => {
  return {
    user: conversation.data.filter(
      (user) => user.id === conversation.activeConversationId
    ),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoPersonalizadaCliente);
