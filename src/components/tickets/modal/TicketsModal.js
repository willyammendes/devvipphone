import React from "react";

import { Modal, Button, Icon } from "semantic-ui-react";
import FormTickets from "../forms/FormTickets";

class TicketsModal extends React.Component {
  componentDidMount() {
    if (this.props.editModalOpen) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }
  save = () => {
    if (this.props.ticket.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      ticket,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      departments,
      onSelectDepartment,
      handleDepartmentAddition,
      loadingDepartments,
      ticketcategorys,
      onSelectCategory,
      handleCategoryAddition,
      loadingCategorys,
      contacts,
      users,
      onSelectUser,
      handleUserAddition,
      loadingUsers,
      ticketStatuss,
      onSelectStat,
      handleStatAddition,
      loadingStats,
      onSearchClientChange,
      onSelectClient,
      clients,
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loadingClients,
      loading,
      handleContactChange,
      handleDepartmentChange,
      handleCategoryChange,
      handleUserChange,
      handleStatusChange,
      tickets_conversation_id,
      save,
      comments,
      submitComment,
      newTicket,
      handleExtraFields,
    } = this.props;
    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_ticket">
              <div className="form-ticket">
                <FormTickets
                  ticket={ticket}
                  handleExtraFields={handleExtraFields}
                  submitComment={submitComment}
                  departments={departments}
                  ticketcategorys={ticketcategorys}
                  contacts={contacts}
                  users={users}
                  ticketStatuss={ticketStatuss}
                  clients={clients}
                  save={save}
                  loading={loading}
                  onChange={onChange}
                  newTicket={newTicket}
                  handleContactChange={handleContactChange}
                  handleDepartmentChange={handleDepartmentChange}
                  handleCategoryChange={handleCategoryChange}
                  handleUserChange={handleUserChange}
                  handleStatusChange={handleStatusChange}
                  onChecked={onChecked}
                  onSelectCategory={onSelectCategory}
                  onSelectDepartment={onSelectDepartment}
                  onSelectUser={onSelectUser}
                  onSelectStat={onSelectStat}
                  onSelectClient={onSelectClient}
                  onSearchClientChange={onSearchClientChange}
                  handleDepartmentAddition={handleDepartmentAddition}
                  handleCategoryAddition={handleCategoryAddition}
                  handleUserAddition={handleUserAddition}
                  handleStatAddition={handleStatAddition}
                  loadingDepartments={loadingDepartments}
                  loadingCategorys={loadingCategorys}
                  loadingClients={loadingClients}
                  loadingUsers={loadingUsers}
                  loadingStats={loadingStats}
                  tickets_conversation_id={tickets_conversation_id}
                  comments={comments}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default TicketsModal;
