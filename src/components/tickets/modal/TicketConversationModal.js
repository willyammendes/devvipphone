import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as TaskActions } from "../../../store/ducks/task";
import { Modal, Button, Icon, Divider, Label } from "semantic-ui-react";
import apiclient from "../../../api/client";
import api from "../../../api/contact";
import TicketInfo from "./TicketInfo";
import LoaderComponent from "../../semantic/Loading";
import PerfectScrollbar from "react-perfect-scrollbar";
import moment from "moment";
import "moment/locale/pt-br";
import logo from "../../../assets/img/logo.png";
import ConversationBody from "../../conversation-body/ConversationBody";
import TaskEvent from "../../events/TaskEvent";

class TicketConversationModal extends React.Component {
  state = {
    allTasks: false,
    showActivities: false,
    client: {},
    copySuccess: false,
    search: "",
    options: false,
    isSearch: true,
  };

  componentDidMount() {
    if (this.props.editModalOpen) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  componentWillMount() {
    this.fetchClient();
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.activeConversation !== prevProps.activeConversation) {
      this.fetchClient();
    }
  }

  onAddItem = (e, { value }) => {
    const { options } = this.state;
    if (options) {
      this.setState((prevState) => ({
        options: [{ text: value, value }, ...prevState.options],
        isSearch: true,
      }));
    } else {
      this.setState({
        options: [{ text: value, value }],
        isSearch: true,
      });
    }
  };

  onSearch = async () => {
    const { search } = this.state;
    try {
      const optionsSearch = await apiclient.client.fetchAll({ search });

      if (optionsSearch.data.length > 0) {
        this.setState({
          options: [
            ...optionsSearch.data.map((data) => {
              return { key: data.id, text: data.name, value: data };
            }),
          ],
        });
      } else {
        this.setState({
          isSearch: false,
        });
      }
    } catch (error) {
      this.setState({ options: [] });
    }
  };

  handleSearch = (e, { searchQuery }) => {
    const { isSearch } = this.state;
    if (isSearch) {
      clearTimeout(this.timer);
      this.setState({ search: searchQuery });
      this.timer = setTimeout(this.onSearch, 300);
    }
  };

  handleChange = async (e, { name, value }) => {
    const { client } = this.state;
    if (name === "name") {
      if (typeof value === "object") {
        this.setState({
          client: {
            ...client,
            ...value,
          },
          isSearch: true,
        });
      } else {
        !value && this.cleanSearch();
        this.setState({
          client: {
            ...client,
            name: value,
          },
          isSearch: true,
        });
      }
    } else {
      this.setState({
        client: {
          ...client,
          [name]: value,
        },
      });
    }
  };

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { client } = this.state;

    const extraFieldIndex = client.extra_fields.findIndex(
      (c) => c.id === extraFieldId
    );

    this.setState({
      client: {
        ...client,
        extra_fields: [
          ...client.extra_fields.slice(0, extraFieldIndex),
          {
            ...client.extra_fields[extraFieldIndex],
            value: rating !== undefined ? rating : value,
          },
          ...client.extra_fields.slice(extraFieldIndex + 1),
        ],
      },
    });
    if (client.id) {
      clearTimeout(this.timer);
      this.timer = setTimeout(this.saveChanges, 300);
    }
  };

  saveChanges = () => {
    const { client } = this.state;
    this.setState({ loading: true });
    apiclient.client.update(client.id, client).then((res) => {
      this.setState({
        client: res.data,
        loading: false,
      });
    });
  };
  copy = () => {
    this.setState(
      {
        copySuccess: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            copySuccess: false,
          });
        }, 4000);
      }
    );
  };
  fetchClient = async (params) => {
    const { activeConversation } = this.props;
    if (activeConversation.contact.client.id) {
      this.setState({ loading: true });
      return await apiclient.client
        .fetchClient(activeConversation.contact.client.id)
        .then((res) => {
          this.setState({
            client: res.data,
            loading: false,
          });
        });
    }
    const extraFields = await apiclient.client.fetchAll();
    this.setState({
      client: {
        extra_fields: extraFields.extra_fields,
      },
      loading: false,
    });
  };
  cleanSearch = async (e) => {
    const { activeConversation, reloadData, refresh } = this.props;
    if (activeConversation.contact.client.id) {
      await api.contact.update(activeConversation.contact.id, {
        ...activeConversation.contact,
        client_id: "",
        client: {},
      });
      reloadData();
      refresh();
    }
    const extraFields = await apiclient.client.fetchAll();
    this.setState({
      client: {
        name: "",
        city: "",
        address: "",
        additional_info: "",
        code: "",
        document: "",
        email: "",
        neighborhood: "",
        number: "",
        phone_number: "",
        state: "",
        zip_code: "",
        extra_fields: extraFields.extra_fields,
      },
    });
  };
  save = () => {
    if (this.props.ticket.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };
  showAllTasks = () => {
    this.setState({ allTasks: !this.state.allTasks });
  };
  toggleActivities = () => {
    this.setState({ showActivities: !this.state.showActivities });
  };

  deleteTask = (id) => {
    const joinTask = {
      ...id,
      status: id.status ? 0 : 1,
      start_at: moment.unix(id.start_at).format("YYYY-MM-DD HH:mm"),
      updated_at: moment.unix(id.updated_at).format("YYYY-MM-DD HH:mm"),
      created_at: moment.unix(id.created_at).format("YYYY-MM-DD HH:mm"),
      end_at: moment.unix(id.end_at).format("YYYY-MM-DD HH:mm"),
    };
    this.props.updatedTaskEvent({
      payload: joinTask,
    });

    this.props.deleteTask(id.id);
    setTimeout(() => {
      this.props.removeEventTask(id);
      this.props.fetchRecords();
    }, 2000);
  };

  changeStatus = (id) => {
    this.props.isEditing({
      loading: false,
    });

    const joinTask = {
      ...id,
      status: id.status ? 0 : 1,
      start_at: moment.unix(id.start_at).format("YYYY-MM-DD HH:mm"),
      updated_at: moment.unix(id.updated_at).format("YYYY-MM-DD HH:mm"),
      created_at: moment.unix(id.created_at).format("YYYY-MM-DD HH:mm"),
      end_at: moment.unix(id.end_at).format("YYYY-MM-DD HH:mm"),
    };

    this.props.alterEventTask({
      payload: joinTask,
    });

    this.props.alterTask({
      id: joinTask.id,
      payload: joinTask,
    });

    setTimeout(() => {
      this.props.fetchRecords();
    }, 2000);
  };
  render() {
    const {
      ticket,
      handleClose,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      activeConversation,
      oldTickets,
      tasks,
      activities,
      hideArrow,
      reloadData,
      refresh,
    } = this.props;

    const { allTasks, showActivities, client, copySuccess } = this.state;
    const activeTask = tasks.filter((c) => c.client_id === ticket.client_id);
    const activeActivities = activities.filter(
      (c) => c.user_id === this.props.user.id
    );

    return (
      <Modal
        size="fullscreen"
        closeIcon
        open={open}
        onClose={handleClose}
        closeOnDimmerClick={false}
        className="modal_fullscreen"
      >
        <TaskEvent />
        <Modal.Header className="modal_chat">
          <Link to="/" className="logo">
            <img src={logo} alt="" />
          </Link>
          <p>{modalHeader}</p>
        </Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_ticket_conversation">
              <div className="side_info_conversation_ticket">
                <TicketInfo
                  oldTickets={oldTickets}
                  activeConversation={activeConversation}
                  contact={client}
                  loading={this.state.loading}
                  user={this.props.user}
                  currentCompany={this.props.currentCompany}
                  handleChange={this.handleChange}
                  saveChanges={this.saveChanges}
                  copy={this.copy}
                  copySuccess={copySuccess}
                  handleExtraFields={this.handleExtraFields}
                  reload={reloadData}
                  refresh={refresh}
                  options={this.state.options}
                  cleanSearch={this.cleanSearch}
                  handleSearch={this.handleSearch}
                  onAddItem={this.onAddItem}
                />
              </div>
              <div className="conversation_body_modal">
                <ConversationBody
                  active={activeConversation}
                  reloadData={reloadData}
                />
              </div>
              <div className="tasks_body_modal">
                <div className="comments_holder">
                  {showActivities ? (
                    <PerfectScrollbar>
                      {/* Atividades */}
                      {loading && (
                        <div className="loading">
                          <LoaderComponent />
                        </div>
                      )}
                      <h2>
                        Atividades{" "}
                        <Button
                          color="blue"
                          className="button_right"
                          onClick={this.showAllTasks}
                        >
                          {allTasks
                            ? "Mostrar atividades do cliente"
                            : "Mostrar todas as atividades"}
                        </Button>
                      </h2>
                      <Divider />
                      {allTasks ? (
                        <div>
                          {activeActivities.map((activities) => (
                            <div
                              className="atividade"
                              key={`activity-${activities.id}`}
                            >
                              <div className="icone_atividade">
                                <Icon
                                  circular
                                  inverted
                                  color="green"
                                  name="users"
                                />
                              </div>

                              <div className="card_atividade">
                                <div className="foto_pessoa">
                                  <img
                                    src={this.props.user.avatar}
                                    alt={activities.user.name}
                                  />
                                </div>
                                <span className="pessoa">
                                  {activities.user.name}
                                </span>
                                <Label as="a" color="blue" basic>
                                  <Icon name="clock" />
                                  {activities.date}
                                </Label>
                                <span>{activities.comment}</span>
                              </div>
                            </div>
                          ))}
                        </div>
                      ) : (
                        <div>
                          {activities.map((activities) => (
                            <div
                              className="atividade"
                              key={`activity-${activities.id}`}
                            >
                              <div className="icone_atividade">
                                <Icon
                                  circular
                                  inverted
                                  color="green"
                                  name="users"
                                />
                              </div>

                              <div className="card_atividade">
                                <div className="foto_pessoa">
                                  <img
                                    src={this.props.user.avatar}
                                    alt={activities.user.name}
                                  />
                                </div>
                                <span className="pessoa">
                                  {activities.user.name}
                                </span>
                                <Label as="a" color="blue" basic>
                                  <Icon name="clock" />
                                  {activities.date}
                                </Label>
                                <span>{activities.comment}</span>
                              </div>
                            </div>
                          ))}
                        </div>
                      )}
                    </PerfectScrollbar>
                  ) : (
                    <PerfectScrollbar>
                      {/* Comentário */}
                      {loading && (
                        <div className="loading">
                          <LoaderComponent />
                        </div>
                      )}
                      <h2>
                        Tarefas{" "}
                        <Button
                          color="blue"
                          className="button_right"
                          onClick={this.showAllTasks}
                        >
                          {allTasks
                            ? "Mostrar tarefas do cliente"
                            : "Mostrar todas as tarefas"}
                        </Button>
                      </h2>
                      <Divider />
                      {allTasks ? (
                        <div>
                          {tasks.length > 0 &&
                            this.props.task.records.map((tasks) => (
                              <div
                                className="tarefa"
                                key={`ticket-item-${tasks.id}`}
                              >
                                <label className="toggleButtonTrash">
                                  <Icon
                                    name="trash alternate"
                                    size="large"
                                    color="red"
                                    onClick={() => this.deleteTask(tasks)}
                                  />
                                </label>
                                <label className="toggleButton">
                                  <input
                                    type="checkbox"
                                    checked={!!tasks.status}
                                  />
                                  <div onClick={() => this.changeStatus(tasks)}>
                                    <svg viewBox="0 0 44 44">
                                      <path
                                        d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758"
                                        transform="translate(-2.000000, -2.000000)"
                                      />
                                    </svg>
                                  </div>
                                </label>
                                <div className="holder_tarefa">
                                  <h4> {tasks.title}</h4>
                                  <span>{tasks.description}</span>
                                  <Label
                                    as="a"
                                    color={tasks.status ? "green" : "orange"}
                                  >
                                    <Icon name="clock" />
                                    {moment
                                      .unix(tasks.start_at)
                                      .locale("pt-br")
                                      .format("L")}{" "}
                                    -{" "}
                                    {moment
                                      .unix(tasks.start_at)
                                      .locale("pt-br")
                                      .format("LT")}
                                  </Label>
                                </div>
                                <Divider />
                              </div>
                            ))}
                        </div>
                      ) : (
                        <div>
                          {activeTask.length > 0 ? (
                            <div>
                              {tasks.length > 0 &&
                                this.props.task.records
                                  .filter(
                                    (c) => c.client_id === ticket.client_id
                                  )
                                  .map((tasks) => (
                                    <div
                                      className="tarefa"
                                      key={`ticket-item-${tasks.id}`}
                                    >
                                      <label className="toggleButtonTrash">
                                        <Icon
                                          name="trash alternate"
                                          size="large"
                                          color="red"
                                          onClick={() => this.deleteTask(tasks)}
                                        />
                                      </label>
                                      <label className="toggleButton">
                                        <input
                                          type="checkbox"
                                          checked={!!tasks.status}
                                        />
                                        <div
                                          onClick={() =>
                                            this.changeStatus(tasks)
                                          }
                                        >
                                          <svg viewBox="0 0 44 44">
                                            <path
                                              d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758"
                                              transform="translate(-2.000000, -2.000000)"
                                            />
                                          </svg>
                                        </div>
                                      </label>
                                      <div className="holder_tarefa">
                                        <h4> {tasks.title}</h4>
                                        <span>{tasks.description}</span>
                                        <Label
                                          as="a"
                                          color={
                                            tasks.status ? "green" : "orange"
                                          }
                                        >
                                          <Icon name="clock" />
                                          {moment
                                            .unix(tasks.start_at)
                                            .locale("pt-br")
                                            .format("L")}{" "}
                                          -{" "}
                                          {moment
                                            .unix(tasks.start_at)
                                            .locale("pt-br")
                                            .format("LT")}
                                        </Label>
                                      </div>
                                      <Divider />
                                    </div>
                                  ))}
                            </div>
                          ) : (
                            <p className="empty_tasks">
                              Nenhuma tarefa para esse cliente
                            </p>
                          )}
                        </div>
                      )}
                    </PerfectScrollbar>
                  )}
                </div>
                <Modal.Actions>
                  {hideArrow ? (
                    ""
                  ) : (
                    <Button.Group>
                      <Button
                        icon
                        onClick={handlePrevious}
                        disabled={previousButtonEnabled}
                      >
                        <Icon name="left arrow" />
                      </Button>
                      <Button
                        icon
                        onClick={handleNext}
                        disabled={nextButtonEnabled}
                      >
                        <Icon name="right arrow" />
                      </Button>
                    </Button.Group>
                  )}

                  <Button.Group>
                    <Button onClick={handleClose}>Cancelar</Button>
                  </Button.Group>
                  <Button.Group>
                    <Button onClick={this.toggleActivities} color="blue">
                      {showActivities ? "Tarefas" : "Atividades"}
                    </Button>
                  </Button.Group>
                </Modal.Actions>
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TaskActions, dispatch);

const mapStatetoProps = ({ user, task }) => {
  return {
    user: user.user,
    task,
    currentCompany: user.user.companies.find(
      (c) => c.id === user.user.company_id
    ),
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(TicketConversationModal);
