import React from "react";
import { Form, Input } from "semantic-ui-react";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const InfoPersonalizadaEditar = ({
  contact,
  handleExtraFields,
  saveChanges,
}) => (
  <div className="infos-editar">
    {contact && contact.extra_fields && (
      <ExtraFieldForm
        className="input-client"
        fluid={true}
        onBlur={saveChanges}
        extra_fields={contact.extra_fields}
        onChange={handleExtraFields}
      />
    )}
  </div>
);

export default InfoPersonalizadaEditar;
