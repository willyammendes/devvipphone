import React from "react";
import { Modal, Button } from "semantic-ui-react";
import DropdownUsers from "../../users/DropdownUsers";
import DropdownStatus from "../../ticket-status/DropdownStatus";

class SelectModal extends React.Component {
  componentDidMount() {
    if (this.props.showNavigation) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", e => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.userModal) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };
  render() {
    const {
      handleClose,
      modalHeader,
      user_id,
      open,
      loading,
      generalError,
      cleanErrors,
      handleUserChange,
      statusModal,
      handleStatusChange,
      status,
      userModal
    } = this.props;

    return (
      <Modal
        size="small"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                <p>{generalError}</p>
              </div>
            )}
            <div className="holder_conversation">
              <div className="form-conversation">
                {userModal ? (
                  <DropdownUsers
                    onSelectUser={handleUserChange}
                    user_id={user_id.user_id}
                    allowAdditions={false}
                  />
                ) : (
                  ""
                )}

                {statusModal ? (
                  <DropdownStatus
                    onSelectStatus={handleStatusChange}
                    status={status.status}
                    allowAdditions
                  />
                ) : (
                  ""
                )}
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default SelectModal;
