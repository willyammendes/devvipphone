import React from "react";
import Icon from "@mdi/react";
import { mdiChevronRight } from "@mdi/js";
import TicketClientInfo from "./TicketClientInfo";
import InfoPersonalizado from "../../ticket-info/InfoPersonalizadaCliente";
import OldTickets from "./OldTickets";

function changeClass() {
  const element = document.getElementsByName("sideinfo");
  for (let i = 0; i < element.length; i += 1) {
    element[i].classList.toggle("ativo");
  }
}

const TicketInfo = ({
  activeConversation,
  oldTickets,
  contact,
  handleChange,
  saveChanges,
  currentCompany,
  user,
  copySuccess,
  copy,
  loading,
  handleExtraFields,
  reload,
  refresh,
  options,
  cleanSearch,
  handleSearch,
  onAddItem,
}) => {
  return (
    <div
      className="side-info-ticket topoinfo"
      name="sideinfo"
      onMouseOver={changeClass}
      onMouseOut={changeClass}
    >
      <div className="openMenu">
        <div className="iconSeta">
          <Icon path={mdiChevronRight} size={0.8} />
        </div>
      </div>
      <TicketClientInfo
        contact={activeConversation.contact}
        company={currentCompany}
        user={user}
        copy={copy}
        copySuccess={copySuccess}
      />
      <InfoPersonalizado
        contact={contact}
        loading={loading}
        handleChange={handleChange}
        saveChanges={saveChanges}
        handleExtraFields={handleExtraFields}
        activeConversation={activeConversation}
        reload={reload}
        refresh={refresh}
        options={options}
        cleanSearch={cleanSearch}
        handleSearch={handleSearch}
        onAddItem={onAddItem}
      />

      <OldTickets tickets={oldTickets} />
    </div>
  );
};

export default TicketInfo;
