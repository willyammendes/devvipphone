import React, { useState } from "react";
import Icon from "@mdi/react";
import { mdiPhone, mdiEmail } from "@mdi/js";

import { Modal, Button } from "semantic-ui-react";

const TicketClientInfo = ({ contact, company, user, copySuccess, copy }) => {
  const [showModal, setModal] = useState(false);

  const clickToCall = () => {
    const url = `${
      company.parameters.monitcall_base_url
    }/apis/v2/ligar.php?chave=${
      company.parameters.monitcall_client_key
    }&ramal=${user.branch_line}&destino=${contact.phone_number.substring(
      2
    )}&controle=${Math.floor(Math.random() * Math.floor(100000))}`;
    setModal(!showModal);

    let monitcall = window.open(
      url,
      "",
      "width=" + window.screen.width / 2 + ",height=100"
    );

    setTimeout(() => {
      monitcall.close();
    }, 1500);
  };
  const teste = e => {
    e.preventDefault();
    var $body = document.getElementsByTagName("body")[0];

    var secretInfo = document.getElementById("secretInfo").innerHTML;

    var copyToClipboard = function(secretInfo) {
      var $tempInput = document.createElement("INPUT");
      $body.appendChild($tempInput);
      $tempInput.setAttribute("value", secretInfo);
      $tempInput.select();
      document.execCommand("copy");
      $body.removeChild($tempInput);
    };

    copyToClipboard(secretInfo);
    copy();
  };
  return (
    <div className="info-cliente">
      <img
        src={
          contact.avatar.indexOf("whatsapp") !== -1
            ? contact.avatar
            : `${contact.avatar}`
        }
        alt=""
      />
      <h2>{contact.name}</h2>
      <span>{contact.client.name}</span>
      <p id="secretInfo" className="invisible">
        {contact.phone_number}
      </p>

      <div className="links-empresa">
        {company.parameters.click_to_call ? (
          <a
            onClick={clickToCall}
            title={contact.phone_number}
            href
            style={{ backgroundColor: "#27bf5c" }}
            className={copySuccess ? "tele copySuccess" : "tele"}
            onContextMenu={e => teste(e)}
          >
            <Icon path={mdiPhone} size={0.8} />
          </a>
        ) : (
          <a
            href={`tel:${contact.phone_number}`}
            title={contact.phone_number}
            className={copySuccess ? "tele copySuccess" : "tele"}
            onContextMenu={e => teste(e)}
          >
            <Icon path={mdiPhone} size={0.8} />
          </a>
        )}
        <a href={`mailto:${contact.email}`} className="tele">
          <Icon path={mdiEmail} size={0.8} />
        </a>

        <Modal
          size={"mini"}
          open={showModal}
          onClose={() => setModal(!showModal)}
        >
          <Modal.Header>Click to Call</Modal.Header>
          <Modal.Content>
            <p>Atenda o ramal {user.branch_line} para completar a ligação!</p>
          </Modal.Content>
          <Modal.Actions>
            <Button
              positive
              icon="checkmark"
              labelPosition="right"
              content="Fechar"
              onClick={() => setModal(!showModal)}
            />
          </Modal.Actions>
        </Modal>
      </div>
    </div>
  );
};

export default TicketClientInfo;
