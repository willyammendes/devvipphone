import React from "react";
import PerfectScrollbar from "react-perfect-scrollbar";

const OldTickets = ({ tickets }) => {
  const navigateToElement = element => {
    const elmnt = document.getElementById(element);
    elmnt.scrollIntoView();
  };
  return (
    <div className="previous-ticket">
      <PerfectScrollbar>
        <span>Tickets Anteriores</span>

        <div className="tickets">
          {tickets.map(ticket => (
            <p
              title={ticket.current_progress >= 100 ? "Ticket Finalizado" : ""}
              key={`ticket-item-${ticket.id}`}
            >
              <a
                href
                onClick={() => navigateToElement(`ticket-${ticket.id}`)}
              >
                #{ticket.ticket_number}
              </a>{" "}
              - {ticket.start_date}
            </p>
          ))}
        </div>
      </PerfectScrollbar>
    </div>
  );
};

export default OldTickets;
