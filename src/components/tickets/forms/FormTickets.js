import React from "react";
import { Form, Input, TextArea, Divider } from "semantic-ui-react";
import DropdownContacts from "../../contact/DropdownContacts";
import DropdownDepartments from "../../departments/DropdownDepartments";
import DropdownCategories from "../../ticket-category/DropdownCategories";
import DropdownUsers from "../../users/DropdownUsers";

import PerfectScrollbar from "react-perfect-scrollbar";
import moment from "moment";
import "moment/locale/pt-br";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const FormTickets = ({
  ticket,
  onChange,
  className,
  handleContactChange,
  handleDepartmentChange,
  handleCategoryChange,
  handleUserChange,
  name,
  tickets_conversation_id,
  handleExtraFields,
  comments,
  newTicket,
}) => {
  return (
    <Form className={className} name={name}>
      <div className="form-row">
        <div className="field-row">
          <Form.Field
            autoComplete="off"
            control={Input}
            label="Título"
            name="title"
            onChange={onChange}
            value={ticket.title}
          />

          <div className="field">
            <label>Responsável</label>
            <DropdownUsers
              onSelectUser={handleUserChange}
              user_id={ticket.user_id}
              allowAdditions={false}
            />
          </div>
          {tickets_conversation_id ? (
            <div className="field">
              <label>Contato</label>
              <DropdownContacts
                onSelectContact={handleContactChange}
                contact_id={ticket.contact_id}
                clearable={false}
                options={[
                  {
                    id: ticket.contact_id,
                    name: ticket.contact_name,
                    phone_number: ticket.contact_number,
                  },
                ]}
                multiple={false}
                name="contact_id"
              />
            </div>
          ) : (
            ""
          )}

          <div className="field">
            <label>Departamento</label>
            <DropdownDepartments
              onSelectDepartment={handleDepartmentChange}
              department_id={ticket.department_id}
            />
          </div>
          <div className="field">
            <label>Categoria</label>
            <DropdownCategories
              handleCategoryChange={handleCategoryChange}
              onSelectCategory={handleCategoryChange}
              ticket_category_id={ticket.ticket_category_id}
              allowAdditions
            />
          </div>

          <Form.Field
            control={TextArea}
            label="Informações do Ticket"
            name="incident"
            onChange={onChange}
            value={ticket.incident}
          />
          <Divider />
          <ExtraFieldForm
            extra_fields={ticket.extra_fields}
            onChange={handleExtraFields}
          />
        </div>
        {newTicket ? (
          ""
        ) : (
          <div className="field-row">
            <div className="comments">
              {comments ? (
                <PerfectScrollbar>
                  {comments.map((c) => (
                    <div key={`comment-${c.id}`}>
                      <div className="comentario">
                        {c.message}
                        <span className="data">
                          <b>
                            {moment
                              .unix(c.timestamp)
                              .locale("pt-br")
                              .format("L")}
                          </b>
                        </span>
                      </div>
                    </div>
                  ))}
                </PerfectScrollbar>
              ) : (
                ""
              )}
            </div>

            <label>Comentário</label>
            <TextArea
              placeholder="Interagir no chamado"
              name="comment"
              onChange={onChange}
              value={ticket.comment}
            />
          </div>
        )}
      </div>
    </Form>
  );
};

export default FormTickets;
