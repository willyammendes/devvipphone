import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import api from "../../api/ticket";
import apiconversation from "../../api/conversation";
import apiactivities from "../../api/activities";
import DataTable from "../table/DataTable";
import TicketsModal from "./modal/TicketsModal";
import TicketConversationModal from "./modal/TicketConversationModal";
import SelectModal from "./modal/SelectModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { Label, Progress, Icon } from "semantic-ui-react";
import AlertError from "../alerts/AlertError";

class Tickets extends Component {
  state = {
    tasks: [],
    activities: [],
    records: [],
    conversations: [],
    columns: {},
    clients: [],
    order: {},
    errors: "",
    conversation: [],
    loading: false,
    tableTickets: true,
    loadingContacts: false,
    loadingStats: false,
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    conversationModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false,
    userTickets: false,
    conversationActive: [],
    oldTickets: [],
    comments: [],
    contact: [],
    errorConversation: "",
    userModal: false,
    user_id: "",
    status: "",
    checked: "",
    closed: 0,
    statusModal: false,
    extra_fields: [],
    selectedConversationId: "",
  };
  componentWillMount() {
    if (this.state.userTickets) {
      this.fetchUserRecords();
    } else {
      this.fetchRecords();
    }
    this.fetchTasks();
    this.fetchActivities();
  }

  select = (selectedDataId) => {
    const { clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];
    this.setState({
      selectedDataIndex: dataIndex,
      conversationModalOpen: false,
      editModalOpen: true,
    });
    if (
      !!data.client_id &&
      clients.filter((c) => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    const extraFieldIndex = records[dataIndex].extra_fields.findIndex(
      (c) => c.id === extraFieldId
    );

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          extra_fields: [
            ...records[dataIndex].extra_fields.slice(0, extraFieldIndex),
            {
              ...records[dataIndex].extra_fields[extraFieldIndex],
              value: rating !== undefined ? rating : value,
            },
            ...records[dataIndex].extra_fields.slice(extraFieldIndex + 1),
          ],
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  selectRow = (selectedDataId) => {
    this.setState({ selectedConversationId: selectedDataId });
    // Get records table Ticket
    const { records } = this.state;
    // Find Index Ticket in table
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    // get especific ticket select
    const data = this.state.records[dataIndex];

    // search in api conversation from select ticket
    apiconversation.conversations.get(data.conversation_id).then((res) => {
      // set State conversation
      this.setState({
        conversations: res.data.data,
        conversationActive: res.data.data,
        loading: false,
      });
      const conversationActive = res.data.data;

      if (conversationActive) {
        const oldTickets = records.filter(
          (c) => conversationActive.id === c.conversation_id
        );
        this.props.getConversation(conversationActive.id).then(() => {
          this.onConversationSelected(conversationActive);
        });
        this.setState({
          selectedDataIndex: dataIndex,
          conversationModalOpen: true,
          conversationActive,
          oldTickets,
          loading: false,
        });
      } else {
        this.setState({
          errors: {
            status: "1",
            data: {
              message: "Não há nenhuma conversa ativa para esse contato",
            },
          },
        });
      }
    });
  };
  openUserModal = () => {
    this.setState({
      userModal: true,
    });
  };
  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce((o, key) => {
          return Object.assign(o, {
            [key]: Array.isArray(records[0][key])
              ? key === "extra_fields"
                ? this.state.extra_fields
                : []
              : "",
          });
        }, {})
      : {
          department_id: "",
          contact_id: "",
          user_id: "",
          incident: "",
          title: "",
          contact_name: "",
          extra_fields: this.state.extra_fields,
        };
    this.setState({
      conversationModalOpen: false,
    });
    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  nextRecordRow = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;
    this.selectRow(selectedDataId);
  };

  previousRecordRow = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;
    this.selectRow(selectedDataId);
  };

  fetchRecords = async (params) => {
    const { closed, userTickets } = this.state;
    const { user } = this.props;
    this.setState({ loading: true });
    const filter = {
      order: "start_date",
      order_direction: "desc",
      ...params,
      filter: userTickets
        ? JSON.stringify([
            ["user_id", "=", user.id],
            closed
              ? ["current_progress", ">=", 100]
              : ["current_progress", "<", 100],
          ])
        : JSON.stringify([["current_progress", closed ? ">=" : "<", 100]]),
    };
    return await api.ticket.fetchAll(filter).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        extra_fields: res.extra_fields,
        loading: false,
      });
    });
  };
  fetchUserRecords = (params) => {
    this.setState({
      userTickets: !this.state.userTickets,
    });
    setTimeout(() => {
      this.fetchRecords();
    }, 200);
  };

  fetchTasks = () => {
    this.setState({ loading: true });
    this.setState({
      tasks: this.props.task.records,
      loading: false,
    });
  };
  fetchActivities = async (params) => {
    this.setState({ loading: true });
    return await apiactivities.activities.fetchAll(params).then((res) => {
      this.setState({
        activities: res.data,
        loading: false,
      });
    });
  };

  reloadData = (params) => {
    const { closed } = this.state;
    this.setState({ loading: true });
    const filter = {
      order: "start_date",
      order_direction: "desc",
      ...params,
      filter: JSON.stringify([["current_progress", closed ? ">=" : "<", 100]]),
    };
    api.ticket.fetchAll(filter).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  handleChange = (e) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleContactChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          contact_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleDepartmentChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          department_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleCategoryChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticket_category_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleUserSelectChange = (value, text) => {
    this.setState({
      user_id: {
        ...this.state.user_id,
        user_id: value,
      },
    });
  };
  handleStatusSelectChange = (value, text) => {
    this.setState({
      status: {
        ...this.state.status,
        status: value,
      },
    });
  };
  handleUserChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          user_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleStatusChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          status: value,
          current_status: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  selectUsers = async (checkedItems) => {
    this.setState({
      checked: checkedItems.toString(),
      userModal: true,
    });
  };

  selectStatus = async (checkedItems) => {
    this.setState({
      checked: checkedItems.toString(),
      statusModal: true,
    });
  };

  changeStatus = () => {
    const { status, checked, records } = this.state;
    const status_id = { status: status.status };
    const join = { data: checked, ...status_id };
    api.ticket
      .changeStatus(join)
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          userModal: false,
          statusModal: false,
          records: [...records],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          userModal: false,
          statusModal: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  changeUser = () => {
    const { user_id, checked, records } = this.state;
    const user = { user_id: user_id.user_id };
    const join = { data: checked, ...user };
    api.ticket
      .changeUser(join)
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          userModal: false,
          statusModal: false,
          records: [...records],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          userModal: false,
          statusModal: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  toggleClosedTickets = () => {
    this.setState({
      closed: !this.state.closed,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.closed !== this.state.closed ||
      prevState.save_alert !== this.state.save_alert
    ) {
      this.fetchRecords();
    }
  }

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.ticket
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.ticket,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          error: err.response.data,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);

        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    api.ticket
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  onInit = (props) =>
    props.conversations.length === 0 && props.fetchAll().then(() => {});

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  delete = (id, noOwnerOnly = false) => api.ticket.delete(id, noOwnerOnly);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      selectedConversationId: "",
      editModalOpen: false,
      userModal: false,
      statusModal: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
    this.props.unselectConversation();
  };

  onConversationSelected = (conversationActive) =>
    this.props.conversationSelected(conversationActive);
  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      tableTickets,
      conversationModalOpen,
      conversationActive,
      oldTickets,
      comments,
      tasks,
      activities,

      errorConversation,
      userModal,
      user_id,
      status,
      statusModal,
    } = this.state;
    const { activeConversationId } = this.props;
    const activeComments =
      comments.length > 0 &&
      comments.find((c) => conversationActive.active_ticket_id === c.id);
    const ticket = this.state.records[selectedDataIndex];

    this.state.extra_fields.forEach((e) => {
      if (e.show_column_on_view) columns[e.input_name] = e.label;
    });

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Tickets</h1>
                {errorConversation || ""}
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                extra_fields={this.state.extra_fields.map((e) => e.input_name)}
                tableTickets={tableTickets}
                toggleClosedTickets={this.toggleClosedTickets}
                oldticketBt
                userTickets={this.state.userTickets}
                fetchUserRecords={this.fetchUserRecords}
                openUserModal={(id) => this.selectUsers(id)}
                changeStatus={(id) => this.selectStatus(id)}
                data={records.map((r) => {
                  return {
                    ...r,
                    incident: (
                      <span title={r.incident}>
                        {r.incident ? r.incident.substring(0, 60) : ""}
                      </span>
                    ),
                    progress: (
                      <Progress
                        percent={r.progress}
                        title={`Progresso ${r.progress}%`}
                        indicating
                        size="tiny"
                      />
                    ),
                    source:
                      r.source === "phone" ? (
                        <Icon
                          name={r.source}
                          size="large"
                          flipped={r.source === "phone" && "horizontally"}
                        />
                      ) : r.source === "monitchat" ? (
                        <Icon name="whatsapp" size="large" color="blue" />
                      ) : r.source === "campaing" ? (
                        <Icon name="bullhorn" size="large" color="blue" />
                      ) : (
                        <Icon name={r.source} size="large" />
                      ),
                    status: (
                      <div>
                        <Label
                          as="a"
                          title={`Progresso atual ${r.progress}%`}
                          horizontal
                          size="mini"
                          color={
                            r.progress < 15
                              ? "red"
                              : r.progress <= 30
                              ? "orange"
                              : r.progress <= 50
                              ? "blue"
                              : r.progress <= 70
                              ? "purple"
                              : r.progress <= 95
                              ? "teal"
                              : "green"
                          }
                        >
                          {r.status}
                        </Label>{" "}
                        {r.unread_messages > 0 ? (
                          <Label
                            title={`${
                              r.unread_messages > 1
                                ? `${r.unread_messages} mensagens não lidas`
                                : `${r.unread_messages} mensagem não lida`
                            } no ticket`}
                            circular
                            color="green"
                            size="mini"
                          >
                            {r.unread_messages}
                          </Label>
                        ) : null}
                      </div>
                    ),
                    contact_name: (
                      <Label
                        as="a"
                        title={r.contact_name}
                        className="contact_name"
                        image
                      >
                        <img src={`${r.contact_avatar}`} alt="" />
                        <span>{r.contact_name.split(" ")[0]}</span>
                      </Label>
                    ),
                    user: r.user ? (
                      <Label as="a" title={r.user} image>
                        <img src={`${r.user_avatar}`} alt="" />
                        <span>{r.user.split(" ")[0]}</span>
                      </Label>
                    ) : null,
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                changeUser={(id) => this.selectUsers(id)}
                onDelete={(id) => this.delete(id)}
                onRowClick={(d) => this.selectRow(d.id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
              />
              <SelectModal
                loading={loading}
                handleUserChange={this.handleUserSelectChange}
                handleStatusChange={this.handleStatusSelectChange}
                open={userModal || statusModal}
                handleClose={this.handleCloseEditModal}
                user_id={user_id}
                status={status}
                userModal={userModal}
                statusModal={statusModal}
                modalHeader={
                  userModal ? "Alterar responsável" : "Alterar Status"
                }
                onClickSave={this.changeUser}
                onClickAdd={this.changeStatus}
              />

              {selectedDataIndex !== -1 ? (
                <TicketsModal
                  handleExtraFields={this.handleExtraFields}
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  ticket={ticket}
                  tickets_conversation_id
                  modalHeader={
                    ticket
                      ? ticket.id
                        ? `Edição do ${ticket.title}`
                        : "Novo Ticket"
                      : "Novo Ticket"
                  }
                  newTicket
                  tasks={tasks}
                  handleContactChange={this.handleContactChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleCategoryChange={this.handleCategoryChange}
                  handleUserChange={this.handleUserChange}
                  handleStatusChange={this.handleStatusChange}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                  comments={activeComments.comments}
                />
              ) : null}
              {this.state.error ? (
                <AlertError
                  style={{
                    zIndex: 10000,
                    padding: "5px 10px",
                    marginTop: "-25px",
                    width: 300,
                  }}
                  message={this.state.error.message}
                />
              ) : null}
              {selectedDataIndex !== -1 && activeConversationId !== -1 ? (
                <TicketConversationModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecordRow}
                  handlePrevious={this.previousRecordRow}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  ticket={ticket}
                  tasks={tasks}
                  refresh={(e) =>
                    this.selectRow(this.state.selectedConversationId)
                  }
                  activities={activities}
                  tickets_conversation_id
                  activeConversation={conversationActive}
                  reloadData={this.reloadData}
                  modalHeader={
                    ticket.id
                      ? `Ticket: ${ticket.ticket_number}`
                      : "Novo Ticket"
                  }
                  oldTickets={oldTickets}
                  handleContactChange={this.handleContactChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleCategoryChange={this.handleCategoryChange}
                  handleUserChange={this.handleUserChange}
                  handleStatusChange={this.handleStatusChange}
                  open={conversationModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Tickets.propTypes = {
  conversation: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        tickets: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            user: PropTypes.any.isRequired,
          })
        ).isRequired,
      }).isRequired
    ),
  }).isRequired,
  media: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
      }).isRequired
    ),
  }),
  activeConversationId: PropTypes.number.isRequired,
  conversationSelected: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

const mapStateToProps = ({ conversation, user, media, task }) => {
  return {
    user: user.user,
    conversation,
    media,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter,
    task,
    conversations: !conversation.data ? [] : Object.values(conversation.data),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tickets);
