import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import api from "../../api/ticket";
import apiconversation from "../../api/conversation";
import apitask from "../../api/tasks";
import apiactivities from "../../api/activities";
import DataTable from "../table/DataTable";
import TicketsModal from "./modal/TicketsModal";
import TicketConversationModal from "./modal/TicketConversationModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { Label, Progress } from "semantic-ui-react";
import AlertError from "../alerts/AlertError";
import { Link } from "react-router-dom";

class OldTickets extends Component {
  state = {
    tasks: [],
    activities: [],
    records: [],
    conversations: [],
    columns: {},
    clients: [],
    order: {},
    errors: "",
    conversation: [],
    loading: false,
    tableTickets: true,
    loadingContacts: false,
    loadingStats: false,
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    conversationModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false,
    conversationActive: [],
    oldTickets: [],
    comments: [],
    contact: [],
    errorConversation: "",
  };
  componentWillMount() {
    this.fetchRecords();
    this.fetchConversations();
    this.fetchTasks();
    this.fetchActivities();

    this.onInit(this.props);
  }

  select = (selectedDataId) => {
    const { clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({
      selectedDataIndex: dataIndex,
      conversationModalOpen: false,
      editModalOpen: true,
    });

    if (
      !!data.client_id &&
      clients.filter((c) => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  selectRow = (selectedDataId) => {
    const { conversations, records } = this.state;
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    const conversationActive = conversations.find(
      (c) => c.id === data.conversation_id
    );

    if (conversationActive) {
      const oldTickets = records.filter(
        (c) => conversationActive.id === c.conversation_id
      );
      this.onConversationSelected(conversationActive.id);

      this.setState({
        selectedDataIndex: dataIndex,
        conversationModalOpen: true,
        conversationActive,
        oldTickets,
        loading: false,
      });
    } else {
      this.setState({
        errors: {
          status: "1",
          data: { message: "Não há nenhuma conversa ativa para esse contato" },
        },
      });
    }
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "" }),
          {}
        )
      : {
          department_id: "",
          contact_id: "",
          user_id: "",
          incident: "",
          title: "",
          contact_name: "",
        };

    this.setState({
      conversationModalOpen: false,
    });

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  nextRecordRow = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.selectRow(selectedDataId);
  };

  previousRecordRow = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.selectRow(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.ticket.fetchClosed(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  fetchConversations = async (params) => {
    this.setState({ loading: true });

    return await apiconversation.conversations.fetchAll(params).then((res) => {
      this.setState({
        conversations: res,
        loading: false,
      });
    });
  };

  fetchTasks = async (params) => {
    this.setState({ loading: true });

    return await apitask.task.fetchAll(params).then((res) => {
      this.setState({
        tasks: res.data,
        loading: false,
      });
    });
  };
  fetchActivities = async (params) => {
    this.setState({ loading: true });

    return await apiactivities.activities.fetchAll(params).then((res) => {
      this.setState({
        activities: res.data,
        loading: false,
      });
    });
  };

  reloadData = () => {
    const {
      recordsPerPage,
      activePage,
      search,
      order,
      orderDirection,
    } = this.state;
    const skip =
      activePage === 1 ? 0 : recordsPerPage * activePage - recordsPerPage;
    this.setState({ loading: true });

    const except = this.props.data.map((c) => c.id);

    const params = {
      skip,
      search,
      take: recordsPerPage,
      except,
      order,
      order_direction: orderDirection,
    };

    this.props.fetchData(params).then((res) => {
      this.setState({ loading: false });
    });
  };

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleContactChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          contact_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleDepartmentChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          department_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleCategoryChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticket_category_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleUserChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          user_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleStatusChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          status: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.ticket
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.ticket,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          error: err.response.data,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);

        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.ticket
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  onInit = (props) =>
    props.conversations.length === 0 && props.fetchAll().then(() => {});

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  delete = (id) => api.ticket.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  onConversationSelected = (id) =>
    this.props.conversationSelected(
      this.props.conversation.data.find((c) => c.id === id)
    );

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      tableTickets,
      conversationModalOpen,
      conversationActive,
      oldTickets,
      comments,
      tasks,
      activities,
      errorConversation,
    } = this.state;
    const { toggleOldTickets } = this.props;

    const activeComments =
      comments.length > 0 &&
      comments.find((c) => conversationActive.active_ticket_id === c.id);

    const ticket = this.state.records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Tickets</h1>
                {errorConversation || ""}
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                tableTickets={tableTickets}
                toggleOldTickets={toggleOldTickets}
                oldticketBt
                data={records.map((r) => {
                  return {
                    ...r,
                    incident: (
                      <span title={r.incident}>
                        {r.incident.substring(0, 60)}
                      </span>
                    ),
                    progress: (
                      <Progress
                        percent={r.progress}
                        title={`Progresso ${r.progress}%`}
                        indicating
                        size="tiny"
                      />
                    ),
                    status: (
                      <div>
                        <Label
                          as="a"
                          title={`Progresso atual ${r.progress}%`}
                          horizontal
                          size="mini"
                          color={
                            r.progress < 15
                              ? "red"
                              : r.progress <= 30
                              ? "orange"
                              : r.progress <= 50
                              ? "blue"
                              : r.progress <= 70
                              ? "purple"
                              : r.progress <= 95
                              ? "teal"
                              : "green"
                          }
                        >
                          {r.status}
                        </Label>{" "}
                        {r.unread_messages > 0 ? (
                          <Label
                            title={`${
                              r.unread_messages > 1
                                ? `${r.unread_messages} mensagens não lidas`
                                : `${r.unread_messages} mensagem não lida`
                            } no ticket`}
                            circular
                            color="green"
                            size="mini"
                          >
                            {r.unread_messages}
                          </Label>
                        ) : null}
                      </div>
                    ),
                    contact_name: (
                      <Label
                        as="a"
                        title={r.contact_name}
                        className="contact_name"
                        image
                      >
                        <img src={`${r.contact_avatar}`} alt="" />
                        <span>{r.contact_name.split(" ")[0]}</span>
                      </Label>
                    ),
                    user: r.user ? (
                      <Label as="a" title={r.user} image>
                        <img src={`${r.user_avatar}`} alt="" />
                        <span>{r.user.split(" ")[0]}</span>
                      </Label>
                    ) : null,
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onRowClick={(d) => this.selectRow(d.id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
              />
              {this.state.error ? (
                this.state.error.no_plan ? (
                  <AlertError
                    style={{
                      zIndex: 10000,
                      padding: "5px 10px",
                      marginTop: "-25px",
                      width: 300,
                    }}
                    message={this.state.error.message}
                  />
                ) : (
                  <AlertError
                    style={{
                      zIndex: 10000,
                      padding: "5px 10px",
                      marginTop: "-25px",
                      width: 300,
                    }}
                    message={this.state.error.message}
                  >
                    <Link to="/company" style={{ color: "blue" }}>
                      Planos
                    </Link>
                  </AlertError>
                )
              ) : null}
              {selectedDataIndex !== -1 ? (
                <TicketsModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  ticket={ticket}
                  tickets_conversation_id
                  modalHeader={
                    ticket.id ? `Edição do ${ticket.title}` : "Novo Ticket"
                  }
                  tasks={tasks}
                  handleContactChange={this.handleContactChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleCategoryChange={this.handleCategoryChange}
                  handleUserChange={this.handleUserChange}
                  handleStatusChange={this.handleStatusChange}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                  comments={activeComments.comments}
                />
              ) : null}

              {selectedDataIndex !== -1 ? (
                <TicketConversationModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecordRow}
                  handlePrevious={this.previousRecordRow}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  ticket={ticket}
                  tasks={tasks}
                  activities={activities}
                  tickets_conversation_id
                  activeConversation={conversationActive}
                  modalHeader={
                    ticket.id
                      ? `Ticket: ${ticket.ticket_number}`
                      : "Novo Ticket"
                  }
                  oldTickets={oldTickets}
                  handleContactChange={this.handleContactChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleCategoryChange={this.handleCategoryChange}
                  handleUserChange={this.handleUserChange}
                  handleStatusChange={this.handleStatusChange}
                  open={conversationModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OldTickets.propTypes = {
  conversation: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        tickets: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            user: PropTypes.any.isRequired,
          })
        ).isRequired,
      }).isRequired
    ),
  }).isRequired,
  media: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
      }).isRequired
    ),
  }).isRequired,
  activeConversationId: PropTypes.number.isRequired,
  conversationSelected: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

const mapStateToProps = ({ conversation, user, media }) => {
  return {
    user: user.user,
    conversation,
    media,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter,
    conversations: !conversation.data ? [] : Object.values(conversation.data),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OldTickets);
