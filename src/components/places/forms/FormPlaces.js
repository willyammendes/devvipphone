import React from "react";
import { Form, Input, TextArea } from "semantic-ui-react";

const FormPlaces = ({ place, onChange }) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Nome:"
                name="name"
                onChange={onChange}
                value={place.name}
            />
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Código:"
                name="menu"
                onChange={onChange}
                value={place.menu}
            />
            <Form.Field
                control={TextArea}
                label="Descrição:"
                name="description"
                onChange={onChange}
                value={place.description}
            />
        </Form>
    );
};

export default FormPlaces;
