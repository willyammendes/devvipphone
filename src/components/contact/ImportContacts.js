import React, { Component } from "react";
import { Modal, Button, Form, Dropdown, Message, Tab } from "semantic-ui-react";

class ImportContacts extends Component {
	state = {
		showMessage: true
	};
	handleDismiss = () => {
		this.setState({
			showMessage: false
		});
	};
	render() {
		const {
			open,
			BasicUpload,
			loading,
			handleClose,
			save,
			submit,
			columns,
			selectedColumns
		} = this.props;
		const panes = [
			{
				menuItem: "Contato",
				render: () => (
					<Tab.Pane>
						<Form>
							{this.state.showMessage && (
								<Message onDismiss={this.handleDismiss}>
									<div className="header">Importante!</div>
									É necessário que seu arquivo possua títulos
									nas colunas, que possam identificar cada
									campo. <br />
									Informe em cada caixa de seleção abaixo, o
									nome da coluna do seu arquivo excel,
									correspondente com os dados solicitados
								</Message>
							)}

							<Form.Group widths="equal">
								<div className="field">
									<label>Nome:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="name_column"
										value={selectedColumns.name_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Cidade:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="city_column"
										value={selectedColumns.city_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>Telefone:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="phone_number_column"
										value={
											selectedColumns.phone_number_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Bairro:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="neighborhood_column"
										value={
											selectedColumns.neighborhood_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>Email:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="email_column"
										value={selectedColumns.email_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Endereço:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="address_column"
										value={selectedColumns.address_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>Campo Extra 1:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="extra_field_1_column"
										value={
											selectedColumns.extra_field_1_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Campo Extra 2:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="extra_field_2_column"
										value={
											selectedColumns.extra_field_2_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>Código:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="code_column"
										value={selectedColumns.code_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>CPF:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="cpf_column"
										value={selectedColumns.cpf_column}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>Telefone Opcional 1:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="phone_number_optional1_column"
										value={
											selectedColumns.phone_number_optional1_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Telefone Opcional 2:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="phone_number_optional2_column"
										value={
											selectedColumns.phone_number_optional2_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
						</Form>
					</Tab.Pane>
				)
			},
			{
				menuItem: "Cliente",
				render: () => (
					<Tab.Pane>
						<Form>
							{this.state.showMessage && (
								<Message onDismiss={this.handleDismiss}>
									<div className="header">Importante!</div>
									É necessário que seu arquivo possua títulos
									nas colunas, que possam identificar cada
									campo. <br />
									Informe em cada caixa de seleção abaixo, o
									nome da coluna do seu arquivo excel,
									correspondente com os dados solicitados
								</Message>
							)}

							<Form.Group widths="equal">
								<div className="field">
									<label>Nome:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="client_name_column"
										value={
											selectedColumns.client_name_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Telefone:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="client_phone_number_column"
										value={
											selectedColumns.client_phone_number_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
							<Form.Group widths="equal">
								<div className="field">
									<label>CNPJ:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="client_cnpj_column"
										value={
											selectedColumns.client_cnpj_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
								<div className="field">
									<label>Email:</label>
									<Dropdown
										fluid
										clearable
										search
										selection
										name="client_email_column"
										value={
											selectedColumns.client_email_column
										}
										onChange={(e, { name, value }) =>
											this.props.onSelectName(e, {
												name,
												value
											})
										}
										options={columns}
									/>
								</div>
							</Form.Group>
						</Form>
					</Tab.Pane>
				)
			}
		];
		return (
			<Modal
				size="small"
				closeIcon
				open={open}
				onClose={handleClose}
				dimmer="blurring"
				closeOnDimmerClick={false}
			>
				<Modal.Header>Importar contatos</Modal.Header>
				<Modal.Content>
					<Modal.Description>
						<div className="form-alertword">
							{columns.length > 0 ? (
								<Tab panes={panes} />
							) : (
								<BasicUpload />
							)}
						</div>
					</Modal.Description>
				</Modal.Content>
				<Modal.Actions>
					<Button.Group>
						<Button onClick={handleClose}>Cancelar</Button>
						<Button.Or />
						<Button
							positive
							onClick={columns.length > 0 ? submit : save}
							loading={loading}
							disabled={loading}
						>
							{columns.length > 0 ? "Importar" : "Enviar"}
						</Button>
					</Button.Group>
				</Modal.Actions>
			</Modal>
		);
	}
}

export default ImportContacts;
