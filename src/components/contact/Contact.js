import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as UserActions } from "../../store/ducks/auth";
import api from "../../api/contact";
import apigroup from "../../api/group";
import DataTable from "../table/DataTable";
import ContactModal from "./modal/ContactModal";
import AlertSuccess from "../alerts/AlertSuccess";
import apiparameter from "../../api/parameters";
import ImportContacts from "./ImportContacts";

class Contact extends React.Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    parameters: [],
    order: {},
    contactColumns: [],
    extra_fields: [],
    selectedColumns: {
      name_column: "",
      phone_number_column: "",
      email_column: "",
      neighborhood_column: "",
      city_column: "",
      code_column: "",
      cpf_column: "",
      client_cnpj_column: "",
      client_name_column: "",
      client_email_column: "",
      client_phone_number_column: "",
      extra_field_1_column: "",
      extra_field_2_column: "",
      phone_number_optional1_column: "",
      phone_number_optional2_column: "",
      address_column: "",
      groups_import: []
    },
    file: [
      {
        file_name: "",
        tmp_file: ""
      }
    ],
    importModal: false,
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    groups: [],
    loadingGroups: false,
    loadingClients: false,
    save_alert: false,
    params: ""
  };

  select = selectedDataId => {
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records, groups } = this.state;

    if (groups.length === 0) this.fetchGroups();
    const newData = records[0]
      ? Object.keys(records[0]).reduce((o, key) => {
          return Object.assign(o, {
            [key]: Array.isArray(records[0][key])
              ? key === "extra_fields"
                ? this.state.extra_fields
                : []
              : ""
          });
        }, {})
      : {
          extra_fields: this.state.extra_fields
        };

    newData.groups = [];
    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  onSelectGroup = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  addGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingGroups: true });

    apigroup.group
      .submit({ name: value })
      .then(data => {
        this.setState({
          groups: [...this.state.groups].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              groups: [...records[dataIndex].groups].concat(data.data.id)
            },
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .then(() => {
        this.setState({ loadingGroups: false });
      });
  };

  fetchGroups = async () => {
    this.setState({ loadingGroups: true });
    await apigroup.group.fetchAll().then(groups => {
      this.setState({
        groups: groups.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingGroups: false
      });
    });
  };

  fetchRecords = async params => {
    this.setState({ loading: true, params });

    return await api.contact.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        params,
        total_records: res.total_records,
        extra_fields: res.extra_fields,
        loading: false
      });
    });
  };

  fetchWallet = () => {
    const { params } = this.state;

    const paramsfilter = {
      ...params,
      filter: JSON.stringify([["id", "=", this.props.user.contacts]])
    };

    api.contact.fetchAll(paramsfilter).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };
  componentWillMount() {
    this.fetchParameters();
    this.fetchRecords();
  }
  fetchParameters = async params => {
    this.setState({ loading: true });

    return await apiparameter.parameter.fetchAll(params).then(res => {
      this.setState({
        parameters: res.data,
        loading: false
      });
    });
  };

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    const extraFieldIndex = records[dataIndex].extra_fields.findIndex(
      c => c.id === extraFieldId
    );

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          extra_fields: [
            ...records[dataIndex].extra_fields.slice(0, extraFieldIndex),
            {
              ...records[dataIndex].extra_fields[extraFieldIndex],
              value: rating !== undefined ? rating : value
            },
            ...records[dataIndex].extra_fields.slice(extraFieldIndex + 1)
          ]
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  submitRestrictedContact = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          send_campaing: !records[dataIndex].send_campaing
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  blockContact = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    if (records[dataIndex].whatsapp_blocked) {
      api.contact.unblockContact(records[dataIndex].id).then(data => {
        this.setState({
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              whatsapp_blocked: !records[dataIndex].whatsapp_blocked
            },
            ...records.slice(dataIndex + 1)
          ],
          loading: false
        });
      });
    } else {
      api.contact.blockContact(records[dataIndex].id).then(data => {
        this.setState({
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              whatsapp_blocked: !records[dataIndex].whatsapp_blocked
            },
            ...records.slice(dataIndex + 1)
          ],
          loading: false
        });
      });
    }
  };

  handleGroupChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  handleClientChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          client_id: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.contact
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.contact
      .update(data.id, data)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.contact.delete(id);

  addWallet = data => {
    const { user } = this.props;
    this.setState({
      loading: true
    });

    const findContact = user.contacts.find(c => c === data.id);
    if (!!findContact) {
      const deleteContact = user.contacts.filter(c => c !== findContact);
      var sendDelete = {
        user_id: user.id,
        contacts: deleteContact,
        contact: data.id
      };
    }

    user.contacts.push(data.id);

    const sendContacts = {
      user_id: user.id,
      contacts: user.contacts,
      contact: data.id
    };

    return api.contact
      .submitContact(findContact ? sendDelete : sendContacts)
      .then(() => {
        this.props.AddToWallet(findContact ? sendDelete : sendContacts);

        this.setState({
          save_alert: true,
          loading: false,
          conversationModalOpen: false,
          contact: []
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,

          contactModalOpen: false
        });
      });
  };
  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  importContacts = () => {
    this.setState({
      importModal: true
    });
  };

  closeImport = () => {
    this.setState({
      importModal: false,
      contactColumns: []
    });
  };

  onSelectName = (e, { name, value }) => {
    this.setState({
      selectedColumns: { ...this.state.selectedColumns, [name]: value }
    });
  };

  saveContact = () => {
    const dataSend = {
      data: this.state.image,
      ext: this.state.file[0].file_name.split(".")[
        this.state.file[0].file_name.split(".").length - 1
      ],
      actual_file: this.state.file[0].tmp_file
    };
    return apigroup.group.importContact(dataSend).then(data => {
      const options = data.columns.map(c => ({
        text: c,
        value: c,
        key: c
      }));

      this.setState({
        contactColumns: options,
        selectedColumns: {
          tmp_file: data.tmp_file,
          ...this.state.selectedColumns
        }
      });
    });
  };

  submitContacts = () => {
    api.contact.importContacts(this.state.selectedColumns).then(data => {
      this.setState({
        save_alert: data.status === "success" ? true : false,
        importModal: false
      });
      setTimeout(
        function() {
          this.setState({ save_alert: false });
        }.bind(this),
        5000
      );
    });
  };

  BasicUpload = props => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16
    };

    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box"
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden"
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%"
    };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback(acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );

      this.setState({
        file: acceptedFiles.map(file => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document",
          tmp_file: URL.createObjectURL(file)
        }))
      });

      acceptedFiles.forEach(file => {
        const reader = new FileReader();

        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const {
      getRootProps,
      getInputProps
    } = useDropzone({ accept: ".xls, .xlsx", onDrop });

    const thumbs = files.map(file => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));

    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [files]
    );

    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>Arraste seu arquivo, ou clique aqui para seleciona-lo.</p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };
  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      clients,
      groups,
      loadingGroups,
      save_alert,
      importModal,
      contactColumns
    } = this.state;

    const contact = this.state.records[selectedDataIndex];

    this.state.extra_fields.forEach(e => {
      if (e.show_column_on_view) columns[e.input_name] = e.label;
    });
    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Contatos</h1>
                <span className="all_companies">
                  Contatos Cadastrados:{""}
                  {total_records}
                </span>
                {save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records}
                extra_fields={this.state.extra_fields.map(e => e.input_name)}
                totalRecords={total_records}
                wallet
                fetchWallet={this.fetchWallet}
                addToWallet={
                  this.state.parameters.length > 0 &&
                  this.state.parameters[0].restrict_contacts_by_contact_user > 0
                    ? true
                    : false
                }
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                contactsImport={true}
                importContacts={this.importContacts}
                cleanErrors={this.cleanErrors}
                onDelete={id => this.delete(id)}
                onEditClick={d => this.select(d.id)}
                addWallet={d => this.addWallet(d)}
                fetchData={this.fetchRecords}
              />
              <ImportContacts
                open={importModal}
                BasicUpload={this.BasicUpload}
                file={this.state.file[0]}
                loading={loading}
                handleClose={this.closeImport}
                save={this.saveContact}
                submit={this.submitContacts}
                columns={contactColumns}
                onSelectName={this.onSelectName}
                selectedColumns={this.state.selectedColumns}
              />
              {selectedDataIndex !== -1 ? (
                <ContactModal
                  handleClose={this.handleCloseEditModal}
                  handleExtraFields={this.handleExtraFields}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  contact={contact}
                  handleGroupChange={this.handleGroupChange}
                  handleClientChange={this.handleClientChange}
                  modalHeader={
                    contact.id ? `Edição do ${contact.name}` : "Novo Contato"
                  }
                  submitRestrictedContact={this.submitRestrictedContact}
                  blockContact={this.blockContact}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  clients={clients}
                  loading={loading}
                  groups={groups}
                  loadingGroups={loadingGroups}
                  onSelectGroup={this.onSelectGroup}
                  handleGroupAddition={this.addGroup}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ conversation, user, media }) => {
  return {
    user: user.user
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);
