import React from "react";
import { Modal, Button, Icon, Image } from "semantic-ui-react";
import FormContact from "../forms/FormContact";
import placeholder from "../../../assets/img/placeholder.png";

class ContactModal extends React.Component {
  componentDidMount() {
    if (this.props.showNavigation) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", e => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.contact.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      contact,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      onSearchClientChange,
      onSelectClient,
      clients,
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      handleGroupAddition,
      groups,
      onSelectGroup,
      loadingGroups,
      loadingClients,
      loading,
      handleGroupChange,
      handleClientChange,
      showNavigation = true,
      send_campaign,
      submitRestrictedContact,
      blockContact,
      handleExtraFields
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_contact">
              <div className={contact.id ? "form-contact" : ""}>
                <FormContact
                  contact={contact}
                  clients={clients}
                  onChange={onChange}
                  onChecked={onChecked}
                  groups={groups}
                  onSelectGroup={onSelectGroup}
                  handleExtraFields={handleExtraFields}
                  handleGroupChange={handleGroupChange}
                  handleClientChange={handleClientChange}
                  handleGroupAddition={handleGroupAddition}
                  loadingGroups={loadingGroups}
                  onSelectClient={onSelectClient}
                  onSearchClientChange={onSearchClientChange}
                  loadingClients={loadingClients}
                />
              </div>
              {contact.id ? (
                <div className="contact-image">
                  {contact.avatar ? (
                    <Image src={`${contact.avatar}`} size="small" circular />
                  ) : (
                    <Image src={placeholder} size="small" circular />
                  )}

                  {send_campaign || contact.send_campaing ? (
                    <span className="green">
                      Usuário pode receber campanhas
                    </span>
                  ) : (
                    <span className="red">
                      Bloqueado, não pode receber campanhas
                    </span>
                  )}
                  <div className="block">
                    <Button
                      color={
                        send_campaign || contact.send_campaing ? "red" : "green"
                      }
                      className="block-button"
                      size="small"
                      onClick={submitRestrictedContact}
                    >
                      {send_campaign || contact.send_campaing
                        ? "Bloquear campanhas para contato"
                        : "Ativar campanhas para contato"}
                    </Button>

                    <Button
                      color={contact.whatsapp_blocked ? "green" : "red"}
                      className="block-button"
                      size="small"
                      onClick={blockContact}
                    >
                      {contact.whatsapp_blocked
                        ? "Desbloquear no Whatsapp"
                        : "Bloquear no Whatsapp "}
                    </Button>
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {showNavigation && (
            <Button.Group>
              <Button
                icon
                onClick={handlePrevious}
                disabled={previousButtonEnabled}
              >
                <Icon name="left arrow" />
              </Button>
              <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
                <Icon name="right arrow" />
              </Button>
            </Button.Group>
          )}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ContactModal;
