import React from "react";
import { Modal, Button, Form, Input } from "semantic-ui-react";

const AddContactModal = ({
	open,
	toggle,
	onChange,
	contact,
	loading,
	save
}) => (
	<Modal
		size="small"
		closeIcon
		open={open}
		onClose={toggle}
		dimmer="blurring"
		closeOnDimmerClick={false}
	>
		<Modal.Header>Cadastro de Contato</Modal.Header>
		<Modal.Content>
			<Modal.Description>
				<Form>
					<Form.Group widths="equal">
						<Form.Field
							control={Input}
							label="Nome"
							name="name"
							onChange={onChange}
							value={contact.name}
						/>
						<Form.Field
							control={Input}
							label="Telefone"
							name="phone_number"
							onChange={onChange}
							value={contact.phone_number}
						/>
					</Form.Group>
				</Form>
			</Modal.Description>
		</Modal.Content>
		<Modal.Actions>
			<Button.Group>
				<Button onClick={toggle}>Cancelar</Button>
				<Button.Or />
				<Button
					positive
					onClick={save}
					loading={loading}
					disabled={loading}
				>
					Salvar
				</Button>
			</Button.Group>
		</Modal.Actions>
	</Modal>
);

export default AddContactModal;
