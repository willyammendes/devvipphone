import React from "react";
import { Card, Icon, Image, Button, Divider } from "semantic-ui-react";
import link from "www.google.com";

const ContactCard = ({ contact }) => (
  <Card fluid>
    <Image src="https://via.placeholder.com/630x150" wrapped ui={false} />
    <Card.Content>
      <Card.Header>
        <div>
          <Image src="https://via.placeholder.com/100x100" avatar />
          <span>Fulano de Tal</span>
        </div>
      </Card.Header>
      <Card.Meta>
        <a href={link}>
          <Icon name="phone" />
          +55 (27) 99999-9999
        </a>
      </Card.Meta>
      <Card.Meta>
        <a href={link}>
          <Icon name="mail" />
          fulano@fulano.com.br
        </a>
      </Card.Meta>
      <Divider />
      <Card.Description>
        Informações Adicionais <br />
        Endereço: Rua Lorem ipsum dolor sit amet,165 <br />
        CEP:22222-222 <br />
      </Card.Description>
    </Card.Content>
    <Card.Content extra className="tr">
      <Button icon labelPosition="left">
        <Icon name="send" />
        Enviar Mensagem
      </Button>
    </Card.Content>
  </Card>
);

export default ContactCard;
