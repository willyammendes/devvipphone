import React from "react";

/* global google */

class FormAutoComplete extends React.Component {
	state = {
		address: "",
		name: "",
		street_address: "",
		number: "",
		city: "",
		state: "",
		zip_code: "",
		googleMapLink: ""
	};

	constructor(props) {
		super(props);
		this.autocompleteInput = React.createRef();
		this.autocomplete = null;
		this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
		this.handlePlaceSelect = this.handlePlaceSelect.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		this.autocomplete = new google.maps.places.Autocomplete(
			document.getElementById("autocomplete"),
			{ types: ["establishment"] }
		);

		this.autocomplete.addListener("place_changed", this.handlePlaceSelect);
	}

	handlePlaceChanged() {
		const place = this.autocomplete.getPlace();
	}

	handleChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	}

	handlePlaceSelect() {
		const addressObject = this.autocomplete.getPlace();
		this.setState({
			name: addressObject.name,
			street_address: addressObject.formatted_address,
			city: addressObject.address_components[3].long_name,
			state: addressObject.address_components[4].long_name,
			zip_code: addressObject.address_components[6].long_name,
			number: addressObject.address_components[0].long_name
		});
	}
	render() {
		return (
			<div>
				<input
					ref={this.autocompleteInput}
					id="autocomplete"
					placeholder="Enter your address"
					type="text"
				/>
				<input
					name={"name"}
					value={this.state.name}
					placeholder={"Nome da Empresa:"}
					onChange={this.handleChange}
				/>
				<input
					name={"street_address"}
					value={this.state.street_address}
					placeholder={"Endereço:"}
					onChange={this.handleChange}
				/>
				<input
					name={"city"}
					value={this.state.city}
					placeholder={"Cidade:"}
					onChange={this.handleChange}
				/>
				<input
					name={"state"}
					value={this.state.state}
					placeholder={"Estado:"}
					onChange={this.handleChange}
				/>
				<input
					name={"zip_code"}
					value={this.state.zip_code}
					placeholder={"CEP"}
					onChange={this.handleChange}
				/>
				<input
					name={"number"}
					value={this.state.number}
					placeholder={"Número:"}
					onChange={this.handleChange}
				/>
			</div>
		);
	}
}
export default FormAutoComplete;
