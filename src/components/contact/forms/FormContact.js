import React from "react";
import { Form, Input, Checkbox, TextArea, Divider } from "semantic-ui-react";
import DropdownGroups from "../../contacts-groups/DropdownGroups";
import DropdownClients from "../../client/DropdownClients";
import InputMask from "react-input-mask";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const FormContact = ({
  contact,
  onChange,
  onChecked,
  handleGroupChange,
  handleClientChange,
  handleExtraFields
}) => {
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Nome"
          name="name"
          onChange={onChange}
          value={contact.name}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Email"
          name="email"
          onChange={onChange}
          value={contact.email}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Celular"
          children={
            <InputMask
              mask="+55 (99) 99999-9999"
              name="phone_number"
              onChange={onChange}
              value={contact.phone_number}
            />
          }
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="CPF"
          children={
            <InputMask
              mask="999.999.999-99"
              name="cpf"
              onChange={onChange}
              value={contact.cpf}
            />
          }
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Cidade"
          name="city"
          onChange={onChange}
          value={contact.city}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Bairro"
          name="neighborhood"
          onChange={onChange}
          value={contact.neighborhood}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="CEP"
          children={
            <InputMask
              mask="99999-999"
              name="code"
              onChange={onChange}
              value={contact.code}
            />
          }
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Endereço"
          name="address"
          onChange={onChange}
          value={contact.address}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          control={TextArea}
          label="Informações Adicionais"
          name="additional_info"
          onChange={onChange}
          value={contact.additional_info}
        />
        <div className="field">
          <label>Webhook</label>
          <Checkbox
            label="Habilitado"
            onChange={onChecked}
            name="webhook_active"
            checked={contact.webhook_active}
          />
        </div>
      </Form.Group>
      <Form.Group widths="equal">
        <div className="field">
          <label>Empresa</label>
          <DropdownClients
            onSelectClient={handleClientChange}
            clients={contact.client_id}
            allowAdditions={true}
          />
        </div>

        <div className="field">
          <label>Grupos de Relacionamento</label>
          <DropdownGroups
            onSelectGroup={handleGroupChange}
            groups={contact.groups}
            allowAdditions={false}
            multiple
          />
        </div>
      </Form.Group>
      <Divider />
      <ExtraFieldForm
        extra_fields={contact.extra_fields}
        onChange={handleExtraFields}
      />
    </Form>
  );
};

export default FormContact;
