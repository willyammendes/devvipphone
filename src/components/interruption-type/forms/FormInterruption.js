import React from "react";
import {
    Form,
    Input,
    Checkbox,
    Button,
} from "semantic-ui-react";

const options = [
    {
        key: "coffee",
        text: "coffee",
        value: "coffee"
    },
    {
        key: "comments outline",
        text: "comments outline",
        value: "comments outline"
    },
    {
        key: "comment alternate outline",
        text: "comment alternate outline",
        value: "comment alternate outline"
    },
    {
        key: "phone",
        text: "phone",
        value: "phone"
    },
    {
        key: "laptop",
        text: "laptop",
        value: "laptop"
    },
    {
        key: "food",
        text: "food",
        value: "food"
    },
    {
        key: "calendar plus outline",
        text: "calendar plus outline",
        value: "calendar plus outline"
    },
    {
        key: "ambulance",
        text: "ambulance",
        value: "ambulance"
    },
    {
        key: "user md",
        text: "user md",
        value: "user md"
    },
    {
        key: "bath",
        text: "bath",
        value: "bath"
    },
    {
        key: "shopping cart",
        text: "shopping cart",
        value: "shopping cart"
    },
    {
        key: "sitemap",
        text: "sitemap",
        value: "sitemap"
    },
    {
        key: "truck",
        text: "truck",
        value: "truck"
    },
    {
        key: "camera retro",
        text: "camera retro",
        value: "camera retro"
    },
    {
        key: "credit card",
        text: "credit card",
        value: "credit card"
    },
    {
        key: "wrench",
        text: "wrench",
        value: "wrench"
    },
    {
        key: "book",
        text: "book",
        value: "book"
    },
    {
        key: "calendar alternate",
        text: "calendar alternate",
        value: "calendar alternate"
    },
    {
        key: "flask",
        text: "flask",
        value: "flask"
    },
    {
        key: "music",
        text: "music",
        value: "music"
    },
    {
        key: "server",
        text: "server",
        value: "server"
    },
    {
        key: "globe",
        text: "globe",
        value: "globe"
    },
    {
        key: "leaf",
        text: "leaf",
        value: "leaf"
    },
    {
        key: "bug",
        text: "bug",
        value: "bug"
    },
    {
        key: "box",
        text: "box",
        value: "box"
    },
    {
        key: "gift",
        text: "gift",
        value: "gift"
    },
    {
        key: "space shuttle",
        text: "space shuttle",
        value: "space shuttle"
    },

    {
        key: "assistive listening systems",
        text: "assistive listening systems",
        value: "assistive listening systems"
    },
    {
        key: "history",
        text: "history",
        value: "history"
    },
    { key: "briefcase", text: "briefcase", value: "briefcase" }
];

const FormInterruption = ({
    interruption,
    onChange,
    onChecked,
    onSelectIcon,
    icon_active
}) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Nome:"
                name="name"
                onChange={onChange}
                value={interruption.name}
            />
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Duração:"
                name="duration"
                onChange={onChange}
                value={interruption.duration}
            />

            <div className="status_select">
                <nav>
                    <ul>
                        {options
                            .filter(s => !!s.value)
                            .map((d, i) => (
                                <li key={`options-${i * 1}`}>
                                    <Button
                                        icon={d.value}
                                        value={d.value}
                                        name="fast_menu_icon"
                                        onClick={onSelectIcon}
                                        className={
                                            icon_active === d.value
                                                ? "ativo"
                                                : "desativado"
                                        }
                                    />
                                </li>
                            ))}
                    </ul>
                </nav>
            </div>

            <div className="field">
                <label>Ativo</label>
                <Checkbox
                    label="Motivo Ativado"
                    onChange={onChecked}
                    name="active"
                    checked={interruption.active}
                />
            </div>
        </Form>
    );
};

export default FormInterruption;
