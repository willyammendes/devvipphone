import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../../api/page-record";
import DataTable from "../table/DataTable";
import CustomPageModal from "./modal/CustomPageModal";
import AlertSuccess from "../alerts/AlertSuccess";
import AlertRed from "../alerts/AlertRed";
import { withRouter } from "react-router";

class CustomPage extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    pageOptions: [],
    pageHeader: "",
    extra_fields: [],
    pageNotFound: false
  };

  select = selectedDataId => {
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    this.setState({
      selectedDataIndex: dataIndex,
      editModalOpen: true,
      pageOptions: this.state.records[dataIndex].options
    });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce((o, key) => {
          return Object.assign(o, {
            [key]: Array.isArray(records[0][key])
              ? key === "extra_fields"
                ? this.state.extra_fields
                : []
              : ""
          });
        }, {})
      : {
          input_options: [],
          extra_fields: this.state.extra_fields
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });
    const pageId = this.props.match.params.id;

    return await api.pageRecord.fetchAll(pageId, params).then(res => {
      this.setState({
        page_id: this.props.match.params.id,
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        extra_fields: res.extra_fields
      });
    });
  };

  handleExtraFields = (e, { name, value }, extraFieldId) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    const extraFieldIndex = records[dataIndex].extra_fields.findIndex(
      c => c.id === extraFieldId
    );

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          extra_fields: [
            ...records[dataIndex].extra_fields.slice(0, extraFieldIndex),
            {
              ...records[dataIndex].extra_fields[extraFieldIndex],
              value: value
            },
            ...records[dataIndex].extra_fields.slice(extraFieldIndex + 1)
          ]
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  componentDidMount() {
    this.verifyPage();
  }

  verifyPage = () => {
    const { companies, user } = this.props;

    const currentCompany = companies.find(c => c.id === user.company_id);

    if (currentCompany) {
      if (currentCompany.pages.length > 0) {
        const page = currentCompany.pages.find(
          p => p.id == this.props.match.params.id
        );
        if (page) {
          this.setState({
            pageHeader: page.header,
            pageNotFound: false
          });
        } else {
          this.setState({
            pageHeader: "Pagina Não Encontrada",
            pageNotFound: true
          });
        }
      } else {
        this.setState({
          pageHeader: "Pagina não encontrada",
          pageNotFound: true
        });
      }
    }
  };
  onAddItem = (e, { value }) => {
    this.setState(prevState => ({
      pageOptions: [{ text: value, value }, ...prevState.pageOptions]
    }));
  };

  handleChange = (e, { name, value, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked !== undefined ? checked : value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.pageRecord
      .submit(this.props.match.params.id, records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.pageRecord
      .update(this.props.match.params.id, data.id, data)
      .then(data => {
        this.setState({
          records: [
            ...this.state.records.slice(0, selectedDataIndex),
            { ...data.data },
            ...this.state.records.slice(selectedDataIndex + 1)
          ],
          editModalOpen: false,
          selectedDataIndex: -1,
          save_alert: true,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  componentWillUpdate(a) {
    if (
      a.match.params.id != this.state.page_id &&
      this.state.page_id != undefined
    ) {
      window.location.reload();
    }
  }

  delete = id => api.pageRecord.delete(this.props.match.params.id, id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      pageHeader
    } = this.state;

    const page = this.state.records[selectedDataIndex];

    this.state.extra_fields.forEach(e => {
      if (e.show_column_on_view) columns[e.input_name] = e.label;
    });

    if (this.state.pageNotFound) {
      return <AlertRed message="Pagina Não Encontrada" />;
    }

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>{pageHeader}</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records}
                extra_fields={this.state.extra_fields.map(e => e.input_name)}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={id => this.delete(id)}
                onEditClick={d => this.select(d.id)}
                fetchData={this.fetchRecords}
              />
              {selectedDataIndex !== -1 ? (
                <CustomPageModal
                  handleExtraFields={this.handleExtraFields}
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  page={page}
                  modalHeader={
                    page.id
                      ? `Edição do Cadastro ${page.id}`
                      : `Novo ${pageHeader}`
                  }
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.errors
                      : ""
                  }
                  generalError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  cleanErrors={this.cleanErrors}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
    companies: user.user.companies
  };
};

export default withRouter(connect(mapStateToProps)(CustomPage));
