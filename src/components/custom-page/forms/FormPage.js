import React from "react";
import { Form } from "semantic-ui-react";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const FormPage = ({ page, handleExtraFields }) => {
  return (
    <Form autocomplete="off">
      <ExtraFieldForm
        extra_fields={page.extra_fields}
        onChange={handleExtraFields}
      />
    </Form>
  );
};

export default FormPage;
