import React from "react";

import { Modal, Button, Icon } from "semantic-ui-react";
import FormPage from "../forms/FormPage";

class CustomPageModal extends React.Component {
  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", (e) => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.page.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      page,
      handleClose,
      onChange,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      messageError,
      generalError,
      cleanErrors,
      handleExtraFields,
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                <p>{generalError}</p>
              </div>
            )}
            <div className="holder_department">
              <div className="form-department">
                <FormPage
                  handleExtraFields={handleExtraFields}
                  page={page}
                  onChange={onChange}
                  messageError={messageError}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default CustomPageModal;
