import React, { Component } from "react";
import { Card, Button, Select, Form, Input, Message } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/parameters";

class Call extends Component {
	state = {
		parameter: [],
		loading: false,
		hasMore: true
	};
	handleChange = (e, { value }) => this.setState({ value });

	componentWillMount() {
		this.setState({ loading: true });
		api.parameter.fetchAll().then(parameter => {
			this.setState({
				parameter,
				loading: false
			});
		});
	}

	render() {
		const { parameter, loading } = this.state;
		return (
			<div className="pageHolder">
				<div className="headingPage">
					<h1>Chamada</h1>
				</div>
				{loading && (
					<div className="loading-conversa">
						<LoaderComponent />
					</div>
				)}
				<div className="holderPage">
					<div className="full">
						<Card fluid>
							<Card.Content>
								<Card.Header>Integração de Chamada</Card.Header>
							</Card.Content>
							<Card.Content>
								<Form>
									<Form.Field
										control={Input}
										label="URL de API de chamada:"
										name="url"
										value={parameter.server}
									/>
									<Form.Field
										control={Input}
										label="Chave de API de chamada:"
										name="key"
										value="92GB9PL9"
									/>
									<Form.Field
										control={Input}
										label="Prefixo para chamada:"
										name="prefix"
										value="0"
									/>
									<Message color="blue">
										O prefixo de chamada e o número
										utilizado para puxar a linha
									</Message>
									<Form.Field
										control={Input}
										label="Endereço do servidor de telefonia:"
										name="server"
										value="187.12.84.203"
									/>
									<Message color="blue">
										Ex: http://192.168.100.250
									</Message>
									<Form.Field
										control={Select}
										label="Exibir popup para chamadas de ramal:"
										name="user"
									/>
									<Button color="blue">Salvar</Button>
									<Button color="white">Limpar</Button>
								</Form>
							</Card.Content>
						</Card>
					</div>
				</div>
			</div>
		);
	}
}

export default Call;
