import React from "react";
import renderHTML from "react-render-html";
import { Image, Modal } from "semantic-ui-react";
import "react-chat-elements/dist/main.css";
import { MessageBox } from "react-chat-elements";
import ReactPlayer from "react-player";
import moment from "moment";
import QuotedMessage from "../quoted-message/QuotedMessage";
import "moment/locale/pt-br";

const ReceivedMessage = ({ message, contact, playing }) => {
  return (
    <div className="mensagem mensagem-recebida">
      <div className="foto-perfil">
        <img
          src={
            contact.avatar.indexOf("whatsapp") !== -1
              ? contact.avatar
              : `${contact.avatar}`
          }
          alt=""
        />
      </div>
      <div className="corpo-mensagem">
        <p className="nome">{contact.name}</p>
        <div>
          {message.quotedMessage && (
            <QuotedMessage message={message.quotedMessage} contact={contact} />
          )}
          {message.message_type === 4 ? (
            <Modal
              trigger={
                <MessageBox
                  position={"right"}
                  className="received_image"
                  type={"photo"}
                  data={{
                    uri: `${message.src}`,
                    status: {
                      click: true,
                    },
                  }}
                />
              }
              basic
              closeIcon
              className="modal_image"
            >
              <Modal.Content>
                <Image src={message.src} />
              </Modal.Content>
            </Modal>
          ) : (
            ""
          )}
          {message.message_type === 3 ? (
            <a
              href={message.src}
              download
              target="_blank"
              rel="noopener noreferrer"
            >
              <MessageBox
                position={"right"}
                className="received_image"
                type={"file"}
                text={
                  message.src.split(
                    "https://monitchat.nyc3.cdn.digitaloceanspaces.com/app/"
                  )[1]
                }
                data={{
                  uri: `${message.src}`,
                  status: {
                    click: false,
                    loading: 0,
                  },
                }}
              />
            </a>
          ) : (
            ""
          )}
          {message.message_type === 1 ? (
            <div className="video_holder">
              <ReactPlayer
                playing={playing}
                controls
                className="video_message"
                url={message.src}
              />
            </div>
          ) : (
            ""
          )}
          {message.message_type === 5 ? (
            <div className="div-audio">
              <audio controls>
                <source src={message.src} type="audio/ogg" />
              </audio>
            </div>
          ) : (
            ""
          )}
          {message.message_type != 0 ? (
            <div
              style={{
                maxWidth: "250px",
                backgroundColor: "#e5f9ff",
                borderRadius: "5px",
                marginLeft: "5px",
              }}
            >
              {message.message &&
              (message.message.includes("<p>") ||
                message.message.includes("<b>"))
                ? renderHTML(message.message)
                : "" + message.message + ""}
            </div>
          ) : message.message &&
            (message.message.includes("<p>") ||
              message.message.includes("<b>")) ? (
            renderHTML(message.message)
          ) : (
            "" + message.message + ""
          )}
        </div>
        <div className="status">
          <p className="nome data">
            <time dateTime={message.timestamp}>
              {moment
                .unix(message.timestamp)
                .locale("pt-br")
                .format("LT")}
            </time>
          </p>
        </div>
      </div>
    </div>
  );
};
export default ReceivedMessage;
