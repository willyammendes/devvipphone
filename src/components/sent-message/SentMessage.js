import React from "react";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";
import { Image, Modal, Icon } from "semantic-ui-react";
import "react-chat-elements/dist/main.css";
import { MessageBox } from "react-chat-elements";
import ReactPlayer from "react-player";
import moment from "moment";
import "moment/locale/pt-br";

const SentMessage = ({ message, playing, resendMessage }) => (
  <div className="mensagem mensagem-enviada">
    <div className="foto-perfil">
      <img src={message.avatar} alt="" />
    </div>
    <div className="corpo-mensagem">
      <p className="nome">{message.user}</p>
      {message.message_type === 4 ? (
        <Modal
          trigger={
            <MessageBox
              position={"right"}
              className="received_image"
              type={"photo"}
              data={{
                uri: `${message.src}`,
                status: {
                  click: true,
                },
              }}
            />
          }
          basic
          closeIcon
          className="modal_image"
        >
          <Modal.Content>
            <Image src={message.src} />
          </Modal.Content>
        </Modal>
      ) : (
        ""
      )}

      {message.message_type === 3 ? (
        <a href={message.src} download>
          <MessageBox
            position={"right"}
            className="received_image"
            type={"file"}
            text={message.src}
            data={{
              uri: `${message.src}`,
              status: {
                click: false,
                loading: 0,
              },
            }}
          />
        </a>
      ) : (
        ""
      )}
      {message.message_type === 1 ? (
        <div className="video_holder">
          <ReactPlayer
            playing={playing}
            controls
            className="video_message"
            url={message.src}
          />
        </div>
      ) : (
        ""
      )}
      {message.message_type === 5 ? (
        <div className="div-audio">
          <audio controls>
            <source src={message.src} type="audio/ogg" />
          </audio>
        </div>
      ) : (
        ""
      )}
      {message.message_type != 0 ? (
        <div
          style={{
            maxWidth: "250px",
            backgroundColor: "#aed2ff",
            borderRadius: "5px",
            marginRight: "18px",
            marginBottom: "2px",
            float: "right",
            padding: "1px",
          }}
        >
          {message.message &&
          (message.message.includes("<p>") || message.message.includes("<b>"))
            ? renderHTML(message.message)
            : "" + message.message + ""}
        </div>
      ) : message.message &&
        (message.message.includes("<p>") || message.message.includes("<b>")) ? (
        renderHTML(message.message)
      ) : (
        "" + message.message + ""
      )}
      {/* STATUS */}
      <div className="status">
        {message.status === 0 ? (
          <Icon
            name="clock outline"
            size="small"
            className="status_cinza"
            title="Aguardando Envio"
          />
        ) : (
          ""
        )}

        {message.status === 1 ? (
          <Icon.Group size="small" className="status_cinza">
            <Icon name="envelope outline" />
            <Icon
              corner="bottom right"
              className="seta"
              name="mail forward"
              title={`Mensagem Enviada por ${message.account_number}`}
            />
          </Icon.Group>
        ) : (
          ""
        )}

        {message.status === 2 ? (
          <Icon
            name="envelope outline"
            size="small"
            className="status_cinza"
            title="Mensagem Entregue"
          />
        ) : (
          ""
        )}
        {message.status === 3 ? (
          <Icon
            name="envelope open outline"
            size="small"
            className="status_cinza azul"
            title="Mensagem Lida"
          />
        ) : (
          ""
        )}
        {message.status === 4 ? (
          <Icon
            name="microphone"
            size="small"
            className="status_cinza azul"
            title="Áudio Reproduzido"
          />
        ) : (
          ""
        )}

        {message.status === -3 ? (
          <Icon
            name="whatsapp"
            color="red"
            size="small"
            style={{ fontWeight: "bold" }}
            className="status_cinza"
            title={`O número ${
              message.phone_number
            } não possui conta ativa no Whatsapp`}
          />
        ) : (
          ""
        )}

        {message.status === 0 || message.status === -3 ? (
          <Icon
            name="sync"
            size="small"
            className="status_cinza"
            title="Enviar Novamente"
            style={{ cursor: "pointer" }}
            onClick={() => resendMessage(message)}
          />
        ) : (
          ""
        )}

        <p className="nome data">
          <time dateTime={message.timestamp}>
            {moment
              .unix(message.timestamp)
              .locale("pt-br")
              .format("LT")}
          </time>
        </p>
      </div>
    </div>
  </div>
);

SentMessage.propTypes = {
  message: PropTypes.shape().isRequired,
};

export default SentMessage;
