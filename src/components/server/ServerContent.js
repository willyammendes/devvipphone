import React from "react";
import PopoverForm from "../semantic/Popover";
import { Table } from "semantic-ui-react";

const ServerContent = ({ server }) => (
  <Table.Body>
    {server.map((server, i) => (
      <Table.Row key={`server-menu-${i * 1}`}>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.id}</span>
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.description}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.host}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.gate}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.sessions}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.limit}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={server.id}>
          <span>{server.active}</span>
          <PopoverForm />
        </Table.Cell>
      </Table.Row>
    ))}
  </Table.Body>
);

export default ServerContent;
