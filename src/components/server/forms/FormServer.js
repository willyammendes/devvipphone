import React from "react";
import { Form, Input, Checkbox, Dropdown } from "semantic-ui-react";

const FormServer = ({
  server,
  onChange,
  onChecked,
  onSearch,
  isFetchingCompanies
}) => {
  return (
    <Form>
      <Form.Field
        autoComplete="off"
        control={Input}
        label="Descrição"
        name="name"
        onChange={onChange}
        placeholder="Descrição"
        value={server.name}
      />
      <Form.Field>
        <label htmlFor="model">Empresa</label>
        <Dropdown
          placeholder="Empresa"
          search
          onSearchChange={onSearch}
          clearable
          selection
          multiple={false}
          name="company_id"
          loading={isFetchingCompanies}
          onChange={onChange}
          value={server.company_id}
          options={server.companies}
        />
      </Form.Field>
      <Form.Field
        autoComplete="off"
        control={Input}
        label="Host/IP Publico"
        name="host_name"
        placeholder="Host/IP Publico"
        onChange={onChange}
        value={server.host_name}
      />
      <Form.Field
        autoComplete="off"
        control={Input}
        label="IP Interno"
        name="internal_address"
        placeholder="IP Interno"
        onChange={onChange}
        value={server.internal_address}
      />
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Porta do Container"
          name="port"
          placeholder="Porta do Container"
          onChange={onChange}
          value={server.port}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Porta Externa"
          name="external_port"
          placeholder="Porta Externa"
          onChange={onChange}
          value={server.external_port}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Contas Conectadas"
          name="accounts"
          placeholder="Contas Conectadas"
          onChange={onChange}
          value={server.accounts}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Limite de Sessões"
          name="max_sessions"
          placeholder="Limite de Sessões"
          onChange={onChange}
          value={server.max_sessions}
        />
      </Form.Group>
      <Form.Field>
        <Checkbox
          label="Servidor Ativo"
          name="active"
          value={server.active}
          checked={server.active}
          onChange={onChecked}
        />
      </Form.Field>
    </Form>
  );
};

export default FormServer;
