import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Icon from "@mdi/react";
import { mdiMagnify, mdiClose } from "@mdi/js";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";

function changeClass() {
  const element = document.getElementsByName("busca-header");
  for (let i = 0; i < element.length; i += 1) {
    element[i].classList.toggle("ativo");
  }
}

const SearchConversation = ({
  placeholder,
  searchQuery,
  onSearchConversation,
  cleanSearch
}) => {
  return (
    <div className="busca">
      <div className="inputBusca" name="busca-header">
        <input
          type="text"
          name="searchQuery"
          value={searchQuery}
          onChange={onSearchConversation}
          placeholder={placeholder}
        />
        <button
          className="botaoBusca"
          onClick={searchQuery.length > 0 ? cleanSearch : changeClass}
        >
          {searchQuery.length > 0 ? (
            <Icon path={mdiClose} size={1} horizontal vertical rotate={180} />
          ) : (
            <Icon path={mdiMagnify} size={1} horizontal vertical rotate={180} />
          )}
        </button>
      </div>
    </div>
  );
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  state => ({ filter: state.conversation.filter }),
  mapDispatchToProps
)(SearchConversation);
