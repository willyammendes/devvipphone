import React from "react";
import { Button, Popup } from "semantic-ui-react";

const PopupSearch = ({}) => {
  return (
    <Popup
      content="Novo contato"
      trigger={<Button icon="plus" className="create_Contact" color="blue" />}
    />
  );
};

export default PopupSearch;
