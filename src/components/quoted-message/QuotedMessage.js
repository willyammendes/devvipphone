import React from "react";
import renderHTML from "react-render-html";
import { Image, Modal } from "semantic-ui-react";
import "react-chat-elements/dist/main.css";
import { MessageBox } from "react-chat-elements";
import moment from "moment";
import ReactPlayer from "react-player";

import "moment/locale/pt-br";

const QuotedMessage = ({ message }) => {
  return (
    <div className="mensagem-quoted">
      {message.message_type === 4 ? (
        <Modal
          trigger={
            <MessageBox
              position={"right"}
              className="received_image_quoted"
              type={"photo"}
              data={{
                uri: `${message.src}`,
                status: {
                  click: true,
                },
              }}
            />
          }
          basic
          closeIcon
          className="modal_image"
        >
          <Modal.Content>
            <Image src={message.src} />
          </Modal.Content>
        </Modal>
      ) : (
        ""
      )}
      {message.message_type === 3 ? (
        <a
          href={message.src}
          download
          target="_blank"
          rel="noopener noreferrer"
        >
          <MessageBox
            position={"right"}
            className="received_image_quoted"
            type={"file"}
            text={
              message.src.split(
                "https://monitchat.nyc3.cdn.digitaloceanspaces.com/app/"
              )[1]
            }
            data={{
              uri: `${message.src}`,
              status: {
                click: false,
                loading: 0,
              },
            }}
          />
        </a>
      ) : (
        ""
      )}
      {message.message_type === 1 ? (
        <div className="video_holder">
          <ReactPlayer
            playing={false}
            controls
            className="video_message"
            url={message.src}
          />
        </div>
      ) : (
        ""
      )}
      {message.message_type === 5 ? (
        <div
          style={{
            marginTop: "20px",
            borderRadius: "5px",
            display: "flex",
            justifyContent: "center",
            alignSelf: "center",
            background: "rgb(71, 77, 80)",
            minHeight: "60px",
            paddingLeft: "5px",
            background:
              "linear-gradient(90deg,rgba(71, 77, 80, 1) 1%,rgba(204, 204, 204, 1) 5px)",
          }}
        >
          <div
            className="div-audio"
            style={{
              alignSelf: "center",
            }}
          >
            <audio controls>
              <source src={message.src} type="audio/ogg" />
            </audio>
          </div>
        </div>
      ) : (
        ""
      )}
      {message.message_type != 0 ? (
        <div
          style={{
            maxWidth: "250px",
            backgroundColor: "#e5f9ff",
            borderRadius: "5px",
            marginLeft: "5px",
          }}
        >
          {message.message &&
          (message.message.includes("<p>") || message.message.includes("<b>"))
            ? renderHTML(message.message)
            : "" + message.message + ""}
        </div>
      ) : (
        <div
          style={{
            marginTop: "20px",
            borderRadius: "5px",
            background: "rgb(71, 77, 80)",
            minHeight: "30px",
            paddingLeft: "5px",
            background:
              "linear-gradient(90deg,rgba(71, 77, 80, 1) 1%,rgba(204, 204, 204, 1) 5px)",
          }}
        >
          <div>
            {message.message &&
            (message.message.includes("<p>") || message.message.includes("<b>"))
              ? renderHTML(message.message)
              : "" + message.message + ""}
          </div>
          <div
            style={{
              textAlign: "right",
              fontSize: "8px",
              paddingRight: "5px",
            }}
          >
            <time dateTime={message.timestamp}>
              {moment
                .unix(message.timestamp)
                .locale("pt-br")
                .format("LT")}
            </time>
          </div>
        </div>
      )}
    </div>
  );
};
export default QuotedMessage;
