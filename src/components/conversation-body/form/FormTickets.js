import React from "react";
import {
  Form,
  Input,
  TextArea,
  Dropdown,
  Select,
  Button
} from "semantic-ui-react";

const FormTickets = ({
  ticket,
  onChange,
  onSelectClient,
  onSearchClientChange,
  className,
  name
}) => {
  return (
    <Form className={className} name={name}>
      <div className="form-row">
        <div className="field-row">
          <Form.Field
            autoComplete="off"
            control={Input}
            label="Título"
            name="title"
            onChange={onChange}
            value={ticket.title}
          />
          <Form.Field
            autoComplete="off"
            control={Input}
            label="ID do contato"
            name="contact_id"
            onChange={onChange}
            value={ticket.contact_id}
          />
          <div className="field">
            <label>Departamento</label>
            <Dropdown
              placeholder="Escolha uma Opção"
              fluid
              search
              selection
              allowadittion
              name="department_id"
              value={ticket.department_id}
              onSearchChange={onSearchClientChange}
              onChange={onSelectClient}
            />
          </div>
          <div className="field">
            <label>Categoria</label>
            <Dropdown
              placeholder="Escolha uma Opção"
              fluid
              search
              selection
              allowAdditions
              name="ticket_category_id"
              value={ticket.ticket_category_id}
              onSearchChange={onSearchClientChange}
              onChange={onSelectClient}
            />
          </div>
          <Form.Field
            autoComplete="off"
            control={Input}
            label="Contato"
            name="contact"
            onChange={onChange}
            value={ticket.contact}
            readOnly
          />
          <div className="field">
            <label>Responsável</label>
            <Dropdown
              placeholder="Escolha uma Opção"
              fluid
              search
              selection
              allowAdditions
              name="user_id"
              value={ticket.user_id}
              onSearchChange={onSearchClientChange}
              onChange={onSelectClient}
            />
          </div>
          <Form.Field
            control={TextArea}
            label="Assunto"
            name="incident"
            onChange={onChange}
            value={ticket.incident}
          />
        </div>
        <div className="field-row">
          <label>Comentário</label>
          <TextArea
            placeholder="Interagir no chamado"
            name="comment"
            onChange={onChange}
            value={ticket.comment}
          />
          <div className="interacoes-comment">
            <Select placeholder="Status" />
            <Button primary>Enviar</Button>
          </div>
        </div>
      </div>
    </Form>
  );
};

export default FormTickets;
