import React from "react";
import {
    Form,
    Input,
    TextArea,
    Dropdown,
    Select,
    Button
} from "semantic-ui-react";
import DropdownCategories from "../../ticket-category/DropdownCategories";

const FormComment = ({
    ticket,
    onChange,
    departments,
    categories,
    onSelectClient,
    onSearchClientChange,
    className,
    name,
    comment,
    handleCategoryChange
}) => {
    return (
        <Form className={className} name={name}>
            <div className="form-row">
                <div className="field-row">
                    <div className="field">
                        <label>Categoria do Ticket</label>
                        <DropdownCategories
                            onSelectCategory={handleCategoryChange}
                            ticket_category_id={comment.ticket_category_id}
                            allowAdditions={false}
                        />
                    </div>
                    <Form.Field
                        control={TextArea}
                        label="Observações ao Ticket:"
                        name="comment"
                        onChange={onChange}
                        value={ticket.message}
                    />
                </div>
            </div>
        </Form>
    );
};

export default FormComment;
