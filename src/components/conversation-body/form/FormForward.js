import React from "react";
import { Form, TextArea, Button } from "semantic-ui-react";
import DropdownUsers from "../../users/DropdownUsers";
import DropdownDepartments from "../../departments/DropdownDepartments";

const FormForward = ({
    activeTicket,
    ticket,
    cleanErrors,
    save,
    messageError,
    textareaCount,
    loading,
    onChange,
    onChecked,
    handleUserChange,
    handleDepartmentChange,
    className,
    name
}) => {
    return (
        <Form className={className} name={name}>
            {messageError && (
                <div className="errors-table">
                    <Button
                        circular
                        basic
                        color="black"
                        icon="close"
                        className="button-close"
                        onClick={cleanErrors}
                    />
                    <p>{messageError}</p>
                </div>
            )}
            <div className="form-row">
                <div className="field-row">
                    <div className="field">
                        <label>Atendente responsável:</label>
                        <DropdownUsers
                            onSelectUser={handleUserChange}
                            user_id={ticket.user_id}
                            allowAdditions={false}
                        />
                    </div>
                    <div className="field">
                        <label>Departamento:</label>
                        <DropdownDepartments
                            onSelectDepartment={handleDepartmentChange}
                            department_id={ticket.department_id}
                            closeOnChange
                        />
                    </div>

                    <div className="field">
                        <label>
                            Observações:{" "}
                            <span className="contagem">
                                Limite de caracteres:{""}
                                <b>{5000 - textareaCount}</b>
                            </span>
                        </label>
                        <TextArea
                            rows="7"
                            maxlength="5000"
                            name="comment"
                            onChange={onChange}
                            value={ticket.comment}
                        />
                    </div>
                </div>
            </div>
        </Form>
    );
};

export default FormForward;
