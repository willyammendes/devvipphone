import React from "react";
import { Modal, Button } from "semantic-ui-react";
import FormForward from "../form/FormForward";

class ForwardModal extends React.Component {
  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", (e) => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.activeTicket.id) {
      this.props.onClickAdd();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      activeTicket,
      ticket,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      loading,
      textareaCount,
      messageError,
      cleanErrors,
      save,
      handleUserChange,
      handleDepartmentChange,
    } = this.props;

    return (
      <Modal
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_ticket">
              <div className="form-ticket">
                <FormForward
                  activeTicket={activeTicket}
                  ticket={ticket}
                  cleanErrors={cleanErrors}
                  save={save}
                  messageError={messageError}
                  textareaCount={textareaCount}
                  loading={loading}
                  onChange={onChange}
                  onChecked={onChecked}
                  handleUserChange={handleUserChange}
                  handleDepartmentChange={handleDepartmentChange}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Enviar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ForwardModal;
