import React from "react";
import { Modal, Button } from "semantic-ui-react";
import FormComment from "../form/FormComment";

const CommentModal = ({
	ticket,
	onClickAdd,
	handleClose,
	onChange,
	open,
	trigger,
	modalHeader = "",
	loading,
	comment,
	handleCategoryChange,
	error
}) => {
	return (
		<Modal
			size="large"
			closeIcon
			open={open}
			trigger={trigger}
			onClose={handleClose}
			dimmer="blurring"
			closeOnDimmerClick={false}
		>
			<Modal.Header>{modalHeader}</Modal.Header>
			<Modal.Content>
				<Modal.Description>
					<div className="form-ticket">
						{error || ""}
						<FormComment
							ticket={ticket}
							comment={comment}
							handleCategoryChange={handleCategoryChange}
							onChange={onChange}
						/>
					</div>
				</Modal.Description>
			</Modal.Content>
			<Modal.Actions>
				<Button.Group>
					<Button onClick={handleClose}>Cancelar</Button>
					<Button.Or />
					<Button
						positive
						onClick={onClickAdd}
						loading={loading}
						disabled={loading}
					>
						Enviar
					</Button>
				</Button.Group>
			</Modal.Actions>
		</Modal>
	);
};

export default CommentModal;
