import React, { Component } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../api/social-media";
import { Button } from "semantic-ui-react";
import SocialMediaModal from "../social-media/modal/SocialMediaModal";
import logo from "../../assets/img/logo.png";

class EmptyBody extends Component {
  state = {
    media: [],
    loading: false,
    hasMore: true,
    records: [],
    columns: {},
    order: {},
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingQrCode: false,
    qrCodeCreated: false,
    qrCodeImage: null,
    loginSuccess: false,
    phoneNumber: ""
  };

  componentDidMount() {
    this.fetchRecords();
    api.media.fetchData().then(media => {
      this.setState({
        media,
        loading: false
      });
    });
  }

  createQrCode = () => {
    this.setState({
      loadingQrCode: true
    });

    api.media.createQrCode().then(res => {});
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = Object.keys(records[0]).reduce(
      (o, key) => Object.assign(o, { [key]: "" }),
      {}
    );

    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.media.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.media
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          loading: false,
          records: [
            ...records.slice(0, dataIndex),
            data.media,
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.media.update(data.id, data).then(data => {
      this.setState({
        records: [
          ...this.state.records.slice(0, selectedDataIndex),
          { ...data.data },
          ...this.state.records.slice(selectedDataIndex + 1)
        ],
        loading: false
      });
    });
  };

  delete = id => api.media.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  openModal = () => {
    this.setState({
      editModalOpen: true
    });
  };

  render() {
    const {
      media,
      records,
      loading,
      selectedDataIndex,
      editModalOpen
    } = this.state;

    return (
      <div className="holder_mobile" name="chat">
        <div className="corpo-chat vazio ativo" name="sideinfo">
          <div className="holder-empty">
            <a href="/" className="logo">
              <img src={logo} alt="" />
            </a>
            <h2>Selecione um contato para começar uma conversa</h2>
            <PerfectScrollbar>
              <p>
                Lembre-se sempre de verificar se sua conta de whatsapp está
                conectada, caso não estiver clique no link para conectar.
              </p>
              <hr />
              <a href onClick={this.openModal}>
                <Button color="green">Conectar</Button>
              </a>
            </PerfectScrollbar>
          </div>
        </div>
        <SocialMediaModal
          handleClose={this.handleCloseEditModal}
          qrCodeImage={this.state.qrCodeImage}
          qrCodeCreated={this.state.qrCodeCreated}
          createQrCode={this.createQrCode}
          loadingQrCode={this.state.loadingQrCode}
          loginSuccess={this.state.loginSuccess}
          phone_number={this.state.phoneNumber}
          onChange={this.handleChange}
          onChecked={this.handleChecked}
          handleNext={this.nextRecord}
          handlePrevious={this.previousRecord}
          onClickSave={this.update}
          onClickAdd={this.submit}
          handleGroupAddition={this.addGroup}
          media={media}
          modalHeader={
            media.id ? `Edição do ${media.virtual_assistant}` : "Novo Número"
          }
          open={editModalOpen}
          previousButtonEnabled={selectedDataIndex === 0}
          nextButtonEnabled={selectedDataIndex === records.length - 1}
          loading={loading}
        />
      </div>
    );
  }
}
export default EmptyBody;
