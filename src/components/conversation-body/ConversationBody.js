import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import apiuser from "../../api/users";
import apiticket from "../../api/ticket";
import PerfectScrollbar from "react-perfect-scrollbar";
import SentMessage from "../sent-message/SentMessage";
import ReceivedMessage from "../received-message/ReceivedMessage";
import FormMesage from "../form-message/FormMesage";
import DataSticky from "../data-stick/DataSticky";
import ServiceStatus from "../service-status/ServiceStatus";
import LoaderComponent from "../semantic/Loading";
import Comment from "../comment/Comment";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import "react-chat-elements/dist/main.css";
import { Progress, Popup, Button } from "semantic-ui-react";
import ModalTicketFinish from "../service-status/ModalTicketFinish";

class ConversationBody extends Component {
  state = {
    value: "",
    title: "",
    comment: "",
    playing: false,
    controls: false,
    loading: false,
    users: [],
    scrollEnd: false,
    endMessages: false,
    endTickets: false,
    ticket: "",
    showArrow: false,
    openModal: false,
    successModal: false,
    ticketFinish: {},
    conversationId: null,
  };
  handlePlayPause = () => {
    this.setState({ playing: !this.state.playing });
  };
  sendForward = async (e) => {
    e.preventDefault();
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const user_id = this.props.user.id;
    const ticketProgress = {
      ...this.state.ticket.data,
      user_id,
    };
    await apiticket.ticket
      .update(this.state.ticket.data.id, ticketProgress)
      .then((data) => {
        this.setState({
          loading: false,
        });
      });
    setTimeout(() => {
      this.props.setTicketUser(conversationId, e).then(() => {
        this.setState({ message: "", loading: false });
      });
    }, 1000);
  };
  fetchUser = async (params) => {
    this.setState({ loading: true });
    return await apiuser.user.fetchUsers(params).then((res) => {
      this.setState({
        users: res.data,
        loading: false,
      });
    });
  };

  fetchActiveConversation = async () => {
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );

    this.fetchUser();
    if (activeConversation.active_ticket_id) {
      apiticket.ticket
        .fetchTicket(activeConversation.active_ticket_id)
        .then((ticket) => {
          this.setState(
            {
              ticket,
              conversationId: activeConversation.id,
            },
            () => {}
          );
        });
    }
  };

  componentWillReceiveProps(a, b) {
    if (a.conversation.activeConversationId !== this.state.conversationId) {
      this.fetchActiveConversation();
    }

    this.setState({
      conversationId: a.conversation.activeConversationId,
    });
  }

  componentWillMount() {
    // this.fetchActiveConversation();
  }
  componentDidMount() {
    this.setState({
      endMessages: false,
      endTickets: false,
      scrollEnd: true,
    });
  }

  scrollBottom = () => {
    const body = document.querySelector(".chat .scrollbar-container");
    const boxes = document.querySelector(".wrap_boxes");
    if (body) {
      body.scrollTo(0, boxes.scrollHeight);
    }
  };
  showArrowButton = () => {
    if (!this.state.showArrow) {
      this.setState({
        showArrow: true,
      });
    }
  };
  hideArrowButton = () => {
    if (this.state.showArrow) {
      this.setState({
        showArrow: false,
      });
    }
  };
  statusLoading = () => {
    this.setState({
      loading: !this.state.loading,
    });
  };
  falseLoading = () => {
    this.setState({
      loading: false,
    });
  };
  handlePlayPause = () => {
    this.setState({ playing: !this.state.playing });
  };
  openTicketModal = (data) => {
    this.setState({
      openModal: true,
      ticketFinish: data,
    });
  };

  closeTicketModal = () => {
    this.setState({
      openModal: false,
      loading: false,
    });
  };

  successModal = () => {
    this.setState({
      successModal: true,
    });
  };

  endModal = () => {
    this.setState({
      successModal: false,
    });
  };

  render() {
    const {
      conversation,
      loadingMessages,
      activeConversationId,
      getConversations,
      reloadData,
    } = this.props;
    const {
      playing,
      loading,
      showArrow,
      openModal,
      successModal,
      ticketFinish,
    } = this.state;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;
    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );

    this.onScrollEnd = () => {
      if (activeConversation && activeTicket && this.state.scrollEnd) {
        const { conversation, activeConversationId } = this.props;
        const activeConversation = conversation.data.find(
          (c) => c.id === activeConversationId
        );
        if (
          activeConversation.messages.filter((c) => c.source === "message")
            .length > 0
        ) {
          this.setState({
            loading: true,
            scrollEnd: false,
          });
          this.setState({ loading: true });
          this.props
            .fetchMessagesTake(
              activeConversation.id,
              50,
              activeConversation.messages.filter((c) => c.source === "message")
                .length
            )
            .then((res) => {
              this.setState({
                comment: "",
                loading: false,
                endMessages: res.messages.data.data.length > 0,
              });
            });
        }
      }
    };
    return (
      <div className="holder_mobile chatmobile open_chat" name="chat">
        <div className="corpo-chat ativo" name="sideinfo">
          {openModal && (
            <ModalTicketFinish
              open={openModal}
              onChange={this.handleChange}
              handleDepartmentChange={this.handleDepartmentChange}
              handleCategoryChange={this.handleCategoryChange}
              activeTicket={activeTicketId}
              handleClose={this.closeTicketModal}
              successModal={this.successModal}
              ticketFinish={ticketFinish}
              loading={loading}
            />
          )}
          {activeTicket &&
          activeTicket.current_progress !== "100.00" &&
          !loading ? (
            <ServiceStatus
              getConversations={getConversations}
              activeTicket={activeTicket}
              statusLoading={this.statusLoading}
              falseLoading={this.falseLoading}
              reloadData={reloadData}
              openTicketModal={this.openTicketModal}
              successModal={successModal}
            >
              {activeTicketId && !loading ? (
                <div className="ticket-ativo">
                  {activeTicket ? (
                    <div className="holder_choices">
                      {activeTicket.current_progress === "100.00" ? (
                        <p>
                          Ticket Finalizado:{" "}
                          <span>#{activeTicket.ticket_number}</span>
                          {activeTicket.user ? (
                            <Popup
                              content={activeTicket.user}
                              position="top left"
                              size="mini"
                              trigger={
                                <img
                                  src={activeTicket.user_avatar}
                                  className="user_avatar"
                                  alt="Avatar do Usuário"
                                />
                              }
                            />
                          ) : (
                            ""
                          )}
                        </p>
                      ) : (
                        <>
                          Ticket Ativo:{" "}
                          <span>#{activeTicket.ticket_number}</span>
                          {activeTicket ? (
                            <span className="holder_avatar">
                              <div className="progresso_holder">
                                <Progress
                                  className="progress_body"
                                  percent={activeTicket.current_progress}
                                  title={`Progresso ${
                                    activeTicket.current_progress
                                  }%`}
                                  indicating
                                  size="tiny"
                                />
                                <span className="progresso_desc">
                                  O progresso desse ticket é de{" "}
                                  <b>{activeTicket.current_progress}%</b>
                                </span>
                              </div>
                              {activeTicket.user ? (
                                <Popup
                                  content={activeTicket.user}
                                  position="top left"
                                  size="mini"
                                  trigger={
                                    <img
                                      src={activeTicket.user_avatar}
                                      className="user_avatar"
                                      alt="Avatar do Usuário"
                                    />
                                  }
                                />
                              ) : (
                                <Popup
                                  content="Atribuir ticket para si mesmo"
                                  position="top left"
                                  size="mini"
                                  trigger={
                                    <Button
                                      icon="magic"
                                      className="user_avatar"
                                      circular
                                      onClick={this.sendForward}
                                    />
                                  }
                                />
                              )}
                            </span>
                          ) : (
                            ""
                          )}
                        </>
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              ) : (
                ""
              )}
            </ServiceStatus>
          ) : (
            ""
          )}{" "}
          <div className="chat" id="conversation">
            <PerfectScrollbar
              id="container"
              onYReachStart={() => this.onScrollEnd()}
              onYReachEnd={() => this.hideArrowButton()}
              onScrollUp={() => this.showArrowButton()}
            >
              {loadingMessages ? (
                <div className="loading-conversa">
                  <LoaderComponent />
                </div>
              ) : (
                <div className="wrap_boxes">
                  {loading && (
                    <div className="loading-message body">
                      <LoaderComponent />
                    </div>
                  )}{" "}
                  {activeConversation.messages
                    .sort((a, b) => a.timestamp - b.timestamp)
                    .map((message) => {
                      const identifier = message.id
                        ? message.id
                        : message.message_token;
                      return message.source === "ticket" ||
                        message.source === "campaing" ||
                        message.source === "phone" ||
                        message.source === "monitchat" ||
                        message.source === "whatsapp" ||
                        message.source === "email" ? (
                        <DataSticky
                          key={`ticket-stick-${identifier}`}
                          ticket={message}
                          contact={activeConversation.contact}
                        />
                      ) : message.source === "message" ? (
                        message.sender === 0 ? (
                          <ReceivedMessage
                            key={`received-message-${identifier}`}
                            playing={playing}
                            handlePlayPause={this.handlePlayPause}
                            message={message}
                            contact={activeConversation.contact}
                          />
                        ) : (
                          <SentMessage
                            key={`sent-message-${identifier}`}
                            playing={playing}
                            handlePlayPause={this.handlePlayPause}
                            message={message}
                            resendMessage={this.props.resendMessage}
                          />
                        )
                      ) : message.source === "user_comment" ||
                        message.source === "comment" ? (
                        <Comment
                          comment={message}
                          key={`ticket-comment-${identifier}`}
                        />
                      ) : null;
                    })}
                </div>
              )}
            </PerfectScrollbar>
            {showArrow ? (
              <Button
                circular
                icon="arrow alternate circle down outline"
                className="scroll_to_bottom"
                onClick={this.scrollBottom}
              />
            ) : (
              ""
            )}
          </div>
          <div className="form-holder">
            <FormMesage
              ticket={this.state.ticket.data}
              comments={activeConversation.messages.sort(
                (a, b) => a.timestamp - b.timestamp
              )}
            />
          </div>
        </div>
      </div>
    );
  }
}
ConversationBody.propTypes = {
  loadingTickets: PropTypes.bool.isRequired,
  loadingMessages: PropTypes.bool.isRequired,
  conversation: PropTypes.shape().isRequired,
  activeConversationId: PropTypes.number.isRequired,
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);
const mapStateToProps = (state) => ({
  conversation: state.conversation,
  user: state.user.user,
  loadingTickets: state.conversation.loadingTickets,
  loadingMessages: state.conversation.loadingMessages,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConversationBody);
