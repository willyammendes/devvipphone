import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormProfessionals = ({ professional, onChange }) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Nome:"
                    name="name"
                    onChange={onChange}
                    value={professional.name}
                />
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Código:"
                    name="menu"
                    onChange={onChange}
                    value={professional.menu}
                />
            </Form.Group>
        </Form>
    );
};

export default FormProfessionals;
