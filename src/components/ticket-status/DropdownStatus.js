import React, { Component } from "react";
import api from "../../api/ticket-status";
import { Dropdown } from "semantic-ui-react";
import AddStatusModal from "./modal/AddStatusModal";

class DropdownStatus extends Component {
	state = {
		ticketStatus: {
			progress_percentage: "",
			description: "",
			default_message: ""
		},
		options: [],
		loading: false,
		search: "",
		openModal: false
	};

	handleStatusAddition = e => {};

	onSearchChange = (e, { searchQuery }) => {
		clearTimeout(this.timer);
		this.setState({ search: searchQuery });
		this.timer = setTimeout(this.onSearchStatus, 300);
	};
	componentWillMount() {
		api.ticketStatus.fetchAll().then(ticketStatus => {
			this.setState({
				options: ticketStatus.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.description
				})),
				loading: false
			});
		});
	}

	componentDidMount() {}

	onSearchStatus = async () => {
		this.setState({ loading: true });
		const { search } = this.state;

		await api.ticketStatus.fetchAll({ search }).then(ticketStatus => {
			this.setState({
				options: ticketStatus.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.description
				})),
				loading: false
			});
		});
	};

	onChange = (e, { name, value }) => {
		this.setState(state => ({
			ticketStatus: { ...state.ticketStatus, [name]: value }
		}));
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { ticketStatus } = this.state;

		this.setState({
			loading: true
		});

		return api.ticketStatus
			.submit(ticketStatus)
			.then(data => {
				this.setState(state => ({
					options: [...state.options].concat({
						key: data.data.id,
						value: data.data.id,
						text: data.data.description
					}),
					loading: false,
					ticketStatus: {
						progress_percentage: "",
						description: "",
						default_message: ""
					},
					openModal: !state.openModal
				}));

				this.props.onSelectStatus(data.data.id);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
				
			});
	};

	toggle = (e, { name, value }) => {
		this.setState(state => ({
			openModal: !state.openModal,
			ticketStatus: {
				description: value,
				progress_percentage: "",
				default_message: ""
			}
		}));
	};

	render() {
		const { options, loading, openModal, ticketStatus } = this.state;

		return (
			<div>
				<Dropdown
					placeholder="Pesquise um Status"
					fluid
					search
					selection
					scrolling
					allowAdditions
					additionLabel="Adicionar: "
					name="status"
					value={this.props.status}
					onSearchChange={this.onSearchChange}
					onChange={(e, { value }) =>
						this.props.onSelectStatus(value)
					}
					options={options}
					onAddItem={this.toggle}
					closeOnChange
					loading={loading}
					clearable
				/>
				<AddStatusModal
					open={openModal}
					ticketStatus={ticketStatus}
					save={this.submit}
					toggle={this.toggle}
					onChange={this.onChange}
				/>
			</div>
		);
	}
}

export default DropdownStatus;
