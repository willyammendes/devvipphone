import React from "react";
import { Modal, Button, Form, Input, TextArea } from "semantic-ui-react";

const AddStatusModal = ({
	open,
	toggle,
	onChange,
	ticketStatus,
	loading,
	save
}) => (
	<Modal
		size="small"
		closeIcon
		open={open}
		onClose={toggle}
		dimmer="blurring"
		closeOnDimmerClick={false}
	>
		<Modal.Header>Cadastro de Departamento</Modal.Header>
		<Modal.Content>
			<Modal.Description>
				<Form>
					<Form.Group widths="equal">
						<Form.Field
							control={Input}
							label="Descrição"
							name="description"
							onChange={onChange}
							value={ticketStatus.description}
						/>
						<Form.Field
							control={Input}
							label="Progresso de Ticket"
							name="progress_percentage"
							onChange={onChange}
							value={ticketStatus.progress_percentage}
						/>
					</Form.Group>
					<Form.Group widths="equal">
						<Form.Field
							control={TextArea}
							label="Mensagem Padrão"
							name="default_message"
							onChange={onChange}
							value={ticketStatus.default_message}
						/>
					</Form.Group>
				</Form>
			</Modal.Description>
		</Modal.Content>
		<Modal.Actions>
			<Button.Group>
				<Button onClick={toggle}>Cancelar</Button>
				<Button.Or />
				<Button
					positive
					onClick={save}
					loading={loading}
					disabled={loading}
				>
					Salvar
				</Button>
			</Button.Group>
		</Modal.Actions>
	</Modal>
);

export default AddStatusModal;
