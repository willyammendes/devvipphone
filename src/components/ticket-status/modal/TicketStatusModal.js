import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormTicketStatus from "../forms/FormTicketStatus";

class TicketStatusModal extends React.Component {
    componentDidMount() {
        document.removeEventListener("keydown", () => {});
        document.addEventListener("keydown", e => {
            if (e.keyCode === 39) this.props.handleNext();
            if (e.keyCode === 37) this.props.handlePrevious();
        });
    }

    save = () => {
        if (this.props.ticketStatus.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    render() {
        const {
            ticketStatus,
            handleClose,
            onChange,
            onChecked,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            onSelectIcon,
            loading,
            icon_active
        } = this.props;

        return (
            <Modal
                size="large"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <div className="holder_ticketStatus">
                            <div className="form-ticketStatus">
                                <FormTicketStatus
                                    ticketStatus={ticketStatus}
                                    onChange={onChange}
                                    onChecked={onChecked}
                                    onSelectIcon={onSelectIcon}
                                    icon_active={icon_active}
                                />
                            </div>
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button
                            icon
                            onClick={handlePrevious}
                            disabled={previousButtonEnabled}
                        >
                            <Icon name="left arrow" />
                        </Button>
                        <Button
                            icon
                            onClick={handleNext}
                            disabled={nextButtonEnabled}
                        >
                            <Icon name="right arrow" />
                        </Button>
                    </Button.Group>{" "}
                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default TicketStatusModal;
