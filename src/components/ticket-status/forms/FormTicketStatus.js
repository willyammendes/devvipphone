import React from "react";
import {
    Form,
    Input,
    Checkbox,
    TextArea,
    Dropdown,
    Button
} from "semantic-ui-react";

const options = [
    {
        key: "shield alternate",
        text: "shield alternate",
        value: "shield alternate"
    },
    {
        key: "car",
        text: "car",
        value: "car"
    },
    {
        key: "graduation cap",
        text: "graduation cap",
        value: "graduation cap"
    },
    {
        key: "life ring",
        text: "life ring",
        value: "life ring"
    },
    {
        key: "money bill alternate",
        text: "money bill alternate",
        value: "money bill alternate"
    },
    {
        key: "share alternate",
        text: "share alternate",
        value: "share alternate"
    },
    {
        key: "suitcase",
        text: "suitcase",
        value: "suitcase"
    },
    {
        key: "coffee",
        text: "coffee",
        value: "coffee"
    },
    {
        key: "chat",
        text: "chat",
        value: "chat"
    },
    {
        key: "food",
        text: "food",
        value: "food"
    },
    {
        key: "wrench",
        text: "wrench",
        value: "wrench"
    },
    {
        key: "shopping cart",
        text: "shopping cart",
        value: "shopping cart"
    },
    {
        key: "sitemap",
        text: "sitemap",
        value: "sitemap"
    },
    {
        key: "truck",
        text: "truck",
        value: "truck"
    },
    {
        key: "camera retro",
        text: "camera retro",
        value: "camera retro"
    },
    {
        key: "credit card",
        text: "credit card",
        value: "credit card"
    },
    {
        key: "wrench",
        text: "wrench",
        value: "wrench"
    },
    {
        key: "book",
        text: "book",
        value: "book"
    },
    {
        key: "calendar alternate",
        text: "calendar alternate",
        value: "calendar alternate"
    },
    {
        key: "flask",
        text: "flask",
        value: "flask"
    },
    {
        key: "music",
        text: "music",
        value: "music"
    },
    {
        key: "server",
        text: "server",
        value: "server"
    },
    {
        key: "ambulance",
        text: "ambulance",
        value: "ambulance"
    },
    {
        key: "video",
        text: "video",
        value: "video"
    },

    {
        key: "moon outline",
        text: "moon outline",
        value: "moon outline"
    },
    {
        key: "trophy",
        text: "trophy",
        value: "trophy"
    },
    {
        key: "road",
        text: "road",
        value: "road"
    },
    {
        key: "computer",
        text: "computer",
        value: "computer"
    },
    {
        key: "plane",
        text: "plane",
        value: "plane"
    },
    {
        key: "globe",
        text: "globe",
        value: "globe"
    },
    {
        key: "leaf",
        text: "leaf",
        value: "leaf"
    },
    {
        key: "bug",
        text: "bug",
        value: "bug"
    },
    {
        key: "box",
        text: "box",
        value: "box"
    },
    {
        key: "gift",
        text: "gift",
        value: "gift"
    },
    {
        key: "space shuttle",
        text: "space shuttle",
        value: "space shuttle"
    },

    {
        key: "assistive listening systems",
        text: "assistive listening systems",
        value: "assistive listening systems"
    },
    {
        key: "history",
        text: "history",
        value: "history"
    },
    {
        key: "reply all",
        text: "reply all",
        value: "reply all"
    },
    {
        key: "address card outline",
        text: "address card outline",
        value: "address card outline"
    },
    {
        key: "laptop",
        text: "laptop",
        value: "laptop"
    },
    {
        key: "handshake outline",
        text: "handshake outline",
        value: "handshake outline"
    },
    {
        key: "thumbs up",
        text: "thumbs up",
        value: "thumbs up"
    },
    {
        key: "thumbs up outline",
        text: "thumbs up outline",
        value: "thumbs up outline"
    },
    {
        key: "handshake",
        text: "handshake",
        value: "handshake"
    }
];

const FormTicketStatus = ({
    ticketStatus,
    onChange,
    onChecked,
    handleGroupAddition,
    onSelectIcon,
    icon_active
}) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Descrição"
                    name="description"
                    onChange={onChange}
                    value={ticketStatus.description}
                />
                {ticketStatus.is_system_status === 1 ? (
                    ""
                ) : (
                    <Form.Field
                        autoComplete="off"
                        control={Input}
                        label="Progresso do Ticket"
                        name="progress_percentage"
                        onChange={onChange}
                        value={ticketStatus.progress_percentage}
                        placeholder="0"
                    />
                )}
            </Form.Group>
            <Form.Field
                control={TextArea}
                label="Mensagem Padrão"
                name="default_message"
                onChange={onChange}
                value={ticketStatus.default_message}
                placeholder=""
            />
            <div className="status_select">
                <nav>
                    <ul>
                        {options
                            .filter(s => !!s.value)
                            .map((d, i) => (
                                <li key={`options-${i * 1}`}>
                                    <Button
                                        icon={d.value}
                                        value={d.value}
                                        name="fast_menu_icon"
                                        onClick={onSelectIcon}
                                        className={
                                            icon_active === d.value
                                                ? "ativo"
                                                : "desativado"
                                        }
                                    />
                                </li>
                            ))}
                    </ul>
                </nav>
            </div>

            <Form.Field>
                <label htmlFor="fast_menu_icon">Icone*</label>
                <Dropdown
                    placeholder="Icone?"
                    fluid
                    selection
                    name="fast_menu_icon"
                    value={ticketStatus.fast_menu_icon}
                    options={options}
                    onChange={onSelectIcon}
                />
            </Form.Field>
            {/* Origem */}
            <Form.Field>
                <Checkbox
                    label="Disponível no Menu rápido"
                    name="fast_menu"
                    onChange={onChecked}
                    checked={ticketStatus.fast_menu}
                />
            </Form.Field>
            <Form.Field>
                <Checkbox
                    label="Status Ativo?"
                    name="active"
                    onChange={onChecked}
                    checked={ticketStatus.active}
                />
            </Form.Field>
        </Form>
    );
};

export default FormTicketStatus;
