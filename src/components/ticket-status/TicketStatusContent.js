import React from "react";
import Popover from "../semantic/Popover";
import { Table } from "semantic-ui-react";

const TicketStatusContent = ({ status }) => (
  <Table.Body>
    {status.map((status, i) => (
      <Table.Row key={`status-menu-${i * 1}`}>
        <Table.Cell className="dataEditar" key={status.id}>
          <span>{status.id}</span>
        </Table.Cell>
        <Table.Cell className="dataEditar" key={status.id}>
          <span>{status.description}</span>
          <Popover />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={status.id}>
          <span>{status.message}</span>
          <Popover />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={status.id}>
          <span>{status.progress}</span>
          <Popover />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={status.id}>
          <span>{status.menu}</span>
          <Popover />
        </Table.Cell>
      </Table.Row>
    ))}
  </Table.Body>
);

export default TicketStatusContent;
