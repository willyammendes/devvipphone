import React, { Component } from "react";
import Icon from "@mdi/react";
import { mdiWhatsapp, mdiAccountMultiplePlusOutline } from "@mdi/js";

class SourceTicket extends Component {
  render() {
    return (
      <div className="origens">
        <span className="origem">
          <Icon
            path={mdiWhatsapp}
            size={0.8}
            horizontal
            vertical
            rotate={180}
          />
        </span>
        <span className="origem">
          <Icon
            path={mdiAccountMultiplePlusOutline}
            size={0.8}
            horizontal
            vertical
            rotate={180}
          />
        </span>
      </div>
    );
  }
}

export default SourceTicket;
