import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Card,
  Form,
  Divider,
  Grid,
  Label,
  Icon,
  Input,
  Button,
} from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/users";
import Socket from "../../services/socket";
import { CardBody } from "reactstrap";

class UserMonitor extends Component {
  state = {
    loading: false,
    records: [],
    filtered: [],
  };

  fetchRecords = (params) => {
    this.setState({ loading: true });

    api.user.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords({ take: 500 });

    Socket.private(`user-pause-changed-${this.props.company_id}`).listen(
      "UserPauseChanged",
      this.setAccountStatus
    );
  }

  setAccountStatus = (e) => {
    const { user_id, currentPause } = e;
    const { records, filtered } = this.state;
    const data = records.find((a) => a.id === user_id);
    const dataIndex = records.findIndex((a) => a.id === user_id);

    const filteredIndex = filtered.findIndex((a) => a.id === user_id);

    if (data) {
      this.setState({
        records: [
          ...records.slice(0, dataIndex),
          {
            ...records[dataIndex],
            currentPause,
          },
          ...records.slice(dataIndex + 1),
        ],
        filtered:
          filtered.length > 0
            ? [
                ...filtered.slice(0, filteredIndex),
                {
                  ...filtered[filteredIndex],
                  currentPause,
                },
                ...filtered.slice(filteredIndex + 1),
              ]
            : [],
      });
    }
  };

  handleChange = (e, { name, value }) => {
    if (value != "") {
      this.setState({
        filtered: this.state.records.filter((company) => {
          const regex = new RegExp(`${value}`, "gi");

          return company.name.match(regex);
        }),
      });
    } else {
      this.setState({
        filtered: [],
      });
    }
  };

  render() {
    const { loading, records, filtered } = this.state;
    const rows = filtered.length > 0 ? filtered : records;
    const styleBlock = {
      textAlign: "center",
      width: "100%",
      height: "100%",
    };

    return (
      <div className=" pageHolder">
        <div className="headingPage">
          <h1>Monitor de Usuários</h1>
        </div>
        {loading && (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        )}

        <div className="holderPage">
          <Divider />
          <Grid columns={6}>
            <Grid.Row>
              <Grid.Column width="6">
                <Input
                  icon="search"
                  placeholder="Pesquisar..."
                  size="mini"
                  style={{ width: "80%" }}
                  onChange={this.handleChange}
                />{" "}
                <Button
                  size="mini"
                  icon="refresh"
                  onClick={() => this.fetchRecords({ take: 500 })}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {rows.map((user) => {
                return (
                  <Grid.Column key={`user-${user.id}`}>
                    <Card
                      fluid
                      title={
                        user.currentPause
                          ? user.currentPause.user_logged_out
                            ? "Usuário não está autenticado no sistema"
                            : user.currentPause.interruptionType.name
                          : !user.active
                          ? "Usuário Inativo"
                          : ""
                      }
                      style={{
                        marginBottom: "20px",
                        backgroundColor: user.active
                          ? user.currentPause
                            ? user.currentPause.interruptionType
                              ? "rgb(244, 245, 180)"
                              : "rgb(255, 219, 221)"
                            : "#e0ffe1"
                          : "#e8e8e8",
                      }}
                    >
                      <Card.Content>
                        {user.currentPause || !user.active ? (
                          <Icon
                            style={{ float: "right" }}
                            title={
                              !user.active
                                ? "Usuário Inativo"
                                : user.currentPause.user_logged_out
                                ? "Usuário não está autenticado no sistema"
                                : user.currentPause.interruptionType.name
                            }
                            name={
                              !user.active
                                ? "user outline"
                                : user.currentPause.user_logged_out
                                ? "lock"
                                : user.currentPause.interruptionType
                                    .fast_menu_icon
                            }
                          />
                        ) : null}
                        <Card.Header
                          style={{
                            color: !user.active
                              ? "rgb(144, 138, 138)"
                              : user.currentPause
                              ? "rgb(189, 37, 37)"
                              : "rgb(19, 103, 33)",
                          }}
                        >
                          {user.name}
                        </Card.Header>
                      </Card.Content>

                      <CardBody
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div
                          title="Atendidos"
                          style={{
                            ...styleBlock,
                            backgroundColor: "rgb(176, 218, 239)",
                          }}
                        >
                          {Math.floor(Math.random() * 100)}
                        </div>
                        <div
                          title="Aguardando"
                          style={{
                            ...styleBlock,
                            backgroundColor: "rgb(255, 182, 138)",
                          }}
                        >
                          {Math.floor(Math.random() * 100)}
                        </div>
                        <div
                          title="Finalizados"
                          style={{
                            ...styleBlock,
                            backgroundColor: "rgb(95, 183, 95)",
                          }}
                        >
                          {Math.floor(Math.random() * 100)}
                        </div>

                        {/* <Form>
                          <Form.Field>
                            <Grid columns={3}>
                              <Grid.Row>
                                {company.social_accounts.map((account) => {
                                  return (
                                    <Grid.Column
                                      style={{ marginBottom: "2px" }}
                                      key={`account-${account.id}`}
                                    >
                                      <Label
                                        as="a"
                                        onClick={
                                          account.server
                                            ? () =>
                                                window.open(
                                                  `http://${
                                                    account.server.host_name
                                                  }:${
                                                    account.server.port
                                                  }/wd/hub`,
                                                  "target=blank"
                                                )
                                            : () => {}
                                        }
                                        size="tiny"
                                        title={account.session_id}
                                        color={
                                          account.connected === 1
                                            ? "green"
                                            : account.connected === 0
                                            ? "orange"
                                            : "red"
                                        }
                                      >
                                        {account.phone_number}
                                      </Label>
                                    </Grid.Column>
                                  );
                                })}
                              </Grid.Row>
                            </Grid>
                          </Form.Field>
                          <Form.Field />
                        </Form> */}
                      </CardBody>
                    </Card>
                  </Grid.Column>
                );
              })}
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user: { user } }) => {
  return {
    company_id: user.company_id,
  };
};

export default connect(mapStateToProps)(UserMonitor);
