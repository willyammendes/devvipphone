import React, { Component } from "react";
import {
  Card,
  Form,
  Divider,
  Grid,
  Label,
  Icon,
  Input,
  Button,
} from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/social-media";
import Socket from "../../services/socket";

class WhatsappMonitor extends Component {
  state = {
    loading: false,
    records: [],
    filtered: [],
  };

  fetchRecords = (params) => {
    this.setState({ loading: true });

    api.media.monitor(params).then((res) => {
      res.data
        .filter((company) => company.social_accounts.length > 0)
        .map((company) => {
          Socket.private(`account-status-changed-${company.id}`).listen(
            "WhatsAppAccountStatusChanged",
            this.setAccountStatus
          );
        });

      this.setState({
        records: res.data.filter(
          (company) => company.social_accounts.length > 0
        ),
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  setAccountStatus = (e) => {
    const { account } = e;
    const { records, filtered } = this.state;
    const data = records.find((a) => a.id === account.company_id);
    const dataIndex = records.findIndex((a) => a.id === account.company_id);
    const filteredIndex = filtered.findIndex(
      (a) => a.id === account.company_id
    );

    const accountIndex = data.social_accounts.findIndex(
      (a) => a.id === account.id
    );

    if (data) {
      this.setState({
        records: [
          ...records.slice(0, dataIndex),
          {
            ...records[dataIndex],
            social_accounts: [
              ...records[dataIndex].social_accounts.slice(0, accountIndex),
              {
                ...records[dataIndex].social_accounts[accountIndex],
                connected: account.connected,
              },
              ...records[dataIndex].social_accounts.slice(accountIndex + 1),
            ],
          },
          ...records.slice(dataIndex + 1),
        ],
        filtered:
          filtered.length > 0
            ? [
                ...filtered.slice(0, filteredIndex),
                {
                  ...filtered[filteredIndex],
                  social_accounts: [
                    ...filtered[filteredIndex].social_accounts.slice(
                      0,
                      accountIndex
                    ),
                    {
                      ...filtered[filteredIndex].social_accounts[accountIndex],
                      connected: account.connected,
                    },
                    ...filtered[filteredIndex].social_accounts.slice(
                      accountIndex + 1
                    ),
                  ],
                },
                ...filtered.slice(filteredIndex + 1),
              ]
            : [],
      });
    }
  };

  handleChange = (e, { name, value }) => {
    if (value != "") {
      this.setState({
        filtered: this.state.records.filter((company) => {
          const regex = new RegExp(`${value}`, "gi");

          return company.name.match(regex);
        }),
      });
    } else {
      this.setState({
        filtered: [],
      });
    }
  };

  render() {
    const { loading, records, filtered } = this.state;

    const rows = filtered.length > 0 ? filtered : records;

    return (
      <div className=" pageHolder">
        <div className="headingPage">
          <h1>Monitor Whatsapp</h1>
        </div>
        {loading && (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        )}

        <div className="holderPage">
          <Divider />
          <Grid columns={4}>
            <Grid.Row>
              <Grid.Column width="6">
                <Input
                  icon="search"
                  placeholder="Pesquisar..."
                  size="mini"
                  style={{ width: "80%" }}
                  onChange={this.handleChange}
                />{" "}
                <Button
                  size="mini"
                  icon="refresh"
                  onClick={() => this.fetchRecords()}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {rows.map((company) => {
                return (
                  <Grid.Column key={`company-${company.id}`}>
                    <Card fluid style={{ marginBottom: "10px" }}>
                      <Card.Content>
                        <Card.Header>{company.name}</Card.Header>
                      </Card.Content>
                      <Card.Content>
                        <Form>
                          <Form.Field>
                            <Grid columns={3}>
                              <Grid.Row>
                                {company.social_accounts.map((account) => {
                                  return (
                                    <Grid.Column
                                      style={{ marginBottom: "2px" }}
                                      key={`account-${account.id}`}
                                    >
                                      <Label
                                        as="a"
                                        onClick={
                                          account.server
                                            ? () =>
                                                window.open(
                                                  `http://${
                                                    account.server.host_name
                                                  }:${
                                                    account.server.port
                                                  }/wd/hub`,
                                                  "target=blank"
                                                )
                                            : () => {}
                                        }
                                        size="tiny"
                                        title={account.session_id}
                                        color={
                                          account.connected === 1
                                            ? "green"
                                            : account.connected === 0
                                            ? "orange"
                                            : "red"
                                        }
                                      >
                                        {account.phone_number}
                                      </Label>
                                    </Grid.Column>
                                  );
                                })}
                              </Grid.Row>
                            </Grid>
                          </Form.Field>
                          <Form.Field />
                        </Form>
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                );
              })}
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default WhatsappMonitor;
