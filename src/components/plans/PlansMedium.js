import React, { Component } from "react";
import { Card, Icon, Form, Dropdown, Button } from "semantic-ui-react";
import api from "../../api/plans";

const planOptions = [
  {
    key: "1-user",
    text: "1 Usuário",
    value: 389.0,
  },
  {
    key: "5-user",
    text: "5 Usuários",
    value: 429.0,
  },
  {
    key: "15-user",
    text: "15 Usuários",
    value: 529.0,
  },
  {
    key: "25-user",
    text: "25 Usuários",
    value: 629.0,
  },
  {
    key: "35-user",
    text: "35 Usuários",
    value: 729.0,
  },
  {
    key: "50-user",
    text: "50 Usuários",
    value: 879.0,
  },
];
const monitOptions = [
  {
    key: "with-monit",
    text: "Com Monitbot",
    value: 149,
  },
  {
    key: "without-monit",
    text: "Sem Monitbot",
    value: 0,
  },
];
class PlansMedium extends Component {
  state = {
    value: 429.0,
    monitbot: 149,
    plan: [],
    loading: false,
    hasMore: true,
  };
  handleChange = (e, { value }) => this.setState({ value });
  handleMonitbot = (e, { value }) => {
    this.setState({ monitbot: value });
  };

  componentWillMount() {
    this.setState({ loading: true });
    const filter = { filter: JSON.stringify([["active", "=", 1]]) };
    api.plan.fetchAll({ filter }).then((plan) => {
      this.setState({
        plan,
        loading: false,
      });
    });
  }

  render() {
    return (
      <Card fluid className="plano">
        <Card.Content className="headerCard">
          <Card.Header>Monitchat Medium</Card.Header>
        </Card.Content>
        <Card.Content className="infos">
          <div className="preco">
            <Form.Field>
              <h1>
                R$ {this.state.value + this.state.monitbot},00
                <span>/mes</span>
              </h1>
            </Form.Field>
          </div>
          <div>
            <div className="info_plano">2 Contas Whatsapp</div>
            <div className="info_plano info_drop">
              {" "}
              <Form>
                {" "}
                <Dropdown
                  placeholder="5 Usuários"
                  fluid
                  selection
                  onChange={this.handleChange}
                  options={planOptions}
                />{" "}
              </Form>
            </div>
            <div className="info_plano info_drop">
              <Form>
                <Dropdown
                  placeholder="Com Monitbot"
                  fluid
                  selection
                  onChange={this.handleMonitbot}
                  options={monitOptions}
                />
              </Form>
            </div>
            <div className="info_plano">
              {" "}
              <Icon name="check" /> <b>Campanhas Automatizadas</b>
            </div>
            <div className="info_plano">
              {" "}
              <Button color="green" content="Assinar" />
            </div>
          </div>
        </Card.Content>
      </Card>
    );
  }
}

export default PlansMedium;
