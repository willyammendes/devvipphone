import React, { Component } from "react";
import { Icon, Button } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/plans";
import PlansSmall from "./PlansSmall";
import PlansMedium from "./PlansMedium";
import PlansLarge from "./PlansLarge";

class Plans extends Component {
  state = {
    value: "199,00",
    plan: [],
    loading: false,
    hasMore: true,
  };
  handleChange = (e, { value }) => this.setState({ value });

  componentWillMount() {
    const filter = { filter: JSON.stringify([["active", "=", 1]]) };

    this.setState({ loading: true });
    api.plan.fetchAll({ filter }).then((plan) => {
      this.setState({
        plan,
        loading: false,
      });
    });
  }

  render() {
    const { loading } = this.state;
    return (
      <div className="pageHolder">
        <div className="headingPage">
          <h1>Planos de Assinatura</h1>
        </div>
        {loading && (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        )}
        <div className="holderPage">
          <div className="options-list">
            <Button icon color="blue">
              <Icon name="backward" />
            </Button>
            <Button icon color="blue">
              <Icon name="refresh" />
            </Button>
          </div>
          <div className="full">
            <div className="quarto terco">
              <PlansSmall />
            </div>
            <div className="quarto terco">
              <PlansMedium />
            </div>
            <div className="quarto terco">
              <PlansLarge />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Plans;
