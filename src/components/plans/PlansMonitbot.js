import React, { Component } from "react";
import { Card, Icon, Form, Dropdown, Button } from "semantic-ui-react";
import api from "../../api/plans";

const planOptions = [
  {
    key: "0-user",
    text: "0 Usuários",
    value: "149.90",
  },
];

class PlansMonitbot extends Component {
  state = {
    value: "149.90",
    plan: [],
    loading: false,
    hasMore: true,
  };
  handleChange = (e, { value }) => this.setState({ value });

  componentWillMount() {
    this.setState({ loading: true });
    const filter = { filter: JSON.stringify([["active", "=", 1]]) };
    api.plan.fetchAll({ filter }).then((plan) => {
      this.setState({
        plan,
        loading: false,
      });
    });
  }

  render() {
    return (
      <Card fluid className="plano">
        <Card.Content className="headerCard">
          <Card.Header>Monitbot</Card.Header>
        </Card.Content>
        <Card.Content className="infos">
          <div className="preco">
            <Form.Field>
              <h1>
                R$ {this.state.value}
                <span>/mes</span>
              </h1>
            </Form.Field>
          </div>
          <div>
            <div className="info_plano">0 Contas Whatsapp</div>
            <div className="info_plano info_drop">
              {" "}
              <Form>
                {" "}
                <Dropdown
                  placeholder="Número de usuários"
                  fluid
                  selection
                  onChange={this.handleChange}
                  options={planOptions}
                />{" "}
              </Form>
            </div>
            <div className="info_plano">
              {" "}
              <Icon name="check" /> <b>Campanhas Automatizadas</b>
            </div>
            <div className="info_plano">
              {" "}
              <Button color="green" content="Assinar" />
            </div>
          </div>
        </Card.Content>
      </Card>
    );
  }
}

export default PlansMonitbot;
