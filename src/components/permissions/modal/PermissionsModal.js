import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormPermissions from "../forms/FormPermissions";

class PermissionsModal extends React.Component {
    state = {
        permissionProfile: false
    };
    componentDidMount() {
        const { permission } = this.props;
        if (permission.id) {
            document.removeEventListener("keydown", () => {});
            document.addEventListener("keydown", e => {
                if (e.keyCode === 39) this.props.handleNext();
                if (e.keyCode === 37) this.props.handlePrevious();
            });
        }
    }

    save = () => {
        if (this.props.permission.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    ChangeTab = () => {
        this.setState({
            permissionProfile: !this.state.permissionProfile
        });
    };

    render() {
        const {
            permission,
            handleClose,
            onChange,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading,
            onChecked,
            handleGroupChange,
            handleDepartmentChange,
            handleMediaChange,
            messageError,
            generalError,
            cleanErrors,
            onSelectPermissions,
            handlePermission,
        } = this.props;

        return (
            <Modal
                size="large"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>

                <Modal.Content>
                    <Modal.Description>
                        {generalError && (
                            <div className="errors-table">
                                <Button
                                    circular
                                    basic
                                    color="black"
                                    icon="close"
                                    className="button-close"
                                    onClick={cleanErrors}
                                />
                                <p>{generalError}</p>
                            </div>
                        )}
                        <div className="form-permission">
                            <FormPermissions
                                permission={permission}
                                onSelectPermissions={onSelectPermissions}
                                permissionProfile={this.state.permissionProfile}
                                onChange={onChange}
                                onChecked={onChecked}
                                handleGroupChange={handleGroupChange}
                                handleDepartmentChange={handleDepartmentChange}
                                handleMediaChange={handleMediaChange}
                                messageError={messageError}
                                handlePermission={handlePermission}
                            />
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    {permission.id ? (
                        <Button.Group>
                            <Button
                                icon
                                onClick={handlePrevious}
                                disabled={previousButtonEnabled}
                            >
                                <Icon name="left arrow" />
                            </Button>
                            <Button
                                icon
                                onClick={handleNext}
                                disabled={nextButtonEnabled}
                            >
                                <Icon name="right arrow" />
                            </Button>
                        </Button.Group>
                    ) : (
                        ""
                    )}

                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default PermissionsModal;
