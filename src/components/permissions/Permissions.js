import React, { Component } from "react";
import api from "../../api/permissions";
import DataTable from "../table/DataTable";
import PermissionsModal from "./modal/PermissionsModal";
import AlertSuccess from "../alerts/AlertSuccess";
import "moment/locale/pt-br";

class Permissions extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    permission_id: "",
    groups: [],
    departments: [],
    permissions: [],
    accounts: [],
    permission: [],
  };

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    this.state.records[dataIndex].number = 1;
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              active: false,
              permissions: [],
              number: 1,
            }),
          {}
        )
      : {
          active: false,
          permissions: [],
          number: 1,
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });
    return await api.permission.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  fetchPermission = async (params) => {
    this.setState({ loading: true });
    return await api.permission.fetchPermission(params).then((res) => {
      var groupBy = function(xs, key) {
        return xs.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };
      const dataGroup = groupBy(res.data, "model");
      const datateste = Object.values(dataGroup);

      this.setState({
        permission: datateste,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
    this.fetchPermission();
  }

  handleChange = (e) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handlePermission = (value) => {
    const { selectedDataIndex, records } = this.state;
    var hasPermission = records[selectedDataIndex].permissions.find(
      (c) => c === value
    )
      ? true
      : false;
    if (hasPermission) {
      this.setState(
        {
          records: [
            ...records.slice(0, selectedDataIndex),
            {
              ...records[selectedDataIndex],
              number: records[selectedDataIndex].number + 1,
              permissions: records[selectedDataIndex].permissions.filter(
                (c) => c !== value
              ),
            },
            ...records.slice(selectedDataIndex + 1),
          ],
        },
        () => {
          setTimeout(function() {}.bind(this), 3000);
        }
      );
    } else {
      records[selectedDataIndex].permissions.push(value);
      this.setState({
        records: [
          ...records.slice(0, selectedDataIndex),
          {
            ...records[selectedDataIndex],
            number: records[selectedDataIndex].number + 1,
          },
          ...records.slice(selectedDataIndex + 1),
        ],
      });
    }
  };
  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleUserChange = (value, text) => {
    this.setState({
      permission_id: {
        ...this.state.permission_id,
        permission_id: value,
      },
    });
  };

  handleGroupChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleDepartmentChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          departments: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleMediaChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          accounts: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  onSelectPermissions = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          permissions: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });

    return api.permission
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    api.permission
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  delete = (id) => {
    const { permission_id } = this.state;
    const permission = { permission_id: permission_id.permission_id };
    api.permission.delete(id, permission);
  };

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
    } = this.state;

    const permission = records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Permissões</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records.map((r) => ({
                  ...r,
                  visible: r.visible ? "Sim" : "Não",
                  for_manager: r.for_manager ? "Sim" : "Não",
                }))}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                selectUserModal={false}
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
                permissionTo={"permission"}
              />

              {selectedDataIndex !== -1 ? (
                <PermissionsModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  onChecked={this.handleChecked}
                  onSelectPermissions={this.onSelectPermissions}
                  modalHeader={
                    permission.id
                      ? `Edição do ${permission.name}`
                      : "Nova Permissão"
                  }
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.errors
                      : ""
                  }
                  generalError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  permission={permission}
                  cleanErrors={this.cleanErrors}
                  handleGroupChange={this.handleGroupChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleMediaChange={this.handleMediaChange}
                  handlePermission={this.handlePermission}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Permissions;
