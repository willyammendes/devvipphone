import React from "react";
import { Form, Input, Checkbox } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";

const FormPermissions = ({ permission, onChange, onChecked, messageError }) => {
  return (
    <Form autoComplete="off">
      <div className="holder_form_permission">
        <Form.Field error={messageError ? !!messageError.name : ""}>
          <label htmlFor="name">Nome</label>
          <Input
            autoComplete="off"
            name="name"
            onChange={onChange}
            value={permission.name}
            placeholder="Nome"
          />

          {messageError ? <InlineError text={messageError.name} /> : ""}
        </Form.Field>

        <Form.Field error={messageError ? !!messageError.model : ""}>
          <label htmlFor="model">Entidade</label>
          <Input
            autoComplete="off"
            name="model"
            onChange={onChange}
            value={permission.model}
            placeholder="Entidade"
          />

          {messageError ? <InlineError text={messageError.model} /> : ""}
        </Form.Field>

        <Form.Field error={messageError ? !!messageError.label : ""}>
          <label htmlFor="label">Descrição</label>
          <Input
            autoComplete="off"
            name="label"
            onChange={onChange}
            value={permission.label}
            placeholder="Descrição"
          />

          {messageError ? <InlineError text={messageError.label} /> : ""}
        </Form.Field>
        <div className="field">
          <Checkbox
            toggle
            label="Acesso para Gerenciador"
            name="for_manager"
            value={permission.for_manager ? true : false}
            checked={permission.for_manager ? true : false}
            onChange={onChecked}
          />
        </div>
        <div className="field">
          <Checkbox
            toggle
            label="Visível"
            name="visible"
            value={permission.visible ? true : false}
            checked={permission.visible ? true : false}
            onChange={onChecked}
          />
        </div>
      </div>
    </Form>
  );
};

export default FormPermissions;
