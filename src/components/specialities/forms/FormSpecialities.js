import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormSpecialities = ({ speciality, onChange }) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Descrição:"
                name="description"
                onChange={onChange}
                value={speciality.description}
            />
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Código:"
                name="menu"
                onChange={onChange}
                value={speciality.menu}
            />
        </Form>
    );
};

export default FormSpecialities;
