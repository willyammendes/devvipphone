import React, { Component } from "react";
import { connect } from "react-redux";

import api from "../../api/users";
import apipause from "../../api/interruption-type";
import { Icon } from "semantic-ui-react";

import PerfectScrollbar from "react-perfect-scrollbar";
import Socket from "../../services/socket";
import PauseInfo from "./PauseInfo";
import WindowChat from "./WindowChat";

class InternalChat extends Component {
  state = {
    records: [],
    interruptions: [],
    loading: false,
    activeChat: false,
    showHeader: false,
    query: "",
    search: "",
    chats: [],
    message: "",
    activeWindow: "",
  };

  handleInputChange = () => {
    clearTimeout(this.timer);
    this.setState({
      query: this.search.value,
    });
    this.timer = setTimeout(this.onSearchUser, 300);
  };
  handleChange = (e, { name, value }) => {
    this.setState({
      message: value,
    });
  };
  onSearchUser = async () => {
    this.setState({
      loading: true,
      query: this.search.value,
    });
    this.setState({ search: this.state.query.toLowerCase() });
    const { search } = this.state;

    await api.user.fetchAll(search ? { search } : { take: 20 }).then((res) => {
      this.setState({
        records: res.data.filter((c) => c.name.toLowerCase().includes(search)),
      });
    });
  };
  cleanSearch = () => {
    this.search.value = "";
    clearTimeout(this.timer);
    this.setState({
      query: this.search.value,
    });
    this.timer = setTimeout(this.onSearchUser, 300);
  };

  fetchRecords = (params) => {
    this.setState({ loading: true });

    api.user.fetchAll(params).then((res) => {
      this.setState({
        records: res.data.filter((c) => c.active === 1),
        loading: false,
      });
    });
  };
  fetchInterruptions = async (params) => {
    this.setState({ loading: true });

    return await apipause.interruption.fetchAll(params).then((res) => {
      this.setState({
        interruptions: res.data,
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords({ take: 50 });
    this.fetchInterruptions({ take: 30 });

    Socket.connector.pusher.config.auth.headers.Authorization = `Bearer ${
      localStorage.token
    }`;

    Socket.private(`user-pause-changed-${this.props.company_id}`).listen(
      "UserPauseChanged",
      this.setUserStatus
    );

    Socket.private(`new-chat-message-${this.props.user_id}`).listen(
      "NewChatMessage",
      (e) => {
        // from: id do usuário que enviou a mensagem
        // to: id do usuário para quem a mensagem foi enviada

        const message = e.message;

        this.updateMessages(message);
      }
    );
  }

  setUserStatus = (e) => {
    const { records } = this.state;
    const index = records.findIndex((user) => user.id === e.user_id);

    this.setState({
      records: [
        ...records.slice(0, index),
        {
          ...records[index],
          currentPause: e.currentPause,
        },
        ...records.slice(index + 1),
      ],
    });
  };

  updateMessages = (message) => {
    const { chats } = this.state;
    const findConversation = this.state.chats.findIndex(
      (c) => c.id === message.from
    );
    const data = this.state.chats.find((c) => c.id === message.from);

    if (findConversation > -1) {
      const indexChat = chats[findConversation].chat.length;
      const dataMessage = {
        from: message.from,
        to: message.to,
        text: message.text,
      };
      this.setState(
        {
          chats: [
            ...chats.slice(0, findConversation),
            {
              ...data,
              chat: [
                ...chats[findConversation].chat.slice(0, indexChat),
                { ...dataMessage },
                ...chats[findConversation].chat.slice(indexChat + 1),
              ],
            },
            ...chats.slice(findConversation + 1),
          ],
        },
        () => {
          const body = document.querySelector(
            `#chat-window-${findConversation} .message-body .scrollbar-container`
          );
          const boxes = document.querySelector(
            `#chat-window-${findConversation} .messages`
          );
          body.scrollTo(0, boxes.scrollHeight);
        }
      );
    }
  };
  activeClick = () => {
    this.setState((state) => ({
      activeChat: !state.activeChat,
    }));
  };

  activeHeader = () => {
    this.setState((state) => ({
      showHeader: !state.showHeader,
    }));
  };
  loadingSwitch = () => {
    const { loading } = this.state;
    this.setState({
      loading: !loading,
    });
  };
  sendMessage = (data, index) => {
    const { user_id } = this.props;
    const { message, chats } = this.state;
    this.setState({
      loading: true,
    });

    const sendObject = {
      from: user_id,
      to: data.id,
      text: message,
    };
    if (message) {
      api.user.sendMessage(sendObject).then((res) => {
        const indexChat = chats[index].chat.length;
        const dataMessage = res.data;

        this.setState(
          {
            message: "",
            chats: [
              ...chats.slice(0, index),
              {
                ...data,
                chat: [
                  ...chats[index].chat.slice(0, indexChat),
                  { ...dataMessage },
                  ...chats[index].chat.slice(indexChat + 1),
                ],
              },
              ...chats.slice(index + 1),
            ],
          },
          () => {
            const body = document.querySelector(
              `#chat-window-${index} .message-body .scrollbar-container`
            );
            const boxes = document.querySelector(
              `#chat-window-${index} .messages`
            );
            body.scrollTo(0, boxes.scrollHeight);
          }
        );
      });
    }
  };

  enterMessage = (e, data, index) => {
    if (e.keyCode === 13 && e.shiftKey === false && this.state.message) {
      e.preventDefault();
      const { user_id } = this.props;
      const { message, chats } = this.state;
      this.setState({
        loading: true,
      });
      const sendObject = {
        from: user_id,
        to: data.id,
        text: message,
      };
      if (message) {
        api.user.sendMessage(sendObject).then((res) => {
          const indexChat = chats[index].chat.length;
          const dataMessage = res.data;

          this.setState(
            {
              message: "",
              chats: [
                ...chats.slice(0, index),
                {
                  ...data,
                  chat: [
                    ...chats[index].chat.slice(0, indexChat),
                    { ...dataMessage },
                    ...chats[index].chat.slice(indexChat + 1),
                  ],
                },
                ...chats.slice(index + 1),
              ],
            },
            () => {
              const body = document.querySelector(
                `#chat-window-${index} .message-body .scrollbar-container`
              );
              const boxes = document.querySelector(
                `#chat-window-${index} .messages`
              );
              body.scrollTo(0, boxes.scrollHeight);
            }
          );
        });
      }
    }
  };
  openChat = (data) => {
    const { chats, loading } = this.state;
    this.setState({
      loading: !loading,
    });
    const findChat = chats.findIndex((c) => c.id === data.id);
    if (findChat > -1) {
      this.setState({
        activeWindow: findChat,
      });
    } else {
      api.user.fetchChat(data.id).then((res) => {
        const chatsIndex = chats.length;
        const dataChat = res.data;
        const findChat = chats.find((c) => c.id === data.id);
        if (!findChat && chats.length < 3) {
          this.setState(
            {
              chats: [
                ...chats.slice(0, chatsIndex),
                { ...data, chat: dataChat },
                ...chats.slice(chatsIndex + 1),
              ],
              activeWindow: chatsIndex,
              message: "",
            },
            () => {
              const body = document.querySelector(
                `#chat-window-${chatsIndex} .message-body .scrollbar-container`
              );
              const boxes = document.querySelector(
                `#chat-window-${chatsIndex} .messages`
              );
              body.scrollTo(0, boxes.scrollHeight);
            }
          );
        }
        if (chats.length > 2) {
          const firstID = chats[0].id;
          const filterChats = chats.filter((c) => c.id !== firstID);
          const filterIndex = filterChats.length;
          this.setState(
            {
              chats: [
                ...filterChats.slice(0, filterIndex),
                { ...data, chat: dataChat },
                ...filterChats.slice(filterIndex + 1),
              ],
              activeWindow: filterIndex,
              message: "",
            },
            () => {
              const body = document.querySelector(
                `#chat-window-${filterIndex} .message-body .scrollbar-container`
              );
              const boxes = document.querySelector(
                `#chat-window-${filterIndex} .messages`
              );
              body.scrollTo(0, boxes.scrollHeight);
            }
          );
        }
      });
    }
  };
  activeWindow = (data) => {
    this.setState({
      activeWindow: data,
    });
  };
  closeWindow = (data, index) => {
    const { chats } = this.state;

    this.setState(
      {
        chats: chats.filter((c) => c.id !== data.id),
        message: "",
      },
      () => {}
    );
  };
  render() {
    const {
      records,
      loading,
      activeChat,
      showHeader,
      interruptions,
      chats,
      message,
      activeWindow,
    } = this.state;
    const { avatar } = this.props;

    const user = records.sort((a, b) => b.active - a.active);

    return (
      <div className={showHeader ? "showHeader ativo" : "showHeader"}>
        <div
          className={
            activeChat ? "internalChat ativo" : "internalChat desativado"
          }
          onMouseEnter={this.activeHeader}
          onMouseLeave={this.activeHeader}
        >
          <div className="headerChat" onClick={this.activeClick}>
            <div className="avatar_user">
              <img src={avatar} alt="" />
            </div>
            <span>Mensagens</span>
            <Icon name="edit outline" />
          </div>
          <div className="bodyChat">
            <PerfectScrollbar
              ref={(ref) => {
                this._scrollBarRef = ref;
              }}
            >
              <div className="busca">
                <div className="inputBusca" name="busca-header">
                  <input
                    type="text"
                    onChange={this.handleInputChange}
                    ref={(input) => (this.search = input)}
                  />

                  <button className="botaoBusca" onClick={this.cleanSearch}>
                    <Icon name={this.state.query ? "close" : "search"} />
                  </button>
                </div>
              </div>

              {user
                .filter((c) => this.props.user_id !== c.id)
                .map((d, interruption, i) => (
                  <div
                    className="holderChat"
                    key={`internal-chat-${d.id}`}
                    onUpdateSize={() => {
                      this._scrollBarRef.updateScroll();
                    }}
                  >
                    <div
                      className={
                        d.currentPause ? "usercard pausado" : "usercard"
                      }
                      onClick={() => this.openChat(d)}
                      key={`user-${i * 1}`}
                    >
                      <div
                        className={
                          d.active ? "avatar_user ativo" : "avatar_user inativo"
                        }
                      >
                        <img src={d.avatar} alt="" />
                      </div>
                      <div className="holder-user">
                        <span className="name">{d.name}</span>
                        {/* <span className="news_messages">3</span> */}
                        {d.currentPause ? (
                          <PauseInfo
                            pause={d.currentPause}
                            interruptions={interruptions}
                          />
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                ))}
            </PerfectScrollbar>
          </div>
          {chats ? (
            <WindowChat
              chats={chats}
              message={message}
              loading={loading}
              activeWindow={activeWindow}
              active={this.activeWindow}
              closeWindow={this.closeWindow}
              enterMessage={this.enterMessage}
              sendMessage={this.sendMessage}
              loadingSwitch={this.loadingSwitch}
              handleChange={this.handleChange}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user: { user } }) => ({
  name: user.name,
  user_id: user.id,
  company_id: user.company_id,
  avatar: user.avatar,
});
export default connect(mapStateToProps)(InternalChat);
