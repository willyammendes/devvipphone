import React, { Component } from "react";
import { Icon, TextArea } from "semantic-ui-react";
import PerfectScrollbar from "react-perfect-scrollbar";

class WindowChat extends Component {
	state = {
		loading: true
	};
	loadingTrue = () => {
		this.setState({
			loading: true
		});
	};
	componentDidMount() {
		const { loading, loadingSwitch } = this.props;
		const body = document.querySelector(
			".message-body .scrollbar-container"
		);
		const boxes = document.querySelector(".messages");
		if (body && boxes && loading) {
			body.scrollTo(0, boxes.scrollHeight);

			loadingSwitch();
		}
	}
	componentWillMount() {}
	render() {
		const {
			chats,
			active,
			activeWindow,
			closeWindow,
			enterMessage,
			sendMessage,
			message,
			loading,

			handleChange
		} = this.props;

		return (
			<div className="holder_windows">
				<div className="chat_window">
					{chats
						.filter(c => c.id)
						.map((c, i) => (
							<div
								className={
									activeWindow === i
										? "window active"
										: "window"
								}
								id={`chat-window-${i * 1}`}
								key={`chat-window-${i * 1}`}
							>
								<div
									className="header_chat_window"
									onClick={() => {
										active(i);
										this.loadingTrue();
									}}
								>
									{c.avatar ? (
										<img src={c.avatar} alt={c.name} />
									) : (
										""
									)}

									<span className="user_name">{c.name}</span>
									<Icon
										name="close"
										onClick={() => closeWindow(c, i)}
									/>
								</div>
								<div
									className={
										loading
											? "message-body"
											: "message-body"
									}
								>
									<PerfectScrollbar
										ref={ref => {
											this._scrollBarRef = ref;
										}}
									>
										<div
											className="messages"
											onUpdateSize={() => {
												this._scrollBarRef.updateScroll();
											}}
										>
											{c.chat.map(d => (
												<div
													className={
														d.from === c.id
															? "message received"
															: "message sended"
													}
												>
													{d.text}
												</div>
											))}
										</div>
									</PerfectScrollbar>
								</div>
								<div className="input_box">
									<TextArea
										placeholder="Enviar mensagem"
										name="message"
										onChange={handleChange}
										value={message}
										onKeyDown={e => enterMessage(e, c, i)}
									/>
									<Icon
										name="send"
										onClick={() => sendMessage(c, i)}
										className="send_message"
									/>
								</div>
							</div>
						))}
				</div>
			</div>
		);
	}
}
export default WindowChat;
