import React from "react";
import { Form, Input, Dropdown } from "semantic-ui-react";

const eventsOptions = [
  { key: 1, text: "Mensagem recebida", value: 1 },
  { key: 2, text: "Mensagem Enviada", value: 2 },
  { key: 5, text: "Novo Ticket Criado", value: 5 },
  { key: 6, text: "Mudança no Status do Ticket", value: 6 }
];

const FormWebhook = ({ webhook, onChange }) => {
  return (
    <Form>
      <Form.Field
        autoComplete="off"
        control={Input}
        label="Url a ser requisitada"
        name="url"
        onChange={onChange}
        value={webhook.url}
      />
      <Form.Field>
        <label>Quando este evento ocorrer:</label>
        <Dropdown
          selection
          name="event_type"
          options={eventsOptions}
          value={webhook.event_type}
          placeholder="Selecione um evento"
          onChange={onChange}
          clearable
        />
      </Form.Field>

      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Cabeçalhos"
          name="headers"
          onChange={onChange}
          value={webhook.headers}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Valores"
          name="headers_value"
          onChange={onChange}
          value={webhook.headers_value}
        />
      </Form.Group>
    </Form>
  );
};

export default FormWebhook;
