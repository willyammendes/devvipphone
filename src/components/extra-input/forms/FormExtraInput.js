import React from "react";
import { Form, Input, Checkbox, Dropdown } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";

const FormExtraInput = ({
  extraInput,
  onChange,
  messageError,
  modelTypes,
  inputType,
  extraInputOptions,
  onAddItem,
}) => {
  return (
    <Form autocomplete="off">
      <Form.Group widths="equal">
        <Form.Field error={!!messageError.label} width={3}>
          <label htmlFor="label">Label</label>
          <Input
            autoComplete="off"
            control={Input}
            name="label"
            onChange={onChange}
            value={extraInput.label}
            placeholder="Ultimo Nome: Primeiro Nome:"
          />
          {messageError.label && <InlineError text={messageError.label} />}
        </Form.Field>
        <Form.Field error={!!messageError.input_type} width={4}>
          <label htmlFor="input_type">Tipo de Controle</label>
          <Dropdown
            placeholder="Selecione o Tipo de Controle"
            search
            selection
            multiple={false}
            scrolling
            onChange={onChange}
            name="input_type"
            value={extraInput.input_type}
            options={inputType}
            closeOnChange
          />
          {messageError.input_type && (
            <InlineError text={messageError.input_type} />
          )}
        </Form.Field>

        <Form.Field error={!!messageError.model} width={4}>
          <label htmlFor="model">Cadastro</label>
          <Dropdown
            placeholder="Onde irá ficar o controle ?"
            search
            selection
            multiple={false}
            scrolling
            name="model"
            onChange={onChange}
            value={extraInput.model}
            options={modelTypes}
            closeOnChange
          />
          {messageError.model && <InlineError text={messageError.model} />}
        </Form.Field>
      </Form.Group>

      {extraInput.input_type === 12 ? (
        <Form.Group>
          <Form.Field error={!!messageError.mask} width={6}>
            <label htmlFor="mask">Máscara</label>
            <Input
              autoComplete="off"
              control={Input}
              name="mask"
              onChange={onChange}
              value={extraInput.mask}
              placeholder="Ex: 99.999.999/9999-99 (Cnpj)"
            />
            {messageError.mask && <InlineError text={messageError.mask} />}
          </Form.Field>
        </Form.Group>
      ) : null}
      {extraInput.input_type === 1 || extraInput.input_type === 4 ? (
        <Form.Group>
          <Form.Field error={!!messageError.input_options}>
            <label htmlFor="input_options">Opções</label>
            <Dropdown
              placeholder="Opções para o Dropdown"
              search
              selection
              multiple
              onChange={onChange}
              onAddItem={onAddItem}
              allowAdditions
              name="input_options"
              options={extraInputOptions}
              value={extraInput.input_options}
            />
            {messageError.input_options && (
              <InlineError text={messageError.input_options} />
            )}
          </Form.Field>
        </Form.Group>
      ) : null}

      {extraInput.input_type === 6 ? (
        <Form.Group>
          <Form.Field error={!!messageError.min_range} width={3}>
            <label htmlFor="min_range">Mínimo</label>
            <Input
              autoComplete="off"
              control={Input}
              name="min_range"
              onChange={onChange}
              value={extraInput.min_range}
              placeholder="0"
            />
            {messageError.min_range && (
              <InlineError text={messageError.min_range} />
            )}
          </Form.Field>
          <Form.Field error={!!messageError.max_range} width={3}>
            <label htmlFor="max_range">Máximo</label>
            <Input
              autoComplete="off"
              control={Input}
              name="max_range"
              onChange={onChange}
              value={extraInput.max_range}
              placeholder="10"
            />
            {messageError.max_range && (
              <InlineError text={messageError.max_range} />
            )}
          </Form.Field>
          <Form.Field error={!!messageError.rating_icon}>
            <label htmlFor="rating_icon">Ícone</label>
            <Dropdown
              placeholder="Selecione um ìcone"
              search
              selection
              onChange={onChange}
              name="rating_icon"
              options={[
                {
                  key: "star",
                  value: "star",
                  text: "Estrela",
                  icon: "star",
                },
                {
                  key: "heart",
                  value: "heart",
                  text: "Coração",
                  icon: "heart",
                },
              ]}
              value={extraInput.rating_icon}
            />
            {messageError.rating_icon && (
              <InlineError text={messageError.rating_icon} />
            )}
          </Form.Field>
          <Form.Field error={!!messageError.rating_size}>
            <label htmlFor="rating_size">Tamanho</label>
            <Dropdown
              placeholder="Selecione um tamanho"
              search
              selection
              onChange={onChange}
              name="rating_size"
              options={[
                {
                  key: "mini",
                  value: "mini",
                  text: "Minusculo",
                  icon: "star",
                },
                {
                  key: "tiny",
                  value: "tiny",
                  text: "Bem Pequeno",
                  icon: "star",
                },
                {
                  key: "small",
                  value: "small",
                  text: "Normal",
                  icon: "star",
                },
                {
                  key: "large",
                  value: "large",
                  text: "Grande",
                  icon: "star",
                },
                { key: "huge", value: "huge", text: "Enorme", icon: "star" },
                {
                  key: "massive",
                  value: "massive",
                  text: "Gigante",
                  icon: "star",
                },
              ]}
              value={extraInput.rating_size}
            />
            {messageError.rating_size && (
              <InlineError text={messageError.rating_size} />
            )}
          </Form.Field>
        </Form.Group>
      ) : null}

      {extraInput.input_type === 7 ? (
        <Form.Group>
          <Form.Field error={!!messageError.image_size}>
            <label htmlFor="image_size">Tamanho</label>
            <Dropdown
              placeholder="Selecione um tamanho"
              search
              selection
              onChange={onChange}
              name="image_size"
              options={[
                {
                  key: "mini",
                  value: "mini",
                  text: "Minusculo",
                  icon: "star",
                },
                {
                  key: "tiny",
                  value: "tiny",
                  text: "Bem Pequeno",
                  icon: "star",
                },
                {
                  key: "small",
                  value: "small",
                  text: "Normal",
                  icon: "star",
                },
                {
                  key: "large",
                  value: "large",
                  text: "Grande",
                  icon: "star",
                },
                { key: "huge", value: "huge", text: "Enorme", icon: "star" },
                {
                  key: "massive",
                  value: "massive",
                  text: "Gigante",
                  icon: "star",
                },
              ]}
              value={extraInput.image_size}
            />
            {messageError.image_size && (
              <InlineError text={messageError.image_size} />
            )}
          </Form.Field>
        </Form.Group>
      ) : null}

      <Form.Group>
        <Form.Field error={!!messageError.order}>
          <label htmlFor="order">Ordem de Exibição</label>
          <Input
            autoComplete="off"
            control={Input}
            name="order"
            onChange={onChange}
            value={extraInput.order}
            placeholder="Ex: 1, 2, 3"
          />
          {messageError.order && <InlineError text={messageError.order} />}
        </Form.Field>
        {extraInput.input_type === 0 && (
          <Form.Field error={!!messageError.is_link} width={2}>
            <label htmlFor="is_link">Link</label>
            <Checkbox
              label="Sim"
              onChange={onChange}
              name="is_link"
              checked={extraInput.is_link}
            />
            {messageError.is_link && (
              <InlineError text={messageError.is_link} />
            )}
          </Form.Field>
        )}
        {extraInput.input_type !== 5 && (
          <Form.Field error={!!messageError.width}>
            <label htmlFor="width">Largura do Campo</label>
            <Input
              autoComplete="off"
              control={Input}
              name="width"
              onChange={onChange}
              value={extraInput.width}
              placeholder="Largura do controle de 1 a 10"
            />
            {messageError.width && <InlineError text={messageError.width} />}
          </Form.Field>
        )}
        {extraInput.input_type !== 5 && (
          <Form.Field error={!!messageError.show_column_on_view}>
            <label htmlFor="input_options">Visível na Visão de Registros</label>
            <Checkbox
              label="Sim"
              onChange={onChange}
              name="show_column_on_view"
              checked={extraInput.show_column_on_view}
            />
            {messageError.show_column_on_view && (
              <InlineError text={messageError.show_column_on_view} />
            )}
          </Form.Field>
        )}
      </Form.Group>
      {extraInput.input_type !== 5 && (
        <Form.Group>
          <Form.Field error={!!messageError.placeholder}>
            <label htmlFor="placeholder">Mensagem de ajuda</label>
            <Input
              autoComplete="off"
              control={Input}
              name="placeholder"
              onChange={onChange}
              value={extraInput.placeholder}
              placeholder="Ex: Informe o nome da empresa"
            />
            {messageError.placeholder && (
              <InlineError text={messageError.placeholder} />
            )}
          </Form.Field>
        </Form.Group>
      )}
    </Form>
  );
};

export default FormExtraInput;
