import React, { Component } from "react";
import api from "../../api/extra-input";
import DataTable from "../table/DataTable";
import ExtraInputModal from "./modal/ExtraInputModal";
import AlertSuccess from "../alerts/AlertSuccess";

class ExtraInput extends Component {
  state = {
    records: [],
    columns: {},
    order: {
      column: "order",
      direction: "asc",
    },
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    extraInputOptions: [],
    inputTypes: [],
    modelTypes: [],
  };

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    this.setState({
      selectedDataIndex: dataIndex,
      editModalOpen: true,
      extraInputOptions: this.state.records[dataIndex].options,
    });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce((o, key) => {
          return Object.assign(o, {
            [key]: Array.isArray(records[0][key]) ? [] : "",
          });
        }, {})
      : {
          input_options: [],
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.extraInput.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        inputType: res.inputTypes,
        modelTypes: res.modelTypes,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords({
      order: this.state.order.column,
      order_direction: this.state.order.direction,
    });
  }

  onAddItem = (e, { value }) => {
    this.setState((prevState) => ({
      extraInputOptions: [
        { text: value, value },
        ...prevState.extraInputOptions,
      ],
    }));
  };

  handleChange = (e, { name, value, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked !== undefined ? checked : value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.extraInput
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.extraInput
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...this.state.records.slice(0, selectedDataIndex),
            { ...data.data },
            ...this.state.records.slice(selectedDataIndex + 1),
          ],
          editModalOpen: false,
          selectedDataIndex: -1,
          save_alert: true,
          loading: false,
        });
        this.fetchRecords({
          order: this.state.order.column,
          order_direction: this.state.order.direction,
        });

        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  delete = (id) => api.extraInput.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      inputType,
      modelTypes,
    } = this.state;

    const extraInput = this.state.records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Campos Adicionais</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records.map((r) => {
                  return {
                    ...r,
                    show_column_on_view: r.show_column_on_view ? "Sim" : "Não",
                    input_type: inputType.find((i) => i.key == r.input_type)
                      ? inputType.find((i) => i.key == r.input_type).text
                      : "",
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
              />
              {selectedDataIndex !== -1 ? (
                <ExtraInputModal
                  modelTypes={modelTypes}
                  onAddItem={this.onAddItem}
                  extraInputOptions={this.state.extraInputOptions}
                  inputType={inputType}
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  extraInput={extraInput}
                  modalHeader={
                    extraInput.id
                      ? `Edição do campo ${extraInput.input_name}`
                      : "Novo Campo Adicional"
                  }
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.errors
                      : ""
                  }
                  generalError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  cleanErrors={this.cleanErrors}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ExtraInput;
