import React from "react";
import { Form, Input, Radio } from "semantic-ui-react";

const FormBotVars = ({ variable, onChange, onChecked }) => {
  
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Descrição:"
          name="description"
          onChange={onChange}
          value={variable.description}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Valor:"
          name="value"
          onChange={onChange}
          value={variable.value}
        />
      </Form.Group>
      <Form.Field>Tipo de Variável</Form.Field>
      <Form.Field>
        <Radio
          label="Global"
          name="type"
          value={0}
          checked={variable.type === 0}
          onChange={onChange}
        />
      </Form.Field>
      <Form.Field>
        <Radio
          label="Usuário"
          name="type"
          value={1}
          checked={variable.type === 1}
          onChange={onChange}
        />
      </Form.Field>
    </Form>
  );
};

export default FormBotVars;
