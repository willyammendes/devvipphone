import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormBotContext from "../forms/FormBotContext";

class BotContextModal extends React.Component {
  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", e => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.context.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      context,
      handleClose,
      onChange,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      handleMediaChange,
      changeAction,
      actionTrue,
      handleActionChange,
      handleDepartmentChange,
      handleUserChange,
      BasicUpload,
      onAddTrigger,
      trigger,
      deleteTrigger,
      onAddReply,
      deleteReply,
      replies,
      rateAdd,
      addIntent,
      intents,
      deleteIntent,
      errorModal,
      closeError,
      openTrigger,
      openTriggerModal,
      closeTriggerModal
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="form-context">
              <FormBotContext
                context={context}
                onChange={onChange}
                trigger={trigger}
                replies={replies}
                intents={intents}
                handleMediaChange={handleMediaChange}
                changeAction={changeAction}
                actionTrue={actionTrue}
                handleActionChange={handleActionChange}
                handleDepartmentChange={handleDepartmentChange}
                handleUserChange={handleUserChange}
                BasicUpload={BasicUpload}
                openTrigger={openTrigger}
                onAddTrigger={onAddTrigger}
                deleteTrigger={deleteTrigger}
                onAddReply={onAddReply}
                deleteReply={deleteReply}
                rateAdd={rateAdd}
                addIntent={addIntent}
                deleteIntent={deleteIntent}
                closeTriggerModal={closeTriggerModal}
                openTriggerModal={openTriggerModal}
                errorModal={errorModal}
                closeError={closeError}
              />
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default BotContextModal;
