import React, { Component, useCallback } from "react";
import api from "../../api/bot-context";
import DataTable from "../table/DataTable";
import BotContextModal from "./modal/BotContextModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { useDropzone } from "react-dropzone";
import renderHTML from "react-render-html";

class BotContext extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    actionTrue: false,
    trigger: [],
    replies: [],
    intents: [],
    errorModal: "",
    params: "",
    openTrigger: false
  };

  changeAction = () => {
    this.setState({
      actionTrue: !this.state.actionTrue
    });
  };

  openTriggerModal = () => {
    this.setState({
      openTrigger: true
    });
  };

  closeTriggerModal = () => {
    this.setState({
      openTrigger: false
    });
  };

  onAddTrigger = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    const newTrigger = {
      description: records[dataIndex].context,
      trigger: records[dataIndex].context,
      final_intent: false
    };
    const hasSame = this.state.trigger.find(
      c => c.description === newTrigger.description
    );

    if (!!!hasSame && newTrigger.description.length > 0) {
      this.setState({
        trigger: this.state.trigger.concat(newTrigger),
        records: [
          ...records.slice(0, dataIndex),
          {
            ...records[dataIndex],
            context: ""
          },
          ...records.slice(dataIndex + 1)
        ]
      });
    }
  };

  deleteTrigger = i => {
    const { trigger } = this.state;
    const holderRate = "";
    this.setState({
      trigger: [...trigger.slice(0, i), ...holderRate, ...trigger.slice(i + 1)]
    });
  };

  onAddReply = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    const newReply = {
      description: records[dataIndex].reply,
      weight: "",
      final_intent: false
    };
    const hasSame = this.state.replies.find(
      c => c.description === newReply.description
    );
    if (!!!hasSame && newReply.description.length > 0) {
      this.setState({
        replies: this.state.replies.concat(newReply),
        records: [
          ...records.slice(0, dataIndex),
          {
            ...records[dataIndex],
            reply: ""
          },
          ...records.slice(dataIndex + 1)
        ]
      });
    }
  };

  rateAdd = (i, value, d) => {
    const { replies } = this.state;
    const holderRate = {
      description: d.description,
      weight: value,
      final_intent: false
    };
    this.setState({
      replies: [
        ...replies.slice(0, i),
        { ...holderRate },
        ...replies.slice(i + 1)
      ]
    });
  };

  deleteReply = i => {
    const { replies } = this.state;
    const holderRate = "";
    this.setState({
      replies: [...replies.slice(0, i), ...holderRate, ...replies.slice(i + 1)]
    });
  };

  addIntent = () => {
    const {
      trigger,
      replies,
      selectedDataIndex: dataIndex,
      records
    } = this.state;
    const actionDefault = {
      action_type: "",
      message: "",
      user_id: "",
      department_id: "",
      get_url: "",
      file_name: "",
      file_data: "",
      get_fields: "",
      get_results: "",
      post_url: "",
      post_fields: "",
      headers: "",
      headers_value: ""
    };
    const newIntents = trigger.map(c => ({
      description: c.description,
      trigger: c.trigger,
      final_intent: c.final_intent,
      action: {
        ...actionDefault,
        ...records[dataIndex].action,
        message: records[dataIndex].message
      },
      replies: replies
    }));
    if (trigger.length > 0 && replies.length > 0) {
      this.setState({
        intents: this.state.intents.concat(newIntents),
        replies: [],
        trigger: []
      });
    } else if (replies.length === 0) {
      this.setState({
        errorModal: "É necessário adicionar algum texto para resposta"
      });
    } else {
      this.setState({
        errorModal: "É necessário adicionar alguma expressão do usuário"
      });
    }
  };

  deleteIntent = i => {
    const { intents } = this.state;
    const holderRate = "";
    this.setState({
      intents: [...intents.slice(0, i), ...holderRate, ...intents.slice(i + 1)]
    });
  };

  handleActionChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          action: { ...records[dataIndex].action, action_type: value }
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  select = selectedDataId => {
    const { records } = this.state;
    const dataIndex = records.findIndex(c => c.id === selectedDataId);
    records[dataIndex].action = "";
    this.setState({
      selectedDataIndex: dataIndex,
      editModalOpen: true,
      intents: records[dataIndex].intents
    });
  };

  newDataClick = () => {
    const { records } = this.state;
    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "", action: "" }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: ""
        };
    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });
    return await api.context.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        params
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  handleMediaChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          accounts: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleDepartmentChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          action: { ...records[dataIndex].action, department_id: value }
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleUserChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          action: {
            ...records[dataIndex].action,
            user_id: value
          }
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records, intents } = this.state;
    this.setState({
      loading: true
    });
    const dataSend = {
      trigger: records[dataIndex].trigger,
      identifier: records[dataIndex].identifier,
      inherits: records[dataIndex].inherits,
      description: records[dataIndex].description,
      name: records[dataIndex].name,
      intents: intents,
      accounts: records[dataIndex].accounts
    };
    return api.context
      .submit({ context: dataSend })
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false
        });
        this.fetchRecords(this.state.params);
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0),
          intents: [],
          replies: [],
          trigger: []
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records, intents } = this.state;
    const data = records[selectedDataIndex];
    const dataSend = {
      trigger: data.trigger,
      bot_trigger_id: data.bot_trigger_id,
      identifier: data.identifier,
      inherits: data.inherits,
      description: data.description,
      name: data.name,
      intents: intents,
      accounts: data.accounts
    };
    this.setState({ loading: true });
    api.context
      .update(data.id, { context: dataSend })
      .then(data => {
        this.setState({
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        this.fetchRecords(this.state.params);
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0),
          intents: [],
          replies: [],
          trigger: []
        });
      });
  };

  delete = id => api.context.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  BasicUpload = props => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16
    };

    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box"
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden"
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%"
    };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback(acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
      this.setState({
        file: acceptedFiles.map(file => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document"
        }))
      });
      acceptedFiles.forEach(file => {
        const reader = new FileReader();

        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const { getRootProps, getInputProps } = useDropzone({ onDrop });
    const thumbs = files.map(file => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));
    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [files]
    );
    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>
            Arraste seu arquivo ou imagem, ou clique aqui para seleciona-lo.
          </p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };

  closeError = () => {
    this.setState({
      errorModal: ""
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      actionTrue,
      trigger,
      replies,
      intents,
      errorModal,
      openTrigger
    } = this.state;

    const context = records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Contextos</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records.map(r => {
                  return {
                    ...r,
                    description: renderHTML(r.description)
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={id => this.delete(id)}
                onEditClick={d => this.select(d.id)}
                fetchData={this.fetchRecords}
              />

              {selectedDataIndex !== -1 ? (
                <BotContextModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  context={context}
                  trigger={trigger}
                  replies={replies}
                  intents={intents}
                  deleteTrigger={this.deleteTrigger}
                  deleteIntent={this.deleteIntent}
                  onAddReply={this.onAddReply}
                  deleteReply={this.deleteReply}
                  rateAdd={this.rateAdd}
                  addIntent={this.addIntent}
                  BasicUpload={this.BasicUpload}
                  handleMediaChange={this.handleMediaChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleUserChange={this.handleUserChange}
                  changeAction={this.changeAction}
                  actionTrue={actionTrue}
                  closeTriggerModal={this.closeTriggerModal}
                  onAddTrigger={this.onAddTrigger}
                  handleActionChange={this.handleActionChange}
                  closeError={this.closeError}
                  openTriggerModal={this.openTriggerModal}
                  openTrigger={openTrigger}
                  modalHeader={
                    context.id ? `Edição do ${context.name}` : "Novo Contexto"
                  }
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                  errorModal={errorModal}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BotContext;
