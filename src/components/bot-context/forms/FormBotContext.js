import React from "react";
import {
    Form,
    Input,
    Checkbox,
    TextArea,
    Icon,
    Divider,
    Button,
    Rating,
    Modal,
    Header
} from "semantic-ui-react";
import DropdownMedia from "../../social-media/DropdownMedia";
import DropdownTriggers from "../../triggers/DropdownTriggers";
import DropdownDepartments from "../../departments/DropdownDepartments";
import DropdownUsers from "../../users/DropdownUsers";
import PerfectScrollbar from "react-perfect-scrollbar";
import EditTrigger from "../EditTrigger";

const FormBotContext = ({
    context,
    onChange,
    handleMediaChange,
    changeAction,
    actionTrue,
    handleActionChange,
    handleDepartmentChange,
    handleUserChange,
    BasicUpload,
    onAddTrigger,
    trigger,
    deleteTrigger,
    onAddReply,
    deleteReply,
    replies,
    rateAdd,
    addIntent,
    intents,
    deleteIntent,
    errorModal,
    closeError,
    openTrigger,
    closeTriggerModal,
    openTriggerModal
}) => {
    return (
        <Form>
            <div className="form-row">
                <div className="field-row context">
                    <Form.Group widths="equal">
                        <Form.Field
                            autoComplete="off"
                            control={Input}
                            label="Nome do Contexto:"
                            name="name"
                            onChange={onChange}
                            value={context.name}
                        />
                        <Form.Field
                            autoComplete="off"
                            control={Input}
                            label="Gatilho:"
                            name="trigger"
                            onChange={onChange}
                            value={context.trigger}
                        />
                    </Form.Group>
                    <Form.Group widths="equal">
                        <Form.Field
                            autoComplete="off"
                            control={Input}
                            label="Identificador:"
                            name="identifier"
                            onChange={onChange}
                            value={context.identifier}
                        />
                        <Form.Field
                            autoComplete="off"
                            control={Input}
                            label="Contextos Herdados:"
                            name="inherits"
                            onChange={onChange}
                            value={context.inherits}
                        />
                    </Form.Group>
                    <Form.Field
                        control={TextArea}
                        label="Mensagem de Entrada:"
                        name="description"
                        onChange={onChange}
                        value={context.description}
                    />
                    <Form.Group widths="equal">
                        <Form.Field>
                            <label>Contas Vinculadas</label>
                            <DropdownMedia
                                onSelectMedia={handleMediaChange}
                                accounts={context.accounts}
                                allowAdditions={false}
                            />
                        </Form.Field>
                    </Form.Group>
                    <Form.Field
                        autoComplete="off"
                        control={Input}
                        label="Adicione uma expressão do usuário:"
                        icon={
                            <Icon
                                name="plus"
                                inverted
                                circular
                                link
                                onClick={onAddTrigger}
                            />
                        }
                        name="context"
                        onChange={onChange}
                        value={context.context}
                    />
                    <div className="intent_triggers">
                        {trigger.map((d, index) => (
                            <div key={`intent-triggers-${d.id}`}>
                                <Icon name="quote left" />
                                {d.description}
                                <Icon
                                    name="trash"
                                    className="fr"
                                    onClick={() => deleteTrigger(index)}
                                />
                            </div>
                        ))}
                    </div>
                    <Divider />
                    <Form.Field
                        control={TextArea}
                        label="Digite um texto para resposta"
                        name="reply"
                        onChange={onChange}
                        value={context.reply}
                    />
                    <Form.Field>
                        <Button basic color="green" onClick={onAddReply}>
                            Adicionar Reposta
                        </Button>
                    </Form.Field>
                    <div className="replies_triggers">
                        {replies.map((d, index) => (
                            <div>
                                <Icon name="quote left" />
                                {d.description}
                                <Icon
                                    name="trash"
                                    className="fr"
                                    onClick={() => deleteReply(index)}
                                />

                                <Rating
                                    icon="star"
                                    maxRating={5}
                                    defaultRating={d.weight}
                                    value={d.weight}
                                    clearable
                                    className="fr"
                                    name="weight"
                                    onRate={(e, { rating }) =>
                                        rateAdd(index, rating, d)
                                    }
                                />
                            </div>
                        ))}
                    </div>
                    <div className="field">
                        <Checkbox
                            toggle
                            label="Executar Uma Ação"
                            value={actionTrue}
                            checked={actionTrue}
                            onChange={changeAction}
                        />
                    </div>
                    {actionTrue ? (
                        <div className="field">
                            <label>Ação a ser executada</label>
                            <DropdownTriggers
                                onSelectAction={handleActionChange}
                                action_type={context.action.action_type}
                            />
                        </div>
                    ) : (
                        ""
                    )}
                    {actionTrue &&
                    (context.action.action_type === 0 ||
                        context.action.action_type === 2) ? (
                        <div className="field">
                            <DropdownDepartments
                                onSelectDepartment={handleDepartmentChange}
                                departments={context.action.department_id}
                            />
                        </div>
                    ) : (
                        ""
                    )}
                    {actionTrue &&
                    (context.action.action_type === 1 ||
                        context.action.action_type === 5) ? (
                        <div className="field">
                            <DropdownUsers
                                onSelectUser={handleUserChange}
                                user_id={context.action.user_id}
                                allowAdditions={false}
                            />
                        </div>
                    ) : (
                        ""
                    )}
                    {actionTrue &&
                    (context.action.action_type === 3 ||
                        context.action.action_type === 4) ? (
                        <div className="holder_requisition">
                            <h3>Informações da Requisição</h3>
                            {context.id ? (
                                <Form.Field
                                    control={Input}
                                    label="URL"
                                    placeholder="Digite a url ex: https://api.monitchat.com"
                                    name={
                                        context.action.action_type === 3
                                            ? "get_url"
                                            : "post_url"
                                    }
                                    onChange={onChange}
                                    value={
                                        context.action.action_type === 3
                                            ? context.action.get_url
                                            : context.action.post_url
                                    }
                                />
                            ) : (
                                <Form.Field
                                    control={Input}
                                    label="URL"
                                    placeholder="Digite a url ex: https://api.monitchat.com"
                                    name={
                                        context.action.action_type === 3
                                            ? "get_url"
                                            : "post_url"
                                    }
                                    onChange={onChange}
                                    value={
                                        context.action.action_type === 3
                                            ? context.get_url
                                            : context.post_url
                                    }
                                />
                            )}

                            {context.id ? (
                                <Form.Field
                                    control={Input}
                                    label="Campos de Retorno"
                                    placeholder="Digite a url ex: https://api.monitchat.com"
                                    name={
                                        context.action.action_type === 3
                                            ? "get_fields"
                                            : "post_fields"
                                    }
                                    onChange={onChange}
                                    value={
                                        context.action.action_type === 3
                                            ? context.action.get_fields
                                            : context.action.post_fields
                                    }
                                />
                            ) : (
                                <Form.Field
                                    control={Input}
                                    label="Campos de Retorno"
                                    placeholder="Digite a url ex: https://api.monitchat.com"
                                    name={
                                        context.action.action_type === 3
                                            ? "get_fields"
                                            : "post_fields"
                                    }
                                    onChange={onChange}
                                    value={
                                        context.action.action_type === 3
                                            ? context.get_fields
                                            : context.post_fields
                                    }
                                />
                            )}

                            <Form.Group widths="equal">
                                <Form.Field
                                    control={Input}
                                    label="Cabeçalhos da Requisição"
                                    placeholder="Cabeçalhos. Ex: Authorization;Content-type;etc..."
                                    name="headers"
                                    onChange={onChange}
                                    value={
                                        context.id
                                            ? context.action.headers
                                            : context.headers
                                    }
                                />
                                <Form.Field
                                    control={Input}
                                    label="Valores dos Cabeçalhos"
                                    placeholder="Valores. Ex: Basic:asdf=s-fsd;application/json;etc..."
                                    name="headers_value"
                                    onChange={onChange}
                                    value={
                                        context.id
                                            ? context.action.headers_value
                                            : context.headers_value
                                    }
                                />
                            </Form.Group>
                        </div>
                    ) : (
                        ""
                    )}
                    {context.action.action_type === 10 && <BasicUpload />}

                    {actionTrue ? (
                        <Form.Field
                            control={TextArea}
                            label="Mensagem de Retorno:"
                            placeholder="Obrigado! Em breve um de nossos analistas irá lhe atender"
                            name={context.id ? "message" : "message"}
                            onChange={onChange}
                            value={
                                context.id
                                    ? context.action.message
                                    : context.action.message
                            }
                        />
                    ) : (
                        ""
                    )}

                    <Button basic color="green" onClick={addIntent}>
                        Incluir Dialogo
                    </Button>
                </div>
                <div className="field-row context">
                    <div className="intent_triggers intents_holder">
                        <PerfectScrollbar>
                            {intents.map((d, index) => (
                                <div key={`intent-intents-${d.id}`}>
                                    <b>
                                        <Icon
                                            name="mail forward"
                                            title="Expressão do usuário"
                                        />
                                        {d.stream
                                            ? d.stream
                                            : d.description
                                            ? d.description
                                            : d.trigger}
                                    </b>

                                    {d.replies.length > 0 ? (
                                        <Icon
                                            name="trash"
                                            className="fr"
                                            title="Deletar intenção"
                                            onClick={() => deleteIntent(index)}
                                        />
                                    ) : (
                                        ""
                                    )}
                                    {d.replies.length > 0 ? (
                                        <EditTrigger
                                            intent={d}
                                            open={openTrigger}
                                            handleClose={closeTriggerModal}
                                            actionTrue={
                                                d.final_intent === 1
                                                    ? true
                                                    : false
                                            }
                                        />
                                    ) : (
                                        ""
                                    )}

                                    {d.final_intent === 1 ? (
                                        <Icon
                                            name="cogs"
                                            className="fr"
                                            title="Possui Ação atribuida"
                                        />
                                    ) : (
                                        ""
                                    )}

                                    {d.replies.map(c => (
                                        <span>
                                            <Icon
                                                name="android"
                                                title="Resposta do Robô"
                                            />
                                            {c.reply ? c.reply : c.description}
                                        </span>
                                    ))}
                                </div>
                            ))}
                        </PerfectScrollbar>
                    </div>
                </div>
            </div>
            <Modal
                open={errorModal.length > 0 ? true : false}
                onClose={closeError}
                closeIcon
                size="tiny"
            >
                <Modal.Content image>
                    <Modal.Description>
                        <Header>
                            <h2>Erro encontrado</h2>
                        </Header>
                        <h4>{errorModal}</h4>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        </Form>
    );
};

export default FormBotContext;
