import React, { Component, useCallback } from "react";
import api from "../../api/trigger";
import { Icon } from "semantic-ui-react";
import TriggersModal from "../triggers/modal/TriggersModal";
import { useDropzone } from "react-dropzone";

class EditTrigger extends Component {
  state = {
    records: [],
    intents: [],
    replies: [],
    file: "",
    image: "",
    actionTypes: {},
    replyInput: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    trigger: [],
    params: "",
    actionTrue: false,
    intent: {}
  };

  changeAction = () => {
    const { actionTrue } = this.state;

    this.setState({
      actionTrue: !actionTrue
    });
  };

  select = selectedDataId => {
    const { records } = this.state;
    const dataIndex = records.findIndex(c => c.id === selectedDataId);

    const replies = records[dataIndex].replies.map(c => {
      return {
        description: c.reply,
        weight: c.weight
      };
    });

    this.setState({
      selectedDataIndex: dataIndex,
      editModalOpen: true,
      replies,
      actionTrue: records[dataIndex].action.action_type > -1
    });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "" }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: ""
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.intent.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        params
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
    this.fetchActions();
    this.setState({
      intent: this.props.intent,
      replies: this.props.intent.replies
    });
  }

  fetchActions = () => {
    api.intent.fetchActions().then(res => {
      this.setState({
        actionTypes: res
      });
    });
  };

  handleChange = (e, { name, value }) => {
    const { intent } = this.state;

    this.setState({
      intent: {
        ...intent,
        [name]: value,
        action: { ...intent.action, [name]: value }
      }
    });
  };

  onAddIntent = (e, { name, value }) => {
    this.setState({
      intents: { ...this.state.intents, [name]: value }
    });
  };
  onAddReplies = (e, { name, value }) => {
    this.setState({
      replyInput: { ...this.state.replyInput, [name]: value }
    });
  };

  clickAddIntent = (e, { name, value }) => {
    const { intents, trigger, replies, replyInput, actionTrue } = this.state;

    const intentsHolder = {
      description: intents.description ? intents.description : "",
      final_intent: !!actionTrue
    };
    const replyInputHolder = {
      description: replyInput.reply ? replyInput.reply : "",
      weight: 0
    };
    const hasSameReply = replies.find(c => c.description === replyInput.reply);

    const hasSame = trigger.find(c => c.description === intents.description);
    if (!hasSame && !!intents.description) {
      this.setState({
        trigger: trigger.concat(intentsHolder),

        intents: { description: "" }
      });
    } else if (!hasSameReply && !!replyInput.reply) {
      this.setState({
        replies: replies.concat(replyInputHolder),
        replyInput: { reply: "" }
      });
    }
  };

  rateAdd = (i, value, d) => {
    const { replies } = this.state;
    const holderRate = {
      reply: d.reply,
      description: d.description,
      weight: value
    };
    this.setState({
      replies: [
        ...replies.slice(0, i),
        { ...holderRate },
        ...replies.slice(i + 1)
      ]
    });
  };

  deleteReply = i => {
    const { replies } = this.state;
    const holderRate = "";
    this.setState({
      replies: [...replies.slice(0, i), ...holderRate, ...replies.slice(i + 1)]
    });
  };

  deleteTrigger = i => {
    const { trigger } = this.state;
    const holderRate = "";
    this.setState({
      trigger: [...trigger.slice(0, i), ...holderRate, ...trigger.slice(i + 1)]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  handleActionChange = value => {
    const { intent } = this.state;

    this.setState({
      intent: {
        ...intent,
        action: { ...intent.action, action_type: value }
      }
    });
  };

  handleDepartmentChange = value => {
    const { intent } = this.state;

    this.setState({
      intent: {
        ...intent,
        action: {
          ...intent.action,
          department_id: value
        }
      }
    });
  };

  handleUserChange = value => {
    const { intent } = this.state;

    this.setState({
      intent: {
        ...intent,
        action: {
          ...intent.action,
          user_id: value
        }
      }
    });
  };
  handleMediaChange = value => {
    const { intent } = this.state;

    this.setState({
      intent: {
        ...intent,
        accounts: value
      }
    });
  };
  submit = () => {
    const {
      selectedDataIndex: dataIndex,
      records,
      replies,
      trigger,
      file,
      image,
      actionTrue
    } = this.state;

    if (actionTrue) {
      var sendAction = {
        replies,
        intents: trigger.map(obj => ({
          ...obj,
          accounts: records[dataIndex].accounts,
          action: {
            action_type: records[dataIndex].action.action_type,
            department_id: records[dataIndex].action.department_id
              ? records[dataIndex].action.department_id
              : "",
            message: records[dataIndex].reply ? records[dataIndex].reply : "",
            user_id: records[dataIndex].action.user_id
              ? records[dataIndex].action.user_id
              : "",
            get_url: records[dataIndex].get_url
              ? records[dataIndex].get_url
              : "",
            mime_type: records[dataIndex].mime_type
              ? records[dataIndex].mime_type
              : "",
            file_name: file ? file[0].file_name : "",
            file_data: image || "",
            get_fields: records[dataIndex].get_fields
              ? records[dataIndex].get_fields
              : "",
            get_results: records[dataIndex].get_results
              ? records[dataIndex].get_results
              : "",
            post_url: records[dataIndex].post_url
              ? records[dataIndex].post_url
              : "",
            post_fields: records[dataIndex].post_fields
              ? records[dataIndex].post_fields
              : "",
            headers: records[dataIndex].headers
              ? records[dataIndex].headers
              : "",
            headers_value: records[dataIndex].headers_value
              ? records[dataIndex].headers_value
              : ""
          }
        }))
      };
    } else {
      var sendInfo = {
        replies,
        intents: trigger.map(obj => ({
          ...obj,
          accounts: records[dataIndex].accounts && records[dataIndex].accounts,
          final_intent: false
        }))
      };
    }

    this.setState({
      loading: true
    });
    return api.intent
      .submit(actionTrue ? sendAction : sendInfo)
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          intents: [],
          trigger: [],
          replies: []
        });
        this.fetchRecords(this.state.params);
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records, intent } = this.state;
    const data = intent;
    this.setState({ loading: true });
    const datasend = {
      intents: [
        {
          description: data.trigger,
          accounts: data.accounts,
          final_intent: data.final_intent,
          action: data.action
        }
      ],
      replies: this.state.replies,
      action: data.action
    };

    api.intent
      .update(data.id, datasend)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.intents },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        this.fetchRecords(this.state.params);
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.intent.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      intents: [],
      trigger: [],
      replies: [],
      records: [...records].filter(c => c.id > 0)
    });
  };

  BasicUpload = props => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16
    };

    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box"
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden"
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%"
    };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback(acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );

      this.setState({
        file: acceptedFiles.map(file => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document"
        }))
      });

      acceptedFiles.forEach(file => {
        const reader = new FileReader();

        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const { getRootProps, getInputProps } = useDropzone({ onDrop });

    const thumbs = files.map(file => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));

    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
      },
      [files]
    );

    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>
            Arraste seu arquivo ou imagem, ou clique aqui para seleciona-lo.
          </p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };
  editModalOpen = () => {
    this.setState({
      editModalOpen: true
    });
  };
  render() {
    const {
      records,
      loading,
      selectedDataIndex,
      editModalOpen,
      intents,
      trigger,
      replyInput,
      replies,
      actionTypes,
      actionTrue
    } = this.state;

    const intent = this.state.intent;

    return (
      <div className="triggerModal">
        <Icon name="edit" onClick={this.editModalOpen} />

        {editModalOpen ? (
          <TriggersModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={this.update}
            onClickAdd={this.submit}
            intent={intent}
            intents={intents}
            trigger={trigger}
            replies={replies}
            onAddReplies={this.onAddReplies}
            replyInput={replyInput}
            actionTypes={actionTypes}
            BasicUpload={this.BasicUpload}
            actionTrue={intent.final_intent === 1 || actionTrue ? true : false}
            modalHeader={
              intent.id ? `Edição do ${intent.trigger}` : "Novo Intenção"
            }
            changeAction={this.changeAction}
            handleActionChange={this.handleActionChange}
            handleDepartmentChange={this.handleDepartmentChange}
            handleUserChange={this.handleUserChange}
            handleMediaChange={this.handleMediaChange}
            rateAdd={this.rateAdd}
            deleteReply={this.deleteReply}
            deleteTrigger={this.deleteTrigger}
            handleKeyDown={this.handleKeyDown}
            clickAddIntent={this.clickAddIntent}
            onAddIntent={this.onAddIntent}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            loading={loading}
          />
        ) : null}
      </div>
    );
  }
}

export default EditTrigger;
