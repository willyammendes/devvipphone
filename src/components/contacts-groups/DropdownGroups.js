import React, { Component } from "react";
import api from "../../api/group";
import { Form } from "semantic-ui-react";
import AddGroupModal from "./modal/AddGroupModal";

class DropdownGroups extends Component {
  state = {
    group: {
      name: "",
      description: "",
    },
    errors: "",
    options: [],
    loading: false,
    search: "",
    openModal: false,
  };

  handleGroupsAddition = (e) => {};

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({ search: searchQuery });
    this.timer = setTimeout(this.onSearchGroups, 300);
  };

  componentDidMount() {
    const { options } = this.props;

    if (options) {
      this.setState({
        options: options.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
      });
    }
  }

  onSearchGroups = async () => {
    this.setState({ loading: true });
    const { search } = this.state;

    await api.group.fetchAll({ search }).then((group) => {
      this.setState({
        options: group.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loading: false,
      });
    });
  };

  onChange = (e, { name, value }) => {
    this.setState((state) => ({
      group: { ...state.group, [name]: value },
    }));
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { group } = this.state;

    this.setState({
      loading: true,
    });

    return api.group
      .submit(group)
      .then((data) => {
        this.setState((state) => ({
          options: [...state.options].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          loading: false,
          group: {
            name: "",
            description: "",
          },
          openModal: !state.openModal,
        }));

        this.props.onSelectGroups(data.data.id);
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  toggle = (e, { name, value }) => {
    this.setState((state) => ({
      openModal: !state.openModal,
      group: {
        description: "",
        name: value,
      },
    }));
  };

  render() {
    const { options, loading, openModal, group } = this.state;

    return (
      <div>
        <Form.Dropdown
          placeholder="Pesquise um Grupo"
          fluid
          search
          selection
          scrolling
          multiple
          allowAdditions
          additionLabel="Adicionar: "
          name="group"
          value={this.props.groups}
          onSearchChange={this.onSearchChange}
          onChange={(e, { value }) => this.props.onSelectGroup(value)}
          options={options}
          onAddItem={this.toggle}
          closeOnChange={!!this.props.closeOnChange}
          loading={loading}
          clearable
        />
        <AddGroupModal
          open={openModal}
          group={group}
          save={this.submit}
          toggle={this.toggle}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default DropdownGroups;
