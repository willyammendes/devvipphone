import React, { Component, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import api from "../../api/group";
import DataTable from "../table/DataTable";
import ContactsGroupsModal from "./modal/ContactsGroupsModal";
import AlertSuccess from "../alerts/AlertSuccess";
import ImportContacts from "./ImportContacts";

class ContactsGroups extends Component {
  state = {
    records: [],
    columns: {},
    tmp_file: "",
    contactColumns: [],
    selectedColumns: {
      name_column: "",
      phone_number_column: "",
      email_column: "",
      neighborhood_column: "",
      city_column: "",
      address_column: "",
      groups_import: [],
    },
    order: {},
    file: [
      {
        file_name: "",
        tmp_file: "",
      },
    ],
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    contactsOpen: false,
    group_action: true,
    groupId: {},
    importModal: false,
    editContact: false,
  };

  select = (selectedDataId) => {
    const { records } = this.state;
    const dataIndex = records.findIndex((c) => c.id === selectedDataId);
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  selectContact = (selectedDataId) => {
    const { records } = this.state;
    const dataIndex = records.findIndex((c) => c.id === selectedDataId);
    this.setState({
      selectedDataIndex: dataIndex,
      editModalOpen: true,
      editContact: true,
    });
  };

  newDataClick = (selectedDataId) => {
    const { records } = this.state;
    const { company_id } = this.props;
    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              company_id,
              group_contacts: {
                contact_id: "",
              },
            }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
          company_id,
          group_contacts: {
            contact_id: "",
          },
        };
    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;
    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });
    return await api.group.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        contactsOpen: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.group
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  fetchListContact = async (selectedDataId, params) => {
    this.setState({
      loading: true,
      contactsOpen: true,
      groupId: selectedDataId,
    });
    return await api.group
      .fetchListContacts(selectedDataId, params)
      .then((res) => {
        this.setState({
          records: res.data,
          order: res.order,
          columns: res.columns,
          total_records: res.total_records,
          loading: false,
        });
      });
  };

  fetchListContactGroup = async (params) => {
    this.setState({
      loading: true,
    });
    const { groupId } = this.state;
    api.group.fetchListContacts(groupId, params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  handleContactChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          group_contacts: { contact_id: value },
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  submitContacts = async (selectedDataId) => {
    const { selectedDataIndex: dataIndex, records, groupId } = this.state;
    this.setState({
      loading: true,
    });
    const filterID = records.filter((c) => !!c.id);
    const sendData = {
      contacts: records[dataIndex].group_contacts.contact_id.concat(
        filterID.map((c) => c.id)
      ),
    };
    return api.group
      .submitContacts(groupId, sendData)
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
        });
        this.fetchListContact(groupId);
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    api.group
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  updateContact = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    api.group
      .updateContact(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  delete = (id) => api.group.delete(id);

  deleteContact = async (id) => {
    const { groupId } = this.state;
    api.group.deleteContact(groupId, id);
  };

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  addContactsToGroup = () => {
    const { records, selectedDataIndex, groupId } = this.state;

    api.group
      .addContacts(groupId, {
        contacts: records[selectedDataIndex].group_contacts.contact_id,
      })
      .then((res) => {
        this.setState({
          save_alert: res.status === "success" ? true : false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      });
  };

  importContacts = () => {
    this.setState({
      importModal: true,
    });
  };
  closeImport = () => {
    this.setState({
      importModal: false,
      contactColumns: [],
    });
  };
  onSelectName = (e, { name, value }) => {
    this.setState({
      selectedColumns: { ...this.state.selectedColumns, [name]: value },
    });
  };
  saveContact = () => {
    const dataSend = {
      data: this.state.image,
      ext: this.state.file[0].file_name.split(".")[
        this.state.file[0].file_name.split(".").length - 1
      ],
      actual_file: this.state.file[0].tmp_file,
    };
    return api.group.importContact(dataSend).then((data) => {
      const options = data.columns.map((c) => ({
        text: c,
        value: c,
        key: c,
      }));

      this.setState({
        contactColumns: options,
        selectedColumns: {
          tmp_file: data.tmp_file,
          ...this.state.selectedColumns,
        },
      });
    });
  };

  importContactsPost = () => {
    const { groupId } = this.state;
    api.group.sendContacts(groupId, this.state.selectedColumns).then((data) => {
      this.setState({
        save_alert: data.status === "success" ? true : false,
        importModal: false,
      });
      setTimeout(
        function() {
          this.setState({ save_alert: false });
        }.bind(this),
        5000
      );
    });
  };

  BasicUpload = (props) => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16,
    };
    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box",
    };
    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden",
    };
    const img = {
      display: "block",
      width: "auto",
      height: "100%",
    };
    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback((acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      this.setState({
        file: acceptedFiles.map((file) => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document",
          tmp_file: URL.createObjectURL(file),
        })),
      });
      acceptedFiles.forEach((file) => {
        const reader = new FileReader();
        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr,
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const { getRootProps, getInputProps } = useDropzone({
      accept: ".xls, .xlsx",
      onDrop,
    });
    const thumbs = files.map((file) => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));
    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach((file) => URL.revokeObjectURL(file.preview));
      },
      [files]
    );
    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>Arraste seu arquivo, ou clique aqui para seleciona-lo.</p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };
  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      group_action,
      contactsOpen,
      editContact,
      importModal,
      contactColumns,
    } = this.state;
    const group = records[selectedDataIndex];
    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Grupos de Contatos</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onDeleteContact={(id) => this.deleteContact(id)}
                onEditClick={(d) => this.select(d.id)}
                onEditContact={(d) => this.selectContact(d.id)}
                onGroupClick={
                  contactsOpen
                    ? this.fetchRecords
                    : (d) => this.fetchListContact(d.id)
                }
                group_action={group_action}
                importContacts={this.importContacts}
                contactsOpen={contactsOpen}
                fetchData={
                  contactsOpen ? this.fetchListContactGroup : this.fetchRecords
                }
              />
              <ImportContacts
                open={importModal}
                BasicUpload={this.BasicUpload}
                file={this.state.file[0]}
                loading={loading}
                handleClose={this.closeImport}
                save={this.saveContact}
                submit={this.importContactsPost}
                columns={contactColumns}
                onSelectName={this.onSelectName}
                selectedColumns={this.state.selectedColumns}
              />
              {selectedDataIndex !== -1 ? (
                <ContactsGroupsModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  onAddGroup={this.addContactsToGroup}
                  group={group}
                  contactsOpen={contactsOpen}
                  editContact={editContact}
                  onEditContact={this.updateContact}
                  handleContactChange={this.handleContactChange}
                  modalHeader={
                    group.id
                      ? `Edição do ${group.name}`
                      : "Novo Grupo de Contatos"
                  }
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ContactsGroups.propTypes = {
  company_id: PropTypes.any.isRequired,
};

const mapStateToProps = (state) => ({
  company_id: state.user.user.company_id,
});

export default connect(mapStateToProps)(ContactsGroups);
