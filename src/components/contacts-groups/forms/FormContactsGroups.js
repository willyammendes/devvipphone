import React from "react";
import { Form, Input, TextArea, Checkbox } from "semantic-ui-react";
import DropdownContacts from "../../contact/DropdownContacts";

const FormContactsGroups = ({
  group,
  onChange,
  onChecked,
  contactsOpen,
  handleContactChange,
  editContact,
}) => {
  return (
    <Form>
      {editContact ? (
        <span>
          <Form.Group widths="equal">
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Nome"
              name="name"
              onChange={onChange}
              value={group.name}
            />
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Email"
              name="email"
              onChange={onChange}
              value={group.email}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Celular"
              name="phone_number"
              onChange={onChange}
              value={group.phone_number}
            />
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Cidade"
              name="city"
              onChange={onChange}
              value={group.city}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Bairro"
              name="neighborhood"
              onChange={onChange}
              value={group.neighborhood}
            />
            <Form.Field
              autoComplete="off"
              control={Input}
              label="CEP"
              name="code"
              onChange={onChange}
              value={group.code}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field
              autoComplete="off"
              control={Input}
              label="Endereço"
              name="address"
              onChange={onChange}
              value={group.address}
            />
            <div className="field">
              <label>Webhook</label>
              <Checkbox
                label="Habilitado"
                onChange={onChecked}
                name="webhook_active"
                checked={group.webhook_active}
              />
            </div>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field
              control={TextArea}
              label="Informações Adicionais"
              name="additional_info"
              onChange={onChange}
              value={group.additional_info}
            />
            <div className="field" />
          </Form.Group>
        </span>
      ) : (
        <span>
          {contactsOpen ? (
            <DropdownContacts
              onSelectContact={handleContactChange}
              contact_id={group.group_contacts.contact_id}
              multiple
              name="contact_id"
            />
          ) : (
            <span>
              <Form.Field
                autoComplete="off"
                control={Input}
                label="Nome"
                placeholder="Nome"
                name="name"
                onChange={onChange}
                value={group.name}
              />
              <label>
                <strong>Contatos</strong>
              </label>
              <DropdownContacts
                onSelectContact={handleContactChange}
                contact_id={
                  group.group_contacts
                    ? group.group_contacts.contact_id
                    : group.contacts.map((c) => c.id)
                }
                options={group.contacts}
                multiple
                name="contact_id"
              />

              <Form.Field
                control={TextArea}
                label="Descrição"
                placeholder="Descrição"
                name="description"
                onChange={onChange}
                value={group.description}
              />
            </span>
          )}
        </span>
      )}
    </Form>
  );
};

export default FormContactsGroups;
