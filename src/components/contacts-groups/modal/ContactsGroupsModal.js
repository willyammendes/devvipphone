import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormContactsGroups from "../forms/FormContactsGroups";

class ContactsGroupsModal extends React.Component {
  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", e => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.group.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      group,
      handleClose,
      onChange,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      contactsOpen,
      onChecked,
      onAddGroup,
      onEditContact,
      handleContactChange,
      editContact
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        {contactsOpen ? (
          <Modal.Header>Contato do Grupo de Contatos</Modal.Header>
        ) : (
          <Modal.Header>{modalHeader}</Modal.Header>
        )}

        <Modal.Content>
          <Modal.Description>
            <div className="form-group">
              <FormContactsGroups
                group={group}
                editContact={editContact}
                onChange={onChange}
                onChecked={onChecked}
                contactsOpen={contactsOpen}
                handleContactChange={handleContactChange}
              />
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            {editContact ? (
              <Button
                positive
                onClick={onEditContact}
                loading={loading}
                disabled={loading}
              >
                Salvar
              </Button>
            ) : (
              <span>
                {contactsOpen ? (
                  <Button
                    positive
                    onClick={onAddGroup}
                    loading={loading}
                    disabled={loading}
                  >
                    Salvar
                  </Button>
                ) : (
                  <Button
                    positive
                    onClick={this.save}
                    loading={loading}
                    disabled={loading}
                  >
                    Salvar
                  </Button>
                )}
              </span>
            )}
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ContactsGroupsModal;
