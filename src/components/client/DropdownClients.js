import React, { Component } from "react";
import api from "../../api/client";
import { Dropdown } from "semantic-ui-react";

class DropdownClients extends Component {
  state = {
    client: {
      name: ""
    },
    options: [],
    loading: false,
    search: "",
    openModal: false
  };

  componentWillMount() {
    api.client.fetchAll({ take: 50 }).then(data => {
      this.setState({
        options: data.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),

        loading: false
      });
    });
  }

  handleClientsAddition = e => {};

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({ search: searchQuery });
    this.timer = setTimeout(this.onSearchClients, 300);
  };

  componentDidMount() {}

  onSearchClients = async () => {
    this.setState({ loading: true });
    const { search } = this.state;

    await api.client.fetchAll({ search }).then(client => {
      this.setState({
        options: client.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loading: false
      });
    });
  };

  onChange = (e, { name, value }) => {
    this.setState(state => ({
      client: { ...state.client, [name]: value }
    }));
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { client } = this.state;

    this.setState({
      loading: true
    });

    return api.client
      .submit(client)
      .then(data => {
        this.setState(state => ({
          options: [...state.options].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          loading: false,
          client: {
            name: "",
            description: ""
          },
          openModal: !state.openModal
        }));

        this.props.onSelectClient(data.data.id);
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  toggle = (e, { value }) => {
    this.setState(state => ({
      openModal: !state.openModal,
      client: {
        name: value,
        groups: [],
        tickets: []
      }
    }));
    this.submit();
  };

  render() {
    const { options, loading } = this.state;

    return (
      <div>
        <Dropdown
          placeholder="Pesquise um Cliente"
          fluid
          search
          selection
          scrolling
          allowAdditions={true}
          additionLabel="Adicionar: "
          name="clients"
          value={this.props.clients}
          onSearchChange={this.onSearchChange}
          onChange={(e, { value }) => this.props.onSelectClient(value)}
          options={options}
          onAddItem={this.toggle}
          closeOnChange
          loading={loading}
          clearable
        />
      </div>
    );
  }
}

export default DropdownClients;
