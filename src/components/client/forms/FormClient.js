import React from "react";
import { Form, Input, Divider, Button, Dropdown } from "semantic-ui-react";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const FormClient = ({
  client,
  onChange,
  handleExtraFields,
  error,
  options,
  cleanSearch,
  handleSearch,
  onAddItem,
}) => {
  return (
    <Form>
      <Form.Group widths="equal">
        {window.location.pathname === "/client" ? (
          <Form.Field
            autoComplete="off"
            control={Input}
            name={"name"}
            value={client.name}
            label={"Nome:"}
            onChange={onChange}
          />
        ) : (
          <Form.Field
            autoComplete="off"
            control={Dropdown}
            options={options}
            onSearchChange={handleSearch}
            name={"name"}
            label={"Nome:"}
            selection
            search
            onAddItem={onAddItem}
            clearable
            allowAdditions
            text={client.name ? client.name : false}
            defaultValue={client.name ? client.name : ""}
            onChange={onChange}
            error={
              error && {
                content: "O nome é obrigatório",
                pointing: "below",
              }
            }
          />
        )}

        <Form.Field
          autoComplete="off"
          control={Input}
          name={"address"}
          value={client.address}
          label={"Endereço:"}
          onChange={onChange}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          name={"city"}
          value={client.city}
          label={"Cidade:"}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          name={"state"}
          value={client.state}
          label={"Estado:"}
          onChange={onChange}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          name={"zip_code"}
          value={client.zip_code}
          label={"CEP"}
          onChange={onChange}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          name={"number"}
          value={client.number}
          label={"Número:"}
          onChange={onChange}
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Celular:"
          name="phone_number"
          onChange={onChange}
          value={client.phone_number}
        />

        <Form.Field
          autoComplete="off"
          control={Input}
          label="Bairro:"
          name="neighborhood"
          onChange={onChange}
          value={client.neighborhood}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Email:"
          name="email"
          onChange={onChange}
          value={client.email}
        />
      </Form.Group>

      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="CPF/CNPJ:"
          name="document"
          onChange={onChange}
          value={client.document}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Código externo:"
          name="code"
          onChange={onChange}
          value={client.code}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Informações adicionais:"
          name="additional_info"
          onChange={onChange}
          value={client.additional_info}
        />
      </Form.Group>

      <Divider />
      {client && client.extra_fields && (
        <ExtraFieldForm
          className="input-client"
          extra_fields={client.extra_fields}
          onChange={handleExtraFields}
        />
      )}
    </Form>
  );
};

export default FormClient;
