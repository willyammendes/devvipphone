import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormClient from "../forms/FormClient";

class ClientModal extends React.Component {
  save = () => {
    if (this.props.client.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      client,
      handleClose,
      onChange,
      open,
      error,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      handleExtraFields,
      options,
      cleanSearch,
      handleSearch,
      onAddItem,
    } = this.props;

    return (
      <Modal size="large" closeIcon open={open} onClose={handleClose}>
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="form-client">
              <FormClient
                error={error}
                client={client}
                handleExtraFields={handleExtraFields}
                onChange={onChange}
                loading={loading}
                options={options}
                cleanSearch={cleanSearch}
                handleSearch={handleSearch}
                onAddItem={onAddItem}
              />
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ClientModal;
