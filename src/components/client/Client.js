import React, { Component } from "react";
import api from "../../api/client";
import DataTable from "../table/DataTable";
import ClientModal from "./modal/ClientModal";
import AlertSuccess from "../alerts/AlertSuccess";

class Client extends Component {
  state = {
    records: [
      {
        data: [
          {
            address: "",
            name: "",
            street_address: "",
            number: "",
            city: "",
            state: "",
            zip_code: "",
            googleMapLink: ""
          }
        ]
      }
    ],
    extra_fields: [],
    errors: "",
    columns: {},
    order: {},
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false
  };

  select = selectedDataId => {
    const { records } = this.state;
    const dataIndex = records.findIndex(c => c.id === selectedDataId);
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce((o, key) => {
          return Object.assign(o, {
            [key]: Array.isArray(records[0][key])
              ? key === "extra_fields"
                ? this.state.extra_fields
                : []
              : ""
          });
        }, {})
      : {
          event_type: "",
          extra_fields: this.state.extra_fields,
          url: "",
          headers: "",
          headers_value: ""
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.client.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        extra_fields: res.extra_fields,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    const extraFieldIndex = records[dataIndex].extra_fields.findIndex(
      c => c.id === extraFieldId
    );

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          extra_fields: [
            ...records[dataIndex].extra_fields.slice(0, extraFieldIndex),
            {
              ...records[dataIndex].extra_fields[extraFieldIndex],
              value: rating !== undefined ? rating : value
            },
            ...records[dataIndex].extra_fields.slice(extraFieldIndex + 1)
          ]
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    const send = {
      ...records[dataIndex],
      groups:
        records[dataIndex].groups.length > 0 ? records[dataIndex].groups : [],
      tickets:
        records[dataIndex].tickets.length > 0 ? records[dataIndex].tickets : []
    };
    return api.client
      .submit(send)
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    const send = {
      ...records[selectedDataIndex],
      groups:
        records[selectedDataIndex].groups.length > 0
          ? records[selectedDataIndex].groups
          : [],
      tickets:
        records[selectedDataIndex].tickets.length > 0
          ? records[selectedDataIndex].tickets
          : []
    };
    api.client
      .update(data.id, send)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.client.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen
    } = this.state;

    const client = records[selectedDataIndex];

    this.state.extra_fields.forEach(e => {
      if (e.show_column_on_view) columns[e.input_name] = e.label;
    });

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Clientes</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records}
                extra_fields={this.state.extra_fields.map(e => e.input_name)}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={id => this.delete(id)}
                onEditClick={d => this.select(d.id)}
                fetchData={this.fetchRecords}
              />

              {selectedDataIndex !== -1 ? (
                <ClientModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleExtraFields={this.handleExtraFields}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  client={client}
                  modalHeader={
                    client.id
                      ? `Edição do Cliente ${client.name}`
                      : "Novo Cliente"
                  }
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Client;
