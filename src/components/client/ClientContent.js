import React from "react";
import Popover from "../semantic/Popover";

const ClientContent = ({ client }) => (
  <nav>
    <ul>
      <li>
        <div className="dataEditar" key={client.id}>
          <b>CNPJ:</b>
          <Popover value={client.cnpj}>Número do CNPJ</Popover>
          <span>{client.cnpj}</span>
        </div>
      </li>
      <li>
        <div className="dataEditar" key={client.id}>
          <b>Telefone:</b>
          <Popover value={client.phone_number}>Número do Telefone</Popover>
          <span>{client.phone_number}</span>
        </div>
      </li>
    </ul>
  </nav>
);

export default ClientContent;
