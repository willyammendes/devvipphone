import React from "react";
import { Form, Input, Dropdown } from "semantic-ui-react";

const FormRestrictedContacts = ({
    restrictedcontact,
    onChange,
    onChecked,
    clients,
    contacts,
    onSelectGroup,
    onSelectClient,
    handleGroupAddition,
    onSearchClientChange,
    loadingGroups,
    loadingClients
}) => {
    return (
        <Form>
            {restrictedcontact.id ? (
                <Form.Group widths="equal">
                    <Form.Field
                        autoComplete="off"
                        control={Input}
                        label="Nome"
                        name="name"
                        onChange={onChange}
                        value={restrictedcontact.name}
                    />
                    <Form.Field
                        autoComplete="off"
                        control={Input}
                        label="Email"
                        name="email"
                        onChange={onChange}
                        value={restrictedcontact.email}
                    />
                </Form.Group>
            ) : (
                ""
            )}

            <Form.Group widths="equal">
                {restrictedcontact.id ? (
                    <Form.Field
                        autoComplete="off"
                        control={Input}
                        label="Celular"
                        name="phone_number"
                        onChange={onChange}
                        value={restrictedcontact.phone_number}
                    />
                ) : (
                    ""
                )}
                <div className="field">
                    <label>Adicionar Contato Restrito</label>
                    <Dropdown
                        placeholder="Contatos"
                        fluid
                        search
                        multiple
                        selection
                        scrolling
                        name="contacts"
                        value={restrictedcontact.contacts}
                        onChange={onSelectGroup}
                        options={contacts}
                        closeOnChange
                        loading={loadingGroups}
                        clearable
                    />
                </div>
            </Form.Group>
        </Form>
    );
};

export default FormRestrictedContacts;
