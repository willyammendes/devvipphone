import React from "react";
import { Modal, Button, Icon, Image } from "semantic-ui-react";
import FormRestrictedContacts from "../forms/FormRestrictedContacts";

class RestrictedContactsModal extends React.Component {
  componentDidMount() {
    if (this.props.showNavigation) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", e => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.restrictedcontact.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      restrictedcontact,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      onSearchClientChange,
      onSelectClient,
      onSelectGroup,
      contacts,
      clients,
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      handleGroupAddition,
      loadingGroups,
      loadingClients,
      loading,
      showNavigation = true
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_restrictedcontact">
              <div className="form-restrictedcontact">
                <FormRestrictedContacts
                  restrictedcontact={restrictedcontact}
                  contacts={contacts}
                  clients={clients}
                  onChange={onChange}
                  onChecked={onChecked}
                  onSelectGroup={onSelectGroup}
                  onSelectClient={onSelectClient}
                  onSearchClientChange={onSearchClientChange}
                  handleGroupAddition={handleGroupAddition}
                  loadingGroups={loadingGroups}
                  loadingClients={loadingClients}
                />
              </div>
              {restrictedcontact.id ? (
                <div className="restrictedcontact-image">
                  <Image
                    src={`${restrictedcontact.avatar}`}
                    size="small"
                    circular
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {showNavigation && (
            <Button.Group>
              <Button
                icon
                onClick={handlePrevious}
                disabled={previousButtonEnabled}
              >
                <Icon name="left arrow" />
              </Button>
              <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
                <Icon name="right arrow" />
              </Button>
            </Button.Group>
          )}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default RestrictedContactsModal;
