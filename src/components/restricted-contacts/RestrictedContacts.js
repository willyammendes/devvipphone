import React, { Component } from "react";
import api from "../../api/restricted-contacts";
import apicontact from "../../api/contact";
import apiclient from "../../api/client";
import DataTable from "../table/DataTable";
import RestrictedContactsModal from "./modal/RestrictedContactsModal";
import AlertSuccess from "../alerts/AlertSuccess";

class RestrictedContacts extends Component {
	state = {
		records: [],
		columns: {},
		clients: [],
		errors: "",
		order: {},
		loading: false,
		contacts: [],
		total_records: 0,
		selectedDataIndex: -1,
		editModalOpen: false,
		loadingGroups: false,
		loadingClients: false,
		save_alert: false
	};

	select = selectedDataId => {
		const { contacts, clients } = this.state;
		const dataIndex = this.state.records.findIndex(
			c => c.id === selectedDataId
		);
		const data = this.state.records[dataIndex];

		this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

		if (contacts.length === 0) this.fetchGroups();
		if (
			!!data.client_id &&
			clients.filter(c => c.key === data.client_id).length === 0
		)
			this.getClient(data.client_id);
	};

	newDataClick = () => {
		const { records, contacts } = this.state;

		if (contacts.length === 0) this.fetchGroups();

		const newData = records[0]
			? Object.keys(records[0]).reduce(
					(o, key) =>
						Object.assign(o, {
							[key]: ""
						}),
					{}
			  )
			: {};

		newData.contacts = [];
		newData.webhook_active = true;
		newData.send_campaing = true;

		this.setState(
			{
				records: [...records].concat(newData),
				editModalOpen: true
			},
			() => {
				this.setState({
					selectedDataIndex: this.state.records.length - 1
				});
			}
		);
	};

	nextRecord = () => {
		const selectedDataIndex =
			this.state.selectedDataIndex < this.state.records.length - 1
				? this.state.selectedDataIndex + 1
				: this.state.records.length - 1;
		const selectedDataId = this.state.records[selectedDataIndex].id;

		this.select(selectedDataId);
	};

	previousRecord = () => {
		const selectedDataIndex =
			this.state.selectedDataIndex - 1 > 0
				? this.state.selectedDataIndex - 1
				: 0;
		const selectedDataId = this.state.records[selectedDataIndex].id;

		this.select(selectedDataId);
	};

	onSelectClient = (e, { name, value }) => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					[name]: value
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	onSearchClientChange = e => {
		clearTimeout(this.timer);
		this.setState({ searchClient: e.target.value });
		this.timer = setTimeout(this.onSearchClient, 300);
	};

	onSearchClient = () => {
		const { searchClient } = this.state;

		if (searchClient.length > 2) {
			this.setState({ fetchingClients: true });

			apiclient.client.search(searchClient).then(clients => {
				this.setState({
					clients: [...this.state.clients].concat(
						clients.map(c => ({
							key: c.id,
							value: c.id,
							text: c.name
						}))
					),
					fetchingClients: false
				});
			});
		}
	};

	onSelectGroup = (e, { name, value }) => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					contacts: value
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	addGroup = (e, { value }) => {
		const { selectedDataIndex: dataIndex, records } = this.state;
		this.setState({ loadingGroups: true });

		apicontact.contact
			.submit({ name: value })
			.then(data => {
				this.setState({
					contacts: [...this.state.contacts].concat({
						key: data.data.id,
						value: data.data.id,
						text: data.data.name
					}),
					records: [
						...records.slice(0, dataIndex),
						{
							...records[dataIndex],
							contacts: [...records[dataIndex].contacts].concat(
								data.data.id
							)
						},
						...records.slice(dataIndex + 1)
					]
				});
			})
			.then(() => {
				this.setState({ loadingGroups: false });
			});
	};

	fetchRecords = async params => {
		this.setState({ loading: true });

		return await api.restrictedcontact.fetchAll(params).then(res => {
			this.setState({
				records: res.data,
				order: res.order,
				columns: res.columns,
				total_records: res.total_records,
				loading: false
			});
		});
	};

	fetchGroups = async () => {
		this.setState({ loadingGroups: true });
		await apicontact.contact.fetchAll().then(contacts => {
			this.setState({
				contacts: contacts.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.name
				})),
				loadingGroups: false
			});
		});
	};

	getClient = async id => {
		this.setState({ loadingClients: true });
		await apiclient.client.get(id).then(client => {
			this.setState({
				clients: [...this.state.clients].concat({
					key: client.id,
					value: client.id,
					text: client.name
				}),
				loadingClients: false
			});
		});
	};

	componentWillMount() {
		this.fetchRecords();
	}

	handleChange = e => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					[e.target.name]: e.target.value
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	handleChecked = (e, { name, checked }) => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					[name]: checked
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { selectedDataIndex: dataIndex, records } = this.state;
		this.setState({
			loading: true
		});
		return api.restrictedcontact
			.submit(records[dataIndex])
			.then(data => {
				this.setState({
					save_alert: true,
					loading: false,
					selectedDataIndex: -1,
					editModalOpen: false,
					records: [
						...records.slice(0, dataIndex),
						data.contact,
						...records.slice(dataIndex + 1)
					]
				});
				setTimeout(
					function() {
						this.setState({ save_alert: false });
					}.bind(this),
					5000
				);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
			});
	};

	update = () => {
		const { selectedDataIndex, records } = this.state;
		const data = records[selectedDataIndex];
		this.setState({ loading: true });

		api.restrictedcontact.update(data.id, data).then(data => {
			this.setState({
				records: [
					...records.slice(0, selectedDataIndex),
					{ ...data.data },
					...records.slice(selectedDataIndex + 1)
				],
				save_alert: true,
				editModalOpen: false,
				selectedDataIndex: -1,
				loading: false
			});
			setTimeout(
				function() {
					this.setState({ save_alert: false });
				}.bind(this),
				5000
			);
		});
	};
	delete = id => {
		const { selectedDataIndex: dataIndex, records } = this.state;
		this.setState({
			loading: true
		});
		const contacts = { contacts: [id], webhook_active: false };
		return api.restrictedcontact
			.deletesubmit(contacts)
			.then(data => {
				this.setState({
					save_alert: true,
					loading: false,
					selectedDataIndex: -1,
					editModalOpen: false,
					records: [
						...records.slice(0, dataIndex),
						data.contact,
						...records.slice(dataIndex + 1)
					]
				});
				setTimeout(
					function() {
						this.setState({ save_alert: false });
					}.bind(this),
					5000
				);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
			});
	};

	handleCloseEditModal = () => {
		const { records } = this.state;
		this.setState({
			editModalOpen: false,
			selectedId: -1,
			selectedDataIndex: -1,
			records: [...records].filter(c => c.id > 0)
		});
	};

	render() {
		const {
			records,
			loading,
			columns,
			total_records,
			selectedDataIndex,
			editModalOpen,
			contacts,
			clients,
			loadingGroups,
			loadingClients
		} = this.state;

		const restrictedcontact = this.state.records[selectedDataIndex];

		return (
			<div className="pageHolder">
				<div className="holderPage">
					<div className="full">
						<div className="tabela-padrao tabela_restritos">
							<div className="headingPage">
								<h1>Contatos restritos</h1>
								{this.state.save_alert && <AlertSuccess />}
							</div>
							<DataTable
								loading={loading}
								onAddClick={this.newDataClick}
								columns={columns}
								data={records}
								totalRecords={total_records}
								messageError={
									this.state.errors.status > 0
										? this.state.errors.data.message
										: ""
								}
								onDelete={id => this.delete(id)}
								onEditClick={d => this.select(d.id)}
								fetchData={this.fetchRecords}
							/>

							{selectedDataIndex !== -1 ? (
								<RestrictedContactsModal
									handleClose={this.handleCloseEditModal}
									onChange={this.handleChange}
									onChecked={this.handleChecked}
									handleNext={this.nextRecord}
									handlePrevious={this.previousRecord}
									onClickSave={this.update}
									onClickAdd={this.submit}
									onSelectGroup={this.onSelectGroup}
									onSelectClient={this.onSelectClient}
									onSearchClientChange={
										this.onSearchClientChange
									}
									handleGroupAddition={this.addGroup}
									restrictedcontact={restrictedcontact}
									modalHeader={
										restrictedcontact.id
											? `Edição do ${
													restrictedcontact.name
											  }`
											: `Novo Contato Restrito`
									}
									open={editModalOpen}
									previousButtonEnabled={
										selectedDataIndex === 0
									}
									nextButtonEnabled={
										selectedDataIndex === records.length - 1
									}
									contacts={contacts}
									clients={clients}
									loading={loading}
									loadingGroups={loadingGroups}
									loadingClients={loadingClients}
								/>
							) : null}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default RestrictedContacts;
