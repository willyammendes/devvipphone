import React, { Component } from "react";
import placeholder from "../../assets/img/placeholder.png";
import { Popup, Icon, Progress, Button } from "semantic-ui-react";
import moment from "moment";
import "moment/locale/pt-br";

class Comment extends Component {
  state = {};
  render() {
    return (
      <div
        className={
          (this.props.comment.source === "user_comment" ||
            this.props.comment.source === "comment") &&
          (this.props.comment.icon || this.props.comment.forwardedTo.id)
            ? "comment"
            : "comment comment_box"
        }
        style={{
          marginLeft: this.props.comment.forwardedTo.id ? "95px" : "",
          width: this.props.comment.forwardedTo.id ? "89%" : ""
        }}
      >
        <div
          className="origem_comment"
          style={{
            marginLeft: this.props.comment.forwardedTo.id ? "-60px" : ""
          }}
        >
          <div className="origem">
            {this.props.comment.avatar ? (
              <Popup
                content={this.props.comment.user}
                position="top left"
                size="mini"
                trigger={<img src={this.props.comment.avatar} alt="Usuário" />}
              />
            ) : (
              <img src={placeholder} alt={this.props.comment.user} />
            )}
          </div>
        </div>
        {this.props.comment.forwardedTo.id && <Icon name="mail forward" />}
        {this.props.comment.forwardedTo.id && (
          <div
            className="origem_comment"
            style={{
              marginLeft: this.props.comment.forwardedTo.id ? "0px" : ""
            }}
          >
            <div className="origem">
              {this.props.comment.avatar ? (
                <Popup
                  content={this.props.comment.forwardedTo.name}
                  position="top left"
                  size="mini"
                  trigger={
                    <img
                      src={this.props.comment.forwardedTo.avatar}
                      alt={this.props.comment.forwardedTo.name}
                    />
                  }
                />
              ) : (
                <img src={placeholder} alt={this.props.comment.user} />
              )}
            </div>
          </div>
        )}

        <div
          className={`box_comment ${
            this.props.comment.forwardedTo.id ? "forward" : ""
          }`}
        >
          <div
            className="box"
            title={`${
              this.props.comment.forwardedTo.id
                ? `Ticket encaminhado de ${ 
                  this.props.comment.user 
                  } para ${ 
                  this.props.comment.forwardedTo.name}`
                : `Novo Comentário de ${  this.props.comment.user}`
            }`}
          >
            {this.props.comment.forwardedTo.id ? (
              <span className="comment_ticket forward-comment">
                Ticket encaminhado de <b>{this.props.comment.user}</b> para{" "}
                <b>{this.props.comment.forwardedTo.name}</b>
              </span>
            ) : (
              ""
            )}

            {this.props.comment.source === "user_comment" &&
            this.props.comment.icon ? (
              ""
            ) : this.props.comment.source === "user_comment" &&
              !this.props.comment.forwardedTo.id ? (
              <span className="comment_ticket">
                <b>{this.props.comment.user}</b> comentou o ticket{" "}
                <b>{this.props.comment.ticket_number}</b>
              </span>
            ) : (
              ""
            )}

            {this.props.comment.message}
          </div>
          <h4 className="comment-title">
            <span className="data">
              {moment
                .unix(this.props.comment.timestamp)
                .locale("pt-br")
                .format("lll")}
            </span>
          </h4>
          {this.props.comment.icon ? (
            <div className="progresso_holder">
              <Progress
                className="progress_body"
                percent={this.props.comment.progress_percentage}
                title={`Progresso ${this.props.comment.progress_percentage}%`}
                indicating
                size="tiny"
              />
              <span className="progresso_desc">
                <Button icon={this.props.comment.icon} />O progresso desse
                ticket é de <b>{this.props.comment.progress_percentage}%</b>
              </span>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}
export default Comment;
