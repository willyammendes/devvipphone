import React, { Component } from "react";
import {
	Card,
	Checkbox,
	Button,
	Form,
	Input,
	Message
} from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/parameters";

class SMS extends Component {
	state = {
		parameter: [],
		loading: false,
		hasMore: true
	};
	handleChange = (e, { value }) => this.setState({ value });

	componentWillMount() {
		this.setState({ loading: true });
		api.parameter.fetchAll().then(parameter => {
			this.setState({
				parameter,
				loading: false
			});
		});
	}

	render() {
		const { parameter, loading } = this.state;
		return (
			<div className="pageHolder">
				<div className="headingPage">
					<h1>SMS</h1>
				</div>
				{loading && (
					<div className="loading-conversa">
						<LoaderComponent />
					</div>
				)}
				<div className="holderPage">
					<div className="full">
						<Card fluid>
							<Card.Content>
								<Card.Header>Integração de SMS</Card.Header>
							</Card.Content>
							<Card.Content>
								<Form>
									<Form.Field
										control={Input}
										label="URL de SMS:"
										name="server"
										value={parameter.server}
									/>
									<Message>
										Coloque a mensagem, número e chave entre
										os sinais de porcentagem.
									</Message>
									<Checkbox label="Ignorar resposta do envio de SMS" />
									<Message>
										Utilizando o serviço de envio de SMS do
										IPBX IPPHONE o VipCRM recebe uma
										resposta padrão informando o status de
										envio. <br />
										Outros sistemas enviam respostas
										diferentes que não podem ser
										identificadas pelo VipCRM
									</Message>
									<Message color="red">
										Se o VipCRM usa o IPBX IPPHONE para
										enviar as mensagens então DESMARQUE esta
										opção, mas se você escolher outro
										sistema(outra URL) para enviar mensagens
										SMS então MARQUE esta opção
									</Message>
									<Form.Field
										control={Input}
										label="Chave de API de SMS:"
										placeholder="crm@vipphone.com.br"
										name="user"
										value="92GB9PL9"
									/>
									<Button color="blue">Salvar</Button>
									<Button color="white">Limpar</Button>
								</Form>
							</Card.Content>
						</Card>
					</div>
				</div>
			</div>
		);
	}
}

export default SMS;
