import React from "react";
import axios from "axios";
import Section from "./Section";
import PerfectScrollbar from "react-perfect-scrollbar";

class MenuAccordion extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: null,
      loading: true,
      error: null
    };
  }

  componentDidMount() {
    axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        const users = res.data;

        this.setState({
          users,
          loading: false,
          error: null
        });
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        });
      });
  }

  /*
   * Method to render the loading screen
   */
  renderLoading = () => {
    return (
      <div className="accordion-container">
        <h1 className="error">Loading...</h1>
      </div>
    );
  };

  renderError = () => {
    return <div>Something went wrong, Will be right back.</div>;
  };

  /*
   * Method to render the users with checkbox
   */
  renderusers() {
    const { users, loading, error } = this.state;

    /*
     * Calling the renderError() method, if there is any error
     */
    if (error) {
      this.renderError();
    }

    return (
      <div className="accordion-container">
        <div className="menu-holder">
          <PerfectScrollbar>
            {users.map(user => (
              <Section user={user} key={user.id} />
            ))}
          </PerfectScrollbar>
        </div>
      </div>
    );
  }

  render() {
    /*
     * Using destructuring to extract the 'error' from state.
     */

    const { loading } = this.state;
    return <div>{loading ? this.renderLoading() : this.renderusers()}</div>;
  }
}

export default MenuAccordion;
