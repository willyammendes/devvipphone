import React, { Component } from "react";
import PropTypes from "prop-types";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { Icon } from "semantic-ui-react";
import MenuContent from "../MenuOffside/MenuContent";
import logo from "../../assets/img/logo.png";

export class MenuOffside extends Component {
  state = {
    conversationsLoaded: "",
  };

  componentWillMount() {
    this.onInit(this.props);
  }

  onInit = (props) => {
    const filter = { filter: JSON.stringify([["archived", "=", 0]]) };

    props.conversations.length === 0 &&
      props.fetchAll(filter).then(() => {
        this.setState(
          {
            conversationsLoaded: this.props.conversation.data.map((c) => c.id),
          },
          () => {}
        );
      });
  };

  render() {
    const {
      user: { permissions = [] },
    } = this.props;

    const companyList = permissions.find((c) => c === "list-company");
    const editInvoices = permissions.find(
      (c) => c === "edit-company-pricing-plan"
    );
    const editCompany = permissions.find((c) => c === "edit-company");
    const ticketList = permissions.find((c) => c === "list-ticket");
    const campaignList = permissions.find((c) => c === "list-campaing");
    const relationshipList = permissions.find((c) => c === "list-relationship");
    const report = permissions.find((p) => p === "list-report");
    const menuConversation = permissions.find((p) => p === "list-conversation");
    const menuConfiguration = permissions.find(
      (p) => p === "list-configuration"
    );

    let navLinks = [
      {
        name: "",
        links: [
          { id: 1, title: "Início", url: "/", icon: "home" },
          {
            id: menuConversation ? 2 : "",
            title: menuConversation ? "Conversas" : "",
            url: menuConversation ? "/conversation" : "",
            icon: menuConversation ? "conversation" : "",
            conversations: true,
          },
          {
            id: relationshipList ? 4 : "",
            title: relationshipList ? "Relacionamento" : "",
            url: relationshipList ? "/relationship" : "",
            icon: relationshipList ? "handshake outline" : "",
          },
          {
            id: campaignList ? 5 : "",
            title: campaignList ? "Campanhas" : "",
            url: campaignList ? "/campaign" : "",
            icon: campaignList ? "bullhorn" : "",
          },
          {
            id: ticketList ? 6 : "",
            title: ticketList ? "Tickets" : "",
            url: ticketList ? "/tickets" : "",
            icon: ticketList ? "ticket alternate" : "",
          },
          {
            id: report ? 7 : "",
            title: report ? "Relatórios" : "",
            url: report ? "/report" : "",
            icon: report ? "newspaper outline" : "",
          },
          {
            id: menuConfiguration ? 8 : "",
            title: menuConfiguration ? "Configurações" : "",
            url: menuConfiguration ? "/configurations" : "",
            icon: menuConfiguration ? "settings" : "",
          },
          {
            id: companyList ? "3" : "",
            title: companyList ? "Administração" : "",
            url: companyList ? "/company" : "",
            icon: companyList ? "configure" : "",
          },
          {
            id: editCompany ? "3" : "",
            title: editCompany ? "Empresas" : "",
            url: editCompany ? "/companies" : "",
            icon: editCompany ? "building" : "",
          },
          {
            id: editInvoices ? 8 : "",
            title: editInvoices ? "Faturas" : "",
            url: editInvoices ? "/invoices" : "",
            icon: editInvoices ? "dollar" : "",
          },
        ],
      },
    ];

    function toggleClass() {
      const element = document.getElementsByName("MenuOffside");
      for (let i = 0; i < element.length; i += 1) {
        element[i].classList.toggle("off-side");
      }
    }

    function screenResize() {
      const w = window.innerWidth;

      if (w <= 768) {
        const element = document.getElementsByName("MenuOffside");
        for (let i = 0; i < element.length; i += 1) {
          element[i].classList.toggle("off-side-mobile");
        }
      }
    }

    return (
      <div
        className="MenuOffside"
        name="MenuOffside"
        onMouseOver={toggleClass}
        onMouseOut={toggleClass}
      >
        <Link to="/" className="logo">
          <img src={logo} alt="" />
        </Link>

        <div className="openMenu" onClick={screenResize}>
          <div className="iconSeta">
            <Icon name="chevron right" />
            <Icon name="chevron left" />
          </div>
        </div>
        <div className="holderNav">
          {navLinks.map((navLink) => (
            <div className="navConfig" key={`menu-offside-${navLink.id}`}>
              <MenuContent
                navLinks={navLink.links}
                conversationsLoaded={
                  this.props.conversations && this.props.conversations.length
                }
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

MenuOffside.propTypes = {
  conversation: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        tickets: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            user: PropTypes.any.isRequired,
          })
        ).isRequired,
      }).isRequired
    ),
  }).isRequired,
  activeConversationId: PropTypes.number.isRequired,
  conversationSelected: PropTypes.func.isRequired,
  fetchSearch: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

const mapStateToProps = ({ conversation, user, media }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
    conversation,
    media,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter ? conversation.filter : "",
    conversations: !conversation.data ? [] : Object.values(conversation.data),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuOffside);
