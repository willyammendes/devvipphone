import React, { Component } from "react";
import { Button } from "semantic-ui-react";

class ButtonAdd extends Component {
  render() {
    const { onAddClick, icon, title } = this.props;
    return (
      <Button
        circular
        icon={icon}
        size="mini"
        className="add-comment"
        title={title}
        onClick={onAddClick}
      />
    );
  }
}

export default ButtonAdd;
