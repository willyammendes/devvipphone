import React, { useCallback } from "react";
import TextareaAutosize from "react-autosize-textarea";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import PropTypes from "prop-types";
import { Checkbox } from "semantic-ui-react";
import QuickMessages from "../quick-messages/QuickMessages";
import api from "../../api/ticket";
import apimedia from "../../api/social-media";
import EmojiSelector from "./EmojiSelector";
import JSEMOJI from "emoji-js";
import MediaUploadButtons from "./MediaUploadButtons";
import WhatsappAccountSelect from "./WhatsappAccountSelect";
import Forward from "./Forward";
import AndroidButton from "./AndroidButton";
import LoaderComponent from "../semantic/Loading";
import EditTicket from "./EditTicket";
import NewTicket from "./NewTicket";
import NewTask from "./NewTask";
import ScheduleButton from "./ScheduleButton";
import $ from "jquery";
import { useDropzone } from "react-dropzone";
import AudioRecorder from "../audio-recorder/AudioRecorder";
import AlertSuccess from "../alerts/AlertSuccess";
import AlertError from "../alerts/AlertError";
import { Link } from "react-router-dom";

const jsemoji = new JSEMOJI();

class FormMessage extends React.Component {
  state = {
    message: "",
    conversations: [],
    scheduledMessage: "",
    date: "",
    time: "",
    dateTime: "",
    datesRange: "",
    sending: false,
    loading: true,
    comment: "",
    ticket: "",
    records: [],
    columns: {},
    order: {},
    images: [],
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    sendImageModalOpen: false,
    account_number: "",
    save_alert: false,
    media: "",
    schedule: "",
    openDocumentModal: false,
  };

  clickTest = () => {};

  scheduleAction = (e) => {
    this.setState({ schedule: e });
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSelectChanged = (e, { name, value }) => {
    this.setState({
      [name]: value,
    });
  };

  handleEnterAsSend = () => {
    this.props.toggleEnterAsSend();
  };

  onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
  }

  imageOnloadHandler = (e, file, ext) => {
    this.setState({
      file_name: file.name,
      ext,
      data: e.target.result,
      message_type: ext === "mp4" ? 1 : 4,
      type: ext === "mp4" ? "video" : "image",
    });
  };
  commentHandler = (e) => {
    this.setState({
      comment: "teste",
    });
  };
  videoOnloadHandler = (e, file, ext) => {
    this.setState({
      file_name: file.name,
      ext,
      data: e.target.result,
      message_type: 1,
      type: "video",
    });
  };
  documentOnloadHandler = (e, file, ext) => {
    this.setState({
      file_name: file.name,
      ext,
      data: e.target.result,
      message_type: 3,
      type: "document",
    });
  };
  clearTmpFile = () => {
    const obj = Object.assign({}, this.state);
    obj.file_name = "";
    obj.ext = "";
    obj.data = "";
    obj.message_type = 0;
    obj.type = "chat";
    obj.message = "";

    $("#document_name").val("");
    $("#img_preview").attr("src", "");
    $("#video_preview").attr("src", "");
    $("#img").attr("src", "");
    document.getElementById("document").value = "";
    document.getElementById("image").value = "";
    document.getElementById("video").value = "";
    this.setState(obj);
  };
  setEmoji = (e) => {
    jsemoji.init_env();
    jsemoji.img_set = "emojione";
    jsemoji.img_sets.emojione.path =
      "https://cdn.jsdelivr.net/emojione/assets/3.0/png/32/";
    jsemoji.supports_css = false;
    jsemoji.allow_native = true;
    jsemoji.replace_mode = "unified";
    const output3 = jsemoji.replace_colons(`:${e.name}:`);

    this.setState((state) => {
      return { message: state.message + output3 };
    });
    $("#textarea-monit").focus();
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.ticket.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  componentDidMount() {
    document.onpaste = (event) => {
      const items = (event.clipboardData || event.originalEvent.clipboardData)
        .items;
      for (const index in items) {
        const item = items[index];
        if (item.kind === "file") {
          const blob = item.getAsFile();
          const reader = new FileReader();
          reader.onload = (event) => {
            this.setState({
              data: event.target.result,
              sendImageModalOpen: true,
            });

            $("#img_preview")
              .fadeIn("fast")
              .attr("src", event.target.result);
          };
          reader.readAsDataURL(blob);
        }
      }
    };
  }

  componentWillMount() {
    this.fetchRecords();
    api.ticket.fetchAll().then((ticket) =>
      this.setState({
        ticket,
        loading: false,
        showInfo: true,
      })
    );

    apimedia.media.fetchAll().then((media) => {
      this.setState({
        media,
        account_number:
          media.data.find((c) => c.connected === 1 && c.can_reply === 1) &&
          media.data.find((c) => c.connected === 1 && c.can_reply === 1)
            .phone_number,
        loading: false,
        showInfo: true,
      });
    });
  }
  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.ticket
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          error: err.response.data,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);

        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  sendTextMessage = (e) => {
    e.preventDefault();

    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;
    if (this.state.message) {
      const message = {
        message: this.state.message,
        account_number: this.state.account_number,
        conversation_id: activeConversation.id,
        ticket_id: ticketId,
        human_date: "agora mesmo",
        source: "message",
        status: 0,
        timestamp: new Date()
          .getTime()
          .toString()
          .substring(0, 10),
        user: this.props.name,
        sender: 1,
        send_at: this.state.schedule,
        message_type: 0,
        message_token: [...Array(32)]
          .map((i) => (~~(Math.random() * 36)).toString(36))
          .join(""),
      };

      this.setState({
        message: "",
      });

      const hasCommentTag = message.message.indexOf("@comment") === 0;

      if (hasCommentTag) {
        const commentText = message.message.replace("@comment", "").trim();

        if (!!commentText) {
          const ticket_id = activeConversation.active_ticket_id;
          const ticket = activeConversation.tickets.find(
            (c) => c.id === ticket_id
          );
          const user_id = this.props.user_id;

          const comment = {
            user_id,
            ticket_id,
            path: 1,
            comment: commentText,
            source: "comment",
            comment_type: 0,
          };

          this.props
            .sendComment(comment, ticket.id, comment.user_id)
            .then(() => {
              this.setState({ message: "", loading: false });
            });
        }
      } else {
        this.props
          .sendTextMessage(conversationId, message)
          .then(() => {
            this.setState({ schedule: "", message: "", sending: false });
          })
          .catch((err) => {
            this.setState({
              error: err.response.data,
            });

            setTimeout(() => {
              this.setState({
                error: null,
              });
            }, 10000);
          });
      }
    }

    setTimeout(() => {
      const body = document.querySelector(".chat .scrollbar-container");
      const boxes = document.querySelector(".wrap_boxes");
      document.getElementById("image").value = "";
      body.scrollTo(0, boxes.scrollHeight);
    }, 100);
  };

  enableAutoReply = (e) => {
    e.preventDefault();
    this.setState({ sending: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;

    const autoReply = {
      auto_reply: 1,
      conversation_id: activeConversation.id,
    };

    this.props.enableAutoReply(conversationId, autoReply).then(() => {
      this.setState({ auto_reply: 1, sending: false });
    });
  };

  disableAutoReply = (e) => {
    e.preventDefault();
    this.setState({ sending: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;

    const autoReply = {
      auto_reply: 0,
      conversation_id: activeConversation.id,
    };

    this.props.enableAutoReply(conversationId, autoReply).then(() => {
      this.setState({ auto_reply: 0, sending: false });
    });
  };

  sendImage = (e) => {
    e.preventDefault();
    this.setState({ sending: true, loading: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );

    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;

    const message = {
      message: this.state.message,
      account_number: this.state.account_number,
      conversation_id: activeConversation.id,
      mime_type: "",
      status: 0,
      user: this.props.name,
      timestamp: new Date()
        .getTime()
        .toString()
        .substring(0, 10),
      source: "message",
      human_date: "agora mesmo",
      file_name: this.state.file_name,
      ext: this.state.ext,
      data: this.state.data,
      src: this.state.data,
      type: this.state.type,
      ticket_id: ticketId,
      sender: 1,
      message_type: 4,
      message_token: [...Array(32)]
        .map((i) => (~~(Math.random() * 36)).toString(36))
        .join(""),
    };

    this.setState({
      loading: false,
      sendImageModalOpen: false,
      sending: false,
    });

    this.props.sendImageMessage(conversationId, message).then(() => {
      this.setState({
        message: "",
        data: null,
      });
    });

    setTimeout(() => {
      const body = document.querySelector(".chat .scrollbar-container");
      const boxes = document.querySelector(".wrap_boxes");
      document.getElementById("image").value = "";
      body.scrollTo(0, boxes.scrollHeight);
    }, 100);
  };

  sendVideo = (e) => {
    e.preventDefault();
    this.setState({ sending: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;

    const message = {
      message: this.state.message,
      conversation_id: activeConversation.id,
      account_number: this.state.account_number,
      mime_type: "",
      file_name: this.state.file_name,
      ext: this.state.ext,
      data: this.state.data,
      type: this.state.type,
      ticket_id: ticketId,
      sender: 1,
      message_type: 1,
      message_token: [...Array(32)]
        .map((i) => (~~(Math.random() * 36)).toString(36))
        .join(""),
    };

    this.props.sendTextMessage(conversationId, message).then(() => {
      this.setState({ message: "", sending: false, loading: false });
    });
  };
  sendDocument = (e) => {
    e.preventDefault();
    this.setState({ sending: true, loading: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;

    const message = {
      message: this.state.message,
      conversation_id: activeConversation.id,
      account_number: this.state.account_number,
      mime_type: "",
      file_name: this.state.file[0].file_name,
      ext: this.state.file[0].ext[this.state.file[0].ext.length - 1],
      data: this.state.image,
      type: this.state.file[0].type,
      ticket_id: ticketId,
      timestamp: new Date()
        .getTime()
        .toString()
        .substring(0, 10),
      source: "message",
      sender: 1,
      message_type: 3,
      message_token: [...Array(32)]
        .map((i) => (~~(Math.random() * 36)).toString(36))
        .join(""),
    };

    this.props.sendDocumentMessage(conversationId, message).then(() => {
      this.setState({
        message: "",
        sending: false,
        loading: false,
        openDocumentModal: false,
      });
    });
  };

  sendVoice = (data) => {
    this.setState({ sending: true, loading: true });
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;

    const message = {
      message: this.state.message,
      conversation_id: activeConversation.id,
      account_number: this.state.account_number,
      mime_type: "audio/mp3",
      file_name: "recording.mp3",
      ext: "mp3",
      data: data,
      type: "audio",
      ticket_id: ticketId,
      timestamp: new Date()
        .getTime()
        .toString()
        .substring(0, 10),
      source: "audio",
      sender: 1,
      message_type: 5,
      message_token: [...Array(32)]
        .map((i) => (~~(Math.random() * 36)).toString(36))
        .join(""),
    };

    this.props.sendVoiceMessage(conversationId, message).then(() => {
      this.setState({
        message: "",
        sending: false,
        loading: false,
      });

      setTimeout(() => {
        const body = document.querySelector(".chat .scrollbar-container");
        const boxes = document.querySelector(".wrap_boxes");
        if (body) {
          body.scrollTo(0, boxes.scrollHeight);
        }
      }, 100);
    });
  };

  toggleImageModal = () => {
    this.setState({
      sendImageModalOpen: !this.state.sendImageModalOpen,
    });
  };
  handleKeyDown = (e) => {
    const { enterAsSend } = this.props;

    if (e.keyCode === 13 && !e.shiftKey) {
      if (e.keyCode === 13 && enterAsSend) {
        e.preventDefault();
        if (this.state.message) {
          this.sendTextMessage(e);
        }
      }
    }
  };

  handleChange = (event, { name, value }) => {
    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: `${value} ` });
    }
  };
  handleUserChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          user_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  addText = (message) => {
    this.setState((state) => {
      return {
        message,
      };
    });

    $("#textarea-monit").focus();
  };
  openModal = () => {
    this.setState({
      openDocumentModal: true,
    });
  };
  BasicUpload = (props) => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16,
    };

    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box",
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden",
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%",
    };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback((acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );

      this.setState({
        file: acceptedFiles.map((file) => ({
          file,
          file_name: file.path,
          type: file.type,
          ext: file.path.split(/[\s,.]+/),
        })),
      });

      acceptedFiles.forEach((file) => {
        const reader = new FileReader();

        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr,
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const { getRootProps, getInputProps } = useDropzone({ onDrop });

    const thumbs = files.map((file) => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));

    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach((file) => URL.revokeObjectURL(file.preview));
      },
      [files]
    );

    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>
            Arraste seu arquivo ou imagem, ou clique aqui para seleciona-lo.
          </p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };
  render() {
    const {
      enterAsSend,
      conversation,
      activeConversationId,
      ticket,
      comments,
      permissions,
    } = this.props;
    const { sending, loading, media } = this.state;

    const voiceMessage = permissions.find((c) => c === "send-voice-message");

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );

    const activeTicketId = activeConversation.active_ticket_id;

    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );

    return (
      <div className="reply">
        <QuickMessages addText={this.addText} />
        {this.state.error ? (
          this.state.error.no_plan ? (
            <AlertError
              style={{
                zIndex: 10000,
                padding: "5px 10px",
                marginTop: "-25px",
                width: 300,
              }}
              message={this.state.error.message}
            />
          ) : (
            <AlertError
              style={{
                zIndex: 10000,
                padding: "5px 10px",
                marginTop: "-25px",
                width: 300,
              }}
              message={this.state.error.message}
            >
              <Link to="/company" style={{ color: "blue" }}>
                Planos
              </Link>
            </AlertError>
          )
        ) : null}
        <div className="formchat">
          <TextareaAutosize
            rows={3}
            maxRows={4}
            onChange={this.onChange}
            value={this.state.message}
            onKeyDown={this.handleKeyDown}
            name="message"
            id="textarea-monit"
          />
        </div>

        <div className="checkbox-enter">
          {activeTicket && activeTicket.current_progress < 100 ? (
            <Forward activeTicket={activeTicket} />
          ) : (
            ""
          )}
          <NewTask />

          {activeTicket ? (
            <EditTicket ticket={ticket} comments={comments} />
          ) : (
            ""
          )}
          <NewTicket />

          <AndroidButton
            clickConsole={this.enableAutoReply}
            clickDisable={this.disableAutoReply}
          />

          <Checkbox
            label="Enviar com enter"
            onChange={this.handleEnterAsSend}
            checked={enterAsSend}
          />
        </div>
        <div className="box-buttons-chat">
          <EmojiSelector setEmoji={this.setEmoji} />

          <MediaUploadButtons
            onClick={this.sendMessage}
            BasicUpload={this.BasicUpload}
            loading={loading}
            imageOnloadHandler={this.imageOnloadHandler}
            videoOnloadHandler={this.videoOnloadHandler}
            documentOnloadHandler={this.documentOnloadHandler}
            clearTmpFile={this.clearTmpFile}
            save={this.sendImage}
            sendImageModalOpen={this.state.sendImageModalOpen}
            toggleImageModal={this.toggleImageModal}
            saveVideo={this.sendVideo}
            sendDocument={this.sendDocument}
            openModal={this.openModal}
            openDocumentModal={this.state.openDocumentModal}
          />
          {media.total_records > 0 ? (
            <WhatsappAccountSelect
              handleChange={this.handleSelectChanged}
              account_number={this.state.account_number}
            />
          ) : (
            ""
          )}
          {voiceMessage ? <AudioRecorder sendVoice={this.sendVoice} /> : null}
          {this.state.message ? (
            <ScheduleButton
              scheduleAction={(value) => this.scheduleAction(value)}
              sendTextMessage={this.sendTextMessage}
              handleOpen={(e) => this.handleOpen(e)}
            />
          ) : (
            ""
          )}

          <div className="reply-icons">
            <a
              href="/"
              className=""
              id="send-message"
              rel="tooltip"
              onClick={this.sendTextMessage}
              title="Enviar"
            >
              <i className="fa fa-send fa-2x" />
            </a>
          </div>
        </div>
        {sending && (
          <div className="loading-message">
            <LoaderComponent />
          </div>
        )}
      </div>
    );
  }
}

FormMessage.propTypes = {
  name: PropTypes.any.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => ({
  user_id: state.user.user.id,
  name: state.user.user.name,
  conversation: state.conversation,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
  permissions: state.user.user.permissions,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormMessage);
