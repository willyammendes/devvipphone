import React from "react";
import placeholder from "../../assets/img/logo-full.png";
import { Button } from "semantic-ui-react";

const UploadImageForm = ({ onClickUpload }) => (
  <div className="row">
    <div className="col-md-6">
      <div className="form-group">
        <label>
          <h1>Imagem:</h1>{" "}
        </label>
        <div className="input-group">
          <img
            id="img_preview"
            name="img_preview"
            alt={placeholder}
            className="img_preview"
            src={placeholder}
            onClick={onClickUpload}
            style={{ width: "415px", height: "auto" }}
          />
          <br />
          <br />
          <Button
            content="Carregar"
            icon="picture"
            labelPosition="left"
            onClick={onClickUpload}
            title="Selecionar Imagem"
          />
        </div>
      </div>
    </div>
  </div>
);

export default UploadImageForm;
