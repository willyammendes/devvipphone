import React from "react";
import TextareaAutosize from "react-autosize-textarea";

const ChatTextArea = ({ onKeyDown, onChange, message, onKeyUp }) => (
  <div className="formchat">
    <TextareaAutosize rows={2} maxRows={4} onChange={onChange} name="message" />
  </div>
);

export default ChatTextArea;
