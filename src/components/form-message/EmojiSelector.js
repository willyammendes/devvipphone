import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import EmojiPicker from "emoji-picker-react";

const EmojiSelector = ({ setEmoji, clickTest }) => (
  <div className="reply-icons reply-emojis">
    <OverlayTrigger
      trigger="click"
      style={{ width: "450px" }}
      rootClose
      placement="top"
      overlay={
        <Popover
          style={{ width: "450px" }}
          id="i-class-fa-fa-fw-fa-pencil-i-form-inside-popover-popover"
          className="emoji-popup"
        >
          <EmojiPicker onEmojiClick={(e, d, f) => setEmoji(d)} />
        </Popover>
      }
    >
      <a
        href
        className="emoji-clicker"
        rel="tooltip"
        data-trigger="hover"
        data-original-title=""
        data-placement="bottom"
        title="Emojis"
      >
        <i className="fa fa-smile-o fa-2x" />
      </a>
    </OverlayTrigger>
  </div>
);

export default EmojiSelector;
