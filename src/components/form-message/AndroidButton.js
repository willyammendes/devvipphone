import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Icon } from "semantic-ui-react";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";

class AndroidButton extends React.Component {
	state = {
		contacts: [],
		contact: {},
		loading: false,
		hasMore: true,
		open: false,
		modalOpen: false,
		groups: []
	};

	componentDidMount = () => this.onInit(this.props);
	onInit = props =>
		props.conversations.length === 0 &&
		props.fetchAll().then(() => {
			this.props.conversation.data.forEach(conversation => {
				props.getStats(conversation);
			});
		});

	onConversationSelected = id =>
		this.props.conversationSelected(
			this.props.conversation.data.find(c => c.id === id)
		);

	render() {
		const {
			conversation,
			activeConversationId,
			clickConsole,
			clickDisable
		} = this.props;
		const activeConversation = conversation.data.find(
			c => c.id === activeConversationId
		);

		return (
			<div className="button_android" name="android" title="Ativar robô">
				{activeConversation.auto_reply === 0 ? (
					<Icon
						name="android"
						className="disable"
						fitted
						size="big"
						onClick={clickConsole}
					/>
				) : (
					<Icon
						name="android"
						fitted
						size="big"
						onClick={clickDisable}
					/>
				)}
			</div>
		);
	}
}

AndroidButton.propTypes = {
	conversation: PropTypes.shape({
		data: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number,
				tickets: PropTypes.arrayOf(
					PropTypes.shape({
						id: PropTypes.number.isRequired
					})
				).isRequired
			}).isRequired
		)
	}).isRequired,
	activeConversationId: PropTypes.number.isRequired,
	conversationSelected: PropTypes.func.isRequired,
	conversations: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number.isRequired
		}).isRequired
	).isRequired
};
const mapStateToProps = ({ conversation }) => {
	return {
		conversation,
		activeConversationId: conversation.activeConversationId,
		filter: conversation.filter,
		conversations: !conversation.data
			? []
			: Object.values(conversation.data)
	};
};

const mapDispatchToProps = dispatch =>
	bindActionCreators(ConversationActions, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AndroidButton);
