import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import api from "../../api/ticket";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import CommentModal from "../conversation-body/modal/CommentModal";
import ButtonAdd from "./ButtonAdd";

const initialState = {
  records: [],
  comment: {
    message: "",
    path: "/",
    comment_type: 0,
    source: "comment",
    comment: "",
    ticket_id: -1,
    sender: 1,
    message_type: 0
  },
  error: "",
  columns: {},
  order: {},
  loading: false,
  total_records: 0,
  selectedDataIndex: -1,
  editModalOpen: false,
  save_alert: false
};

class NewComment extends Component {
  state = initialState;

  componentWillMount() {
    const { conversation, activeConversationId, user } = this.props;

    const activeConversation = conversation.data.find(
      c => c.id === activeConversationId
    );

    api.ticket.fetchTicket(activeConversation.active_ticket_id).then(res => {
      this.setState({
        comment: {
          ...this.state.comment,
          ticket_category_id: res.data.ticket_category_id
        },
        ticket: res.data
      });
    });
  }
  newDataClick = () => {
    const { conversation, activeConversationId, user } = this.props;

    const activeConversation = conversation.data.find(
      c => c.id === activeConversationId
    );

    api.ticket.fetchTicket(activeConversation.active_ticket_id).then(res => {
      this.setState({
        comment: {
          ...this.state.comment,
          ticket_category_id: res.data.ticket_category_id
        },
        ticket: res.data
      });
    });
    this.setState({
      editModalOpen: true
    });
  };

  handleChange = (e, { name, value }) => {
    this.setState({
      comment: {
        ...this.state.comment,
        [name]: value
      }
    });
  };

  handleCategoryChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      comment: {
        ...this.state.comment,
        ticket_category_id: value
      }
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    this.setState({
      loading: true
    });
    const { conversation, activeConversationId, user } = this.props;

    const activeConversation = conversation.data.find(
      c => c.id === activeConversationId
    );

    const activeTicket = activeConversation.tickets.find(
      c => c.id === activeConversation.active_ticket_id
    );

    const { comment } = this.state;
    const joincomment = {
      ...comment,
      ticket_number: activeTicket.ticket_number,
      ticket_id: activeTicket.id,
      user_id: user.id,
      path: "/",
      sender: 1,
      message_type: 0
    };
    const updateCategory = {
      ...this.state.ticket,
      ticket_category_id: comment.ticket_category_id
    };
    if (comment.ticket_category_id) {
      api.ticket.update(activeTicket.id, updateCategory).then(res => {
        this.setState({
          comment: {
            ticket_category_id: res.data.ticket_category_id
          }
        });
      });
    } else {
      this.setState({
        error: "É preciso escolher uma categoria."
      });
    }

    this.props.sendComment(joincomment, activeTicket.ticket_number).then(() => {
      this.setState({
        comment: {
          ...this.state.comment,
          message: "",
          comment: ""
        },
        loading: false,
        editModalOpen: false
      });
    });
  };

  handleCloseEditModal = () => {
    this.setState({
      comment: {
        ...this.state.comment,
        message: "",
        comment: ""
      },
      loading: false,
      editModalOpen: false
    });
  };

  render() {
    const { loading, editModalOpen, comment, error } = this.state;
    const { conversation, activeConversationId, name } = this.props;

    const activeConversation = conversation.data.find(
      c => c.id === activeConversationId
    );

    const activeTicketId = activeConversation.active_ticket_id;

    const activeTicket = activeConversation.tickets.find(
      c => c.id === activeTicketId
    );

    return (
      <div className="icone-add">
        <ButtonAdd
          onAddClick={this.newDataClick}
          icon="plus"
          title="Novo Comentário"
        />
        <CommentModal
          handleClose={this.handleCloseEditModal}
          onChange={this.handleChange}
          onClickAdd={this.submit}
          ticket={activeTicket}
          comment={comment}
          error={error}
          handleCategoryChange={this.handleCategoryChange}
          activeTicket={activeTicket}
          modalHeader={`Novo comentário de ${name} no ticket ${
            activeTicket.ticket_number
          }`}
          open={editModalOpen}
          loading={loading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = state => ({
  conversation: state.conversation,
  user: state.user.user,
  name: state.user.user.name,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewComment);
