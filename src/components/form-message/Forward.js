import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import api from "../../api/ticket";
import apiticketcategory from "../../api/ticket-category";
import apiclient from "../../api/client";
import apicontact from "../../api/contact";
import apiticketStatus from "../../api/ticket-status";
import ForwardModal from "../conversation-body/modal/ForwardModal";
import ButtonAdd from "./ButtonAdd";

class Forward extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    departments: [],
    ticketcategorys: [],
    ticketStatuss: [],
    contacts: [],
    loadingContacts: false,
    loadingStats: false,
    users: [],
    usersActive: [],
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false,
    activeTicket: "",
    textareaCount: "",
    errors: "",
  };

  select = (selectedDataId) => {
    const {
      departments,
      ticketcategorys,
      clients,
      contacts,
      ticketStatuss,
      users,
    } = this.state;

    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (departments.length === 0) this.fetchDepartments();
    if (ticketcategorys.length === 0) this.fetchCategorys();
    if (contacts.length === 0) this.fetchContacts();
    if (users.length === 0) this.fetchUsers();
    if (ticketStatuss.length === 0) this.fetchStats();

    if (
      !!data.client_id &&
      clients.filter((c) => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };
  newDataClick = () => {
    const { conversation, activeConversationId } = this.props;

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;
    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );
    if (activeConversation.active_ticket_id) {
      api.ticket
        .fetchTicket(activeConversation.active_ticket_id)
        .then((res) => {
          const newData = {
            contact_id: activeConversation.contact.id,
            ticket_id: activeTicket.id,
            user_id: "",
            forward: true,
            path: 1,
          };
          this.setState(
            {
              records: { ...res.data, ...newData },
            },
            () => {
              this.setState({
                selectedDataIndex: 0,
                editModalOpen: true,
              });
            }
          );
        });
    }
  };

  sendForward = (e) => {
    e.preventDefault();

    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const conversationId = activeConversation.id;
    const activeTicketIndex = activeConversation.activeTicketIndex;
    const ticket = activeConversation.tickets[activeTicketIndex];
    const ticketId = ticket ? ticket.id : null;

    const message = {
      message: this.state.message,
      account_number: this.state.account_number,
      conversation_id: activeConversation.id,
      ticket_id: ticketId,
      sender: 1,
      message_type: 0,
      source: "comment",
    };

    this.props.sendTextMessage(conversationId, message).then(() => {
      this.setState({ message: "" });
    });
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  onSearchClientChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };
  componentWillMount() {
    const { conversation, activeConversationId } = this.props;

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;
    this.fetchRecords(activeTicketId);
  }

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then((clients) => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map((c) => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false,
        });
      });
    }
  };
  addStat = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingStats: true });

    apiticketStatus.ticketStatus
      .submit({ description: value })
      .then((data) => {
        this.setState({
          ticketStatuss: [...this.state.ticketStatuss].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.description,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              ticketStatuss: [...records[dataIndex].ticketStatuss].concat(
                data.data.id
              ),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingStats: false });
      });
  };

  fetchStats = async () => {
    this.setState({ loadingStats: true });
    await apiticketStatus.ticketStatus.fetchAll().then((ticketStatuss) => {
      this.setState({
        ticketStatuss: ticketStatuss.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.description,
        })),
        loadingStats: false,
      });
    });
  };

  onSelectStat = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticketStatuss: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleUserChange = (value) => {
    const { records } = this.state;

    this.setState({
      records: {
        ...records,
        user: value,
        user_id: value,
      },
    });
  };

  addContact = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingContacts: true });

    apicontact.contact
      .submit({ name: value })
      .then((data) => {
        this.setState({
          contacts: [...this.state.contacts].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              contacts: [...records[dataIndex].contacts].concat(data.data.id),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingContacts: false });
      });
  };

  fetchContacts = async () => {
    this.setState({ loadingContacts: true });
    await apicontact.contact.fetchAll().then((contacts) => {
      this.setState({
        contacts: contacts.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingContacts: false,
      });
    });
  };

  onSelectContact = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          contacts: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  addCategory = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingCategorys: true });

    apiticketcategory.ticketcategory
      .submit({ description: value })
      .then((data) => {
        this.setState({
          ticketcategorys: [...this.state.ticketcategorys].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.description,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              ticketcategorys: [...records[dataIndex].ticketcategorys].concat(
                data.data.id
              ),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingCategorys: false });
      });
  };

  fetchCategorys = async () => {
    this.setState({ loadingCategorys: true });
    await apiticketcategory.ticketcategory
      .fetchAll({ take: 200 })
      .then((ticketcategorys) => {
        this.setState({
          ticketcategorys: ticketcategorys.data.map((c) => ({
            key: c.id,
            value: c.id,
            text: c.description,
          })),
          loadingCategorys: false,
        });
      });
  };

  onSelectCategory = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticketcategorys: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleDepartmentChange = (value) => {
    const { records } = this.state;

    this.setState({
      records: {
        ...records,
        department_id: value,
      },
    });
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.ticket.fetchTicket(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  getClient = async (id) => {
    this.setState({ loadingClients: true });
    await apiclient.client.get(id).then((client) => {
      this.setState({
        clients: [...this.state.clients].concat({
          key: client.id,
          value: client.id,
          text: client.name,
        }),
        loadingClients: false,
      });
    });
  };

  handleChange = (e) => {
    const { records } = this.state;
    const { conversation, activeConversationId } = this.props;

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;

    this.setState({
      records: {
        ...records,
        [e.target.name]: e.target.value,
        ticket_id: activeTicketId,
        path: 1,
        forward: true,
      },

      textareaCount: e.target.value,
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { records } = this.state;
    const data = {
      ...records,
      avatar: this.props.user.avatar,
    };
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;
    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );
    this.setState({
      loading: true,
    });

    api.ticket.update(activeTicket.id, records).then((data) => {
      this.setState({
        save_alert: true,

        loading: false,
      });
    });

    this.props
      .sendComment({
        ...data,
        ticket_id: activeTicket.id,
        user_id: this.props.user.id,
        source: "comment",
      })
      .then(() => {
        this.setState({
          save_alert: true,
          loading: false,
          editModalOpen: false,
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    const activeTicketId = activeConversation.active_ticket_id;
    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeTicketId
    );
    this.setState({ loading: true });

    api.ticket.update(activeTicket.id, data).then((data) => {
      this.setState({
        records: [
          ...records.slice(0, selectedDataIndex),
          { ...data.data },
          ...records.slice(selectedDataIndex + 1),
        ],
        save_alert: true,

        selectedDataIndex: -1,
        loading: false,
      });
    });
  };

  delete = (id) => api.ticket.delete(id);

  handleCloseEditModal = () => {
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
    });
  };

  render() {
    const {
      records,
      loading,
      selectedDataIndex,
      editModalOpen,
      textareaCount,
    } = this.state;
    const { activeTicket } = this.props;
    const ticket = records;

    return (
      <div className="icone-add">
        <ButtonAdd
          onAddClick={this.newDataClick}
          icon="mail forward"
          title="Encaminhar Ticket"
        />
        {selectedDataIndex !== -1 && records.contact_id ? (
          <ForwardModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={this.update}
            onClickAdd={this.submit}
            handleUserChange={this.handleUserChange}
            handleDepartmentChange={this.handleDepartmentChange}
            ticket={ticket}
            textareaCount={textareaCount.length}
            cleanErrors={this.cleanErrors}
            messageError={
              this.state.errors.status > 0 ? this.state.errors.data.message : ""
            }
            modalHeader={
              activeTicket.id
                ? `Encaminhar ${activeTicket.ticket_number}`
                : "Encaminhar Ticket"
            }
            activeTicket={activeTicket}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            loading={loading}
          />
        ) : null}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => ({
  conversation: state.conversation,
  user: state.user.user,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Forward);
