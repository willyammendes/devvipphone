import React from "react";
import { Input, Label, Icon } from "semantic-ui-react";

const UploadDocumentForm = ({ onClickUpload, clearTmpFile }) => (
  <div className="row">
    <div className="col-md-12">
      <div className="form-group">
        <label>
          <strong>Arquivo:</strong>{" "}
        </label>
        <div className="input-group">
          <Input labelPosition="center" type="text" fluid>
            <Label basic onClick={onClickUpload} tabIndex="-1">
              <Icon name="upload" />
            </Label>
            <input
              className="form-control"
              type="text"
              id="document_name"
              name="document_name"
              disabled
            />
            <Label onClick={clearTmpFile} tabIndex="-1">
              <Icon name="delete" />
            </Label>
          </Input>
        </div>
      </div>
    </div>
  </div>
);

export default UploadDocumentForm;
