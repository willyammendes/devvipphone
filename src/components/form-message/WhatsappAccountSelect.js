import React, { Component } from "react";
import api from "../../api/social-media";
import { Popup, Dropdown } from "semantic-ui-react";

class WhatsappAccountSelect extends Component {
  state = {
    media: [],
    loading: false,
    hasMore: true,
    columns: {},
    order: {},
    total_records: 0,
    selectedDataIndex: -1
  };

  componentWillMount() {
    this.setState({ loading: true });
    api.media.fetchData().then(media => {
      this.setState({
        media,
        loading: false
      });
    });
  }

  render() {
    const { media } = this.state;

    return (
      <div className="reply-icons whatsapp-select">
        <Popup
          trigger={
            <a
              href
              className=""
              rel="tooltip"
              data-trigger="hover"
              data-original-title=""
              data-placement="bottom"
              title="Escolha por onde a mensagem será enviada"
            >
              <i className="fa fa-phone fa-2x" />
            </a>
          }
          content={
            <Dropdown
              placeholder="Escolha uma Opção"
              selection
              onChange={this.props.handleChange}
              value={this.props.account_number}
              name="account_number"
              options={media.map(e => ({
                key: e.id,
                value: e.phone_number,
                text: e.phone_number
              }))}
            />
          }
          className="popup-upload"
          on="click"
          position="top center"
        />
      </div>
    );
  }
}
export default WhatsappAccountSelect;
