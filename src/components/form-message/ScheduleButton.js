import React, { useState, useEffect } from "react";
import { Popup, Form, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { DateInput, TimeInput } from "semantic-ui-calendar-react";

function ScheduleButton(props) {
  // Set initial date
  const today = new Date();

  const todayDate =
    today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();

  const monthZero =
    today.getMonth() + 1 > 9
      ? today.getMonth() + 1
      : `0${today.getMonth() + 1}`;

  const date = `${today.getFullYear()}-${monthZero}-${todayDate}`;
  const dateShow = `${todayDate}-${monthZero}-${today.getFullYear()}`;
  const todayMinute =
    today.getMinutes() < 10 ? `0${today.getMinutes()}` : today.getMinutes();
  const todayHour =
    today.getHours() < 10 ? `0${today.getHours()}` : today.getHours();
  const todayHourFormatted = `${todayHour}:${todayMinute}`;

  // Set State Component
  const [dateChange, setDateChange] = useState(date);
  const [showDate, setShowDate] = useState(dateShow);
  const [hourChange, setHourChange] = useState(todayHourFormatted);
  const [sendDate, setSendDate] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [isDisabled, setisDisabled] = useState();
  const [popupIsOpen, setPopupIsOpen] = useState(false);
  const [popupContent, setPopupContent] = useState("Agendado com sucesso!");

  // Change state is open or closed
  const changeOpen = () => {
    setIsOpen(!isOpen);
  };
  useEffect(() => {
    return () => {
      setDateChange(date);
      setShowDate(dateShow);
      setHourChange(todayHourFormatted);
    };
  }, [isOpen, date, dateShow, todayHourFormatted]);

  // Get date and minute
  const getScheduleDate = (e, { value }) => {
    const dateSplit = value.split("-");
    const datereverse = dateSplit.reverse().join("-");
    setDateChange(datereverse);
    setShowDate(value);
  };

  const getScheduleHour = (e, { value }) => {
    setHourChange(value);
    setSendDate(`${dateChange} ${hourChange}`);
  };
  const {scheduleAction} = props

  // sync schedule Hour
  useEffect(() => {
    const modify = Math.floor(
      new Date(`${dateChange} ${hourChange}`).getTime() / 1000.0
    );
    const isToday = Math.floor(
      new Date(`${date} ${todayHourFormatted}`).getTime() / 1000.0
    );
    if (modify > isToday) {
      setisDisabled(false);
    } else {
      setisDisabled(true);
    }
    setTimeout(() => {}, 900);
    scheduleAction(sendDate);
  }, [dateChange, hourChange, sendDate, date, todayHourFormatted, scheduleAction]);

  // Send to state component Father
  const sendSchedule = e => {
    setSendDate(`${dateChange} ${hourChange}`);
    if (!isDisabled) {
      setTimeout(() => {
        setIsOpen(!isOpen);
        props.sendTextMessage(e);
      }, 900);
    }
  };

  const handlePupopOpen = () => {
    setPopupIsOpen(true);
    isDisabled
      ? setPopupContent("Data e a hora não pode ser inferior a data de agora!")
      : setPopupContent("Agendado com Sucesso!");
    setTimeout(() => {
      setPopupIsOpen(false);
    }, 1500);
  };
  const handlePupopClose = () => {
    setPopupIsOpen(false);
  };

  return (
    <div className="reply-icons reply-emojis">
      <Popup
        className="popup-upload"
        on="click"
        open={isOpen}
        onOpen={changeOpen}
        position="top center"
        trigger={
          <a
            href
            className=""
            rel="tooltip"
            data-trigger="hover"
            data-original-title=""
            data-placement="bottom"
            title="Agendar Mensagem"
          >
            <i className="fa clock outline icon fa-2x" />
          </a>
        }
        content={
          <>
            <Form>
              <Form.Field>
                <label>Data do Agendamento</label>

                <>
                  <DateInput
                    closable
                    dateFormat="DD-MM-YYYY"
                    placeholder="Data e hora do inicio da campanha"
                    name="start_date"
                    value={showDate}
                    minDate={date}
                    iconPosition="left"
                    onChange={getScheduleDate}
                  />
                  <label>Selecine a Hora</label>
                  <TimeInput
                    closable
                    value={hourChange}
                    onChange={getScheduleHour}
                  />
                </>
              </Form.Field>
              <Popup
                content={popupContent}
                on="click"
                style={isDisabled ? { color: "red" } : { color: "green" }}
                open={popupIsOpen}
                onClose={handlePupopClose}
                onOpen={handlePupopOpen}
                trigger={
                  <Button
                    name="Agendar"
                    color="green"
                    onClick={e => sendSchedule(e)}
                    content={"Agendar"}
                  />
                }
              />
              <Button
                name="cancelar"
                color="red"
                onClick={e => {
                  e.preventDefault();
                  setIsOpen(!isOpen);
                }}
              >
                Cancelar
              </Button>
            </Form>
          </>
        }
      />
    </div>
  );
}

const mapStateToProps = state => ({
  conversation: state.conversation
});

export default connect(mapStateToProps)(ScheduleButton);
