import React from "react";
import { Button } from "semantic-ui-react";
import placeholder from "../../assets/img/logo-full.png";

const UploadVideoForm = ({ message, onClickUpload, onChange }) => (
  <div className="row">
    <div className="col-md-6">
      <div className="form-group">
        <label>
          <h1>Video:</h1>{" "}
        </label>
        <div className="input-group">
          <img
            src={placeholder}
            alt={placeholder}
            id="video_preview"
            className="img_preview"
            onClick={onClickUpload}
            style={{ width: "415px", height: "auto" }}
          />
          <br />
          <br />
          <Button
            content="Carregar"
            icon="picture"
            labelPosition="left"
            onClick={onClickUpload}
            title="Selecionar Imagem"
          />
        </div>
      </div>
    </div>
  </div>
);

export default UploadVideoForm;
