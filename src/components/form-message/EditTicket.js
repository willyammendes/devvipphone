import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import api from "../../api/ticket";
import apidepartment from "../../api/department";
import apiticketcategory from "../../api/ticket-category";
import apiclient from "../../api/client";
import apicontact from "../../api/contact";
import apiticketStatus from "../../api/ticket-status";
import apiuser from "../../api/users";
import TicketsModal from "../tickets/modal/TicketsModal";
import ButtonAdd from "./ButtonAdd";
import AlertError from "../alerts/AlertError";
import { Link } from "react-router-dom";

class EditTicket extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    departments: [],
    ticketcategorys: [],
    ticketStatuss: [],
    contacts: [],
    loadingContacts: false,
    loadingStats: false,
    users: [],
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false,
    ticket: "",
    comments: "",
  };

  select = (selectedDataId) => {
    const {
      departments,
      ticketcategorys,
      clients,
      contacts,
      ticketStatuss,
      users,
    } = this.state;
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (departments.length === 0) this.fetchDepartments();
    if (ticketcategorys.length === 0) this.fetchCategorys();
    if (contacts.length === 0) this.fetchCategorys();
    if (users.length === 0) this.fetchUsers();
    if (ticketStatuss.length === 0) this.fetchStats();

    if (
      !!data.client_id &&
      clients.filter((c) => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  newDataClick = () => {
    const { ticket, comments } = this.props;
    const filterComments = comments.filter(
      (c) => c.ticket_id === ticket.id || c.source === "user_comment"
    );
    apiuser.user.fetchUser(this.props.user.id).then((c) => {
      this.setState(
        {
          ticket,
          comments: filterComments,
        },
        () => {
          this.setState({
            editModalOpen: true,
            ticket: {
              ...ticket,
              department_id:
                ticket.user_id.length > 0 && ticket.department_id.length > 0
                  ? ""
                  : c.data.departments[0],
              user_id: ticket.user_id.length > 0 ? "" : this.props.user.id,
            },
          });
        }
      );
    });
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  onSearchClientChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then((clients) => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map((c) => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false,
        });
      });
    }
  };
  addStat = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingStats: true });

    apiticketStatus.ticketStatus
      .submit({ description: value })
      .then((data) => {
        this.setState({
          ticketStatuss: [...this.state.ticketStatuss].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.description,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              ticketStatuss: [...records[dataIndex].ticketStatuss].concat(
                data.data.id
              ),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingStats: false });
      });
  };

  fetchStats = async () => {
    this.setState({ loadingStats: true });
    await apiticketStatus.ticketStatus.fetchAll().then((ticketStatuss) => {
      this.setState({
        ticketStatuss: ticketStatuss.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.description,
        })),
        loadingStats: false,
      });
    });
  };

  onSelectStat = (e, { description, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticketStatuss: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  addUser = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingUsers: true });

    apiuser.user
      .submit({ name: value })
      .then((data) => {
        this.setState({
          users: [...this.state.users].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              users: [...records[dataIndex].users].concat(data.data.id),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingUsers: false });
      });
  };

  fetchUsers = async () => {
    this.setState({ loadingUsers: true });
    await apiuser.user.fetchAll().then((users) => {
      this.setState({
        users: users.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingUsers: false,
      });
    });
  };

  onSelectUser = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          users: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  addContact = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingContacts: true });

    apicontact.contact
      .submit({ name: value })
      .then((data) => {
        this.setState({
          contacts: [...this.state.contacts].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              contacts: [...records[dataIndex].contacts].concat(data.data.id),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingContacts: false });
      });
  };

  fetchContacts = async () => {
    this.setState({ loadingContacts: true });
    await apicontact.contact.fetchAll().then((contacts) => {
      this.setState({
        contacts: contacts.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingContacts: false,
      });
    });
  };

  onSelectContact = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          contacts: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  addCategory = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingCategorys: true });

    apiticketcategory.ticketcategory
      .submit({ description: value })
      .then((data) => {
        this.setState({
          ticketcategorys: [...this.state.ticketcategorys].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.description,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              ticketcategorys: [...records[dataIndex].ticketcategorys].concat(
                data.data.id
              ),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingCategorys: false });
      });
  };

  fetchCategorys = async () => {
    this.setState({ loadingCategorys: true });
    await apiticketcategory.ticketcategory
      .fetchAll({ take: 200 })
      .then((ticketcategorys) => {
        this.setState({
          ticketcategorys: ticketcategorys.data.map((c) => ({
            key: c.id,
            value: c.id,
            text: c.description,
          })),
          loadingCategorys: false,
        });
      });
  };

  onSelectCategory = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          ticketcategorys: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  onSelectDepartment = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          departments: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  addDepartment = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingDepartments: true });

    apidepartment.department
      .submit({ name: value })
      .then((data) => {
        this.setState({
          departments: [...this.state.departments].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              departments: [...records[dataIndex].departments].concat(
                data.data.id
              ),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingDepartments: false });
      });
  };

  fetchDepartments = async () => {
    this.setState({ loadingDepartments: true });
    await apidepartment.department.fetchAll().then((departments) => {
      this.setState({
        departments: departments.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingDepartments: false,
      });
    });
  };

  getClient = async (id) => {
    this.setState({ loadingClients: true });
    await apiclient.client.get(id).then((client) => {
      this.setState({
        clients: [...this.state.clients].concat({
          key: client.id,
          value: client.id,
          text: client.name,
        }),
        loadingClients: false,
      });
    });
  };

  handleChange = (e) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, [e.target.name]: e.target.value },
    });
  };

  handleContactChange = (value) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, contact_id: value },
    });
  };

  handleDepartmentChange = (value) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, department_id: value },
    });
  };

  handleCategoryChange = (value) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, ticket_category_id: value },
    });
  };
  handleUserChange = (value) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, user_id: value },
    });
  };

  handleStatusChange = (value) => {
    const { ticket } = this.state;

    this.setState({
      ticket: { ...ticket, status: value },
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.ticket
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });

        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          error: err.response.data,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);

        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  submitComment = () => {
    this.setState({
      loading: true,
    });
    const { conversation, activeConversationId, user } = this.props;

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );

    const activeTicket = activeConversation.tickets.find(
      (c) => c.id === activeConversation.active_ticket_id
    );
    const { ticket } = this.state;

    const joincomment = {
      comment: ticket.comment,
      timestamp: Math.floor(Date.now() / 1000),
      message: ticket.comment,
      ticket_number: activeTicket.ticket_number,
      ticket_id: activeTicket.id,
      user_id: user.id,
      path: "/",
      sender: 1,
      message_type: 0,
    };
    this.props
      .sendComment(joincomment, activeTicket.ticket_number)
      .then((data) => {
        this.setState({
          comments: [...this.state.comments, joincomment],
          loading: false,
        });
      });
  };

  update = () => {
    const { ticket } = this.state;
    const data = ticket;
    this.setState({ loading: true });
    this.submitComment();
    api.ticket
      .update(data.id, data)
      .then((data) => {
        this.setState({
          ticket: { ticket, ...data.data },
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });

        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
        });
      });
  };

  delete = (id) => api.ticket.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      records,
      loading,
      selectedDataIndex,
      editModalOpen,
      departments,
      ticketcategorys,
      contacts,
      users,
      clients,
      ticketStatuss,
      loadingDepartments,
      loadingCategorys,
      loadingContacts,
      loadingUsers,
      loadingStats,
      loadingClients,
      ticket,
      comments,
    } = this.state;

    return (
      <div className="icone-add">
        <ButtonAdd
          onAddClick={this.newDataClick}
          icon="plus"
          title="Comentar e Editar Ticket"
        />
        {this.state.error ? (
          this.state.error.no_plan ? (
            <AlertError
              style={{
                zIndex: 10000,
                padding: "5px 10px",
                marginTop: "-25px",
                width: 300,
              }}
              message={this.state.error.message}
            />
          ) : (
            <AlertError
              style={{
                zIndex: 10000,
                padding: "5px 10px",
                marginTop: "-25px",
                width: 300,
              }}
              message={this.state.error.message}
            >
              <Link to="/company" style={{ color: "blue" }}>
                Planos
              </Link>
            </AlertError>
          )
        ) : null}
        {ticket ? (
          <TicketsModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={this.update}
            onClickAdd={this.update}
            onSelectDepartment={this.onSelectDepartment}
            onSelectCategory={this.onSelectCategory}
            onSelectContact={this.onSelectContact}
            onSelectUser={this.onSelectUser}
            onSelectStat={this.onSelectStat}
            onSelectClient={this.onSelectClient}
            onSearchClientChange={this.onSearchClientChange}
            handleDepartmentAddition={this.addDepartment}
            handleCategoryAddition={this.addCategory}
            handleContactAddition={this.addContact}
            handleUserAddition={this.addUser}
            handleStatAddition={this.addStat}
            submitComment={this.submitComment}
            ticket={ticket}
            comments={comments}
            tickets_conversation_id={false}
            modalHeader={
              ticket.id ? `Edição do ${ticket.title}` : "Novo Ticket"
            }
            handleContactChange={this.handleContactChange}
            handleDepartmentChange={this.handleDepartmentChange}
            handleCategoryChange={this.handleCategoryChange}
            handleUserChange={this.handleUserChange}
            handleStatusChange={this.handleStatusChange}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            departments={departments}
            ticketcategorys={ticketcategorys}
            contacts={contacts}
            users={users}
            ticketStatuss={ticketStatuss}
            clients={clients}
            loading={loading}
            loadingDepartments={loadingDepartments}
            loadingCategorys={loadingCategorys}
            loadingContacts={loadingContacts}
            loadingUsers={loadingUsers}
            loadingStats={loadingStats}
            loadingClients={loadingClients}
          />
        ) : null}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => ({
  user: state.user.user,
  conversation: state.conversation,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTicket);
