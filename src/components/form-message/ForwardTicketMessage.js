import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import { Dropdown } from "semantic-ui-react";

const ForwardTicketMessage = ({
  handleSelectChangedForward,
  onChange,
  text_comment,
  users,
  createComment,
  forward_to_user_id
}) => (
  <div className="reply-icons forward-message">
    <OverlayTrigger
      trigger="click"
      rootClose
      placement="top"
      overlay={
        <Popover
          id="i-class-fa-fa-fw-fa-pencil-i-form-inside-popover-popover"
          style={{ width: "550px" }}
        >
          <Dropdown
            error={!forward_to_user_id}
            onChange={handleSelectChangedForward}
            options={users}
            placeholder="Escolha uma Opção"
            selection
            name="forward_to_user_id"
            value={forward_to_user_id}
          />
          <br />
          <br />
          <textarea
            autoFocus
            placeholder="Comentário"
            id="text_comment"
            name="text_comment"
            value={text_comment}
            onChange={onChange}
            rows="3"
            style={{
              wordWrap: "break-word",
              resize: "none",
              width: "270px",
              borderColor: !text_comment ? "#c57471" : ""
            }}
          />
          &nbsp;
          <a
            className=""
            onClick={createComment}
            id="forward-ticket-bt"
            rel="tooltip"
            title="Encaminhar"
            style={{ marginTop: "4%", position: "absolute" }}
          >
            <i className="fa fa-send fa-2x" />
          </a>
        </Popover>
      }
    >
      <a className="" id="forward-ticket" rel="tooltip" title="Encaminhar">
        <i className="fa fa-mail-forward fa-2x" />
      </a>
    </OverlayTrigger>
  </div>
);

export default ForwardTicketMessage;
