import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as TaskAction } from "../../store/ducks/task";
import PropTypes from "prop-types";
import { Button } from "semantic-ui-react";
import api from "../../api/tasks";
import TasksModal from "../events/modal/TasksModal.js";
import "moment/locale/pt-br";
import "../../../node_modules/react-big-calendar/lib/css/react-big-calendar.css";

class NewTask extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: 1,
    editModalOpen: false,
    save_alert: false,
    state_test: true,
    event: []
  };

  newDataClick = () => {
    this.props.editModal({ payload: true, selectedDataIndex: 1 });
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.task.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
    api.task.fetchAll().then(tasks => {
      this.setState({
        event: tasks.data.map(c => ({
          id: c.id,
          title: c.title,
          start: new Date(`${c.date},${c.hour}`),
          end: new Date(`${c.date},${c.hour}`)
        })),
        loading: false
      });
    });
  }

  render() {
    return (
      <div className="icone-add">
        <Button
          onClick={this.newDataClick}
          circular
          icon="calendar alternate outline"
          size="mini"
          className="add-comment"
          title="Nova Tarefa"
        />

        {this.props.selectedDataIndex !== -1 ? <TasksModal /> : null}
      </div>
    );
  }
}

NewTask.propTypes = {
  name: PropTypes.any.isRequired,
  id: PropTypes.any.isRequired,
  conversation: PropTypes.any.isRequired,
  activeConversationId: PropTypes.number.isRequired
};

const mapDispatchToProps = dispatch => bindActionCreators(TaskAction, dispatch);

const mapStateToProps = state => ({
  name: state.user.user.name,
  conversation: state.conversation,
  activeConversationId: state.conversation.activeConversationId,
  id: state.user.user.id,
  editModal: state.task,
  selectedDataIndex: state.task.selectedDataIndex
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewTask);
