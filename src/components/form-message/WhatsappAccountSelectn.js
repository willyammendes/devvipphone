import React, { Component } from "react";
import api from "../../api/social-media";
import { Popup, Icon, Dropdown } from "semantic-ui-react";
import SocialMediaModal from "../social-media/modal/SocialMediaModal";
import Socket from "./../../services/socket";

class WhatsappAccountSelect extends Component {
	state = {
		media: [],
		loading: false,
		hasMore: true,
		records: [],
		columns: {},
		order: {},
		total_records: 0,
		selectedDataIndex: -1,
		editModalOpen: false,
		loadingQrCode: false,
		qrCodeCreated: false,
		qrCodeImage: null,
		loginSuccess: false,
		phoneNumber: ""
	};
	componentWillMount() {
		this.fetchRecords();
		this.setState({ loading: true });
		api.media.fetchData().then(media => {
			this.setState({
				media,
				loading: false
			});
		});
	}

	createQrCode = () => {
		this.setState({
			loadingQrCode: true
		});

		api.media.createQrCode().then(res => {});
	};

	newDataClick = () => {
		const { records } = this.state;

		const newData = Object.keys(records[0]).reduce(
			(o, key) => Object.assign(o, { [key]: "" }),
			{}
		);

		newData.webhook_active = true;
		newData.send_campaing = true;

		this.setState(
			{
				records: [...records].concat(newData),
				editModalOpen: true
			},
			() => {
				this.setState({
					selectedDataIndex: this.state.records.length - 1
				});
			}
		);
	};

	nextRecord = () => {
		const selectedDataIndex =
			this.state.selectedDataIndex < this.state.records.length - 1
				? this.state.selectedDataIndex + 1
				: this.state.records.length - 1;
		const selectedDataId = this.state.records[selectedDataIndex].id;

		this.select(selectedDataId);
	};

	previousRecord = () => {
		const selectedDataIndex =
			this.state.selectedDataIndex - 1 > 0
				? this.state.selectedDataIndex - 1
				: 0;
		const selectedDataId = this.state.records[selectedDataIndex].id;

		this.select(selectedDataId);
	};

	fetchRecords = async params => {
		this.setState({ loading: true });

		return await api.media.fetchAll(params).then(res => {
			this.setState({
				records: res.data,
				order: res.order,
				columns: res.columns,
				total_records: res.total_records,
				loading: false
			});
		});
	};

	handleChange = e => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					[e.target.name]: e.target.value
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	handleChecked = (e, { name, checked }) => {
		const { selectedDataIndex: dataIndex, records } = this.state;

		this.setState({
			records: [
				...records.slice(0, dataIndex),
				{
					...records[dataIndex],
					[name]: checked
				},
				...records.slice(dataIndex + 1)
			]
		});
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { selectedDataIndex: dataIndex, records } = this.state;
		this.setState({
			loading: true
		});
		return api.media
			.submit(records[dataIndex])
			.then(data => {
				this.setState({
					loading: false,
					records: [
						...records.slice(0, dataIndex),
						data.media,
						...records.slice(dataIndex + 1)
					]
				});
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
				
			});
	};

	update = () => {
		const { selectedDataIndex, records } = this.state;
		const data = records[selectedDataIndex];
		this.setState({ loading: true });

		api.media.update(data.id, data).then(data => {
			this.setState({
				records: [
					...this.state.records.slice(0, selectedDataIndex),
					{ ...data.data },
					...this.state.records.slice(selectedDataIndex + 1)
				],
				loading: false
			});
		});
	};

	delete = id => api.media.delete(id);

	handleCloseEditModal = () => {
		const { records } = this.state;
		this.setState({
			editModalOpen: false,
			selectedId: -1,
			selectedDataIndex: -1,
			records: [...records].filter(c => c.id > 0)
		});
	};

	openModal = () => {
		this.setState({
			editModalOpen: true
		});
	};

	render() {
		const {
			media,
			records,
			loading,
			columns,
			total_records,
			selectedDataIndex,
			editModalOpen
		} = this.state;

		return (
			<div className="reply-icons whatsapp-select">
				<Popup
					trigger={
						<a
							className=""
							rel="tooltip"
							data-trigger="hover"
							data-original-title=""
							data-placement="bottom"
							title="Escolha por onde a mensagem será enviada"
						>
							<i className="fa fa-phone fa-2x" />
						</a>
					}
					content={
						<Dropdown
							placeholder="Escolha uma Opção"
							selection
							name="account_number"
						>
							<Dropdown.Menu>
								{media.map((d, i) => (
									<Dropdown.Item text={d.phone_number} />
								))}
							</Dropdown.Menu>
						</Dropdown>
					}
					className="popup-upload"
					on="click"
					position="top center"
				/>
			</div>
		);
	}
}
export default WhatsappAccountSelect;
