import React from "react";
import { Button, Icon, Modal } from "semantic-ui-react";
import Camera from "react-html5-camera-photo";
import UploadImageForm from "./UploadImageForm";
import UploadVideoForm from "./UploadVideoForm";
import "react-html5-camera-photo/build/css/index.css";
import $ from "jquery";

const MediaUploadButtons = ({
  save,
  saveVideo,
  sendDocument,
  loading = false,
  imageOnloadHandler,
  videoOnloadHandler,
  sendImageModalOpen = false,
  toggleImageModal,
  BasicUpload,
  openModal,
  openDocumentModal
}) => (
  <div className="reply-icons media-upload">
    <a href title="Anexar Arquivos">
      <i className="fa fa-paperclip fa-2x" />
    </a>
    <div className="popup-control popup-upload">
      <div className="actions_popup">
        <ul>
          <li>
            <Button color="red" circular icon onClick={toggleImageModal}>
              <Icon name="picture" />
            </Button>
          </li>
          <input
            type="file"
            id="image"
            name="myImage"
            className="upload_image"
            accept="image/x-png,image/gif,image/jpeg,.mp4"
            title="Fotos e Videos"
          />
          <input
            className="upload_image"
            type="file"
            accept=".mp4"
            id="video"
            name="video"
          />
          <input
            type="file"
            accept=".xlsx,.xls,.doc,.docx,.pdf,.txt"
            id="document"
            name="document"
          />
          <Modal
            id="uploadImage"
            modalTitle="Envio de Imagem"
            confirmButton={"Enviar"}
            size="small"
            onClose={toggleImageModal}
            open={sendImageModalOpen}
          >
            {(function() {
              $("#image").change(function(e) {
                if (e.target.files.length > 0) {
                  const tmppath = URL.createObjectURL(e.target.files[0]);
                  $("#img_preview")
                    .fadeIn("fast")
                    .attr("src", tmppath);

                  const file = document.getElementById("image").files[0];
                  const reader = new FileReader();
                  const ext = file.name.split(".").pop();
                  reader.readAsDataURL(file);

                  reader.onload = e => {
                    imageOnloadHandler(e, file, ext);
                  };
                }
              });
            })()}
            <Modal.Content>
              <UploadImageForm
                onClickUpload={() => {
                  $("#image").click();
                }}
              />
            </Modal.Content>
            <Modal.Actions>
              <Button.Group>
                <Button onClick={toggleImageModal}>Cancelar</Button>
                <Button.Or />
                <Button positive onClick={save} loading={loading}>
                  Enviar
                </Button>
              </Button.Group>
            </Modal.Actions>
          </Modal>
          <Modal
            id="uploadVideo"
            modalTitle="Envio de Vídeo"
            confirmButton={"Enviar"}
            size="small"
            loading={loading}
            trigger={
              <li>
                <Button color="red" circular icon>
                  <Icon name="video" />
                </Button>
              </li>
            }
          >
            {(function() {
              $("#video").change(function(e) {
                if (e.target.files.length > 0) {
                  const tmppath = URL.createObjectURL(e.target.files[0]);

                  const video = document.createElement("video");

                  video.onloadedmetadata = function() {
                    this.currentTime = Math.min(
                      Math.max(0, (1 < 0 ? this.duration : 0) + 1),
                      this.duration
                    );
                  };

                  video.onseeked = function(e) {
                    const canvas = document.createElement("canvas");
                    canvas.height = video.videoHeight;
                    canvas.width = video.videoWidth;
                    const ctx = canvas.getContext("2d");
                    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
                    const img = $("#video_preview");
                    img.src = canvas.toDataURL();

                    $("#video_preview")
                      .fadeIn("fast")
                      .attr("src", img.src);
                  };

                  video.src = tmppath;

                  const file = document.getElementById("video").files[0];
                  const reader = new FileReader();
                  const ext = file.name.split(".").pop();
                  reader.readAsDataURL(file);

                  reader.onload = e => {
                    videoOnloadHandler(e, file, ext);
                  };
                }
              });
            })()}
            <Modal.Content>
              <UploadVideoForm
                onClickUpload={() => {
                  $("#video").click();
                }}
              />
            </Modal.Content>
            <Modal.Actions>
              <Button.Group>
                <Button>Cancelar</Button>
                <Button.Or />
                <Button positive onClick={saveVideo}>
                  Enviar
                </Button>
              </Button.Group>
            </Modal.Actions>
          </Modal>
          <li>
            <Modal
              trigger={
                <Button color="green" circular icon>
                  <Icon name="camera" />
                </Button>
              }
            >
              <Modal.Header>Capturar foto</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <Camera
                    onTakePhoto={dataUri => {
                      this.onTakePhoto(dataUri);
                    }}
                  />
                </Modal.Description>
              </Modal.Content>
            </Modal>
          </li>
          <li onClick={openModal}>
            <Button color="blue" circular icon>
              <Icon name="file outline" />
            </Button>
          </li>
          <Modal
            id="uploadDocument"
            modalTitle="Envio de Arquivo"
            confirmButton={"Enviar"}
            size="small"
            loading={loading}
            open={openDocumentModal}
          >
            <Modal.Content>
              <BasicUpload />
            </Modal.Content>
            <Modal.Actions>
              <Button.Group>
                <Button>Cancelar</Button>
                <Button.Or />
                <Button
                  positive
                  onClick={sendDocument}
                  loading={loading}
                  disabled={loading}
                >
                  Enviar
                </Button>
              </Button.Group>
            </Modal.Actions>
          </Modal>
        </ul>
      </div>
    </div>
  </div>
);

export default MediaUploadButtons;
