import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";

const FastMessageSelect = ({ setMessage, fastMessages }) => (
  <div className="col-sm-1 col-xs-1 reply-icons fast-message-select">
    <OverlayTrigger
      trigger="click"
      rootClose
      placement="top"
      overlay={
        <Popover
          id="i-class-fa-fa-fw-fa-pencil-i-form-inside-popover-popover"
          style={{ width: "550px" }}
        >
          <div style={{ overflowY: "scroll", height: "450px" }}>
            <ul>
              {fastMessages.map((fastMessage, id) => {
                return (
                  <li
                    key={`fast-message-${id}`}
                    style={{ padding: "5px 5px 0px 0px" }}
                  >
                    <p
                      className=""
                      style={{
                        cursor: "pointer",
                        width: "auto",
                        wordBreak: "keep-all",
                        padding: "2px 5px 2px 5px",
                        textAlign: "justify",
                        fontSize: "12px",
                        border: "1px solid #ccc",
                        borderRadius: "5px"
                      }}
                      id={`fast-message-${fastMessage.id}`}
                      onClick={event => setMessage(event)}
                      rel="tooltip"
                      title={` (ref: ${fastMessage.id})`}
                      data-placement="bottom"
                    >
                      {`${fastMessage.id} - ${fastMessage.message}`}
                    </p>
                  </li>
                );
              })}
            </ul>
          </div>
        </Popover>
      }
    >
      <a
        className=""
        rel="tooltip"
        data-trigger="hover"
        data-original-title=""
        data-placement="bottom"
        title="Mensagem Rápida"
      >
        <i className="fa fa-search-plus fa-2x" />
      </a>
    </OverlayTrigger>
  </div>
);

export default FastMessageSelect;
