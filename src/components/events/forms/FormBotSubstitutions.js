import React from "react";
import PropTypes from "prop-types";
import {
  Form,
  Input,
  Checkbox,
  TextArea,
  Dropdown,
  Icon,
  Divider,
  Button,
  List,
  Radio
} from "semantic-ui-react";

const FormBotSubstitutions = ({
  substitution,
  onChange,
  onChecked,
  clients,
  groups,
  onSelectGroup,
  onSelectClient,
  onSearchGroupChange,
  onSearchClientChange
}) => {
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Usuário digitou:"
          name="word"
          onChange={onChange}
          value={substitution.word}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Substitua por:"
          name="substitution"
          onChange={onChange}
          value={substitution.substitution}
        />
      </Form.Group>
    </Form>
  );
};

FormBotSubstitutions.propTypes = {
  substitution: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  onChecked: PropTypes.func.isRequired
};

export default FormBotSubstitutions;
