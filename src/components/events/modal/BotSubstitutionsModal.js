import React from "react";
import PropTypes from "prop-types";

import { Modal, Button } from "semantic-ui-react";
import FormBotSubstitutions from "../forms/FormBotSubstitutions";

const BotSubstitutionsModal = ({
  substitution,
  handleClick = () => {},
  handleClose = () => {},
  onChange,
  onChecked,
  trigger,
  modalHeader = "",
  onSearchGroupChange,
  onSearchGroup,
  onSelectGroup,
  groups,
  clients
}) => {
  return (
    <Modal size="small" closeIcon trigger={trigger} onClose={handleClose}>
      <Modal.Header>{modalHeader}</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <div className="holder_substitution">
            <div className="form-substitution">
              <FormBotSubstitutions
                substitution={substitution}
                groups={groups}
                clients={clients}
                onChange={onChange}
                onChecked={onChecked}
                onSearchGroupChange={onSearchGroupChange}
                onSelectGroup={onSelectGroup}
              />
            </div>
          </div>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button primary onClick={handleClose}>
          Cancelar
        </Button>
        <Button primary onClick={handleClick}>
          Salvar
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

BotSubstitutionsModal.propTypes = {
  substitution: PropTypes.shape().isRequired,
  handleClick: PropTypes.func.isRequired,
  trigger: PropTypes.shape().isRequired,
  handleClose: PropTypes.func.isRequired,
  modalHeader: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onChecked: PropTypes.func.isRequired
};

export default BotSubstitutionsModal;
