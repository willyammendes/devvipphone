import React from "react";
import {
  Modal,
  Button,
  Form,
  Input,
  TextArea,
  Message,
  Dropdown,
} from "semantic-ui-react";
import DropdownUsers from "../../users/DropdownUsers";
import DropdownClients from "../../client/DropdownClients";
import { DateInput, TimeInput } from "semantic-ui-calendar-react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as TaskAction } from "../../../store/ducks/task";
import DropdownContacts from "../../contact/DropdownContacts";

const intervals = [
  { key: "minute", text: "minuto(s)", value: "minute" },
  { key: "second", text: "segundo(s)", value: "second" },
  { key: "hour", text: "hora(s)", value: "hour" },
  { key: "day", text: "dia(s)", value: "day" },
  { key: "month", text: "mes(es)", value: "month" },
  { key: "week", text: "semana(s)", value: "week" },
];

class TasksModal extends React.Component {
  state = {
    records: [],
    columns: {},
    order: {},
    error: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    state_test: true,
    contacts: [],
    event: [],
    task: {},
    dateShow: {},
  };

  // close modal
  handleCloseEditModal = () => {
    this.props.editModal({ payload: false, selectedDataIndex: -1 });
  };

  //set timestamp for send api
  setTimesTamp = (date, start) => {
    const splitDate = date.split("-");
    if (start === -2) {
      const [, b, c, d, e] = date.split(" ");
      const getMonth = new Date(`${d}-${c}-${b}`).getMonth();
      const showMonth = getMonth + 1 > 9 ? getMonth + 1 : `0${getMonth + 1}`;

      return `${d}-${showMonth}-${c} ${e}`;
    } else if (start === -1) {
      const [a, b, c] = splitDate;
      const secondSplit = c.split(" ");
      const [d, e] = secondSplit;
      const dateTime = [d, b, a];
      const joinDate = dateTime.join("-") + ` ${e}`;
      return joinDate;
    } else if (start) {
      const [dateStart, hourStart] = this.state.task.start_at.split(" ");
      if (splitDate.length === 3) {
        const [a, b, c] = splitDate;
        const joinDate = [c, b, a].join("-");
        return `${joinDate} ${hourStart}`;
      } else {
        return `${dateStart} ${date}`;
      }
    } else {
      if (this.state.task.end_at) {
        const [dateEnd, hourEnd] = this.state.task.end_at.split(" ");
        if (splitDate.length === 3) {
          const [a, b, c] = splitDate;
          const joinDate = [c, b, a].join("-");
          return hourEnd ? `${joinDate} ${hourEnd}` : joinDate;
        } else {
          return dateEnd ? `${dateEnd} ${date}` : date;
        }
      } else {
        if (splitDate.length === 3) {
          const [a, b, c] = splitDate;
          const joinDate = [c, b, a].join("-");
          return joinDate;
        }
        return date;
      }
    }
  };

  // get onchange for set data for send api
  onChange = (e, { name, value }) => {
    switch (name) {
      case "title":
        return this.setState({
          task: {
            ...this.state.task,
            title: value,
            status: 0,
            created_by: this.props.user.id,
            send_automatically: 0,
          },
        });
      case "description":
        return this.setState({
          task: { ...this.state.task, description: value },
        });
      case "start_date":
        return this.setState({
          dateShow: { ...this.state.dateShow, start_date_show: value },
          task: {
            ...this.state.task,
            start_at: this.setTimesTamp(value, 1),
            end_at: this.setTimesTamp(value, 0),
          },
        });
      case "start_hour":
        return this.setState({
          dateShow: { ...this.state.dateShow, start_hour_show: value },
          task: {
            ...this.state.task,
            start_at: this.setTimesTamp(value, 1),
            end_at: this.setTimesTamp(value, 0),
          },
        });
      case "end_hour":
        return this.setState({
          dateShow: { ...this.state.dateShow, end_hour_show: value },
          task: { ...this.state.task, end_at: this.setTimesTamp(value, 0) },
        });
      case "end_date":
        return this.setState({
          dateShow: { ...this.state.dateShow, end_date_show: value },
          task: {
            ...this.state.task,
            end_at: this.setTimesTamp(value, 0),
          },
        });
      default:
        this.setState({
          task: {
            ...this.state.task,
            [name]: value,
          },
        });
        break;
    }
  };

  //get date Today for set date show and date for send api
  handleTodayDate = () => {
    const today = new Date();

    const todayDate =
      today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();

    const monthZero =
      today.getMonth() + 1 > 9
        ? today.getMonth() + 1
        : `0${today.getMonth() + 1}`;

    const dateShow = `${todayDate}-${monthZero}-${today.getFullYear()}`;
    const todayMinute =
      today.getMinutes() < 10 ? `0${today.getMinutes()}` : today.getMinutes();
    const todayHour =
      today.getHours() < 10 ? `0${today.getHours()}` : today.getHours();
    const todayHourFormatted = `${todayHour}:${todayMinute}`;

    const getStartCalendarMinutes = this.props.start
      ? this.props.start.getMinutes() < 10
        ? `0${this.props.start.getMinutes()}`
        : this.props.start.getMinutes()
      : "";
    const getStartCalendarHour = this.props.start
      ? this.props.start.getHours() < 10
        ? `0${this.props.start.getHours()}`
        : this.props.start.getHours()
      : "";
    const getStartCalendarClock = this.props.start
      ? `${getStartCalendarHour}:${getStartCalendarMinutes}`
      : "";

    const getEndCalendarMinutes = this.props.end
      ? this.props.end.getMinutes() < 10
        ? `0${this.props.end.getMinutes()}`
        : this.props.end.getMinutes()
      : "";
    const getEndCalendarHour = this.props.end
      ? this.props.end.getHours() < 10
        ? `0${this.props.end.getHours()}`
        : this.props.end.getHours()
      : "";
    const getEndCalendarClock = this.props.end
      ? `${getEndCalendarHour}:${getEndCalendarMinutes}`
      : "";

    this.props.start !== this.props.end
      ? this.setState({
          dateShow: {
            ...this.state.task,
            start_date_show: this.props.start,
            start_hour_show: getStartCalendarClock,
            end_date_show: this.props.end,
            end_hour_show: getEndCalendarClock,
          },
          task: {
            ...this.state.task,
            start_at: this.setTimesTamp(
              `${this.props.start} ${getStartCalendarClock}`,
              -2
            ),
            end_at: this.setTimesTamp(
              `${this.props.end} ${getEndCalendarClock}`,
              -2
            ),
          },
        })
      : this.setState({
          dateShow: {
            ...this.state.task,
            start_date_show: this.props.start,
            start_hour_show: getStartCalendarClock,
          },
          task: {
            ...this.state.task,
            start_at: this.setTimesTamp(
              `${this.props.start} ${getStartCalendarClock}`,
              -2
            ),
            end_at: this.setTimesTamp(
              `${this.props.end} ${getEndCalendarClock}`,
              -2
            ),
          },
        });

    !!!this.props.start &&
      this.setState({
        dateShow: {
          ...this.state.task,
          start_date_show: dateShow,
          start_hour_show: todayHourFormatted,
        },
        task: {
          ...this.state.task,
          start_at: this.setTimesTamp(`${dateShow} ${todayHourFormatted}`, -1),
          end_at: this.setTimesTamp(`${dateShow} ${todayHourFormatted}`, -1),
        },
      });
  };

  // get user change the dropdown users
  handleUserChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      task: {
        ...this.state.task,
        ...records[dataIndex],
        user_id: value,
        ...records.slice(dataIndex + 1),
      },
    });
  };

  handleClientChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      task: {
        ...this.state.task,

        ...records[dataIndex],
        client_id: value,
        ...records.slice(dataIndex + 1),
      },
    });
  };

  // get user change the dropdown Clients
  handleContactChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      task: {
        ...this.state.task,
        ...records[dataIndex],
        contacts: value,
        ...records.slice(dataIndex + 1),
      },
    });
  };

  // start getting date now
  componentDidMount() {
    this.handleTodayDate();
  }

  // action send api
  submit = () => {
    if (
      new Date(this.state.task.start_at).getTime() <=
        new Date(this.state.task.end_at).getTime() &&
      this.state.task.title &&
      this.state.task.description &&
      this.state.task.user_id
    ) {
      this.setState({
        loading: true,
      });
      this.props.editModal({ payload: false, selectedDataIndex: -1 });
      this.props.addTask(this.state.task);
      setTimeout(() => {
        this.props.fetchRecords();
      }, 175);
    } else {
      this.setState({
        error: {
          list: [
            "Inicio da tarefa tem que ser maior que o final da tarefa",
            "Favor preencher todos os campos obrigatórios",
          ],
          pointing: "below",
        },
      });

      setTimeout(() => {
        this.setState({
          error: false,
        });
      }, 2000);
    }
  };

  render() {
    return (
      <Modal
        size="tiny"
        closeIcon
        onClose={this.handleCloseEditModal}
        open={this.props.editModal}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>Nova Tarefa</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="form-task">
              <Form>
                <Form.Group widths="equal">
                  <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Titulo:"
                    name="title"
                    onChange={this.onChange}
                    value={this.state.task.title}
                    error={this.state.task.title ? "" : this.state.error}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Field
                    control={TextArea}
                    rows={2}
                    label="Descrição:"
                    name="description"
                    onChange={this.onChange}
                    value={this.state.task.description}
                    error={this.state.task.description ? "" : this.state.error}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Data de Inicio:</label>
                    <DateInput
                      placeholder="Data de inicio da tarefa"
                      name="start_date"
                      dateTimeFormat="DD-MM-YYYY"
                      value={this.state.dateShow.start_date_show}
                      iconPosition="left"
                      onChange={this.onChange}
                      error={
                        this.state.dateShow.start_date_show
                          ? ""
                          : this.state.error
                      }
                    />
                  </div>
                  <div className="field">
                    <label>Hora de Inicio:</label>
                    <TimeInput
                      placeholder="Hora de inicio da tarefa"
                      name="start_hour"
                      value={this.state.dateShow.start_hour_show}
                      onChange={this.onChange}
                      error={
                        this.state.dateShow.start_hour_show
                          ? ""
                          : this.state.error
                      }
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Data de Encerramento:</label>
                    <DateInput
                      placeholder="Data de encerramento da tarefa"
                      name="end_date"
                      dateTimeFormat="DD-MM-YYYY HH:mm"
                      value={this.state.dateShow.end_date_show}
                      iconPosition="left"
                      onChange={this.onChange}
                      error={this.state.error}
                    />
                  </div>
                  <div className="field">
                    <label>Hora de Encerramento:</label>
                    <TimeInput
                      placeholder="Hora de Encerramento da tarefa"
                      name="end_hour"
                      value={this.state.dateShow.end_hour_show}
                      onChange={this.onChange}
                      error={this.state.error}
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <Input
                      label={
                        <Dropdown
                          defaultValue="minute"
                          name="reminder_interval"
                          value={this.state.task.reminder_interval}
                          options={intervals}
                          onChange={this.onChange}
                        />
                      }
                      labelPosition="right"
                      placeholder="Defina um lembrete para te avisar antes da tarefa começar"
                      name="reminder"
                      onChange={this.onChange}
                      value={this.state.task.reminder}
                      error={this.state.task.reminder ? "" : this.state.error}
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Responsável</label>
                    <DropdownUsers
                      onSelectUser={this.handleUserChange}
                      user_id={this.state.task.users}
                      allowAdditions={false}
                      error={this.state.task.users ? "" : this.state.error}
                    />
                  </div>
                  <div className="field">
                    <label>Cliente</label>
                    <DropdownClients
                      onSelectClient={this.handleClientChange}
                      clients={this.state.task.client_id}
                      allowAdditions={false}
                      error={this.state.client_id ? "" : this.state.error}
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Participantes</label>
                    <DropdownContacts
                      onSelectContact={this.handleContactChange}
                      contact_id={this.state.task.contacts}
                      allowAdditions={false}
                      options={this.state.task.contacts}
                      multiple
                    />
                  </div>
                </Form.Group>
              </Form>
            </div>
          </Modal.Description>
          {this.state.error && <Message error list={this.state.error.list} />}
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button onClick={this.handleCloseEditModal}>Cancelar</Button>
            <Button.Or text="ou" />
            <Button
              positive
              onClick={this.submit}
              loading={this.state.loading}
              disabled={this.state.loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TaskAction, dispatch);

const mapStateToProps = (state) => ({
  user: state.user.user,
  task: state.task,
  editModal: state.task.editModalOpen,
  selectedDataIndex: state.task.selectedDataIndex,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksModal);
