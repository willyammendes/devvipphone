import React from "react";
import {
  Modal,
  Form,
  Input,
  TextArea,
  Button,
  Message,
} from "semantic-ui-react";
import { DateInput, TimeInput } from "semantic-ui-calendar-react";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/pt-br";
import { bindActionCreators } from "redux";
import { Creators as TaskAction } from "../../../store/ducks/task";
import DropdownUsers from "../../users/DropdownUsers";
import DropdownClients from "../../client/DropdownClients";

class EditTask extends React.Component {
  state = {
    id: this.props.value,
    title: "",
    created_at: "",
    description: "",
    start_at: "",
    start_date: "",
    start_hour: "",
    end_at: "",
    end_date: "",
    client_id: "",
    status: "",
    start_date_show: "",
    start_hour_show: "",
    end_date_show: "",
    end_hour_show: "",
    user_id: "",
    error: false,
  };

  //mask Hour for show in screen
  maskHour = (hour) => {
    const newHour = new Date(hour * 1000).getHours();
    const newMinutes = new Date(hour * 1000).getMinutes();
    const formatMinutes = newMinutes < 10 ? `0${newMinutes}` : newMinutes;
    const formatHour = newHour < 10 ? `0${newHour}` : newHour;
    return `${formatHour}:${formatMinutes}`;
  };
  maskDate = (date) => {
    const newDate = new Date(date * 1000).getDate();
    const newMonth = new Date(date * 1000).getMonth() + 1;
    const newYear = new Date(date * 1000).getFullYear();
    const fortmatDate = newDate < 10 ? `0${newDate}` : newDate;
    const fortmatMonth = newMonth < 10 ? `0${newMonth}` : newMonth;
    return `${fortmatDate}-${fortmatMonth}-${newYear} `;
  };

  // fetch data for edit task
  fetchData = () => {
    this.props.task.records.forEach((task) => {
      if (task.id === this.props.value) {
        if (task.start_at === task.end_at) {
          return this.setState({
            title: task.title,
            user_id: parseInt(task.user_id),
            created_at: task.created_by,
            description: task.description,
            start_at: moment.unix(task.start_at).format("YYYY-MM-DD HH:mm"),
            end_at: moment.unix(task.end_at).format("YYYY-MM-DD HH:mm"),
            start_date_show: this.maskDate(task.start_at),
            start_hour_show: this.maskHour(task.start_at),
            client_id: task.client_id,
            status: task.status,
          });
        } else {
          return this.setState({
            title: task.title,
            user_id: parseInt(task.user_id),
            created_at: task.created_by,
            description: task.description,
            start_at: moment.unix(task.start_at).format("YYYY-MM-DD HH:mm"),
            end_at: moment.unix(task.end_at).format("YYYY-MM-DD HH:mm"),
            start_date_show: this.maskDate(task.start_at),
            start_hour_show: this.maskHour(task.start_at),
            end_date_show: this.maskDate(task.end_at),
            end_hour_show: this.maskHour(task.end_at),
            client_id: task.client_id,
            status: task.status,
          });
        }
      }
    });
  };

  // fetch data fo mont component
  componentDidMount() {
    this.fetchData();
  }
  // open and close modal
  handleCloseEditModal = () => {
    this.props.editModal({ payload: false, selectedDataIndex: -1 });
  };

  //change status task
  changeStatusTask = () => {
    this.props.task.records.forEach((task) => {
      if (task.id === this.props.value) {
        const sendUpdateTask = {
          id: task.id,
          status: task.status ? 0 : 1,
          title: task.title,
          description: task.description,
          created_by: task.created_by,
          company_id: task.company_id,
          send_automatically: task.send_automatically,
          user_id: task.user_id,
          created_at: task.created_at,
          client_id: task.client_id,
          ticket_id: task.ticket_id,
          start_at: moment.unix(task.start_at).format("YYYY-MM-DD HH:mm"),
          end_at: moment.unix(task.end_at).format("YYYY-MM-DD HH:mm"),
        };

        this.props.alterTask({
          id: sendUpdateTask.id,
          payload: sendUpdateTask,
        });

        this.props.alterEventTask({
          payload: sendUpdateTask,
        });
        setTimeout(() => {
          this.props.fetchRecords();
        }, 2000);
        setTimeout(() => {
          this.props.removeEventTask(task);
        }, 1000);
      }
    });

    this.props.editModal({ payload: false, selectedDataIndex: -1 });
  };

  //delete task
  deleteTask = () => {
    this.props.task.records.forEach((task) => {
      if (task.id === this.props.value) {
        this.props.deleteTask(task.id);
        setTimeout(() => {
          this.props.fetchRecords();
        }, 1000);
      }
    });
    this.props.editModal({ payload: false, selectedDataIndex: -1 });
  };

  //set date timestamp for send server
  setTimesTamp = (date, start) => {
    const firstSplit = date.split("-");
    if (start) {
      if (firstSplit.length === 3) {
        const getHour = this.state.start_at.split(" ");
        return this.setTimesfunction(date, getHour);
      } else {
        const getHour = this.state.start_at.split(" ");
        return this.setTimesfunction(date, getHour);
      }
    } else {
      if (firstSplit.length === 3) {
        const getHour = this.state.end_at.split(" ");
        return this.setTimesfunction(date, getHour);
      } else {
        const getHour = this.state.end_at.split(" ");
        return this.setTimesfunction(date, getHour);
      }
    }
  };
  setTimesfunction = (date, state) => {
    const splitDate = date.split("-");
    if (splitDate.length === 3) {
      const getHour = state;
      const [, hour] = getHour;
      const [a, b, c] = splitDate;
      const secondSplit = [c, b, a];
      const joinDate = secondSplit.join("-");
      return `${joinDate} ${hour}`;
    } else {
      const getHour = state;
      const [dates] = getHour;
      return `${dates} ${date}`;
    }
  };

  // onchange component, for alter elements the form
  onChange = (e, { name, value }) => {
    switch (name) {
      case "title":
        return this.setState({
          title: value,
        });
      case "description":
        return this.setState({
          description: value,
        });
      case "start_date":
        if (this.props.start_at === this.props.end_at) {
          return this.setState({
            start_date_show: value,
            start_at: this.setTimesTamp(value, 1),
            end_at: this.setTimesTamp(value, 0),
          });
        } else {
          return this.setState({
            start_date_show: value,
            start_at: this.setTimesTamp(value, 1),
          });
        }
      case "end_date":
        return this.setState({
          end_date_show: value,
          end_at: this.setTimesTamp(value, 0),
        });
      case "start_hour":
        if (this.props.start_at === this.props.end_at) {
          return this.setState({
            start_hour_show: value,
            start_at: this.setTimesTamp(value, 1),
            end_at: this.setTimesTamp(value, 0),
          });
        } else {
          return this.setState({
            start_hour_show: value,
            start_at: this.setTimesTamp(value, 1),
          });
        }

      case "end_hour":
        return this.setState({
          end_hour_show: value,
          end_at: this.setTimesTamp(value, 0),
        });
      default:
        return;
    }
  };

  //get user change
  handleUserChange = (event) => {
    this.setState({
      user_id: event,
    });
  };

  //get company change
  handleContactChange = (event) => {
    this.setState({
      client_id: event,
    });
  };

  //send data for api
  submit = () => {
    if (
      new Date(this.state.start_at).getTime() <=
        new Date(this.state.end_at).getTime() &&
      this.state.title &&
      this.state.description &&
      this.state.user_id
    ) {
      const newData = {
        id: this.state.id,
        title: this.state.title,
        user_id: this.state.user_id,
        description: this.state.description,
        start_at: this.state.start_at,
        end_at: this.state.end_at,
        client_id: this.state.client_id,
        status: this.state.status,
      };
      this.props.alterTask({
        id: newData.id,
        payload: newData,
      });
      setTimeout(() => {
        this.props.fetchRecords();
      }, 1000);

      this.props.alterEventTask({
        payload: newData,
      });
      setTimeout(() => {
        this.props.removeEventTask(newData);
      }, 1000);
      this.props.editModal({ payload: false, selectedDataIndex: -1 });
    } else {
      this.setState({
        error: {
          list: [
            "Inicio da tarefa tem que ser maior que o final da tarefa",
            "Favor preencher todos os campos obrigatórios",
          ],
          pointing: "below",
        },
      });

      setTimeout(() => {
        this.setState({
          error: false,
        });
      }, 2000);
    }
  };

  render() {
    return (
      <Modal
        onClose={this.handleCloseEditModal}
        closeIcon
        size="tiny"
        dimmer="blurring"
        open={this.props.editModalOpen}
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          <label> Edição Tarefas</label>
        </Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="form-task">
              <Form>
                <Form.Group widths="equal">
                  <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Titulo:"
                    name="title"
                    onChange={this.onChange}
                    value={this.state.title}
                    error={this.state.title ? "" : this.state.error}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Field
                    autoComplete="off"
                    control={TextArea}
                    label="Titulo:"
                    name="description"
                    onChange={this.onChange}
                    value={this.state.description}
                    error={this.state.description ? "" : this.state.error}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Alterar Data de Inicio:</label>
                    <DateInput
                      placeholder="Data de inicio da tarefa"
                      name="start_date"
                      dateTimeFormat="DD-MM-YYYY"
                      value={this.state.start_date_show}
                      iconPosition="left"
                      onChange={this.onChange}
                      error={this.state.start_date_show ? "" : this.state.error}
                    />
                  </div>
                  <div className="field">
                    <label>Alterar Hora de Inicio:</label>
                    <TimeInput
                      placeholder="Hora de inicio da tarefa"
                      name="start_hour"
                      value={this.state.start_hour_show}
                      onChange={this.onChange}
                      error={this.state.start_hour_show ? "" : this.state.error}
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Alterar Data de Encerramento:</label>
                    <DateInput
                      placeholder="Data de encerramento da tarefa"
                      name="end_date"
                      dateTimeFormat="DD-MM-YYYY HH:mm"
                      value={this.state.end_date_show}
                      iconPosition="left"
                      onChange={this.onChange}
                      error={
                        new Date(this.state.start_at).getTime() <=
                        new Date(this.state.end_at).getTime()
                          ? ""
                          : this.state.error
                      }
                    />
                  </div>
                  <div className="field">
                    <label>Alterar Hora de Encerramento:</label>
                    <TimeInput
                      placeholder="Hora do fim da tarefa"
                      name="end_hour"
                      value={this.state.end_hour_show}
                      onChange={this.onChange}
                      error={
                        new Date(this.state.start_at).getTime() <=
                        new Date(this.state.end_at).getTime()
                          ? ""
                          : this.state.error
                      }
                    />
                  </div>
                </Form.Group>
                <Form.Group widths="equal">
                  <div className="field">
                    <label>Responsável</label>
                    <DropdownUsers
                      onSelectUser={this.handleUserChange}
                      user_id={this.state.user_id}
                      allowAdditions={false}
                    />
                  </div>
                  <div className="field">
                    <label>Cliente</label>
                    <DropdownClients
                      onSelectClient={this.handleContactChange}
                      clients={this.state.client_id}
                      allowAdditions={false}
                    />
                  </div>
                </Form.Group>
              </Form>
            </div>
          </Modal.Description>
          {this.state.error && <Message error list={this.state.error.list} />}
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button onClick={this.changeStatusTask} color="orange">
              {this.state.status ? "Refazer Tarefa" : "Concluir Tarefa"}
            </Button>
            <Button.Or text="ou" />
            <Button onClick={this.deleteTask} color="red">
              Deletar Tarefa
            </Button>
            <Button.Or text="ou" />
            <Button
              positive
              onClick={this.submit}
              loading={this.state.loading}
              disabled={this.state.loading}
            >
              Alterar Tarefa
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TaskAction, dispatch);

const mapStateToProps = (state) => ({
  user: state.user.user,
  task: state.task,
  editModalOpen: state.task.editModalOpen,
  selectedDataIndex: state.task.selectedDataIndex,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTask);
