import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as TaskAction } from "../../store/ducks/task";
import PropTypes from "prop-types";
import { Icon, Button, Card } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import TasksModal from "./modal/TasksModal";
import EditTask from "./modal/EditTask";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "moment/locale/pt-br";
import "../../../node_modules/react-big-calendar/lib/css/react-big-calendar.css";

class Events extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    event: [],
    color: [],
    start: "",
    end: ""
  };

  // open modal new task and edit task
  newDataClick = event => {
    if (event.id) {
      this.props.editModal({ payload: true, selectedDataIndex: 2 });
      this.setState({ records: event.id });
    } else {
      this.props.editModal({ payload: true, selectedDataIndex: 1 });
    }
  };

  // mask hour for screen users
  maskHour = hour => {
    const newHour = new Date(hour * 1000).getHours();
    const newMinutes = new Date(hour * 1000).getMinutes();
    const date = new Date(hour * 1000).getDate();
    const month = new Date(hour * 1000).getMonth() + 1;
    const year = new Date(hour * 1000).getFullYear();
    const formatDate = date < 10 ? `0${date}` : date;
    const formatMonth = month < 10 ? `0${month}` : month;
    const formatMinutes = newMinutes < 10 ? `0${newMinutes}` : newMinutes;
    const formatHour = newHour < 10 ? `0${newHour}` : newHour;
    return `${formatDate}/${formatMonth}/${year} - ${formatHour}:${formatMinutes}`;
  };

  // get event the big calendar for mark in calendar
  componentDidUpdate(prevProps) {
    if (this.props.task !== prevProps.task) {
      this.setState({
        start: "",
        end: ""
      });
      setTimeout(() => {
        this.setState({
          event: this.props.task.records.map(c => ({
            id: c.id,
            title: `${c.title}: ${c.description}(Finaliza em ${this.maskHour(
              c.end_at
            )})`,
            start: new Date(c.start_at * 1000),
            end: new Date(c.end_at * 1000),
            user_id: c.user_id
          })),
          loading: false
        });
      }, 500);
    }
  }

  // fetch data for calendar
  componentWillMount() {
    this.props.fetchRecords();
    this.setState({
      start: "",
      end: "",
      event: this.props.task.records.map(c => ({
        id: c.id,
        title: `${c.title}: ${c.description}(Finaliza em ${this.maskHour(
          c.end_at
        )})`,
        start: new Date(c.start_at * 1000),
        end: new Date(c.end_at * 1000),
        user_id: c.user_id
      })),
      loading: false
    });
  }
  handleSelect = ({ start, end }) => {
    setTimeout(() => {
      this.props.editModal({ payload: true, selectedDataIndex: 1 });
    }, 100);

    this.setState({
      start: start,
      end: end
    });
  };

  // set color in calendar
  editingCalendar = event => {
    const user = this.props.task.usercolors.find(
      user => user.id === event.user_id
    );
    const backgroundColor = user ? user.color : "#c2c2c2";
    return { style: { backgroundColor, color: "white", fontWeight: "bold" } };
  };

  render() {
    const { loading } = this.state;

    const localizer = momentLocalizer(moment);
    // render names description colors
    const listItens = this.props.task.usercolors.map(items => {
      return (
        <Card key={items.id} size="mini">
          <Card.Header
            style={{
              backgroundColor: `${items.color}`,
              textAlign: "center",
              color: "white"
            }}
          >
            <strong>{items.name} </strong>
          </Card.Header>
        </Card>
      );
    });

    return (
      <div className="pageHolder">
        <div className="headingPage" />
        {loading && (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        )}
        <div className="holderPage">
          <div className="calendar full">
            <div className="tabela-padrao">
              <Button
                onClick={event => this.newDataClick(event)}
                color="blue"
                className="button_blue"
              >
                <Icon name="plus" />
                Adicionar Tarefa
              </Button>
              {this.props.selectedDataIndex === 2 ? (
                <EditTask value={this.state.records} />
              ) : this.props.selectedDataIndex !== -1 ? (
                <TasksModal start={this.state.start} end={this.state.end} />
              ) : null}

              <Calendar
                selectable
                localizer={localizer}
                events={this.state.event}
                onSelectEvent={event => this.newDataClick(event)}
                eventPropGetter={this.editingCalendar}
                onSelectSlot={this.handleSelect}
                dayLayoutAlgorithm="dayLayoutAlgorithm"
                startAccessor="start"
                style={{ height: 500 }}
                endAccessor="end"
                messages={{
                  date: "Data",
                  time: "Tempo",
                  event: "Evento",
                  allDay: "Dia Inteiro",
                  week: "Semana",
                  work_week: "Dias uteis",
                  day: "Dia",
                  month: "Mês",
                  previous: "Anterior",
                  next: "Próximo",
                  yesterday: "Ontem",
                  tomorrow: "Amanhã",
                  today: "Hoje",
                  agenda: "Agenda",
                  noEventsInRange: "Não existem eventos nesse escopo."
                }}
              />
              <Card fluid>
                <Card.Header>Descrição cores</Card.Header>
                <Card.Content>
                  <Card.Group>{listItens}</Card.Group>
                </Card.Content>
              </Card>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Events.propTypes = {
  name: PropTypes.any.isRequired,
  id: PropTypes.any.isRequired
};
const mapDispatchToProps = dispatch => bindActionCreators(TaskAction, dispatch);

const mapStateToProps = state => ({
  user: state.user.user,
  name: state.user.user.name,
  id: state.user.user.id,
  editModal: state.task.editModalOpen,
  selectedDataIndex: state.task.selectedDataIndex,
  task: state.task
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Events);
