import React, { Component } from "react";
import api from "../../api/users";
import { Dropdown } from "semantic-ui-react";

class DropdownRoles extends Component {
  state = {
    roles: {
      name: ""
    },
    options: [],
    loading: false,
    search: "",
    openModal: false
  };

  handleUserAddition = e => {};

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({ search: searchQuery });
    this.timer = setTimeout(this.onSearchUser, 300);
  };
  componentWillMount() {
    const take = 50;
    api.user.fetchRole({ take }).then(users => {
      this.setState({
        options: users.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loading: false
      });
    });
  }

  componentDidMount() {}

  onSearchUser = async () => {
    this.setState({ loading: true });
    const { search } = this.state;

    await api.user.fetchRole({ search }).then(users => {
      this.setState({
        options: users.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loading: false
      });
    });
  };

  onChange = (e, { name, value }) => {
    this.setState(state => ({
      roles: { ...state.roles, [name]: value }
    }));
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { user } = this.state;

    this.setState({
      loading: true
    });

    return api.user
      .submit(user)
      .then(data => {
        this.setState(state => ({
          options: [...state.options].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          loading: false,
          roles: {
            name: ""
          },
          openModal: !state.openModal
        }));

        this.props.onSelectUser(data.data.id);
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  toggle = (e, { name, value }) => {
    this.setState(state => ({
      openModal: !state.openModal,
      roles: {
        name: value
      }
    }));
  };

  render() {
    const { options, loading } = this.state;

    return (
      <div>
        <Dropdown
          placeholder="Pesquise um Perfil"
          fluid
          search
          selection
          scrolling
          multiple
          allowAdditions={false}
          className="online"
          additionLabel="Adicionar: "
          name="roles"
          value={this.props.roles}
          onSearchChange={this.onSearchChange}
          onChange={(e, { value, options }) =>
            this.props.onSelectRoles(value, options)
          }
          options={options}
          onAddItem={this.toggle}
          closeOnChange
          loading={loading}
          clearable
        />
      </div>
    );
  }
}

export default DropdownRoles;
