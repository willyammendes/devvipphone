import React, { Component } from "react";
import api from "../../api/users";
import { Dropdown } from "semantic-ui-react";
import AddUserModal from "./modal/AddUserModal";

class DropdownUsers extends Component {
	state = {
		user: {
			name: "",
			email: "",
			password: "",
			phone_number: ""
		},
		options: [],
		loading: false,
		search: "",
		openModal: false
	};

	handleUserAddition = e => {};

	onSearchChange = (e, { searchQuery }) => {
		clearTimeout(this.timer);
		this.setState({ search: searchQuery });
		this.timer = setTimeout(this.onSearchUser, 300);
	};
	componentWillMount() {
		const take = 50;
		api.user.fetchAll({ take }).then(users => {
			this.setState({
				options: users.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.name,
					icon: c.active ? { avatar: "true", src: "" } : null
				})),
				loading: false
			});
		});
	}

	componentDidMount() {}

	onSearchUser = async () => {
		this.setState({ loading: true });
		const { search } = this.state;

		await api.user.fetchAll({ search }).then(users => {
			this.setState({
				options: users.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.name,
					icon: c.active ? { avatar: "true", src: "" } : null
				})),
				loading: false
			});
		});
	};

	onChange = (e, { name, value }) => {
		this.setState(state => ({
			user: { ...state.user, [name]: value }
		}));
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { user } = this.state;

		this.setState({
			loading: true
		});

		return api.user
			.submit(user)
			.then(data => {
				this.setState(state => ({
					options: [...state.options].concat({
						key: data.data.id,
						value: data.data.id,
						text: data.data.name
					}),
					loading: false,
					user: {
						name: "",
						email: "",
						password: "",
						phone_number: ""
					},
					openModal: !state.openModal
				}));

				this.props.onSelectUser(data.data.id);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
			});
	};

	toggle = (e, { name, value }) => {
		this.setState(state => ({
			openModal: !state.openModal,
			user: {
				name: value,
				email: "",
				password: "",
				phone_number: ""
			}
		}));
	};

	render() {
		const { options, loading, openModal, user } = this.state;

		return (
			<div>
				<Dropdown
					placeholder="Pesquise um Usuário"
					fluid
					search
					selection
					scrolling
					allowAdditions={false}
					className="online"
					additionLabel="Adicionar: "
					name="user_id"
					value={this.props.user_id}
					onSearchChange={this.onSearchChange}
					onChange={(e, { value, options }) => this.props.onSelectUser(value, options)}
					options={options}
					onAddItem={this.toggle}
					closeOnChange
					loading={loading}
					clearable
				/>
				<AddUserModal
					open={openModal}
					user={user}
					save={this.submit}
					toggle={this.toggle}
					onChange={this.onChange}
				/>
			</div>
		);
	}
}

export default DropdownUsers;
