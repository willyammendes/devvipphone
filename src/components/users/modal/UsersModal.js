import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormUsers from "../forms/FormUsers";

class UsersModal extends React.Component {
    state = {
        userProfile: false
    };
    componentDidMount() {
        const { user } = this.props;
        if (user.id) {
            document.removeEventListener("keydown", () => {});
            document.addEventListener("keydown", e => {
                if (e.keyCode === 39) this.props.handleNext();
                if (e.keyCode === 37) this.props.handlePrevious();
            });
        }
    }

    save = () => {
        if (this.props.user.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    ChangeTab = () => {
        this.setState({
            userProfile: !this.state.userProfile
        });
    };

    render() {
        const {
            user,
            handleClose,
            onChange,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading,
            onChecked,
            handleGroupChange,
            handleDepartmentChange,
            handleMediaChange,
            messageError,
            generalError,
            cleanErrors,
            onSelectRoles
        } = this.props;

        return (
            <Modal
                size="tiny"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                {!this.state.userProfile ? (
                    <Modal.Header>
                        {modalHeader} |{" "}
                        <a href="/#" onClick={this.ChangeTab}>
                            Perfil de Usuário
                        </a>
                    </Modal.Header>
                ) : (
                    <Modal.Header>
                        <a href="/#" onClick={this.ChangeTab}>
                            {modalHeader}
                        </a>
                        | Perfil de Usuário
                    </Modal.Header>
                )}

                <Modal.Content>
                    <Modal.Description>
                        {generalError && (
                            <div className="errors-table">
                                <Button
                                    circular
                                    basic
                                    color="black"
                                    icon="close"
                                    className="button-close"
                                    onClick={cleanErrors}
                                />
                                <p>{generalError}</p>
                            </div>
                        )}
                        <div className="form-user">
                            <FormUsers
                                user={user}
                                onSelectRoles={onSelectRoles}
                                userProfile={this.state.userProfile}
                                onChange={onChange}
                                onChecked={onChecked}
                                handleGroupChange={handleGroupChange}
                                handleDepartmentChange={handleDepartmentChange}
                                handleMediaChange={handleMediaChange}
                                messageError={messageError}
                            />
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    {user.id ? (
                        <Button.Group>
                            <Button
                                icon
                                onClick={handlePrevious}
                                disabled={previousButtonEnabled}
                            >
                                <Icon name="left arrow" />
                            </Button>
                            <Button
                                icon
                                onClick={handleNext}
                                disabled={nextButtonEnabled}
                            >
                                <Icon name="right arrow" />
                            </Button>
                        </Button.Group>
                    ) : (
                        ""
                    )}

                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default UsersModal;
