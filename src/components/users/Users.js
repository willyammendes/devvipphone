import React, { Component } from "react";
import api from "../../api/users";
import DataTable from "../table/DataTable";
import UsersModal from "./modal/UsersModal";
import AlertSuccess from "../alerts/AlertSuccess";
import moment from "moment";
import "moment/locale/pt-br";

class Users extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    user_id: "",
    groups: [],
    departments: [],
    roles: [],
    accounts: []
  };

  select = selectedDataId => {
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              active: false
            }),
          {}
        )
      : {
          active: false
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.user.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleUserChange = (value, text) => {
    this.setState({
      user_id: {
        ...this.state.user_id,
        user_id: value
      }
    });
  };

  handleGroupChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  handleDepartmentChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          departments: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  handleMediaChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          accounts: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  onSelectRoles = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          roles: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.user
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.user
      .update(data.id, data)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  delete = id => {
    const { user_id } = this.state;
    const user = { user_id: user_id.user_id };
    api.user.delete(id, user);
  };

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      user_id
    } = this.state;

    const user = records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Usuários</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records.map(r => {
                  return {
                    ...r,
                    last_seen: r.last_seen
                      ? moment
                          .unix(r.last_seen)
                          .locale("pt-br")
                          .format("lll")
                      : "",
                    active: r.active ? "Sim" : "Não"
                  };
                })}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records.map(r => {
                  return {
                    ...r,
                    last_seen: r.last_seen ? (
                      <span>
                        {moment
                          .unix(r.last_seen)
                          .locale("pt-br")
                          .format("lll")}
                      </span>
                    ) : (
                      ""
                    ),
                    active: r.active ? "Sim" : "Não"
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                selectUserModal
                user_id={user_id}
                handleUserChange={this.handleUserChange}
                cleanErrors={this.cleanErrors}
                onDelete={id => this.delete(id)}
                onEditClick={d => this.select(d.id)}
                fetchData={this.fetchRecords}
              />

              {selectedDataIndex !== -1 ? (
                <UsersModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  user={user}
                  onChecked={this.handleChecked}
                  onSelectRoles={this.onSelectRoles}
                  modalHeader={
                    user.id ? `Edição do ${user.name}` : "Novo Usuário"
                  }
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.errors
                      : ""
                  }
                  generalError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  cleanErrors={this.cleanErrors}
                  handleGroupChange={this.handleGroupChange}
                  handleDepartmentChange={this.handleDepartmentChange}
                  handleMediaChange={this.handleMediaChange}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Users;
