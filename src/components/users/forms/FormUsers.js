import React from "react";
import { Form, Input, Dropdown, TextArea, Checkbox } from "semantic-ui-react";
import DropdownGroups from "../../contacts-groups/DropdownGroups";
import DropdownDepartments from "../../departments/DropdownDepartments";
import DropdownMedia from "../../social-media/DropdownMedia";
import DropdownRoles from "../DropdownRoles";
import InlineError from "../../messages/InlineError";
import InputMask from "react-input-mask";

const options = [
  {
    key: "Administrador",
    text: "Administrador",
    value: "2",
  },
  {
    key: "Usuário",
    text: "Usuário",
    value: "1",
  },
];

const FormUsers = ({
  user,
  onChange,
  onChecked,
  handleGroupChange,
  handleDepartmentChange,
  handleMediaChange,
  messageError,
  userProfile,
  onSelectRoles,
}) => {
  return (
    <Form autoComplete="off">
      {!userProfile ? (
        <div className="holder_form_user">
          <Form.Group widths="equal">
            <Form.Field error={messageError ? !!messageError.name : ""}>
              <label htmlFor="name">Nome</label>
              <Input
                autoComplete="off"
                control={Input}
                name="name"
                onChange={onChange}
                value={user.name}
                placeholder="Nome"
              />
              {messageError ? <InlineError text={messageError.name} /> : ""}
            </Form.Field>

            <Form.Field error={messageError ? !!messageError.email : ""}>
              <label htmlFor="email">Email</label>
              <Input
                autoComplete="off"
                control={Input}
                name="email"
                onChange={onChange}
                value={user.email}
              />
              {messageError ? <InlineError text={messageError.email} /> : ""}
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field error={messageError ? !!messageError.phone_number : ""}>
              <label htmlFor="phone_number">Telefone*</label>
              <Input
                autoComplete="off"
                control={Input}
                children={
                  <InputMask
                    mask="+55 (99) 99999-9999"
                    name="phone_number"
                    onChange={onChange}
                    value={user.phone_number}
                    placeholder="Telefone"
                  />
                }
              />
              {messageError ? (
                <InlineError text={messageError.phone_number} />
              ) : (
                ""
              )}
            </Form.Field>

            <Form.Field error={messageError ? !!messageError.password : ""}>
              <label htmlFor="password">Senha</label>
              <Input
                autoComplete="off"
                control={Input}
                type="password"
                name="password"
                onChange={onChange}
                value={user.password}
              />
              {messageError ? <InlineError text={messageError.password} /> : ""}
            </Form.Field>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field error={messageError ? !!messageError.branch_line : ""}>
              <label htmlFor="branch_line">Ramal</label>
              <Input
                autoComplete="off"
                control={Input}
                name="branch_line"
                onChange={onChange}
                value={user.branch_line}
              />
              {messageError ? (
                <InlineError text={messageError.branch_line} />
              ) : (
                ""
              )}
            </Form.Field>

            <Form.Field error={messageError ? !!messageError.branch_line : ""}>
              <label htmlFor="branch_line">Agente IPBX</label>
              <Input
                autoComplete="off"
                control={Input}
                name="agent_ipbx"
                onChange={onChange}
                value={user.agent_ipbx}
              />
              {messageError ? (
                <InlineError text={messageError.agent_ipbx} />
              ) : (
                ""
              )}
            </Form.Field>
          </Form.Group>
          <Form.Field
            error={messageError ? !!messageError.additional_info : ""}
          >
            <label htmlFor="additional_info">Informações adicionais</label>
            <Input
              control={TextArea}
              name="additional_info"
              onChange={onChange}
              value={user.additional_info}
            />
            {messageError ? (
              <InlineError text={messageError.additional_info} />
            ) : (
              ""
            )}
          </Form.Field>

          <div className="field">
            <label>Grupos de Relacionamento</label>
            <DropdownGroups
              onSelectGroup={handleGroupChange}
              groups={user.groups}
              allowAdditions={false}
            />
            {messageError ? <InlineError text={messageError.groups} /> : ""}
          </div>

          <div className="field">
            <label>Perfis de Acesso</label>
            <DropdownRoles
              onSelectRoles={onSelectRoles}
              roles={user.roles}
              allowAdditions={false}
            />
            {messageError ? <InlineError text={messageError.roles} /> : ""}
          </div>
          <div className="field">
            <label>Departamentos</label>
            <DropdownDepartments
              onSelectDepartment={handleDepartmentChange}
              multiple={true}
              departments={user.departments}
            />
            {messageError ? (
              <InlineError text={messageError.departments} />
            ) : (
              ""
            )}
          </div>
          <div className="field">
            <label>Contas Vinculadas</label>
            <DropdownMedia
              onSelectMedia={handleMediaChange}
              accounts={user.accounts}
              allowAdditions={false}
            />
            {messageError ? <InlineError text={messageError.accounts} /> : ""}
          </div>

          <div className="field">
            <Checkbox
              label="Ativo"
              onChange={onChecked}
              name="active"
              checked={user.active}
            />
          </div>

          <div className="field">
            <Checkbox
              label="Pode Executar Comandos Pelo Whatsapp"
              onChange={onChecked}
              name="can_execute_commands"
              checked={user.can_execute_commands}
            />
          </div>
        </div>
      ) : (
        <div className="holder_form_user">
          <Form.Field error={messageError ? !!messageError.roles : ""}>
            <label htmlFor="roles">Perfis de Acesso*</label>
            <Dropdown
              placeholder="Perfis de Acesso do Usuário"
              fluid
              selection
              search
              name="roles"
              value={user.roles}
              options={options}
              onChange={onSelectRoles}
            />
            {messageError ? <InlineError text={messageError.roles} /> : ""}
          </Form.Field>
          {/* Perfis */}
          <Form>
            <Form.Field>
              <Checkbox
                label="Pode Atender Ticket"
                onChange={onChecked}
                name="monitcall"
                checked={user.monitcall}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                label="Pode Excluir mensagens"
                onChange={onChecked}
                name="monitcall1"
                checked={user.monitcall1}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                label="Pode Excluir Tickets"
                onChange={onChecked}
                name="monitcall2"
                checked={user.monitcall2}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                label="Pode Editar Robô"
                onChange={onChecked}
                name="monitcall3"
                checked={user.monitcall3}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                label="Pode Encaminhar Tickets"
                onChange={onChecked}
                name="monitcall4"
                checked={user.monitcall4}
              />
            </Form.Field>
          </Form>
        </div>
      )}
    </Form>
  );
};

export default FormUsers;
