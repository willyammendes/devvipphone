import React from "react";
import Popover from "../semantic/Popover";

const UserContent = ({ user }) => (
  <ul>
    <li className="w-20">
      <div className="dataEditar" key={user.id}>
        <Popover value={user.name}>Nome:</Popover>
        <span>{user.name}</span>
      </div>
    </li>
    <li className="w-20">
      <div className="dataEditar" key={user.id}>
        <b />
        <Popover value={user.email}>Email</Popover>
        <span>{user.email}</span>
      </div>
    </li>
    <li className="w-15">
      <div className="dataEditar" key={user.id}>
        <b />
        <Popover value={user.phone_number}>Número do Celular</Popover>
        <span>{user.phone_number}</span>
      </div>
    </li>
    <li className="w-30">
      <div className="dataEditar" key={user.id}>
        <b />
        <Popover value={user.additional_info}>Descrição</Popover>
        <span>{user.additional_info}</span>
      </div>
    </li>
  </ul>
);

export default UserContent;
