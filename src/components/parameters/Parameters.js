import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Card,
  Icon,
  Checkbox,
  Form,
  Input,
  TextArea,
  Button,
  Modal,
  Header,
  Divider,
  Message,
  Grid,
} from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/parameters";
import apiService from "../../services/api";
import apicompany from "../../api/company";
import CardGreeting from "./CardGreeting";

class Parameters extends Component {
  state = {
    records: [],
    columns: [],
    parameter: [],
    activeParamater: [],
    loading: false,
    hasMore: true,
    save: false,
    loadingConnection: true,
    selectedDataIndex: 0,
    connected: false,
    error: "",
    dataIndex: 0,
    resetCode: "",
    confirmationCode: "",
    deleteContacts: false,
    deleteCampaings: false,
    deleteTickets: false,
    deleteMessages: false,
    deleteConversations: false,
    warned: false,
    success: false,
    errorTitle: "",
    successMessage: "",
    requestDeleteModal: false,
    restrict_contacts_by_departments: false,
    restrict_contacts_by_accounts: false,
    restrict_contacts_by_group: false,
    restrict_contacts_by_contact_user: false,
    monitcall_base_url: "",
    open_ticket_from_call: false,
    login_logout_monitcall_on_monitchat_auth: false,
    pause_monitcall_on_monitchat_pause: false,
    monitcall_client_key: "",
    click_to_call: false,
    default_message_on_ticket_created: "",
    send_message_on_ticket_created: false,
    unpause_user_on_login: false,
    pause_user_on_logoff: false,
    create_ticket_on_new_call_received: false,
    create_ticket_on_new_message_received: false,
    multi_contact_user: false,
    user_can_see_contact_with_no_user: false,
  };

  handleChange = (e, { name, value }) => {
    const { records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, 0),
        {
          ...records[0],
          [name]: value,
        },
        ...records.slice(0 + 1),
      ],
    });
  };

  handleChangeDelete = (e, { name, value }) => {
    this.setState({
      [name]: value,
    });
  };

  closeSuccess = () => {
    this.setState({
      success: false,
    });
  };

  testServer = (server) => {
    this.setState({ loadingConnection: true });
    apiService
      .get("/monitcall/serverTest?server=" + server)
      .then((res) => {
        this.setState({
          connected: res.data.connected,
          loadingConnection: false,
        });
      })
      .catch((err) => {
        this.setState({
          connected: false,
          loadingConnection: false,
        });
      });
  };

  validateDeleteModal = () => {
    const {
      deleteContacts,
      deleteCampaings,
      deleteConversations,
      deleteMessages,
      deleteTickets,
      confirmationCode,
      warned,
    } = this.state;

    if (
      deleteContacts ||
      deleteConversations ||
      deleteMessages ||
      deleteTickets ||
      deleteCampaings
    ) {
      this.setState({
        deleteModalOpen: true,
        error: "",
      });
    } else {
      this.setState({
        error: "Escolha os dados que deseja deletar",
      });
      return;
    }

    if (!confirmationCode) {
      this.setState({
        error: "Informe o codigo de confirmação",
        deleteModalOpen: false,
      });

      return;
    }
    this.setState({
      error: "",
      deleteModalOpen: true,
    });

    if (!warned) {
      this.setState({
        error: "Você precisa marcar o checkbox, concordando com esta operação!",
        deleteModalOpen: false,
      });
    } else {
      this.setState({
        error: "",
        deleteModalOpen: true,
      });
    }
  };

  deleteData = () => {
    this.setState({
      loading: true,
    });
    const {
      deleteContacts,
      deleteTickets,
      deleteMessages,
      deleteConversations,
      confirmationCode,
      deleteCampaings,
      warned,
    } = this.state;

    apicompany.company
      .resetData({
        confirmationCode,
        warned,
        deleteContacts,
        deleteCampaings,
        deleteConversations,
        deleteMessages,
        deleteTickets,
      })
      .then((res) => {
        this.setState(
          {
            deleteModalOpen: false,
            deleteContacts: false,
            deleteCampaings: false,
            deleteConversations: false,
            deleteMessages: false,
            deleteTickets: false,
            warned: false,
            confirmationCode: "",
            resetCode: "",
            success: true,
            successMessage: res.data.message,
            loading: false,
          },
          () => {
            this.requestResetCode();
          }
        );
      })
      .catch((err) =>
        this.setState({
          error: err.response ? err.response.data.message : err,
          loading: false,
          errorTitle: err.response
            ? err.response.data.title
            : "Erro na solicitação",
        })
      );
  };

  closeDeleteModal = () => {
    this.setState({
      deleteModalOpen: !this.state.deleteModalOpen,
    });
  };

  closeRequestDeleteModal = () => {
    this.setState({
      requestDeleteModal: !this.state.requestDeleteModal,
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { records } = this.state;
    this.setState({ loading: true });
    const parameter = records.length > 0 && records[0];

    api.parameter.update(parameter.id, { [name]: checked }).then((data) => {
      this.setState({
        save: true,
        loading: false,
        save_alert: false,
        records: [
          ...records.slice(0, 0),
          {
            ...records[0],
            [name]: checked,
          },
          ...records.slice(0 + 1),
        ],
      });
    });
  };

  handleCheckedDelete = (e, { name, checked }) => {
    this.setState(
      {
        [name]: checked,
      },
      () => {
        if (this.state.deleteContacts) {
          this.setState({
            deleteMessages: true,
            deleteConversations: true,
            deleteTickets: true,
            deleteCampaings: true,
          });
        }

        if (this.state.deleteConversations) {
          this.setState({
            deleteMessages: true,
          });
        }
      }
    );
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.parameter.fetchData(params).then((res) => {
      this.setState({
        records: res.data,
        columns: res.columns,
        loading: false,
      });
    });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    this.setState({ loading: true });
    const parameter = records.length > 0 && records[selectedDataIndex];

    api.parameter.update(parameter.id, parameter).then((data) => {
      this.setState({
        save: true,
        loading: false,
      });
      setTimeout(
        function() {
          this.setState({ save_alert: false });
        }.bind(this),
        5000
      );
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  componentDidMount() {
    const { company_id, companies } = this.props;

    const parameter = companies.find((company) => company.id === company_id)
      .parameters;

    this.testServer(parameter.monitcall_base_url);
  }

  requestResetCode = () => {
    apicompany.company.generateResetDataCode().then((data) => {
      this.setState({
        resetCode: data.data,
      });
    });
  };
  render() {
    const { loading, records, selectedDataIndex } = this.state;

    const parameter = records.length > 0 && records[selectedDataIndex];

    return (
      <div className=" pageHolder">
        <div className="headingPage">
          <h1>Parâmetros</h1>
        </div>
        {loading && (
          <div className="loading-conversa">
            <LoaderComponent />
          </div>
        )}
        <div className="holderPage">
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <div className="options-list">
                  <Button icon color="blue">
                    <Icon name="backward" />
                  </Button>
                  <Button icon color="blue">
                    <Icon name="refresh" />
                  </Button>
                  <Button icon color="blue">
                    <Icon name="save outline" onClick={this.update} />
                  </Button>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Divider />
          <Grid columns={4}>
            <Grid.Row>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Chave de Acesso</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        <Input value={parameter.client_key} />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Confirmação de Leitura</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Marque abaixo se deseja marcar como lida apenas quando
                        responder a mensagem.
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Marcar mensagem como lida apenas na resposta"
                          onChange={this.handleChecked}
                          name="reply_to_read_messages"
                          checked={parameter.reply_to_read_messages}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Atribuição do Ticket</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Ativar caso todos possam visualizar uma mensagem sem
                        responsável.
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Todos podem visualizar"
                          onChange={this.handleChecked}
                          name="all_users_can_see_ticket_no_owner"
                          checked={parameter.all_users_can_see_ticket_no_owner}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Ordernar Por Mensagens Não Lidas</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Ordena as conversas, na tela de conversas, por mensagens
                        não lidas primeiro
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Ativar "
                          onChange={this.handleChecked}
                          name="sort_by_unread_messages"
                          checked={parameter.sort_by_unread_messages}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Criação automática de Ticket</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Cria um ticket, quando uma mensagem de campanha é
                        disparada
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Criar Ticket ao Enviar uma Campanha "
                          onChange={this.handleChecked}
                          name="create_ticket_on_campaing"
                          checked={parameter.create_ticket_on_campaing}
                        />
                      </Form.Field>

                      <Form.Field>
                        <Checkbox
                          label="Criar Ticket Quando Receber uma Ligação"
                          onChange={this.handleChecked}
                          name="create_ticket_on_new_call_received"
                          checked={parameter.create_ticket_on_new_call_received}
                        />
                      </Form.Field>

                      <Form.Field>
                        <Checkbox
                          label="Criar Ticket Quando Receber uma Mensagem"
                          onChange={this.handleChecked}
                          name="create_ticket_on_new_message_received"
                          checked={
                            parameter.create_ticket_on_new_message_received
                          }
                        />
                      </Form.Field>

                      <Form.Field>
                        <Checkbox
                          label="Criar Ticket Ao Enviar Uma Mensagem"
                          title="Se habilitada esta opção, o Monitchat criará um ticket,
                          caso não exista, quando um atendente enviar uma mensagem
                          para um contato."
                          onChange={this.handleChecked}
                          name="create_ticket_on_send_message"
                          checked={parameter.create_ticket_on_send_message}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Solicitar Preenchimento do Ticket</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Se habilitada esta opção, o usuário terá que preencher
                        as informações do ticket ao finaliza-lo.
                      </Form.Field>

                      <Form.Field>
                        <Checkbox
                          label="Ativar "
                          onChange={this.handleChecked}
                          name="fill_out_ticket_before_finalizing"
                          checked={parameter.fill_out_ticket_before_finalizing}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Restrição de Contatos á Usuários</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Escolha como os contatos e tickets serão direcionados
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Restringir Contatos Por Departamento"
                          title="Usuários só poderão ver tickets de mesmo departamento se a regra de Carteira estiver desabilitada"
                          onChange={this.handleChecked}
                          name="restrict_contacts_by_departments"
                          checked={parameter.restrict_contacts_by_departments}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Restringir Contatos por Carteira"
                          title="Usuários só poderão ver contatos listados em sua Carteira"
                          onChange={this.handleChecked}
                          name="restrict_contacts_by_contact_user"
                          checked={parameter.restrict_contacts_by_contact_user}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Pausa Automática Logout/Login</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        <Checkbox
                          label="Pausar Usuário ao Efetuar Logout"
                          title="Quando o usuário efetuar Logout no sistema, ele será pausado imediatamente"
                          onChange={this.handleChecked}
                          name="pause_user_on_logoff"
                          checked={parameter.pause_user_on_logoff}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Remover Pausa do Usuário, ao efetuar Login"
                          title="Quando o usuário efetuar Login no sistema, ele a pausa será removida imediatamente"
                          onChange={this.handleChecked}
                          name="unpause_user_on_login"
                          checked={parameter.unpause_user_on_login}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Visibilidade de Contatos</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        <Checkbox
                          label="Contato pode estar em mais de uma carteira"
                          title="Permite que os usuários adicionem contatos as suas carteiras, mesmo quando um contato já está atribuido a outra carteira"
                          onChange={this.handleChecked}
                          name="multi_contact_user"
                          checked={parameter.multi_contact_user}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Contatos Sem Carteira são Visiveis a todos"
                          title="Permite que todos os usuários visualizem os contatos que não estão em nenhuma carteira"
                          onChange={this.handleChecked}
                          name="user_can_see_contact_with_no_user"
                          checked={parameter.user_can_see_contact_with_no_user}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Grid.Column style={{ marginTop: "10px" }}>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Tempo limite de atendimento</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Descreva o tempo limite para atendimento padrão dos
                        tickets.
                      </Form.Field>
                      <Form.Field>
                        <Input
                          onChange={this.handleChange}
                          name="service_timeout"
                          value={parameter.service_timeout}
                          placeholder="Tempo Limite"
                        />
                      </Form.Field>
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>
                      Tempo entre a criação automatica de Tickets
                    </Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Tempo em minutos que o sistema deverá aguardar, até
                        criar um novo Ticket automaticamente.
                      </Form.Field>
                      <Form.Field>
                        <Input
                          onChange={this.handleChange}
                          name="min_time_between_automatic_tickets_creation"
                          value={
                            parameter.min_time_between_automatic_tickets_creation
                          }
                          placeholder="Tempo entre Tickets"
                        />
                      </Form.Field>
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Mensagem na Criação do Ticket</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Mensagem a ser enviada quando um ticket for Criado
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Ativar Mensagem na Criação do Ticket"
                          onChange={this.handleChecked}
                          name="send_message_on_ticket_created"
                          checked={parameter.send_message_on_ticket_created}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Form.Field
                          control={TextArea}
                          placeholder="Mensagem"
                          name="default_message_on_ticket_created"
                          onChange={this.handleChange}
                          value={parameter.default_message_on_ticket_created}
                        />
                      </Form.Field>
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <CardGreeting
                  parameter={parameter}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                >
                  <div className="full">
                    <Button
                      positive
                      className="save_parameters"
                      onClick={this.update}
                      loading={loading}
                      disabled={loading}
                    >
                      Salvar
                    </Button>
                  </div>
                </CardGreeting>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Integração com Monitcall</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Integrando a plataforma Monitchat com a plataforma
                        Monitcall
                      </Form.Field>

                      <Form.Field>
                        <Checkbox
                          label="Abrir ticket ao atender uma ligação"
                          onChange={this.handleChecked}
                          name="open_ticket_from_call"
                          checked={parameter.open_ticket_from_call}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Logar e Deslogar no Monitchat/Monitcall"
                          onChange={this.handleChecked}
                          name="login_logout_monitcall_on_monitchat_auth"
                          checked={
                            parameter.login_logout_monitcall_on_monitchat_auth
                          }
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Pausar e despausar no Monitchat/Monitcall"
                          onChange={this.handleChecked}
                          name="pause_monitcall_on_monitchat_pause"
                          checked={parameter.pause_monitcall_on_monitchat_pause}
                        />
                      </Form.Field>
                      <Form.Field>
                        <Checkbox
                          label="Habilitar Click To Call"
                          onChange={this.handleChecked}
                          name="click_to_call"
                          checked={parameter.click_to_call}
                        />
                      </Form.Field>
                    </Form>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Url de integração com o Atendimento Automático Monitcall
                      </Form.Field>

                      <Form.Field>
                        <Input
                          label="URL"
                          onChange={this.handleChange}
                          name="monitcall_base_url"
                          value={parameter.monitcall_base_url}
                          placeholder="http://app.monitcall"
                          action={{
                            loading: this.state.loadingConnection,
                            color: this.state.connected ? "green" : "red",
                            icon: "check",
                            title: "Testar Conexão",
                            onClick: () => {
                              this.testServer(parameter.monitcall_base_url);
                            },
                          }}
                        />
                      </Form.Field>
                      <Form.Field>Chave Monitcall</Form.Field>

                      <Form.Field>
                        <Input
                          label={"Chave"}
                          onChange={this.handleChange}
                          name="monitcall_client_key"
                          value={parameter.monitcall_client_key}
                        />
                      </Form.Field>
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Integração Intelliway</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Url de integração com o Atendimento Automático
                        Intelliway, quando habilitada.
                      </Form.Field>

                      <Form.Field>
                        <Input
                          label="URL"
                          onChange={this.handleChange}
                          name="intelliway_url"
                          value={parameter.intelliway_url}
                          placeholder="http://whatsapp.intelliway.com.br:3000/whatsapp"
                        />
                      </Form.Field>
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid>
                  <Card.Content>
                    <Card.Header>Horario de Atendimento</Card.Header>
                  </Card.Content>
                  <Card.Content>
                    <Form>
                      <Form.Field>
                        Informe o Horário de Atendimento e uma Mensagem Padrão
                        para atendimentos fora do horário. Caso o Horário esteja
                        vazio, nenhuma mensagem será enviada
                      </Form.Field>
                      <Form.Field>
                        <Form.Field>
                          <Checkbox
                            label="Atendimento Automático"
                            onChange={this.handleChecked}
                            name="monitbot_enabled"
                            checked={parameter.monitbot_enabled}
                          />
                        </Form.Field>
                      </Form.Field>
                      <Form.Group widths="equal">
                        <Form.Field
                          control={Input}
                          label="Segunda a Sexta:"
                          type="time"
                          name="week_day_start_time"
                          onChange={this.handleChange}
                          value={parameter.week_day_start_time}
                        />
                        <Form.Field
                          control={Input}
                          label=" "
                          className="label_height"
                          name="week_day_end_time"
                          type="time"
                          onChange={this.handleChange}
                          value={parameter.week_day_end_time}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          control={Input}
                          label="Sábado:"
                          type="time"
                          onChange={this.handleChange}
                          name="saturday_start_time"
                          value={parameter.saturday_start_time}
                        />
                        <Form.Field
                          control={Input}
                          label=" "
                          className="label_height"
                          type="time"
                          onChange={this.handleChange}
                          name="saturday_end_time"
                          value={parameter.saturday_end_time}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          control={Input}
                          label="Domingo:"
                          type="time"
                          name="sunday_start_time"
                          value={parameter.sunday_start_time}
                          onChange={this.handleChange}
                        />
                        <Form.Field
                          control={Input}
                          label=" "
                          className="label_height"
                          name="sunday_end_time"
                          value={parameter.sunday_end_time}
                          type="time"
                          onChange={this.handleChange}
                        />
                      </Form.Group>
                      <Form.Field
                        control={TextArea}
                        placeholder="Mensagem de Aviso:"
                        name="extra_time_message"
                        value={parameter.extra_time_message}
                        onChange={this.handleChange}
                      />
                    </Form>
                    <div className="full">
                      <Button
                        positive
                        className="save_parameters"
                        onClick={this.update}
                        loading={loading}
                        disabled={loading}
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Divider />
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Button
                  fluid
                  color="red"
                  onClick={() => {
                    this.requestResetCode();
                    this.closeRequestDeleteModal();
                  }}
                >
                  Apagar Dados
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Modal
            open={this.state.requestDeleteModal}
            onClose={this.closeRequestDeleteModal}
            centered
          >
            <Modal.Header>Selecione os dados que deseja apagar</Modal.Header>
            <Modal.Content image>
              <Modal.Description>
                {this.state.success && (
                  <Message
                    color="green"
                    onDismiss={this.closeSuccess}
                    header="Soliciação Realizada"
                    content={this.state.successMessage}
                  />
                )}
                {this.state.error && (
                  <Message negative>
                    <Message.Header>{this.state.errorTitle}</Message.Header>
                    <p>{this.state.error}</p>
                  </Message>
                )}
                <Header>A Exclusão é Permanente e Irreversível</Header>
                <Form.Field>
                  <Checkbox
                    color="red"
                    label="Contatos"
                    onChange={this.handleCheckedDelete}
                    name="deleteContacts"
                    checked={this.state.deleteContacts}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    label="Tickets"
                    onChange={this.handleCheckedDelete}
                    name="deleteTickets"
                    checked={this.state.deleteTickets}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    label="Conversas"
                    onChange={this.handleCheckedDelete}
                    name="deleteConversations"
                    checked={this.state.deleteConversations}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    label="Mensagens"
                    onChange={this.handleCheckedDelete}
                    name="deleteMessages"
                    checked={this.state.deleteMessages}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    label="Campanhas"
                    onChange={this.handleCheckedDelete}
                    name="deleteCampaings"
                    checked={this.state.deleteCampaings}
                  />
                </Form.Field>
                <Divider />
                <p>
                  Para proseguir, informe o código de confirmação{" "}
                  <strong>
                    <span style={{ color: "red" }}>{this.state.resetCode}</span>
                  </strong>{" "}
                  no campo de texto abaixo!
                </p>
                <Form.Field>
                  <Input
                    label="Codigo de Confirmação"
                    onChange={this.handleChangeDelete}
                    name="confirmationCode"
                    value={this.state.confirmationCode}
                  />
                </Form.Field>
                <Divider />
                <Form.Field>
                  <Checkbox
                    label="Estou ciente de que esta operação é irreversível e a exclusão será executada a partir das 23:59 do dia corrente! Concordo em Continuar"
                    onChange={this.handleCheckedDelete}
                    name="warned"
                    checked={this.state.warned}
                  />
                </Form.Field>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Modal
                open={this.state.deleteModalOpen}
                centered
                closeOnEscape
                onClose={this.closeDeleteModal}
              >
                <Modal.Header>
                  Esta operação é Irreversível. Deseja Continuar ?
                </Modal.Header>
                <Modal.Content image>
                  <Modal.Description>
                    {this.state.error && (
                      <Message negative>
                        <Message.Header>{this.state.errorTitle}</Message.Header>
                        <p>{this.state.error}</p>
                      </Message>
                    )}
                    <Header>Você está apagando os dados selecionados:</Header>
                    <Form.Field>
                      <Checkbox
                        label="Contatos"
                        onChange={this.handleCheckedDelete}
                        name="deleteContacts"
                        checked={this.state.deleteContacts}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        label="Tickets"
                        onChange={this.handleCheckedDelete}
                        name="deleteTickets"
                        checked={this.state.deleteTickets}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        label="Conversas"
                        onChange={this.handleCheckedDelete}
                        name="deleteConversations"
                        checked={this.state.deleteConversations}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        label="Mensagens"
                        onChange={this.handleCheckedDelete}
                        name="deleteMessages"
                        checked={this.state.deleteMessages}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        label="Campanhas"
                        onChange={this.handleCheckedDelete}
                        name="deleteCampaings"
                        checked={this.state.deleteCampaings}
                      />
                    </Form.Field>
                    <Divider />
                    <p>
                      Para proseguir, informe o código de confirmação{" "}
                      <strong>
                        <span style={{ color: "red" }}>
                          {this.state.resetCode}
                        </span>
                      </strong>{" "}
                      no campo de texto abaixo!
                    </p>
                    <Form.Field>
                      <Input
                        label="Codigo de Confirmação"
                        onChange={this.handleChangeDelete}
                        name="confirmationCode"
                        value={this.state.confirmationCode}
                      />
                    </Form.Field>
                  </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                  <Button
                    disabled={this.setState.loading}
                    loading={this.state.loading}
                    color="red"
                    onClick={this.deleteData}
                  >
                    <Icon name="trash" /> Sim
                  </Button>

                  <Button color="green" onClick={this.closeDeleteModal}>
                    <Icon name="close" /> Voltar
                  </Button>
                </Modal.Actions>
              </Modal>
              <Button color="red" onClick={this.validateDeleteModal}>
                <Icon name="trash" /> Apagar
              </Button>
              <Button color="green" onClick={this.closeRequestDeleteModal}>
                <Icon name="close" /> Cancelar
              </Button>
            </Modal.Actions>
          </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    company_id: state.user.user.company_id,
    companies: state.user.user.companies,
  };
};

export default connect(mapStateToProps)(Parameters);
