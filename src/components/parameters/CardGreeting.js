import React from "react";
import { Form, TextArea, Checkbox, Card } from "semantic-ui-react";

const CardGreeting = ({ onChange, parameter, onChecked, children }) => {
  return (
    <Card fluid>
      <Card.Content>
        <Card.Header>Mensagem de Agradecimento</Card.Header>
      </Card.Content>
      <Card.Content>
        <Form>
          <Form.Field>
            Mensagem de agradecimento Padrão ao Finalizar um Ticket
          </Form.Field>
          <Form.Field>
            <Checkbox
              label="Ativar Mensagem Padrão"
              onChange={onChecked}
              name="greeting_message_active"
              checked={parameter.greeting_message_active}
            />
          </Form.Field>
          <Form.Field>
            <Form.Field
              control={TextArea}
              placeholder="Mensagem"
              name="greeting_message"
              onChange={onChange}
              value={parameter.greeting_message}
            />
          </Form.Field>
        </Form>
        {children}
      </Card.Content>
    </Card>
  );
};

export default CardGreeting;
