import React from "react";
import { Form, Input, Dropdown, Checkbox } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";

const FormPage = ({ page, onChange, messageError, icons = [] }) => {
  return (
    <Form autocomplete="off">
      <Form.Group widths="equal">
        <Form.Field error={!!messageError.title}>
          <label htmlFor="title">Título</label>
          <Input
            autoComplete="off"
            control={Input}
            name="title"
            onChange={onChange}
            value={page.title}
            placeholder="Ex: Produtos, Equipamentos..."
          />
          {messageError.title && <InlineError text={messageError.title} />}
        </Form.Field>

        <Form.Field error={!!messageError.header}>
          <label htmlFor="header">Cabeçalho da Tela</label>
          <Input
            autoComplete="off"
            control={Input}
            name="header"
            onChange={onChange}
            value={page.header}
            placeholder="Ex: Cadastro de Produtos, Cadastro de Equipamentos"
          />
          {messageError.header && <InlineError text={messageError.header} />}
        </Form.Field>
      </Form.Group>

      <Form.Group>
        <Form.Field error={!!messageError.menu_order}>
          <label htmlFor="menu_order">Ordem de Exibição no Menu</label>
          <Input
            autoComplete="off"
            control={Input}
            name="menu_order"
            onChange={onChange}
            value={page.menu_order}
            placeholder="Ex: 1, 2, 3..."
          />
          {messageError.menu_order && (
            <InlineError text={messageError.menu_order} />
          )}
        </Form.Field>

        <Form.Field error={!!messageError.menu_icon}>
          <label htmlFor="menu_icon">Ícone do Menu</label>
          <Dropdown
            name="menu_icon"
            onChange={onChange}
            search
            clearable
            selection
            closeOnChange
            scrolling
            options={icons.map((i) => ({ key: i, value: i, text: i, icon: i }))}
            value={page.menu_icon}
          />
          {messageError.menu_icon && (
            <InlineError text={messageError.menu_icon} />
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field error={!!messageError.category} width="6">
          <label htmlFor="category">Categoria</label>
          <Input
            autoComplete="off"
            control={Input}
            name="category"
            onChange={onChange}
            value={page.category}
            placeholder="Ex: Servidores e Acessos, Licenciamento, Produtos"
          />
          {messageError.category && (
            <InlineError text={messageError.category} />
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field>
          <Form.Field>
            <label htmlFor="active">Menu Ativo</label>
            <Checkbox
              label="Sim"
              onChange={onChange}
              name="active"
              checked={page.active}
            />
            {messageError.active && <InlineError text={messageError.active} />}
          </Form.Field>
        </Form.Field>
      </Form.Group>
    </Form>
  );
};

export default FormPage;
