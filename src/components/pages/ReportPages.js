import React from "react";
import MainContainer from "../main-container/MainContainer";
import Report from "../report/Report";

const body = document.body;

const ReportPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Report />
	</MainContainer>
);

export default ReportPages;
