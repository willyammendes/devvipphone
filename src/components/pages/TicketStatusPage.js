import React from "react";
import MainContainer from "../main-container/MainContainer";
import TicketStatus from "../ticket-status/TicketStatus";

const TicketStatusPage = () => (
	<MainContainer>
		<TicketStatus />
	</MainContainer>
);

export default TicketStatusPage;
