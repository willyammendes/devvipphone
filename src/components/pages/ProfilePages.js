import React from "react";
import MainContainer from "../main-container/MainContainer";
import Profile from "../profile/Profile";

const body = document.body;

const ProfilePages = () => (
  <MainContainer data-tut="reactour__iso">
    {body.classList.remove("homepage")}
    <Profile />
  </MainContainer>
);

export default ProfilePages;
