import React from "react";
import { connect } from "react-redux";
import MainContainer from "../main-container/MainContainer";
import ConfigNav from "../config-nav/ConfigNav";

const body = document.body;

const ConfigPage = ({ permissions }) => (
  <MainContainer>
    {body.classList.remove("homepage")}
    <ConfigNav permissions={permissions} />
  </MainContainer>
);

const mapStateToProps = ({ user }) => {
  return {
    permissions: user.user.permissions
  };
};

export default connect(
  mapStateToProps,
  null
)(ConfigPage);
