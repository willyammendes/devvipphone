import React from "react";
import MainContainer from "../main-container/MainContainer";
import SMS from "../sms/SMS";

const body = document.body;

const SMSPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<SMS />
	</MainContainer>
);

export default SMSPage;
