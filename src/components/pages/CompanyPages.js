import React from "react";
import MainContainer from "../main-container/MainContainer";
import Company from "../company/Company";

const body = document.body;

const CompanyPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Company />
	</MainContainer>
);

export default CompanyPages;
