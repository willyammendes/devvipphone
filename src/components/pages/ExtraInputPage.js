import React from "react";
import MainContainer from "../main-container/MainContainer";
import ExtraInput from "../extra-input/ExtraInput";

const ExtraInputPage = () => (
  <MainContainer>
    <ExtraInput />
  </MainContainer>
);

export default ExtraInputPage;
