import React from "react";
import MainContainer from "../main-container/MainContainer";
import BotSubstitutions from "../bot-substitutions/BotSubstitutions";

const body = document.body;

const BotSubstitutionsPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<BotSubstitutions />
	</MainContainer>
);

export default BotSubstitutionsPages;
