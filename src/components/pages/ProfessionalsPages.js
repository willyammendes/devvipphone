import React from "react";
import MainContainer from "../main-container/MainContainer";
import Professionals from "../professionals/Professionals";

const body = document.body;

const ProfessionalsPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Professionals />
	</MainContainer>
);

export default ProfessionalsPage;
