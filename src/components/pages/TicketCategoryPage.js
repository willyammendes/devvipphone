import React from "react";
import MainContainer from "../main-container/MainContainer";
import TicketCategory from "../ticket-category/TicketCategory";

const TicketCategoryPage = () => (
	<MainContainer>
		<TicketCategory />
	</MainContainer>
);

export default TicketCategoryPage;
