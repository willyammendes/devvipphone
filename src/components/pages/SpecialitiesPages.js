import React from "react";
import MainContainer from "../main-container/MainContainer";
import Specialities from "../specialities/Specialities";

const body = document.body;

const SpecialitiesPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Specialities />
	</MainContainer>
);

export default SpecialitiesPage;
