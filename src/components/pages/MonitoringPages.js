import React from "react";
import MainContainer from "../main-container/MainContainer";
import Monitoring from "../monitoring/Monitoring";

const body = document.body;

const MonitoringPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Monitoring />
	</MainContainer>
);

export default MonitoringPages;
