import React from "react";
import MainContainer from "../main-container/MainContainer";
import Users from "../users/Users";

const UsersPage = () => (
	<MainContainer>
		<Users />
	</MainContainer>
);

export default UsersPage;
