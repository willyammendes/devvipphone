import React from "react";
import MainContainer from "../main-container/MainContainer";
import Call from "../call/Call";

const body = document.body;

const CallPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Call />
	</MainContainer>
);

export default CallPage;
