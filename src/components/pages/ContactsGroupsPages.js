import React from "react";
import MainContainer from "../main-container/MainContainer";
import ContactsGroups from "../contacts-groups/ContactsGroups";

const body = document.body;

const ConfigPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<ContactsGroups />
	</MainContainer>
);

export default ConfigPage;
