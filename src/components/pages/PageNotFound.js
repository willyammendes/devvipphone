import React from "react";
import MainContainer from "../main-container/MainContainer";
import PageNotFoundComponent from "../page-not-found/PageNotFoundComponent";

const PageNotFound = ({ loading }) => (
  <MainContainer>
    <PageNotFoundComponent loading={loading} />
  </MainContainer>
);

export default PageNotFound;
