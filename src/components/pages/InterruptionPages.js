import React from "react";
import MainContainer from "../main-container/MainContainer";
import Interruption from "../interruption-type/Interruption";

const body = document.body;

const InterruptionPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Interruption />
	</MainContainer>
);

export default InterruptionPages;
