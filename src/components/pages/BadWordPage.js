import React from "react";
import MainContainer from "../main-container/MainContainer";
import BadWord from "../bad-word/BadWord";

const BadWordPage = () => (
	<MainContainer>
		<BadWord />
	</MainContainer>
);

export default BadWordPage;
