import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import RegisterForm from "../forms/RegisterForm";
import { Creators as AuthActions } from "../../store/ducks/auth";
import logo from "../assets/img/logo-full.png";

class RegisterPage extends React.Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/"));

  render() {
    return (
      <div className="login register">
        <div className="box_login">
          <a href="/" className="logo">
            <img src={logo} alt="" />
          </a>
          <div className="form_login">
            <RegisterForm submit={this.submit} />
          </div>
        </div>
      </div>
    );
  }
}
RegisterPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(RegisterPage);
