import React from "react";
import MainContainer from "../main-container/MainContainer";
import QuickMessage from "../quick-message/QuickMessage";

const QuickMessagePage = () => (
	<MainContainer>
		<QuickMessage />
	</MainContainer>
);

export default QuickMessagePage;
