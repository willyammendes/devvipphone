import React from "react";
import MainContainer from "../main-container/MainContainer";
import WebHook from "../webhook/WebHook";

const WebHookPage = () => (
	<MainContainer>
		<WebHook />
	</MainContainer>
);

export default WebHookPage;
