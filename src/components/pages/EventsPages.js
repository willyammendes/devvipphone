import React from "react";
import MainContainer from "../main-container/MainContainer";
import Events from "../events/Events";

const body = document.body;

const EventsPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Events />
	</MainContainer>
);

export default EventsPages;
