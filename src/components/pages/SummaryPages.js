import React from "react";
import { connect } from "react-redux";
import MainContainer from "../main-container/MainContainer";
import Summary from "../summary/Summary";
import { bindActionCreators } from "redux";
import { Creators as AuthActions } from "../../store/ducks/auth";



const body = document.body;


const SummaryPages = ({ getCurrentPause }) => {
	
	return (
		<MainContainer >
			{body.classList.remove("homepage")}
			<Summary />
		</MainContainer>
	);
};

const mapDispatchToProps = dispatch =>
	bindActionCreators(AuthActions, dispatch);

export default connect(
	null,
	mapDispatchToProps
)(SummaryPages);
