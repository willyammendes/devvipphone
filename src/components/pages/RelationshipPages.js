import React from "react";
import { connect } from "react-redux";
import MainContainer from "../main-container/MainContainer";
import RelationshipNav from "../relationship-nav/RelationshipNav";

const body = document.body;

const RelationshipPage = ({ permissions }) => (
  <MainContainer>
    {body.classList.remove("homepage")}
    <RelationshipNav permissions={permissions} />
  </MainContainer>
);
const mapStateToProps = ({ user }) => {
  return {
    permissions: user.user.permissions
  };
};

export default connect(
  mapStateToProps,
  null
)(RelationshipPage);
