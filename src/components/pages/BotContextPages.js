import React from "react";
import MainContainer from "../main-container/MainContainer";

import BotContext from "../bot-context/BotContext";

const body = document.body;

const BotContextPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<BotContext />
	</MainContainer>
);

export default BotContextPage;
