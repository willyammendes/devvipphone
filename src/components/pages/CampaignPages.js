import React from "react";
import MainContainer from "../main-container/MainContainer";
import Campaign from "../campaign/Campaign";

const body = document.body;

const CampaignPage = () => (
  <MainContainer>
    {body.classList.remove("homepage")}
    <Campaign />
  </MainContainer>
);

export default CampaignPage;
