import React from "react";
import MainContainer from "../main-container/MainContainer";
import Client from "../client/Client";

const body = document.body;

const ClientPage = () => (
  <MainContainer>
    {body.classList.remove("homepage")}
    <Client />
  </MainContainer>
);

export default ClientPage;
