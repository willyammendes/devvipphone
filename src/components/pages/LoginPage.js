import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LoginForm from "../forms/LoginForm";
import { Creators as AuthActions } from "../../store/ducks/auth";
import logo from "../assets/img/logo-full.png";

class LoginPage extends React.Component {
  submit = (data) =>
    this.props.login(data).then(() => {
      if (localStorage.redirect) {
        this.props.history.push(localStorage.redirect);

        localStorage.removeItem("redirect");
      }
    });

  render() {
    return (
      <div className="login">
        <div className="box_login">
          <a href="/" className="logo">
            <img src={logo} alt="" />
          </a>
          <div className="form_login">
            <LoginForm submit={this.submit} />
          </div>
        </div>
      </div>
    );
  }
}
LoginPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  login: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(AuthActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(LoginPage);
