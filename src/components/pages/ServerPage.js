import React from "react";
import MainContainer from "../main-container/MainContainer";
import Server from "../server/Server";

const ServerPage = () => (
	<MainContainer>
		<Server />
	</MainContainer>
);

export default ServerPage;
