import React from "react";
import MainContainer from "../main-container/MainContainer";
import Permissions from "../permissions/Permissions";

const body = document.body;

const PermissionsPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Permissions />
	</MainContainer>
);

export default PermissionsPages;
