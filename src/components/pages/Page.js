import React from "react";
import MainContainer from "../main-container/MainContainer";
import PageComponent from "../page/Page";

const Page = () => (
  <MainContainer>
    <PageComponent />
  </MainContainer>
);

export default Page;
