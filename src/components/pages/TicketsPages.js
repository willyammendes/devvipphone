import React, { Component } from "react";
import Tour from "reactour";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Creators as TourActions } from "../../store/ducks/tour";
import MainContainer from "../main-container/MainContainer";
import Tickets from "../tickets/Tickets";
import OldTickets from "../tickets/OldTickets";

class TicketsPage extends Component {
  state = {
    oldTickets: false,
  };

  toggleOldTickets = () => {
    this.setState({
      oldTickets: !this.state.oldTickets,
    });
  };
  render() {
    const body = document.body;
    const { oldTickets } = this.state;
    return (
      <MainContainer>
        <Tour
          steps={this.props.tour.ticketsSteps}
          isOpen={this.props.tour.isOpenTickets}
          onRequestClose={this.props.closeTourTickets}
        />
        {body.classList.remove("homepage")}
        {oldTickets ? (
          <OldTickets toggleOldTickets={this.toggleOldTickets} />
        ) : (
          <Tickets toggleOldTickets={this.toggleOldTickets} />
        )}
      </MainContainer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tour: state.tour,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TicketsPage);
