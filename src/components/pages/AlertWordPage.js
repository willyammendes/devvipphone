import React from "react";
import MainContainer from "../main-container/MainContainer";
import AlertWord from "../alert-word/AlertWord";

const AlertWordPage = () => (
	<MainContainer>
		<AlertWord />
	</MainContainer>
);

export default AlertWordPage;
