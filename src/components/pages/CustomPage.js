import React from "react";
import MainContainer from "../main-container/MainContainer";
import CustomPageComponent from "../custom-page/CustomPage";

const CustomPage = () => (
  <MainContainer>
    <CustomPageComponent />
  </MainContainer>
);

export default CustomPage;
