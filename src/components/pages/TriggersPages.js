import React from "react";
import MainContainer from "../main-container/MainContainer";
import Triggers from "../triggers/Triggers";

const body = document.body;

const TriggersPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Triggers />
	</MainContainer>
);

export default TriggersPage;
