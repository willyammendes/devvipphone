import React from "react";
import MainContainer from "../main-container/MainContainer";
import Email from "../email/Email";

const body = document.body;

const EmailPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Email />
	</MainContainer>
);

export default EmailPage;
