import React from "react";
import MainContainer from "../main-container/MainContainer";
import Contact from "../contact/Contact";

const body = document.body;

const ConfigPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Contact />
	</MainContainer>
);

export default ConfigPage;
