import React from "react";
import MainContainer from "../main-container/MainContainer";
import UserMonitor from "../monitor/UserMonitor";

const UserMonitorPage = () => (
  <MainContainer>
    <UserMonitor />
  </MainContainer>
);

export default UserMonitorPage;
