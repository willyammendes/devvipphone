import React from "react";
import MainContainer from "../main-container/MainContainer";
import Plans from "../plans/Plans";

const PlansPage = () => (
	<MainContainer>
		<Plans />
	</MainContainer>
);

export default PlansPage;
