import React from "react";
import MainContainer from "../main-container/MainContainer";
import BotVars from "../bot-vars/BotVars";

const body = document.body;

const BotVarsPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}

		<BotVars />
	</MainContainer>
);

export default BotVarsPage;
