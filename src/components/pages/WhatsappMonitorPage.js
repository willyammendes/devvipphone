import React from "react";
import MainContainer from "../main-container/MainContainer";
import WhatsappMonitor from "../monitor/WhatsappMonitor";

const WhatsappMonitorPage = () => (
  <MainContainer>
    <WhatsappMonitor />
  </MainContainer>
);

export default WhatsappMonitorPage;
