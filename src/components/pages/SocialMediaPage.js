import React from "react";
import MainContainer from "../main-container/MainContainer";
import SocialMedia from "../social-media/SocialMedia";

const SocialMediaPage = () => (
	<MainContainer>
		<SocialMedia />
	</MainContainer>
);

export default SocialMediaPage;
