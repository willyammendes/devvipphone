import React from "react";
import MainContainer from "../main-container/MainContainer";
import Department from "../departments/Department";

const DepartmentPage = () => (
	<MainContainer>
		<Department />
	</MainContainer>
);

export default DepartmentPage;
