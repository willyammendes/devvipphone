import React from "react";
import MainContainer from "../main-container/MainContainer";
import Roles from "../roles/Roles";

const body = document.body;

const RolesPages = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Roles />
	</MainContainer>
);

export default RolesPages;
