import React from "react";
import MainContainer from "../main-container/MainContainer";
import RestrictedContacts from "../restricted-contacts/RestrictedContacts";

const body = document.body;

const RestrictedContactsPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<RestrictedContacts />
	</MainContainer>
);

export default RestrictedContactsPage;
