import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ForgotForm from "../forms/ForgotForm";
import { Creators as AuthActions } from "../../store/ducks/auth";
import logo from "../assets/img/logo-full.png";

class ForgotPage extends React.Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/"));

  render() {
    return (
      <div className="login register">
        <div className="box_login box_forgot">
          <a href="/" className="logo">
            <img src={logo} alt="" />
          </a>
          <div className="form_login">
            <ForgotForm />
          </div>
        </div>
      </div>
    );
  }
}
ForgotPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ForgotPage);
