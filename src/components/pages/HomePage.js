import React from "react";
import { connect } from "react-redux";
import api from "../../api/social-media";
import isEmpty from "lodash/isEmpty";
import { bindActionCreators } from "redux";
import Tour from "reactour";

import { Creators as TourActions } from "../../store/ducks/tour";

import MainContainer from "../main-container/MainContainer";
import Conversations from "../conversations/Conversations";
import ConversationBody from "../conversation-body/ConversationBody";
import EmptyBody from "../conversation-body/EmptyBody";
import TicketInfo from "../ticket-info/TicketInfo";

const body = document.body;

class HomePage extends React.Component {
  state = {
    media: [],
    getConversations: false,
    iconConversation: "",
  };

  fetchMedia = async (params) => {
    this.setState({ loading: true });

    return await api.media.fetchAll(params).then((res) => {
      this.setState({
        media: res.data,
        loading: false,
      });
    });
  };
  getConversations = (value) => {
    this.setState({
      getConversations: !this.state.getConversations,
      iconConversation: value,
    });
  };

  falseConversations = () => {
    this.setState({
      getConversations: false,
      iconConversation: "",
    });
  };
  componentWillMount() {
    this.fetchMedia();
  }

  render() {
    const { activeConversation } = this.props;
    const { getConversations, iconConversation } = this.state;
    return (
      <div className="ovhy">
        <Tour
          steps={this.props.tour.conversationSteps}
          isOpen={this.props.tour.isOpenConversation}
          onRequestClose={this.props.closeTourConversation}
        />
        <MainContainer>
          <Conversations
            getConversations={getConversations}
            toggleGet={this.falseConversations}
            iconConversation={iconConversation}
          />
          {!isEmpty(activeConversation) ? (
            <ConversationBody getConversations={this.getConversations} />
          ) : (
            <EmptyBody />
          )}
          {!isEmpty(activeConversation) ? (
            <TicketInfo activeConversation={activeConversation} />
          ) : null}
        </MainContainer>
        {body.classList.add("homepage")}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

const mapStateToProps = (state) => {
  return {
    activeConversation:
      state.conversation.data.find(
        (c) => c.id === state.conversation.activeConversationId
      ) || {},
    tour: state.tour,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
