import React from "react";
import MainContainer from "../main-container/MainContainer";
import Places from "../places/Places";

const body = document.body;

const PlacesPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Places />
	</MainContainer>
);

export default PlacesPage;
