import React from "react";
import MainContainer from "../main-container/MainContainer";
import Parameters from "../parameters/Parameters";

const ParametersPage = () => (
	<MainContainer>
		<Parameters />
	</MainContainer>
);

export default ParametersPage;
