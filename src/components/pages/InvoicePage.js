import React from "react";
import MainContainer from "../main-container/MainContainer";
import Invoice from "../invoice/Invoice";

const body = document.body;

const InvoicePage = () => (
  <MainContainer>
    {body.classList.remove("homepage")}
    <Invoice />
  </MainContainer>
);

export default InvoicePage;
