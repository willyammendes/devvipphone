import React from "react";
import MainContainer from "../main-container/MainContainer";
import Schedules from "../schedules/Schedules";

const body = document.body;

const SchedulesPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Schedules />
	</MainContainer>
);

export default SchedulesPage;
