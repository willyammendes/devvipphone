import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ResetForm from "../forms/ResetForm";
import { Creators as AuthActions } from "../../store/ducks/auth";
import logo from "../assets/img/logo-full.png";

class ResetPage extends React.Component {
  submit = data => this.props.resetLogin(data);

  render() {
    return (
      <div className="login register">
        <div className="box_login box_forgot">
          <a href="/" className="logo">
            <img src={logo} alt="" />
          </a>
          <div className="form_login">
            <ResetForm submit={this.submit} />
          </div>
        </div>
      </div>
    );
  }
}
ResetPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ResetPage);
