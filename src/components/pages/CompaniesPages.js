import React from "react";
import MainContainer from "../main-container/MainContainer";
import Companies from "../companies/Companies";

const body = document.body;

const CompaniesPage = () => (
	<MainContainer>
		{body.classList.remove("homepage")}
		<Companies />
	</MainContainer>
);

export default CompaniesPage;
