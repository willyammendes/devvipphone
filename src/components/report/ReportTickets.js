import React, { Component } from "react";
import { Card, Button, Statistic, Modal } from "semantic-ui-react";
import { Pie } from "react-chartjs-2";
import api from "../../api/stats";
import apiProductivity from "../../api/user-productivity-table";

import TableOpenTickets from "./tables/TableOpenTickets";
import TableClosedTickets from "./tables/TableClosedTickets";
import TableAssignedTickets from "./tables/TableAssignedTickets";
import TableWaitingTickets from "./tables/TableWaitingTickets";

class ReportTickets extends Component {
  state = {
    value_choice: ["hoje"],
    total_records: " ",
    total_open_ticket: " ",
    total_assigned_ticket: " ",
    total_closed_ticket: " ",
    total_waiting_ticket: " ",
    total_productivity: "",
    dataLine: {
      labels: [
        "Tickets Abertos",
        "Tickets Atendidos",
        "Tickets Aguardando",
        "Tickets Fechados"
      ],
      datasets: [
        {
          data: [],
          backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FFDC30"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FFDC30"]
        }
      ]
    },
    dataLine2: {
      labels: ["Whatsapp", "Telefone", "Monitchat", "email"],
      datasets: [
        {
          data: [12, 233, 123, 111],
          backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FFDC30"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FFDC30"]
        }
      ]
    }
  };

  fetchRecords = async e => {
    const data = [];

    await api.openTickets
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({ total_open_ticket: res, total_records: res });
        data.push(res);
      });

    await api.assignedTickets
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({ total_assigned_ticket: res });
        data.push(res);
      });

    await api.waitingTicket
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({ total_waiting_ticket: res });
        data.push(res);
      });

    await api.closedTickets
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({
          total_closed_ticket: res,
          total_records: this.state.total_records + res
        });
        data.push(res);
      });
    await apiProductivity.userProductivityTable
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({ total_productivity: res });
      });
    this.setState({ dataLine: { datasets: [{ data: data }] } });
  };

  componentWillUpdate(prevProps) {
    if (prevProps.value_choice !== this.props.value_choice) {
      setTimeout(() => {
        this.fetchRecords(this.props);
      }, 200);
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.fetchRecords(this.props);
    }, 200);
  }

  render() {
    return (
      <div className="wrap_cards">
        <Card.Group className="cards_five">
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total Tickets
            </Card.Header>
            {this.state.total_records === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>{this.state.total_records}</Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Tickets Abertos
            </Card.Header>
            {this.state.total_open_ticket === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_open_ticket}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Tickets Atendidos
            </Card.Header>
            {this.state.total_assigned_ticket === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_assigned_ticket}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Tickets Fechados
            </Card.Header>
            {this.state.total_closed_ticket === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_closed_ticket}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Tickets Aguardando
            </Card.Header>
            {this.state.total_waiting_ticket === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_waiting_ticket}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
        </Card.Group>
        <Card className="card_graphic">
          <Card.Header>
            <h3>
              Tickets <small>{this.props.value_choice[1]}</small>
            </h3>
            <Modal
              style={{ width: "90%" }}
              trigger={<Button floated="right">Detalhar</Button>}
            >
              <Modal.Header>Tickets</Modal.Header>
              <Modal.Content>
                <div className="pageHolder" style={{ margin: "0" }}>
                  <div className="holderPage">
                    <TableOpenTickets />
                    <TableClosedTickets />
                    <TableAssignedTickets />
                    <TableWaitingTickets />
                  </div>
                </div>
              </Modal.Content>
            </Modal>
          </Card.Header>
          <Card.Content>
            <Pie
              data={this.state.dataLine}
              width={100}
              height={50}
              options={{ maintainAspectRatio: false }}
            />
          </Card.Content>
        </Card>
        <Card className="card_graphic">
          <Card.Header>
            <h3>
              Origem do Tickets <small>{this.props.value_choice[1]}</small>
            </h3>
            <Modal
              style={{ width: "90%" }}
              trigger={<Button floated="right">Detalhar</Button>}
            >
              <Modal.Header>Origem do Tickets</Modal.Header>
              <Modal.Content>
                <div className="pageHolder" style={{ margin: "0" }}>
                  <div className="holderPage">
                    <TableOpenTickets />
                    <TableClosedTickets />
                    <TableAssignedTickets />
                    <TableWaitingTickets />
                  </div>
                </div>
              </Modal.Content>
            </Modal>
          </Card.Header>
          <Card.Content>
            <Pie
              data={this.state.dataLine2}
              width={100}
              height={50}
              options={{ maintainAspectRatio: false }}
            />
          </Card.Content>
        </Card>
      </div>
    );
  }
}

export default ReportTickets;
