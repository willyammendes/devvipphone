import React, { Component } from "react";
import { Card, Dropdown, Button, Statistic } from "semantic-ui-react";
import DataTable from "../table/DataTable";
import api from "../../api/interruption-type";

const userFilter = [
  {
    key: "Eu",
    text: "Eu",
    value: "Eu",
    icon: "user outline"
  },
  {
    key: "Empresa",
    text: "Empresa",
    value: "Empresa",
    icon: "globe"
  },
  {
    key: "Monitcall",
    text: "Monitcall",
    value: "Monitcall",
    icon: "building outline"
  }
];

const columns = {
  id: "ID",
  agent: "Nome do agente",
  department: "Departamento",
  pause_start: "Pausou",
  pause_end: "Despausou",
  reason: "Motivo",
  session: "Sessão"
};

const data = [];

class ReportBotAnswered extends Component {
  state = {
    total_records: " "
  };
  fetchData = async () => {
    await api.interruption.teste().then(res => {
      this.setState({ total_records: res.data.total_records });
    });
  };

  componentDidMount() {
    this.fetchData();
  }
  render() {
    return (
      <div className="wrap_cards">
        <Card.Group className="cards_five">
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total Pausas
            </Card.Header>
            {this.state.total_records === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>{this.state.total_records}</Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
        </Card.Group>
        <Card fluid>
          <Card.Header>
            <h3>Pausas</h3>
            <Dropdown
              placeholder="Empresa"
              className="userFilter"
              header="Filtro de usuários"
              value="Empresa"
              selection
              options={userFilter}
            />
            <Button
              icon="file pdf outline"
              className="extrair_pdf"
              alt="extrair pdf"
            />
            <Button
              icon="file pdf outline"
              className="extrair_pdf icon"
              alt="extrair pdf"
            >
              <i className="xls" />
            </Button>
          </Card.Header>
          <Card.Content>
            <div className="tabela-padrao">
              <DataTable columns={columns} data={data} />
            </div>
          </Card.Content>
        </Card>
      </div>
    );
  }
}

export default ReportBotAnswered;
