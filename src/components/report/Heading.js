import React, { Component } from "react";
import {
	Card,
	Dropdown,
	Label,
	Icon,
	Button,
	Modal,
	Header,
	Divider
} from "semantic-ui-react";

class Heading extends Component {
	render() {
		const userLastSeen = this.props;
		
		return (
			<div className="headingPage">
				<h1>Relatorios</h1>
				<div className="subHeadingPage">
					<h2>Clientes</h2>
					<Button
						icon="file pdf outline"
						className="extrair_pdf"
						alt="extrair pdf"
					/>
				</div>
			</div>
		);
	}
}

export default Heading;
