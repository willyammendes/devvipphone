import React, { Component } from "react";
import { Card, Dropdown, Button,Statistic } from "semantic-ui-react";
import { Pie } from "react-chartjs-2";

import TableBotAnswered from "./tables/TableBotAnswered";

const userFilter = [
	{
		key: "Eu",
		text: "Eu",
		value: "Eu",
		icon: "user outline"
	},
	{
		key: "Empresa",
		text: "Empresa",
		value: "Empresa",
		icon: "globe"
	}
];

const dataLine = {
	labels: ["Atendimento", "Suporte", "Vendas"],
	datasets: [
		{
			label: "Produtividade",
			data: [65, 59, 80],
			backgroundColor: [
				"rgba(255, 99, 132, 0.7)",
				"rgba(54, 162, 235, 0.7)",
				"rgba(255, 206, 86, 0.7)",
				"rgba(75, 192, 192, 0.7)",
				"rgba(153, 102, 255, 0.7)",
				"rgba(255, 159, 64, 0.7)",
				"rgba(190, 64, 12, 0.7)"
			],

			hoverBackgroundColor: [
				"rgba(255, 99, 132, 1)",
				"rgba(54, 162, 235, 1)",
				"rgba(255, 206, 86, 1)",
				"rgba(75, 192, 192, 1)",
				"rgba(153, 102, 255, 1)",
				"rgba(255, 159, 64, 1)",
				"rgba(190, 64, 12, 1)"
			]
		}
	]
};
class ReportBotAnswered extends Component {
	state = {
		total_records:" ",
		closedTicketsBot: " "
	}
	handleTotalRecords = (e) =>{
		this.setState({total_records: e})
	}
	handleClosedTickets = (e) =>{
		this.setState({closedTicketsBot:e})
	}

	render() {
		return (
			<div className="wrap_cards">
          <Card.Group className="cards_five">
          <Card>
		  <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Ticket atendidos</Card.Header>
		  	{this.state.total_records ===" "? (
				  <Card.Content>
					<Statistic>
						<Statistic.Value>-</Statistic.Value>
					</Statistic>
				</Card.Content>
			  ): (<Card.Content>
					<Statistic>
						<Statistic.Value>{this.state.total_records}</Statistic.Value>
					</Statistic>
				</Card.Content>)}
                
            </Card>
			<Card>
			<Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Total Fechados</Card.Header>
		  	{this.state.closedTicketsBot ===" "? (
				<Card.Content>
					<Statistic>
					<Statistic.Value>-</Statistic.Value>
					</Statistic>
				</Card.Content>
			  ) : (
				<Card.Content>
					<Statistic>
					<Statistic.Value>{this.state.closedTicketsBot}</Statistic.Value>
					</Statistic>
				</Card.Content>
			  ) }
                
            </Card>
			</Card.Group>
					<Card fluid>
						<Card.Header>
							<h3>Atendimento pelo Robô</h3>
							<Dropdown
								placeholder="Empresa"
								className="userFilter"
								header="Filtro de usuários"
								value="Empresa"
								selection
								options={userFilter}
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf"
								alt="extrair pdf"
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf icon"
								alt="extrair pdf"
							>
								<i className="xls" />
							</Button>
						</Card.Header>
						<Card.Content>
							<TableBotAnswered getTotalRecords={(e) =>{this.handleTotalRecords(e)}} getClosedTotalRecords={ (e) => {this.handleClosedTickets(e)}} />
						</Card.Content>
					</Card>
					<Card fluid>
						<Card.Header>
							<h3>Departamento encaminhado</h3>
							<Dropdown
								placeholder="Empresa"
								className="userFilter"
								header="Filtro de usuários"
								value="Empresa"
								selection
								options={userFilter}
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf"
								alt="extrair pdf"
							/>
						</Card.Header>
						<Card.Content>
							<Pie data={dataLine} />
						</Card.Content>
					</Card>
			</div>
		);
	}
}

export default ReportBotAnswered;
