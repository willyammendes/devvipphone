import React, { Component } from "react";
import { Card, Button, Statistic, Modal, Table } from "semantic-ui-react";
import PerfectScrollbar from "react-perfect-scrollbar";

import api from "../../api/legal-person";
import apiGroup from "../../api/group";
import apiTotal from "../../api/natural-person";

import { Line, Pie } from "react-chartjs-2";

import TableNaturalPerson from "./tables/TableNaturalPerson";

class ReportClients extends Component {
  state = {
    total_records: " ",
    total_client_group: " ",
    total_client_juridic: " ",
    dataLine: {
      labels: [
        "Janeiro",
        "Fevereiro",
        "Março",
        "Abril",
        "Maio",
        "Junho",
        "Julho",
        "Agosto",
        "Setembro",
        "Outubro",
        "Novembro",
        "Dezembro"
      ],
      datasets: [
        {
          label: "Clientes Pessoa Fisica",
          fill: false,
          lineTension: 0,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [800, 300, 1050, 400, 900, 1231, 400, 30, 40, 70, 90, 10, 123]
        }
      ]
    },
    datapie: {
      labels: ["Inativo", "Ativo", "Possui Carteira"],
      datasets: [
        {
          data: [300, 50, 100],
          backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"]
        }
      ]
    }
  };

  fetchRecords = async () => {
    await apiGroup.group.fetchAll().then(res => {
      this.handleGroupsTotalRecords(res);
    });
    await api.legalPerson.fetchAll().then(res => {
      this.handleJuridicTotalRecords(res);
    });
    await apiTotal.naturalPerson
      .fetchAll({ filter: { create: "today" } })
      .then(res => {
        this.handleTotalRecords(res);
      });
  };
  componentDidMount() {
    this.fetchRecords();
  }

  handleTotalRecords = e => {
    this.setState({ total_records: e.total_records });
  };
  handleJuridicTotalRecords = e => {
    this.setState({ total_client_juridic: e.total_records });
  };
  handleGroupsTotalRecords = e => {
    this.setState({ total_client_group: e.total_records });
  };
  componentWillUpdate(prevProps) {
    if (prevProps.value_choice !== this.props.value_choice) {
      setTimeout(() => {
        this.setState({
          dataLine: { ...this.state.dataLine, labels: this.props.value_choice }
        });
      }, 200);
    }
  }

  render() {
    return (
      <div className="wrap_cards">
        <Card.Group className="cards_four">
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total cadastrados
            </Card.Header>
            {this.state.total_records === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>{this.state.total_records}</Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total pessoa fisica
            </Card.Header>
            {this.state.total_records === " " ||
            this.state.total_client_juridic === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_records - this.state.total_client_juridic}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total pessoa Juridica
            </Card.Header>
            {this.state.total_client_juridic === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_client_juridic}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total grupos de clientes
            </Card.Header>
            {this.state.total_client_group === " " ? (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>-</Statistic.Value>
                </Statistic>
              </Card.Content>
            ) : (
              <Card.Content>
                <Statistic>
                  <Statistic.Value>
                    {this.state.total_client_group}
                  </Statistic.Value>
                </Statistic>
              </Card.Content>
            )}
          </Card>
        </Card.Group>
        <Card className="card_graphic">
          <Card.Header>
            <h3>Clientes cadastrados</h3>
            <Modal
              style={{ width: "90%" }}
              trigger={<Button floated="right">Detalhar</Button>}
            >
              <Modal.Header>Clientes Cadastrados</Modal.Header>
              <Modal.Content>
                <div className="pageHolder" style={{ margin: "0" }}>
                  <div className="holderPage">
                    <TableNaturalPerson />
                  </div>
                </div>
              </Modal.Content>
            </Modal>
          </Card.Header>
          <Card.Content>
            <Line
              data={this.state.dataLine}
              width={100}
              height={50}
              options={{ maintainAspectRatio: false }}
            />
          </Card.Content>
        </Card>
        <Card className="card_graphic">
          <Card.Header>
            <h3>Situação do Cliente</h3>
            <Modal
              style={{ width: "90%" }}
              trigger={<Button floated="right">Detalhar</Button>}
            >
              <Modal.Header>Situação do cliente</Modal.Header>
              <Modal.Content>
                <div className="pageHolder" style={{ margin: "0" }}>
                  <div className="holderPage">
                    <TableNaturalPerson />
                  </div>
                </div>
              </Modal.Content>
            </Modal>
          </Card.Header>
          <Card.Content>
            <Pie
              data={this.state.datapie}
              width={100}
              height={50}
              options={{ maintainAspectRatio: false }}
            />
          </Card.Content>
        </Card>
        <Card.Group style={{ width: "1200px" }}>
          <Card style={{ width: "45%", height: "300px" }}>
            <PerfectScrollbar>
              <Card.Header>
                <h3>Estado Cliente</h3>
              </Card.Header>
              <Card.Content>
                <Table celled textAlign="center">
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Nomes Estado</Table.HeaderCell>
                      <Table.HeaderCell>Quantidade</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Acre</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Alagoas</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Amapá</Table.Cell>
                      <Table.Cell>75</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Amazonas</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Bahia</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ceará</Table.Cell>
                      <Table.Cell>3</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Distrito Federal</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Espírito Santo</Table.Cell>
                      <Table.Cell>231</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Goiás</Table.Cell>
                      <Table.Cell>1234</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Maranhão</Table.Cell>
                      <Table.Cell>345</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Mato Grosso</Table.Cell>
                      <Table.Cell>123</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Mato Grosso do Sul</Table.Cell>
                      <Table.Cell>64</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Minas Gerais</Table.Cell>
                      <Table.Cell>23</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Pará</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Paraíba</Table.Cell>
                      <Table.Cell>83</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Paraná</Table.Cell>
                      <Table.Cell>123</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Pernambuco</Table.Cell>
                      <Table.Cell>125</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Piauí</Table.Cell>
                      <Table.Cell>52</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Rio de Janeiro</Table.Cell>
                      <Table.Cell>23</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Rio Grande do Norte</Table.Cell>
                      <Table.Cell>9</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Rio Grande do Sul</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Rondônia</Table.Cell>
                      <Table.Cell>55</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Roraima</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Santa Catarina</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>São Paulo</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Sergipe</Table.Cell>
                      <Table.Cell>43</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Tocantins</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Total</Table.HeaderCell>
                      <Table.HeaderCell>12313</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                </Table>
              </Card.Content>
            </PerfectScrollbar>
          </Card>

          <Card style={{ width: "45%", height: "300px" }}>
            <PerfectScrollbar>
              <Card.Header>
                <h3>Tickets do cliente</h3>
              </Card.Header>
              <Card.Content>
                <Table celled textAlign="center">
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Nome</Table.HeaderCell>
                      <Table.HeaderCell>Tickets Abertos</Table.HeaderCell>
                      <Table.HeaderCell>Tickets Fechados</Table.HeaderCell>
                      <Table.HeaderCell>Tickets Aguardando</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Alisson</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Willyam</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Alan</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>João</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Igor</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Victor</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Otávio</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Ricardo</Table.Cell>
                      <Table.Cell>1</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Alisson</Table.Cell>
                      <Table.Cell>12</Table.Cell>
                      <Table.Cell>2</Table.Cell>
                      <Table.Cell>0</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </Card.Content>
            </PerfectScrollbar>
          </Card>
        </Card.Group>
      </div>
    );
  }
}

export default ReportClients;
