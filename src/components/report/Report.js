import React, { Component } from "react";
import { Card, Dropdown, Tab, Form } from "semantic-ui-react";
import { DateInput } from "semantic-ui-calendar-react";
import Tour from "reactour";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as TourActions } from "../../store/ducks/tour";

import PerfectScrollbar from "react-perfect-scrollbar";

import ReportClients from "./ReportClients";
import ReportTickets from "./ReportTickets";

import ReportProductivity from "./ReportProductivity";
import ReportBotAnswered from "./ReportBotAnswered";
import ReportAgentsPaused from "./ReportAgentsPaused";
import ReportSchedule from "./ReportSchedule";
import ReportCampaign from "./ReportCampaign";

const dateFilter = [
  {
    key: "Hoje",
    text: "Hoje",
    value: "Hoje"
  },
  {
    key: "Ontem",
    text: "Ontem",
    value: "Ontem"
  },
  {
    key: "Esta semana",
    text: "Esta semana",
    value: "Esta semana"
  },
  {
    key: "Últimos 7 dias",
    text: "Últimos 7 dias",
    value: "Últimos 7 dias"
  },
  {
    key: "Semana passada",
    text: "Semana passada",
    value: "Semana passada"
  },
  {
    key: "Este mês",
    text: "Este mês",
    value: "Este mês"
  },
  {
    key: "Últimos 30 dias",
    text: "Últimos 30 dias",
    value: "Últimos 30 dias"
  },
  {
    key: "Mês passado",
    text: "Mês passado",
    value: "Mês passado"
  },
  {
    key: "Este ano",
    text: "Este ano",
    value: "Este ano"
  },
  {
    key: "Ano passado",
    text: "Ano passado",
    value: "Ano passado"
  },
  {
    key: "Personalizado",
    text: "Personalizado",
    value: "Personalizado"
  }
];

class Summary extends Component {
  state = {
    value_drop: "",
    value_choice: ["month", "Este mês"],
    panes: [
      {
        menuItem: {
          key: "clients",
          icon: "address card outline",
          content: "Clientes"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              {" "}
              <Card.Content>
                <ReportClients value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "ticket",
          icon: "ticket alternate",
          content: "Tickets"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              <Card.Content>
                <ReportTickets value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "produtividade",
          icon: "chart line",
          content: "Produtividade"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              <Card.Content>
                <ReportProductivity value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "robo",
          icon: "android",
          content: "Atendido por Robo"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              <Card.Content>
                <ReportBotAnswered value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "pause",
          icon: "pause",
          content: "Pausas dos Agentes"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              <Card.Content>
                <ReportAgentsPaused value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "login",
          icon: "sign-in",
          content: "Sessões de Login"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              {" "}
              <Card.Content>
                <ReportSchedule value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: "campanha",
          icon: "announcement",
          content: "Campanha"
        },
        render: () => (
          <Tab.Pane>
            <Card.Group>
              <Card.Content>
                <ReportCampaign value_choice={this.state.value_choice} />
              </Card.Content>
            </Card.Group>
          </Tab.Pane>
        )
      }
    ]
  };
  handleChangeOption = (e, { value }) => {
    this.setState({ value_drop: value });
    switch (value) {
      case "Hoje":
        return this.setState({ value_choice: ["today", "Hoje"] });
      case "Ontem":
        return this.setState({ value_choice: ["yesterday", "Ontem"] });
      case "Esta semana":
        return this.setState({ value_choice: ["week", "Esta semana"] });
      case "Últimos 7 dias":
        return this.setState({
          value_choice: ["last_seven_days", "Últimos 7 dias"]
        });
      case "Semana passada":
        return this.setState({ value_choice: ["last_week", "Semana passada"] });
      case "Este mês":
        return this.setState({ value_choice: ["month", "Este mês"] });
      case "Últimos 30 dias":
        return this.setState({
          value_choice: ["last_thirty_days", "Últimos 30 dias"]
        });
      case "Mês passado":
        return this.setState({ value_choice: ["last_month", "Mês passado"] });
      case "Este ano":
        return this.setState({ value_choice: ["year", "Este ano"] });
      case "Ano passado":
        return this.setState({ value_choice: ["last_year", "Ano passado"] });
      default:
        return this.setState({
          value_choice: ["Personalizado", "Personalizado"]
        });
    }
  };

  render() {
    return (
      <div className="pageHolder page_stats report CofigNav ">
        <Tour
          steps={this.props.tour.reportSteps}
          isOpen={this.props.tour.isOpenReport}
          onRequestClose={this.props.closeTourReport}
        />
        <div className="headingPage">
          <Card.Content>
            <Form.Group>
              <h1>Relatórios</h1>
              <Dropdown
                placeholder="Selecione uma data"
                className="dateFilter"
                header="Filtro de data"
                value={this.state.value_drop}
                selection
                onChange={this.handleChangeOption}
                size="large"
                options={dateFilter}
              />
              {this.state.value_drop === "Personalizado" && (
                <>
                  <DateInput placeholder="Data inicio" />
                  <DateInput placeholder="Data final" />
                </>
              )}
            </Form.Group>
          </Card.Content>
        </div>
        <div className="holderPageReport">
          <div className="report_card ">
            <PerfectScrollbar>
              <Tab panes={this.state.panes} />
            </PerfectScrollbar>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tour: state.tour
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Summary);
