import React, { Component } from "react";
import { Card, Statistic } from "semantic-ui-react";
import {  Bar } from "react-chartjs-2";
import apiProductivity from "../../api/user-productivity-table";
import api from "../../api/department"


//import ProductivityTable from "./tables/ProductivityTable";

// const userFilter = [
//   {
//     key: "Eu",
//     text: "Eu",
//     value: "Eu",
//     icon: "user outline"
//   },
//   {
//     key: "Empresa",
//     text: "Empresa",
//     value: "Empresa",
//     icon: "globe"
//   }
// ];

// const dataLine = {
//   labels: ["Tickets Abertos", "Tickets Fechados", "Tickets Aguardando"],
//   datasets: [
//     {
//       label: "Produtividade",
//       data: [65, 59, 80],
//       backgroundColor: [
//         "rgba(255, 99, 132, 0.7)",
//         "rgba(54, 162, 235, 0.7)",
//         "rgba(255, 206, 86, 0.7)",
//         "rgba(75, 192, 192, 0.7)",
//         "rgba(153, 102, 255, 0.7)",
//         "rgba(255, 159, 64, 0.7)",
//         "rgba(190, 64, 12, 0.7)"
//       ],

//       hoverBackgroundColor: [
//         "rgba(255, 99, 132, 1)",
//         "rgba(54, 162, 235, 1)",
//         "rgba(255, 206, 86, 1)",
//         "rgba(75, 192, 192, 1)",
//         "rgba(153, 102, 255, 1)",
//         "rgba(255, 159, 64, 1)",
//         "rgba(190, 64, 12, 1)"
//       ]
//     }
//   ]
// };
class ReportProductivity extends Component {
  state = {
    total_productivity: "",
    departaments: ""
  }

  fetchRecords = async e => {
    await api.department.fetchAll().then( res => {
      this.setState({departaments: res})
    })
    await apiProductivity.userProductivityTable
      .fetchAll({ filter: { created: `${e.value_choice[0]}` } })
      .then(res => {
        this.setState({total_productivity: res})
      });
  }

  componentWillUpdate(prevProps) {
    if (prevProps.value_choice !== this.props.value_choice) {
      setTimeout(() => {
        this.fetchRecords(this.props);
      }, 200);
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.fetchRecords(this.props);
    }, 200);
  }
  render() {
    const users = this.state.total_productivity.data&& this.state.total_productivity.data.filter(res => res.openTickets && res.user )
    const openTickets = this.state.total_productivity.data&&this.state.total_productivity.data.filter(res => res.openTickets && res.openTickets)
    const waitingTicket = this.state.total_productivity.data&&this.state.total_productivity.data.filter(res => res.openTickets && res.waitingTicket)
    const closed = this.state.total_productivity.data&&this.state.total_productivity.data.filter(res => res.openTickets && res.closedTickets)
    const departaments = this.state.departaments.data &&this.state.departaments.data.filter(res => res.name)
    const dataUsers = {
      labels: users&&users.map(res => res.user) ,
      backgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      hoverBackgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      datasets: [
        {
          label: "Aguardando",
          data: openTickets&&openTickets.map(res => res.waitingTicket),
          backgroundColor: "rgba(54, 162, 235, 0.7)"
        },
        {
          label: "Abertos",
          data: waitingTicket&&waitingTicket.map(res => res.openTickets),
          backgroundColor: "blue"
        },
        {
          label: "Fechados",
          data: closed&&closed.map(res => res.closedTickets),
          backgroundColor: "#233a96"
        }
      ]
    };
    const dataUsers2 = {
      labels: departaments&&departaments.map(res => res.name),
      backgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      hoverBackgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      datasets: [
        {
          label: "Aguardando",
          data:[12, 22,1,4] ,
          backgroundColor: "rgba(54, 162, 235, 0.7)"
        },
        {
          label: "Abertos",
          data:[23,12,44,11] ,
          backgroundColor: "blue"
        },
        {
          label: "Fechados",
          data: [12,12,44,1],
          backgroundColor: "#233a96"
        }
      ]
    };


    return (
      <div className="wrap_cards">
          <Card.Group className="cards_five">
          <Card>
          <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Tickets criado por hora</Card.Header>
                <Card.Content>
                <Statistic>
                  <Statistic.Value>12</Statistic.Value>
                </Statistic>
                </Card.Content>
            </Card>
            <Card>
          <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Tickets criado por dia</Card.Header>
                <Card.Content>
                <Statistic>
                  <Statistic.Value>12</Statistic.Value>
                </Statistic>
                </Card.Content>
            </Card>
            <Card>
          <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Tickets criado por semana</Card.Header>
                <Card.Content>
                <Statistic>
                  <Statistic.Value>12</Statistic.Value>
                </Statistic>
                </Card.Content>
            </Card>
            <Card>
          <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Tickets criado por mês</Card.Header>
                <Card.Content>
                <Statistic>
                  <Statistic.Value>12</Statistic.Value>
                </Statistic>
                </Card.Content>
            </Card>
            <Card>
            <Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Tickets criado por ano</Card.Header>
                <Card.Content>
                <Statistic>
                  <Statistic.Value>12</Statistic.Value>
                </Statistic>
                </Card.Content>
            </Card>
          </Card.Group>
          <Card fluid>
            <Card.Header>
              <h3>Produtividade dos usuários <small>{this.props.value_choice[1]}</small></h3>
            </Card.Header>
            <Card.Content>
            <Bar
                data={dataUsers}
                options={{
                  scales: {
                    xAxes: [
                      {
                        stacked: true
                      }
                    ],
                    yAxes: [
                      {
                        stacked: true,
                        ticks: {
                          suggestedMin: 0,
                          suggestedMax: 10,
                          beginAtZero: true
                        }
                      }
                    ]
                  }
                }}
              />
            </Card.Content>
          </Card>
          <Card fluid>
            <Card.Header>
              <h3>Produtividade por departamento <small>{this.props.value_choice[1]}</small></h3>
            </Card.Header>
            <Card.Content>
            <Bar
                data={dataUsers2}
                options={{
                  scales: {
                    xAxes: [
                      {
                        stacked: true
                      }
                    ],
                    yAxes: [
                      {
                        stacked: true,
                        ticks: {
                          suggestedMin: 0,
                          suggestedMax: 10,
                          beginAtZero: true
                        }
                      }
                    ]
                  }
                }}
              />
            </Card.Content>
          </Card>
      </div>
    );
  }
}

export default ReportProductivity;
