import React, { Component } from "react";
import { Card, Dropdown, Button, Statistic } from "semantic-ui-react";

import DataTable from "../table/DataTable";
import api from "../../api/stats";

const userFilter = [
  {
    key: "Eu",
    text: "Eu",
    value: "Eu",
    icon: "user outline"
  },
  {
    key: "Empresa",
    text: "Empresa",
    value: "Empresa",
    icon: "globe"
  },
  {
    key: "Monitcall",
    text: "Monitcall",
    value: "Monitcall",
    icon: "building outline"
  }
];

const columns = {
  id: "ID",
  user: "Usuário",
  department: "Departamento",
  login_start: "Login",
  login_end: "Logout",
  time: "Tempo",
  status: "Status"
};

const data = [];

class ReportSchedule extends Component {
  fetchData = async () => {
    await api.statistic.fetchAll({ take: 50 }).then(res => {});
  };

  componentDidMount() {
    this.fetchData();
  }
  render() {
    return (
      <div className="wrap_cards">
        <Card.Group className="cards_five">
          <Card>
            <Card.Header style={{ backgroundColor: "rgb(226, 232, 234)" }}>
              Total Pausas
            </Card.Header>
            <Card.Content>
              <Statistic>
                <Statistic.Value>0</Statistic.Value>
              </Statistic>
            </Card.Content>
          </Card>
        </Card.Group>
        <Card fluid>
          <Card.Header>
            <h3>Sessões</h3>
            <Dropdown
              placeholder="Empresa"
              className="userFilter"
              header="Filtro de usuários"
              value="Empresa"
              selection
              options={userFilter}
            />
            <Button
              icon="file pdf outline"
              className="extrair_pdf"
              alt="extrair pdf"
            />
            <Button
              icon="file pdf outline"
              className="extrair_pdf icon"
              alt="extrair pdf"
            >
              <i className="xls" />
            </Button>
          </Card.Header>
          <Card.Content>
            <div className="tabela-padrao">
              <DataTable columns={columns} data={data} />
            </div>
          </Card.Content>
        </Card>
      </div>
    );
  }
}

export default ReportSchedule;
