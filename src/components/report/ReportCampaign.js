import React, { Component } from "react";
import { Card, Dropdown, Button, Statistic } from "semantic-ui-react";
import { Line, Bar } from "react-chartjs-2";

import CampaignTable from "./tables/CampaignTable";

const campaignFilter = [
	{
		key: "Campanha 1",
		text: "Campanha 1",
		value: "Campanha 1"
	},
	{
		key: "Campanha 2",
		text: "Campanha 2",
		value: "Campanha 2"
	},
	{
		key: "Campanha 3",
		text: "Campanha 3",
		value: "Campanha 3"
	}
];

const dataBar = {
	labels: ["Enviadas", "Entregues", "Lidas"],
	datasets: [
		{
			label: "Performance da Campanha",
			data: [100, 41, 5],
			backgroundColor: [
				"rgba(255, 99, 132, 0.7)",
				"rgba(54, 162, 235, 0.7)",
				"rgba(255, 206, 86, 0.7)",
				"rgba(75, 192, 192, 0.7)",
				"rgba(153, 102, 255, 0.7)",
				"rgba(255, 159, 64, 0.7)",
				"rgba(190, 64, 12, 0.7)"
			],

			hoverBackgroundColor: [
				"rgba(255, 99, 132, 1)",
				"rgba(54, 162, 235, 1)",
				"rgba(255, 206, 86, 1)",
				"rgba(75, 192, 192, 1)",
				"rgba(153, 102, 255, 1)",
				"rgba(255, 159, 64, 1)",
				"rgba(190, 64, 12, 1)"
			]
		}
	]
};

const dataLine = {
	labels: [
		"Janeiro",
		"Fevereiro",
		"Março",
		"Abril",
		"Maio",
		"Junho",
		"Julho"
	],
	datasets: [
		{
			label: "Performance da Campanha",
			fill: false,
			lineTension: 0,
			backgroundColor: "rgba(75,192,192,0.4)",
			borderColor: "rgba(75,192,192,1)",
			borderCapStyle: "butt",
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: "miter",
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "#fff",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(75,192,192,1)",
			pointHoverBorderColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [65, 59, 80, 81, 56, 55, 40]
		}
	]
};
class ReportCampaign extends Component {
	state = {
		total_records: " "
	}

	handleTotalRecords = (e) =>{
		this.setState({total_records: e.total_records})
	}
	render() {
		return (
			<div className="wrap_cards">
          <Card.Group className="cards_five">
            <Card>
			<Card.Header style={{backgroundColor: "rgb(226, 232, 234)"}}>Total Camapanhas</Card.Header>
				{this.state.total_records ===" "?  
				(
					<Card.Content>
						<Statistic>
							<Statistic.Value>-</Statistic.Value>
						</Statistic>
					</Card.Content>
				)
				 : 
				 (
					<Card.Content>
						<Statistic>
							<Statistic.Value>{this.state.total_records}</Statistic.Value>
						</Statistic>
					</Card.Content>
				) }
              
            </Card>
			</Card.Group>
					<Card fluid>
						<Card.Header>
							<h3>Campanha</h3>
							<Dropdown
								placeholder="Campanha"
								className="campaignFilter"
								header="Filtro de Campanhas"
								value="Campanha"
								selection
								options={campaignFilter}
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf"
								alt="extrair pdf"
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf icon"
								alt="extrair pdf"
							>
								<i className="xls" />
							</Button>
						</Card.Header>
						<Card.Content>
							<CampaignTable getTotalRecords={ e => this.handleTotalRecords(e)} />
						</Card.Content>
					</Card>

						<Card fluid>
							<Card.Header>
								<h3>Campanha</h3>
								<Dropdown
									placeholder="Campanha"
									className="campaignFilter"
									header="Filtro de Campanhas"
									value="Campanha"
									selection
									options={campaignFilter}
								/>
								<Button
									icon="file pdf outline"
									className="extrair_pdf"
									alt="extrair pdf"
								/>
							</Card.Header>
							<Card.Content>
								<Bar data={dataBar} />
							</Card.Content>
						</Card>
						<Card fluid>
							<Card.Header>
								<h3>Campanha 2</h3>
								<Dropdown
									placeholder="Campanha"
									className="campaignFilter"
									header="Filtro de Campanhas"
									value="Campanha"
									selection
									options={campaignFilter}
								/>
								<Button
									icon="file pdf outline"
									className="extrair_pdf"
									alt="extrair pdf"
								/>
							</Card.Header>
							<Card.Content>
								<Line data={dataLine} />
							</Card.Content>
						</Card>
			</div>
		);
	}
}

export default ReportCampaign;
