import React, { Component } from "react";
import ReportLinks from "./ReportLinks";
import { Card } from "semantic-ui-react";

const navLinks = [
  {
    name: "Painéis",
    links: [
      { id: 1, title: "Clientes", url: "parameters", icon: "settings" },
      { id: 2, title: "Tickets Abertos", url: "webhook", icon: "lightning" },
      { id: 3, title: "Tickets Fechados", url: "server", icon: "server" },
      { id: 4, title: "Produtividade", url: "email", icon: "mail" },
      { id: 5, title: "Atendido por Robô", url: "sms", icon: "mobile" },
      { id: 6, title: "Pausas dos Agentes", url: "call", icon: "phone" },
      { id: 7, title: "Horário de atendimento", url: "call", icon: "phone" }
    ]
  }
];

class SideReports extends Component {
  render() {
    return (
      <div className="SideReports">
        <div className="headingPage">
          <h1>Configurações</h1>
        </div>
        <div className="holderPage">
          {navLinks.map((navLink, i) => (
            <div className="navConfig" key={`config-nav-${i * 1}`}>
              <Card fluid>
                <Card.Content header={navLink.name} />
                <Card.Content>
                  <ReportLinks navLinks={navLink.links} />
                </Card.Content>
              </Card>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default SideReports;
