import React, { Component } from "react";
import { Card, Dropdown, Button } from "semantic-ui-react";
import Chart from "chart.js";

const userFilter = [
	{
		key: "Eu",
		text: "Eu",
		value: "Eu",
		icon: "user outline"
	},
	{
		key: "Empresa",
		text: "Empresa",
		value: "Empresa",
		icon: "globe"
	},
	{
		key: "Monitcall",
		text: "Monitcall",
		value: "Monitcall",
		icon: "building outline"
	}
];

const dateFilter = [
	{
		key: "Hoje",
		text: "Hoje",
		value: "Hoje"
	},
	{
		key: "Ontem",
		text: "Ontem",
		value: "Ontem"
	},
	{
		key: "Esta semana",
		text: "Esta semana",
		value: "Esta semana"
	},
	{
		key: "Últimos 7 dias",
		text: "Últimos 7 dias",
		value: "Últimos 7 dias"
	},
	{
		key: "Semana passada",
		text: "Semana passada",
		value: "Semana passada"
	},
	{
		key: "Este mês",
		text: "Este mês",
		value: "Este mês"
	},
	{
		key: "Últimos 30 dias",
		text: "Últimos 30 dias",
		value: "Últimos 30 dias"
	},
	{
		key: "Mês passado",
		text: "Mês passado",
		value: "Mês passado"
	},
	{
		key: "Este ano",
		text: "Este ano",
		value: "Este ano"
	},
	{
		key: "Ano passado",
		text: "Ano passado",
		value: "Ano passado"
	}
];

class ReportClosedTickets extends Component {
	render() {
		window.onload = function() {
			const ctx = document.getElementById("myChart");

			const ctx2 = document.getElementById("myChart2");

			const ctx3 = document.getElementById("myChart3");
		};
		return (
			<div className="wrap_cards">
				<div className="card_meio card_full">
					<Card fluid>
						<Card.Header>
							<h3>Número de Tickets fechados</h3>
							<Dropdown
								placeholder="Empresa"
								className="userFilter"
								header="Filtro de usuários"
								value="Empresa"
								selection
								options={userFilter}
							/>
							<Dropdown
								placeholder="Hoje"
								className="dateFilter"
								header="Filtro de data"
								value="Hoje"
								selection
								options={dateFilter}
							/>
							<Button
								icon="file pdf outline"
								className="extrair_pdf"
								alt="extrair pdf"
							/>
						</Card.Header>
						<Card.Content>
							<canvas id="myChart" width="400" height="100" />
						</Card.Content>
					</Card>
				</div>
				<div className="linha">
					<div className="card_meio">
						<Card fluid>
							<Card.Header>
								<h3>Tickets fechados nesse mês</h3>
								<Dropdown
									placeholder="Empresa"
									className="userFilter"
									header="Filtro de usuários"
									value="Empresa"
									selection
									options={userFilter}
								/>
								<Dropdown
									placeholder="Hoje"
									className="dateFilter"
									header="Filtro de data"
									value="Hoje"
									selection
									options={dateFilter}
								/>
								<Button
									icon="file pdf outline"
									className="extrair_pdf"
									alt="extrair pdf"
								/>
							</Card.Header>
							<Card.Content>
								<canvas
									id="myChart2"
									width="400"
									height="200"
								/>
							</Card.Content>
						</Card>
					</div>
					<div className="card_meio">
						<Card fluid>
							<Card.Header>
								<h3>Tickets fechados nessa semana</h3>
								<Dropdown
									placeholder="Empresa"
									className="userFilter"
									header="Filtro de usuários"
									value="Empresa"
									selection
									options={userFilter}
								/>
								<Dropdown
									placeholder="Hoje"
									className="dateFilter"
									header="Filtro de data"
									value="Hoje"
									selection
									options={dateFilter}
								/>
								<Button
									icon="file pdf outline"
									className="extrair_pdf"
									alt="extrair pdf"
								/>
							</Card.Header>
							<Card.Content>
								<canvas
									id="myChart3"
									width="400"
									height="200"
								/>
							</Card.Content>
						</Card>
					</div>
				</div>
			</div>
		);
	}
}

export default ReportClosedTickets;
