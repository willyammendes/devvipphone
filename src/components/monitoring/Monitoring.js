import React, { Component } from "react";
import api from "../../api/company";
import DataTable from "../table/DataTable";
import CompanyModal from "../company/modal/CompanyModal";
import AlertSuccess from "../alerts/AlertSuccess";
import Validator from "validator";
import ErrorBoundarie from "../messages/ErrorBoundarie";

class Monitoring extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    hide_actions: true,
    confirm: {
      email: "",
      username: "",
      password: "",
      confirmPassword: ""
    }
  };

  select = selectedDataId => {
    const { records } = this.state;
    const dataIndex = records.findIndex(c => c.id === selectedDataId);
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              company_type: 1,
              active: false,
              intelliway_integration: false,
              paid_account: false,
              accept_terms: true,
              free_registration: true
            }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
          company_type: 1,
          accept_terms: true,
          free_registration: true
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };
  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email Invalído";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };
  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };
  onSelectOrigin = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          origin: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  onSelectSegment = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          segment: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.company.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  onChangeState = e =>
    this.setState({
      confirm: { ...this.state.data, [e.target.name]: e.target.value }
    });

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records, confirm } = this.state;
    this.setState({
      loading: true
    });
    const data = records[dataIndex];

    const password = data.password;
    const confirmPassword = confirm.confirmPassword;
    if (password !== confirmPassword) {
      alert("As senhas não correspondem");
      this.setState({
        loading: false
      });
    } else {
      api.company.submitRegister(records[dataIndex]).then(data => {
        this.setState({
          records: [
            ...records.slice(0, dataIndex),
            { ...data.data },
            ...records.slice(dataIndex + 1)
          ],
          save_alert: true,
          selectedDataIndex: -1,
          loading: false
        });
      });
      return api.company
        .submit(records[dataIndex])
        .then(data => {
          this.setState({
            save_alert: true,
            loading: false,
            selectedDataIndex: -1,
            editModalOpen: false,
            records: [
              ...records.slice(0, dataIndex),
              data.data,
              ...records.slice(dataIndex + 1)
            ]
          });
          setTimeout(
            function() {
              this.setState({ save_alert: false });
            }.bind(this),
            5000
          );
        })
        .catch(err => {
          this.setState({
            loading: false
          });
        });
    }
  };

  update = () => {
    const { selectedDataIndex, records, confirm } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    const password = data.password;
    const confirmPassword = confirm.confirmPassword;
    if (password !== confirmPassword) {
      alert("As senhas não correspondem");
    } else {
      api.company.update(data.id, data).then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      });
    }
  };

  delete = id => api.company.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      errors,
      confirm,
      hide_actions
    } = this.state;

    const company = records[selectedDataIndex];

    return (
      <ErrorBoundarie>
        <div className="pageHolder monitoring">
          <div className="holderPage">
            <div className="full">
              <div className="tabela-padrao">
                <div className="headingPage">
                  <h1>Monitoramento</h1>
                  {this.state.save_alert && <AlertSuccess />}
                </div>
                <DataTable
                  loading={loading}
                  onAddClick={this.newDataClick}
                  csv={records}
                  columns={columns}
                  data={records}
                  totalRecords={total_records}
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  onDelete={id => this.delete(id)}
                  onEditClick={d => this.select(d.id)}
                  fetchData={this.fetchRecords}
                  hide_actions={hide_actions}
                />

                {selectedDataIndex !== -1 ? (
                  <CompanyModal
                    handleClose={this.handleCloseEditModal}
                    onChange={this.handleChange}
                    onChecked={this.handleChecked}
                    handleNext={this.nextRecord}
                    handlePrevious={this.previousRecord}
                    onClickSave={this.update}
                    onClickAdd={this.submit}
                    company={company}
                    onChangeState={this.onChangeState}
                    data={confirm}
                    modalHeader={
                      company.id ? `Edição do ${company.name}` : "Nova Empresa"
                    }
                    onSelectOrigin={this.onSelectOrigin}
                    onSelectSegment={this.onSelectSegment}
                    errors={errors}
                    open={editModalOpen}
                    previousButtonEnabled={selectedDataIndex === 0}
                    nextButtonEnabled={selectedDataIndex === records.length - 1}
                    loading={loading}
                  />
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </ErrorBoundarie>
    );
  }
}

export default Monitoring;
