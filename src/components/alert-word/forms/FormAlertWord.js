import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormAlertWord = ({ alertword, onChange }) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Palavra:"
                name="word"
                onChange={onChange}
                value={alertword.word}
            />
        </Form>
    );
};

export default FormAlertWord;
