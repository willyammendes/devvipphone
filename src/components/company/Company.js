import React, { Component } from "react";
import api from "../../api/company";
import DataTable from "../table/DataTable";
import CompanyModal from "./modal/CompanyModal";
import AlertSuccess from "../alerts/AlertSuccess";
import Validator from "validator";
import ErrorBoundarie from "../messages/ErrorBoundarie";
import moment from "moment";
import "moment/locale/pt-br";

class Company extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    confirm: {
      email: "",
      username: "",
      password: "",
      confirmPassword: "",
    },
    extra_fields: [],
  };

  select = (selectedDataId) => {
    const { records } = this.state;
    const dataIndex = records.findIndex((c) => c.id === selectedDataId);
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: Array.isArray(records[0][key])
                ? key === "extra_fields"
                  ? this.state.extra_fields
                  : []
                : "",
              company_type: 1,
              active: false,
              intelliway_integration: false,
              paid_account: false,
              accept_terms: true,
              free_registration: true,
              groups: [],
              accounts: [],
              departments: [],
              username: "",
            }),
          {}
        )
      : {
          extra_fields: this.state.extra_fields,
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
          company_type: 1,
          accept_terms: true,
          free_registration: true,
          username: "",
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };
  validate = (data) => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email Invalído";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };
  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };
  onSelectOrigin = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          origin: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  onSelectCode = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  onSelectSegment = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          segment: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.company.fetchCompanies(params).then((res) => {
      this.setState({
        records: res.data || [],
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        extra_fields: res.extra_fields,
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleExtraFields = (e, { name, value, rating }, extraFieldId) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    const extraFieldIndex = records[dataIndex].extra_fields.findIndex(
      (c) => c.id === extraFieldId
    );

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          extra_fields: [
            ...records[dataIndex].extra_fields.slice(0, extraFieldIndex),
            {
              ...records[dataIndex].extra_fields[extraFieldIndex],
              value: rating !== undefined ? rating : value,
            },
            ...records[dataIndex].extra_fields.slice(extraFieldIndex + 1),
          ],
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  onChangeState = (e) =>
    this.setState({
      confirm: { ...this.state.data, [e.target.name]: e.target.value },
    });

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records, confirm } = this.state;
    this.setState({
      loading: true,
    });
    const data = records[dataIndex];
    const dataSend = {
      ...data,
    };
    const password = data.password;
    const confirmPassword = confirm.confirmPassword;
    if (password !== confirmPassword) {
      alert("As senhas não correspondem");
      this.setState({
        loading: false,
      });
    } else {
      return api.company
        .submit(dataSend)
        .then((data) => {
          this.setState({
            save_alert: true,
            loading: false,
            selectedDataIndex: -1,
            editModalOpen: false,
            confirm: {
              email: "",
              username: "",
              password: "",
              confirmPassword: "",
            },
            records: [
              ...records.slice(0, dataIndex),
              data.data,
              ...records.slice(dataIndex + 1),
            ],
          });
          setTimeout(
            function() {
              this.setState({ save_alert: false });
            }.bind(this),
            5000
          );
        })
        .catch((err) => {
          this.setState({
            loading: false,
            errors: err.response,
          });
        });
    }
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.company
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          confirm: {
            email: "",
            username: "",
            password: "",
            confirmPassword: "",
          },
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  delete = (id) => api.company.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
      errors: "",
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      errors,
      confirm,
    } = this.state;

    const company = records[selectedDataIndex];

    this.state.extra_fields.forEach((e) => {
      if (e.show_column_on_view) columns[e.input_name] = e.label;
    });

    return (
      <ErrorBoundarie>
        <div className="pageHolder">
          <div className="holderPage">
            <div className="full">
              <div className="tabela-padrao">
                <div className="headingPage">
                  <h1>Empresas</h1>
                  <span className="all_companies">
                    Empresas Cadastradas:{""}
                    {total_records}
                  </span>

                  {this.state.save_alert && <AlertSuccess />}
                </div>
                <DataTable
                  loading={loading}
                  onAddClick={this.newDataClick}
                  csv={records}
                  columns={columns}
                  extra_fields={this.state.extra_fields.map(
                    (e) => e.input_name
                  )}
                  data={records.map((r) => ({
                    ...r,
                    name: <span className="nowrap">{r.name}</span>,
                    updated_at: (
                      <span>
                        {moment
                          .unix(r.updated_at)
                          .locale("pt-br")
                          .format("l")}
                      </span>
                    ),
                    paid_account: r.paid_account ? "Sim" : "Não",
                    active: r.active ? "Sim" : "Não",
                    monitbot_active: r.monitbot_active ? "Sim" : "Não",
                    intelliway_integration: r.intelliway_integration
                      ? "Sim"
                      : "Não",
                    external_server: r.external_server ? "Sim" : "Não",
                    trial_period_expired: r.trial_period_expired
                      ? "Sim"
                      : "Não",
                  }))}
                  totalRecords={total_records}
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  cleanErrors={this.cleanErrors}
                  onDelete={(id) => this.delete(id)}
                  onEditClick={(d) => this.select(d.id)}
                  fetchData={this.fetchRecords}
                />

                {selectedDataIndex !== -1 ? (
                  <CompanyModal
                    handleClose={this.handleCloseEditModal}
                    onChange={this.handleChange}
                    handleExtraFields={this.handleExtraFields}
                    onChecked={this.handleChecked}
                    handleNext={this.nextRecord}
                    handlePrevious={this.previousRecord}
                    onClickSave={this.update}
                    onClickAdd={this.submit}
                    company={company}
                    onChangeState={this.onChangeState}
                    data={confirm}
                    showNavigation
                    modalHeader={"Empresa"}
                    onSelectCode={this.onSelectCode}
                    generalError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.errors
                        : ""
                    }
                    messageError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.errors
                        : ""
                    }
                    cleanErrors={this.cleanErrors}
                    onSelectOrigin={this.onSelectOrigin}
                    onSelectSegment={this.onSelectSegment}
                    errors={errors}
                    open={editModalOpen}
                    previousButtonEnabled={selectedDataIndex === 0}
                    nextButtonEnabled={selectedDataIndex === records.length - 1}
                    loading={loading}
                  />
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </ErrorBoundarie>
    );
  }
}

export default Company;
