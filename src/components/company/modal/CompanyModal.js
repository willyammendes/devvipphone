import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormCompany from "../forms/FormCompany";
import FormNewCompany from "../forms/FormNewCompany";

class CompanyModal extends React.Component {
  state = {
    part: 0,
    titlePart: "",
  };
  componentDidMount() {
    const { company } = this.props;
    if (this.props.showNavigation && company.id) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.company.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };
  back = () => {
    this.setState({
      part: this.state.part - 1,
    });
  };
  advance = () => {
    const { part } = this.state;
    this.setState({
      part: part + 1,
    });
  };

  render() {
    const {
      company,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      clients,
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      groups,
      loading,
      errors,
      onSelectOrigin,
      onSelectSegment,
      data,
      onChangeState,
      generalError,
      cleanErrors,
      messageError,
      editCompany,
      onSelectCode,
      handleExtraFields,
      showNavigation = true,
    } = this.props;

    const { part } = this.state;
    return (
      <Modal
        size={this.props.company.id ? "large" : "small"}
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          {part === 0 ? modalHeader : ""}
          {part === 1 ? "Localização" : ""}
          {part === 2 ? "Licenciamento" : ""}
          {part === 3 ? "Faturas" : ""}
        </Modal.Header>

        <Modal.Content
          style={{
            minHeight: "450px",
          }}
        >
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                {generalError.email ? <p>{generalError.email}</p> : ""}
                {generalError.username ? <p>{generalError.username}</p> : ""}
                {generalError.password ? <p>{generalError.password}</p> : ""}
                {generalError.name ? <p>{generalError.name}</p> : ""}
                {generalError.cpf_cnpj ? <p>{generalError.cpf_cnpj}</p> : ""}
              </div>
            )}

            <div className="holder_company">
              <div className="form-company">
                {this.props.company.id ? (
                  <FormCompany
                    company={company}
                    clients={clients}
                    onChange={onChange}
                    onChecked={onChecked}
                    handleExtraFields={handleExtraFields}
                    groups={groups}
                    errors={errors}
                    messageError={messageError}
                    onChangeState={onChangeState}
                    data={data}
                    onSelectCode={onSelectCode}
                    onSelectOrigin={onSelectOrigin}
                    onSelectSegment={onSelectSegment}
                    part={part}
                  />
                ) : (
                  <FormNewCompany
                    messageError={messageError}
                    handleExtraFields={handleExtraFields}
                    company={company}
                    onChange={onChange}
                    onChecked={onChecked}
                    onChangeState={onChangeState}
                    data={data}
                  />
                )}
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {showNavigation && company.id ? (
            <Button.Group>
              <Button
                icon
                onClick={handlePrevious}
                disabled={previousButtonEnabled}
              >
                <Icon name="left arrow" />
              </Button>
              <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
                <Icon name="right arrow" />
              </Button>
            </Button.Group>
          ) : (
            ""
          )}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            {!this.props.company.id ? (
              ""
            ) : (
              <Button
                onClick={this.back}
                loading={loading}
                disabled={part === 0 ? true : loading}
                color="grey"
              >
                Voltar
              </Button>
            )}

            <Button.Or />
            {(part === 3 && !editCompany) ||
            (part === 3 && editCompany) ||
            !this.props.company.id ? (
              <Button
                positive
                onClick={this.save}
                loading={loading}
                disabled={loading}
              >
                Salvar
              </Button>
            ) : (
              <Button
                positive
                onClick={this.advance}
                loading={loading}
                disabled={loading}
                color="blue"
              >
                Avançar
              </Button>
            )}
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default CompanyModal;
