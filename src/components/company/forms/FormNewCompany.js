import React from "react";
import { Form, Input } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";
import InputMask from "react-input-mask";
import ExtraFieldForm from "../../extra-input/ExtraFieldForm";

const FormNewCompany = ({
  company,
  onChange,
  onChecked,
  handleExtraFields,
  messageError,
  onChangeState,
  data,
}) => {
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field error={messageError ? !!messageError.name : ""}>
          <label htmlFor="name">Razão Social*</label>
          <Input
            autoComplete="off"
            control={Input}
            name="name"
            onChange={onChange}
            value={company.name}
            placeholder="Nome"
          />
          {messageError ? <InlineError text={messageError.name} /> : ""}
        </Form.Field>

        <Form.Field error={messageError ? !!messageError.username : ""}>
          <label htmlFor="username">Nome do Administrador*</label>
          <Input
            autoComplete="off"
            control={Input}
            type="text"
            name="username"
            onChange={onChange}
            value={company.username}
            placeholder="Nome do Administrador"
          />
          {messageError ? <InlineError text={messageError.username} /> : ""}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field error={messageError ? !!messageError.email : ""}>
          <label htmlFor="email">Email para Login*</label>
          <Input
            autoComplete="off"
            type="email"
            control={Input}
            name="email"
            onChange={onChange}
            value={company.email}
            placeholder="Email"
          />
          {messageError ? <InlineError text={messageError.email} /> : ""}
        </Form.Field>
        <Form.Field error={messageError ? !!messageError.phone_number : ""}>
          <label htmlFor="phone_number">Telefone*</label>
          <Input
            autoComplete="off"
            control={Input}
            children={
              <InputMask
                mask="+55 (99) 99999-9999"
                name="phone_number"
                onChange={(e) =>
                  onChange(e, { name: e.target.name, value: e.target.value })
                }
                value={company.phone_number}
                placeholder="+55 (__) _____-____"
              />
            }
          />
          {messageError ? <InlineError text={messageError.phone_number} /> : ""}
        </Form.Field>
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Field error={messageError ? !!messageError.password : ""}>
          <label htmlFor="password">Senha*</label>
          <Input
            autoComplete="off"
            control={Input}
            name={"password"}
            type="password"
            value={company.password}
            placeholder="Senha"
            onChange={onChange}
          />
          {messageError ? <InlineError text={messageError.password} /> : ""}
        </Form.Field>

        {company.id ? (
          ""
        ) : (
          <Form.Field
            autoComplete="off"
            control={Input}
            name={"confirmPassword"}
            type="password"
            value={company.id ? company.password : data.confirmPassword}
            placeholder="Confirmar Senha"
            label={"Confirmar Senha*"}
            onChange={onChangeState}
          />
        )}
      </Form.Group>
      <ExtraFieldForm
        extra_fields={company.extra_fields}
        onChange={handleExtraFields}
      />
    </Form>
  );
};

export default FormNewCompany;
