import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormRoles from "../forms/FormRoles";

class RolesModal extends React.Component {
    state = {
        roleProfile: false
    };
    componentDidMount() {
        const { role } = this.props;
        if (role.id) {
            document.removeEventListener("keydown", () => {});
            document.addEventListener("keydown", e => {
                if (e.keyCode === 39) this.props.handleNext();
                if (e.keyCode === 37) this.props.handlePrevious();
            });
        }
    }

    save = () => {
        if (this.props.role.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    ChangeTab = () => {
        this.setState({
            roleProfile: !this.state.roleProfile
        });
    };

    render() {
        const {
            role,
            handleClose,
            onChange,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading,
            onChecked,
            handleGroupChange,
            handleDepartmentChange,
            handleMediaChange,
            messageError,
            generalError,
            cleanErrors,
            onSelectRoles,
            permission,
            handlePermission,
            permissions
        } = this.props;

        return (
            <Modal
                size="large"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>

                <Modal.Content>
                    <Modal.Description>
                        {generalError && (
                            <div className="errors-table">
                                <Button
                                    circular
                                    basic
                                    color="black"
                                    icon="close"
                                    className="button-close"
                                    onClick={cleanErrors}
                                />
                                <p>{generalError}</p>
                            </div>
                        )}
                        <div className="form-role">
                            <FormRoles
                                role={role}
                                onSelectRoles={onSelectRoles}
                                roleProfile={this.state.roleProfile}
                                onChange={onChange}
                                onChecked={onChecked}
                                handleGroupChange={handleGroupChange}
                                handleDepartmentChange={handleDepartmentChange}
                                handleMediaChange={handleMediaChange}
                                messageError={messageError}
                                permission={permission}
                                permissions={permissions}
                                handlePermission={handlePermission}
                            />
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    {role.id ? (
                        <Button.Group>
                            <Button
                                icon
                                onClick={handlePrevious}
                                disabled={previousButtonEnabled}
                            >
                                <Icon name="left arrow" />
                            </Button>
                            <Button
                                icon
                                onClick={handleNext}
                                disabled={nextButtonEnabled}
                            >
                                <Icon name="right arrow" />
                            </Button>
                        </Button.Group>
                    ) : (
                        ""
                    )}

                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default RolesModal;
