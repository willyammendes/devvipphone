import React from "react";
import { Form, Input, Checkbox, Card } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";

const FormRoles = ({
  role,
  onChange,
  messageError,
  permission,
  handlePermission
}) => {
  return (
    <Form autoComplete="off">
      <div className="holder_form_role">
        <Form.Field error={messageError ? !!messageError.name : ""}>
          <label htmlFor="name">Nome</label>
          <Input
            autoComplete="off"
            control={Input}
            name="name"
            onChange={onChange}
            value={role.name}
            placeholder="Nome"
          />
          <p className="invisible">{role.number}</p>
          {messageError ? <InlineError text={messageError.name} /> : ""}
          <div className="holder_permission">
            {permission.map(c => (
              <Card className="card_permission" fluid>
                <Card.Content
                  header={c[0].description ? c[0].description : c[0].model}
                />
                <Card.Content>
                  {c.map(d => (
                    <Checkbox
                      value={d.id}
                      checked={
                        role.permissions.find(c => c === d.id) ? true : false
                      }
                      onChange={() => handlePermission(d.id)}
                      label={d.label}
                    />
                  ))}
                </Card.Content>
              </Card>
            ))}
          </div>
        </Form.Field>
      </div>
    </Form>
  );
};

export default FormRoles;
