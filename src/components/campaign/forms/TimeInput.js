import moment from "moment";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import InputMoment from "input-moment";

class TimeInput extends Component {
  state = {
    m: moment()
  };

  handleChange = m => {
    this.setState({ m });
  };

  handleSave = () => {
    
  };

  render() {
    return (
      <div className="app">
        <form>
          <div className="input">
            <input type="text" value={this.state.m.format("llll")} readOnly />
          </div>
          <InputMoment
            moment={this.state.m}
            onChange={this.handleChange}
            minStep={5}
            onSave={this.handleSave}
          />
        </form>
      </div>
    );
  }
}

export default TimeInput;
