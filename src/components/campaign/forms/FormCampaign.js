import React from "react";
import {
  Form,
  Input,
  TextArea,
  Modal,
  Button,
  Dropdown,
  Message,
} from "semantic-ui-react";
import DropdownGroups from "../../contacts-groups/DropdownGroups";
import DropdownContacts from "../../contact/DropdownContacts";
import DropdownMedia from "../../social-media/DropdownMedia";
import DropdownUsers from "../../users/DropdownUsers";
import { DateTimeInput } from "semantic-ui-calendar-react";
import "moment/locale/pt-br";

const FormCampaign = ({
  campaign,
  onChange,
  handleGroupChange,
  handleContactChange,
  handleMediaChange,
  BasicUpload,
  UploadList,
  openColumnModal,
  selectedColumns,
  closeModal,
  columns,
  handleDismiss,
  showMessage,
  onSelectName,
  loading,
  handleClose,
  handleUserChange,
  submit,
}) => {
  return (
    <Form>
      <div className="form-row">
        <div className="field-row">
          <Form.Field
            autoComplete="off"
            control={Input}
            label="Nome"
            name="title"
            onChange={onChange}
            value={campaign.title}
          />
          <Form.Field
            control={TextArea}
            label="Mensagem a Ser Enviada"
            name="message"
            onChange={onChange}
            value={campaign.message}
          />
          <DateTimeInput
            dateTimeFormat="YYYY-MM-DD HH:mm"
            placeholder="Data e hora do inicio da campanha"
            name="start_date"
            value={campaign.start_date}
            iconPosition="left"
            onChange={onChange}
          />

          <div className="field">
            <label>Enviar Arquivo ou imagem</label>
            <BasicUpload />
          </div>
        </div>
        <div className="field-row">
          <div className="field">
            <label>
              <h3>Escolha os Contatos</h3>
              <span>Por Grupo</span>
            </label>
            <DropdownGroups
              onSelectGroup={handleGroupChange}
              groups={campaign.groups.map((c) =>
                typeof c === "object" ? c.id : c
              )}
              options={campaign.groups}
              allowAdditions={false}
            />
          </div>
          <div className="field">
            <label>Por Contato</label>
            <DropdownContacts
              onSelectContact={handleContactChange}
              contact_id={campaign.contacts.map((c) =>
                typeof c === "object" ? c.id : c
              )}
              allowAdditions={false}
              options={campaign.contacts}
              multiple
            />
          </div>
          <div className="field">
            <label>Importar lista de contatos</label>
            <UploadList />
          </div>
          <div className="field">
            <label>Escolha as contas que podem disparar a campanha:</label>
            <DropdownMedia
              onSelectMedia={handleMediaChange}
              accounts={campaign.accounts}
              allowAdditions={false}
            />
          </div>
          <div className="field">
            <label>Escolha o responsável pela campanha:</label>
            <DropdownUsers
              onSelectUser={handleUserChange}
              user_id={campaign.user_id}
              allowAdditions={false}
            />
          </div>
          <Modal
            size="small"
            closeIcon
            open={openColumnModal}
            onClose={handleClose}
            dimmer="blurring"
            closeOnDimmerClick={false}
          >
            <Modal.Header>Importar contatos</Modal.Header>
            <Modal.Content>
              <Modal.Description>
                <div className="form-alertword">
                  <Form>
                    {showMessage && (
                      <Message onDismiss={handleDismiss}>
                        <div className="header">Importante!</div>É necessário
                        que seu arquivo possua títulos nas colunas, que possam
                        identificar cada campo. <br />
                        Informe em cada caixa de seleção abaixo, o nome da
                        coluna do seu arquivo excel, correspondente com os dados
                        solicitados
                      </Message>
                    )}

                    <Form.Group widths="equal">
                      <div className="field">
                        <label>Nome:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="name_column"
                          value={selectedColumns.name_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                      <div className="field">
                        <label>Cidade:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="city_column"
                          value={selectedColumns.city_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                    </Form.Group>
                    <Form.Group widths="equal">
                      <div className="field">
                        <label>Telefone:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="phone_number_column"
                          value={selectedColumns.phone_number_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                      <div className="field">
                        <label>Bairro:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="neighborhood_column"
                          value={selectedColumns.neighborhood_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                    </Form.Group>
                    <Form.Group widths="equal">
                      <div className="field">
                        <label>Email:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="email_column"
                          value={selectedColumns.email_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                      <div className="field">
                        <label>Endereço:</label>
                        <Dropdown
                          fluid
                          clearable
                          search
                          selection
                          name="address_column"
                          value={selectedColumns.address_column}
                          onChange={(e, { name, value }) =>
                            onSelectName(e, {
                              name,
                              value,
                            })
                          }
                          options={columns}
                        />
                      </div>
                    </Form.Group>
                  </Form>
                </div>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Button.Group>
                <Button onClick={handleClose}>Cancelar</Button>
                <Button.Or />
                <Button
                  positive
                  onClick={submit}
                  loading={loading}
                  disabled={loading}
                >
                  {columns.length > 0 ? "Importar" : "Enviar"}
                </Button>
              </Button.Group>
            </Modal.Actions>
          </Modal>
        </div>
      </div>
    </Form>
  );
};

export default FormCampaign;
