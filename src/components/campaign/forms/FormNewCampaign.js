import React from "react";
import {
  Form,
  Input,
  TextArea,
  Modal,
  Message,
  Dropdown,
  Button,
} from "semantic-ui-react";
import DropdownGroups from "../../contacts-groups/DropdownGroups";
import DropdownContacts from "../../contact/DropdownContacts";
import DropdownMedia from "../../social-media/DropdownMedia";
import DropdownUsers from "../../users/DropdownUsers";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { DateTimeInput } from "semantic-ui-calendar-react";
import "moment/locale/pt-br";

const FormNewCampaign = ({
  campaign,
  onChange,
  handleGroupChange,
  handleContactChange,
  handleMediaChange,
  handleUserChange,
  part,
  value,
  handleChangeSlide,
  BasicUpload,
  UploadList,
  openColumnModal,
  selectedColumns,
  handleClose,
  columns,
  loading,
  submit,
  showMessage,
  handleDismiss,
  onSelectName,
}) => {
  return (
    <Form>
      {part === 0 ? (
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 1 - Informações Básicas</h3>
            <Form.Field
              control={Input}
              label="Título da Campanha"
              onChange={onChange}
              name="title"
              value={campaign.title}
              placeholder="Título da Campanha"
            />
            <Form.Field
              control={TextArea}
              label="Mensagem a Ser Enviada"
              name="message"
              onChange={onChange}
              value={campaign.message}
              placeholder="Mensagem"
              icon="pencil"
              rows={6}
            />
          </div>
        </div>
      ) : part === 1 ? (
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 2 - Tipo de Mídia - Escolha Uma, das Duas Opções</h3>

            <div className="field">
              <label>Enviar Arquivo ou imagem</label>
              <BasicUpload />
            </div>
          </div>
        </div>
      ) : part === 2 ? (
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 3 - Para quem vai enviar?</h3>
            <div className="field">
              <label>
                <h4>Escolha os Contatos</h4>
                <span>Por Grupo</span>
              </label>
              <DropdownGroups
                onSelectGroup={handleGroupChange}
                groups={
                  campaign.groups
                    ? campaign.groups.map((c) =>
                        typeof c === "object" ? c.id : c
                      )
                    : []
                }
                options={campaign.groups}
                allowAdditions={false}
                multiple={true}
                name="contacts"
              />
            </div>
            <div className="field">
              <label>Por Contato</label>
              <DropdownContacts
                onSelectContact={handleContactChange}
                contact_id={
                  campaign.contacts
                    ? campaign.contacts.map((c) =>
                        typeof c === "object" ? c.id : c
                      )
                    : []
                }
                allowAdditions={false}
                options={campaign.contacts}
                multiple
              />
            </div>
            <div className="field">
              <label>Importar lista de contatos</label>
              <UploadList />
            </div>
            <Modal
              size="small"
              closeIcon
              open={openColumnModal}
              onClose={handleClose}
              dimmer="blurring"
              closeOnDimmerClick={false}
            >
              <Modal.Header>Importar contatos</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <div className="form-alertword">
                    <Form>
                      {showMessage && (
                        <Message onDismiss={handleDismiss} color="red">
                          <div className="header">Importante!</div>
                          É necessário que seu arquivo possua títulos nas
                          colunas, que possam identificar cada campo. <br />
                          Informe em cada caixa de seleção abaixo, o nome da
                          coluna do seu arquivo excel, correspondente com os
                          dados solicitados
                        </Message>
                      )}

                      <Form.Group widths="equal">
                        <div className="field">
                          <label>Nome:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="name_column"
                            value={selectedColumns.name_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                        <div className="field">
                          <label>Cidade:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="city_column"
                            value={selectedColumns.city_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                      </Form.Group>
                      <Form.Group widths="equal">
                        <div className="field">
                          <label>Telefone:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="phone_number_column"
                            value={selectedColumns.phone_number_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                        <div className="field">
                          <label>Bairro:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="neighborhood_column"
                            value={selectedColumns.neighborhood_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                      </Form.Group>
                      <Form.Group widths="equal">
                        <div className="field">
                          <label>Email:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="email_column"
                            value={selectedColumns.email_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                        <div className="field">
                          <label>Endereço:</label>
                          <Dropdown
                            fluid
                            clearable
                            search
                            selection
                            name="address_column"
                            value={selectedColumns.address_column}
                            onChange={(e, { name, value }) =>
                              onSelectName(e, {
                                name,
                                value,
                              })
                            }
                            options={columns}
                          />
                        </div>
                      </Form.Group>
                    </Form>
                  </div>
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <Button.Group>
                  <Button onClick={handleClose}>Cancelar</Button>
                  <Button.Or />
                  <Button
                    positive
                    onClick={submit}
                    loading={loading}
                    disabled={loading}
                  >
                    {columns.length > 0 ? "Importar" : "Enviar"}
                  </Button>
                </Button.Group>
              </Modal.Actions>
            </Modal>
          </div>
        </div>
      ) : part === 3 ? (
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 4 - Data e Hora do Envio</h3>
            <DateTimeInput
              placeholder="Data e hora do inicio da campanha"
              name="start_date"
              dateTimeFormat="YYYY-MM-DD HH:mm"
              value={campaign.start_date}
              iconPosition="left"
              onChange={onChange}
            />

            <div className="field">
              <label htmlFor="value">
                Qual período as mensagens podem ser enviadas?
              </label>
              <InputRange
                formatLabel={(value) => `${value}:00`}
                maxValue={24}
                minValue={0}
                value={value}
                onChange={handleChangeSlide}
              />
            </div>
          </div>
        </div>
      ) : part === 4 ? (
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 5 - Finalizar!</h3>
            <Form.Group widths="equal">
              <div className="field">
                <label>Escolha as contas que podem disparar a campanha:</label>
                <DropdownMedia
                  onSelectMedia={handleMediaChange}
                  accounts={campaign.accounts}
                  spamOnly={true}
                  allowAdditions={false}
                />
              </div>
              <div className="field">
                <label>Escolha o responsável pela campanha:</label>
                <DropdownUsers
                  onSelectUser={handleUserChange}
                  user_id={campaign.user_id}
                  allowAdditions={false}
                />
              </div>
            </Form.Group>
          </div>
        </div>
      ) : (
        ""
      )}
    </Form>
  );
};

export default FormNewCampaign;
