import React, { Component } from "react";
import TimeRangeSlider from "react-time-range-slider";

class TimeRange extends Component {
	constructor(props) {
		super(props);
		this.featureRef = React.createRef();
		this.changeStartHandler = this.changeStartHandler.bind(this);
		this.timeChangeHandler = this.timeChangeHandler.bind(this);
		this.changeCompleteHandler = this.changeCompleteHandler.bind(this);
		this.state = {
			value: {
				start: "08:00",
				end: "18:00"
			}
		};
	}

	changeStartHandler(time) {
	}

	timeChangeHandler(time) {
		this.setState({
			value: time
		});
	}

	changeCompleteHandler(time) {
	}
	render() {
		return (
			<div>
				<div className="time-range">
					<div className="start">
						<b>Início:</b> {this.state.value.start}
					</div>
					<div className="end">
						{" "}
						<b>Fim:</b> {this.state.value.end}
					</div>
				</div>
				<TimeRangeSlider
					disabled={false}
					format={24}
					maxValue={"23:59"}
					minValue={"00:00"}
					name={"time_range"}
					onChangeStart={this.changeStartHandler}
					onChangeComplete={this.changeCompleteHandler}
					onChange={this.timeChangeHandler}
					step={15}
					value={this.state.value}
				/>
			</div>
		);
	}
}
export default TimeRange;
