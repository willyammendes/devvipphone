import React, { Component } from "react";
import ImageUploader from "react-images-upload";

class ImageUpload extends Component {
    constructor(props) {
        super(props);
        this.state = { pictures: [] };
        this.onDrop = this.onDrop.bind(this);
    }

    onDrop(picture) {
        this.setState({
            pictures: this.state.pictures.concat(picture)
        });
        
    }
    render() {
        const { onDrop } = this.props;
        return (
            <ImageUploader
                withIcon
                singleImage
                withPreview
                buttonText="Escolha a imagem"
                fileSizeError="Arquivo muito grande"
                fileTypeError="Extensão de arquivo não é aceita"
                label="Tamanho máximo de arquivo: 5mb,formatos: .jpg, .gif, .png, .gif"
                onChange={onDrop}
                imgExtension={[".jpg", ".png", "jpeg", "csv"]}
                maxFileSize={5242880}
            />
        );
    }
}
export default ImageUpload;
