import React, { Component, useCallback } from "react";
import Tour from "reactour";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Creators as TourActions } from "../../store/ducks/tour";
import api from "../../api/campaign";
import apigroup from "../../api/group";
import DataTable from "../table/DataTable";
import CampaignModal from "./modal/CampaignModal";
import NewCampaignModal from "./modal/NewCampaignModal";
import { Label, Progress, Icon } from "semantic-ui-react";
import AlertSuccess from "../alerts/AlertSuccess";
import $ from "jquery";
import { useDropzone } from "react-dropzone";
import moment from "moment";
import CampaingMessagesModal from "./modal/CampaingMessagesModal";

class Campaign extends Component {
  state = {
    campaign: {
      mime_type: "",
      file_name: "",
      ext: "",
      data: "",
      type: "",
      m: moment(),
    },
    showMessage: true,
    contactColumns: [],
    selectedColumns: {
      name_column: "",
      phone_number_column: "",
      email_column: "",
      neighborhood_column: "",
      city_column: "",
      address_column: "",
      groups_import: [],
    },
    file: "",
    list: "",
    openColumnModal: false,
    colunmModal: false,
    image: "",
    listData: "",
    value: { min: 8, max: 18 },
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    groups: [],
    loadingGroups: false,
    rowSelected: false,
    selectedRowId: "",
  };

  select = (selectedDataId) => {
    const { records } = this.state;
    const dataIndex = records.findIndex((c) => c.id === selectedDataId);

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const today = new Date();

    const todayDate =
      today.getDate().length === 1 ? `0${today.getDate()}` : today.getDate();

    const date = `${today.getFullYear()}-${today.getMonth() + 1}-${todayDate}`;

    const timeZero =
      today.getHours() > 9 ? today.getHours() : `0${today.getHours()}`;

    const minutesZero =
      today.getMinutes() > 9 ? today.getMinutes() : `0${today.getMinutes()}`;

    const time = `${timeZero}:${minutesZero}`;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              status: 0,
              start: date,
              time,
              paused: 0,
              progress: 0,
              range: 0,
              all_contacts: false,
              moment: "llll",
              groups: [],
              contacts: [],
            }),
          {}
        )
      : {
          status: 0,
          paused: 0,
          progress: 0,
          range: 0,
          start: date,
          time,
          all_contacts: false,
          url: "",
          headers: "",
          headers_value: "",
          moment: "llll",
        };

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  onDrop = (picture, file) => {
    this.setState({
      image: picture,
      file_image: file,
    });
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };
  onSelectGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  componentWillMount() {
    this.fetchRecords();
  }

  documentOnloadHandler = (e, file, ext) => {
    this.setState({
      file_name: file.name,
      ext,
      data: e.target.result,
      message_type: 3,
      type: "document",
    });
  };

  clearTmpFile = () => {
    const obj = Object.assign({}, this.state);
    obj.file_name = "";
    obj.ext = "";
    obj.data = "";
    obj.message_type = 0;
    obj.type = "chat";
    obj.message = "";

    $("#document_name").val("");

    document.getElementById("document").value = "";

    this.setState(obj);
  };
  sendDocument = (e) => {
    e.preventDefault();
    const { campaign } = this.state;

    return api.campaign
      .submit(campaign)
      .then((data) => {
        this.setState((state) => ({
          loading: false,
          campaign: {
            mime_type: "data:image/jpeg;base64",
            file_name: this.state.file_name,
            ext: this.state.ext,
            data: this.state.data,
            type: this.state.type,
          },
        }));
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
        });
      });
  };
  handleUserChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          user_id: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    const filter = {
      order: "created_at",
      order_direction: "desc",
      ...params,
    };
    return await api.campaign.fetchAll(filter).then((res) => {
      this.setState({
        records: res.data,
        rowSelected: false,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
        selectedRowId: "",
      });
    });
  };

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleGroupChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleDocumentChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          file_name: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleContactChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          contacts: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleMediaChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          accounts: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  handleChangeSlide = (value) => {
    this.setState({ value });
  };

  onSliderChange = (evt) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          min_time: evt.from,
          max_time: evt.to,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const {
      selectedDataIndex: dataIndex,
      records,
      value,
      image,
      file,
    } = this.state;
    this.setState({
      loading: true,
    });
    const minFix = value.min < 10 ? `0${value.min}` : value.min;
    const maxFix = value.max < 10 ? `0${value.max}` : value.max;

    const campaign = records[dataIndex];

    const CampaignSend = {
      ...campaign,
      ...this.state.selectedColumns,
      start_date: campaign.start_date,
      min_time: `${minFix}:00`,
      max_time: `${maxFix}:00`,
      file_name: file ? file[0].file_name : "",
      type: file ? file[0].type : "",
      data: image || "",
    };
    return api.campaign
      .submit(CampaignSend)
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          file: "",
          image: "",
          list: "",
          selectedColumns: {
            name_column: "",
            phone_number_column: "",
            email_column: "",
            neighborhood_column: "",
            city_column: "",
            address_column: "",
            groups_import: [],
          },
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          file: "",
          image: "",
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records, image, file } = this.state;
    this.setState({ loading: true });
    const campaign = records[selectedDataIndex];

    const CampaignSend = {
      ...campaign,
      ...this.state.selectedColumns,
      start_date: campaign.start_date,
      file_name: file ? file[0].file_name : "",
      type: file ? file[0].type : "",
      data: image || "",
      contacts: campaign.contacts.map((c) =>
        typeof c === "object" ? c.id : c
      ),
      groups: campaign.groups.map((c) => (typeof c === "object" ? c.id : c)),
    };

    api.campaign
      .update(campaign.id, CampaignSend)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          file: "",
          image: "",
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          list: "",
          loading: false,
          selectedColumns: {
            name_column: "",
            phone_number_column: "",
            email_column: "",
            neighborhood_column: "",
            city_column: "",
            address_column: "",
            groups_import: [],
          },
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          file: "",
          image: "",
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  delete = (id) => api.campaign.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  BasicUpload = (props) => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16,
    };

    const thumb = {
      display: "inline-flex",
      borderRadius: 2,
      border: "1px solid #eaeaea",
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: "border-box",
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden",
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%",
    };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback((acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );

      this.setState({
        file: acceptedFiles.map((file) => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document",
        })),
      });

      acceptedFiles.forEach((file) => {
        const reader = new FileReader();

        reader.onload = () => {
          const binaryStr = reader.result;

          this.setState({
            image: binaryStr,
          });
        };
        reader.readAsDataURL(file);
      });
    }, []);
    const { getRootProps, getInputProps } = useDropzone({ onDrop });

    const thumbs = files.map((file) => (
      <div className="preview_holder">
        {file.type.includes("image") ? (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} alt="file preview" />
            </div>
          </div>
        ) : (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        )}
      </div>
    ));

    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach((file) => URL.revokeObjectURL(file.preview));
      },
      [files]
    );

    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>
            Arraste seu arquivo ou imagem, ou clique aqui para seleciona-lo.
          </p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };

  onSelectName = (e, { name, value }) => {
    this.setState({
      selectedColumns: { ...this.state.selectedColumns, [name]: value },
    });
  };

  closeImport = () => {
    this.setState({
      openColumnModal: false,
      contactColumns: [],
      list: "",
      selectedColumns: {
        name_column: "",
        phone_number_column: "",
        email_column: "",
        neighborhood_column: "",
        city_column: "",
        address_column: "",
        groups_import: [],
      },
    });
  };
  submitImport = () => {
    this.setState({
      openColumnModal: false,
    });
  };
  handleDismiss = () => {
    this.setState({
      showMessage: false,
    });
  };

  saveContact = () => {
    const dataSend = {
      data: this.state.listData,
      ext: this.state.list[0].file_name.split(".")[
        this.state.list[0].file_name.split(".").length - 1
      ],
      actual_file: this.state.list[0].tmp_file,
    };
    return apigroup.group.importContact(dataSend).then((data) => {
      const options = data.columns
        ? data.columns.map((c) => ({
            text: c,
            value: c,
            key: c,
          }))
        : [];

      this.setState(
        {
          contactColumns: options,
          selectedColumns: {
            tmp_file: data.tmp_file,
            excel_import_src: data.tmp_file,
            excel_import_name: this.state.list[0].file_name,
            ...this.state.selectedColumns,
          },
        },
        () => {
          setTimeout(
            function() {
              this.setState({
                openColumnModal: true,
                loading: false,
              });
            }.bind(this),
            500
          );
        }
      );
    });
  };

  UploadList = (props) => {
    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      marginTop: 16,
    };

    // const thumb = {
    //   display: "inline-flex",
    //   borderRadius: 2,
    //   border: "1px solid #eaeaea",
    //   marginBottom: 8,
    //   marginRight: 8,
    //   width: 100,
    //   height: 100,
    //   padding: 4,
    //   boxSizing: "border-box"
    // };

    // const thumbInner = {
    //   display: "flex",
    //   minWidth: 0,
    //   overflow: "hidden"
    // };

    // const img = {
    //   display: "block",
    //   width: "auto",
    //   height: "100%"
    // };

    const [files, setFiles] = React.useState([]);
    const onDrop = useCallback((acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );

      this.setState({
        list: acceptedFiles.map((file) => ({
          file_name: file.path,
          type: file.type.includes("image") ? "image" : "document",
          tmp_file: URL.createObjectURL(file),
        })),
        loading: true,
      });

      acceptedFiles.forEach((file) => {
        const reader = new FileReader();

        /* reader.onabort = () => 
        reader.onerror = () =>  */
        reader.onload = () => {
          // Do whatever you want with the file contents
          const binaryStr = reader.result;

          this.setState(
            {
              listData: binaryStr,
            },
            () => {
              setTimeout(
                function() {
                  if (this.state.listData) {
                    this.saveContact();
                  }
                }.bind(this),
                1000
              );
            }
          );
        };
        reader.readAsDataURL(file);
      });
    }, []);

    const { getRootProps, getInputProps } = useDropzone({
      accept: ".xls, .xlsx",
      onDrop,
    });

    const thumbs = files.map((file) => (
      <div className="preview_holder">
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      </div>
    ));
    React.useEffect(
      () => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach((file) => URL.revokeObjectURL(file.preview));
      },
      [files]
    );

    return (
      <section className="container upload_box">
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <p>Clique aqui para importar sua lista de contatos.</p>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
      </section>
    );
  };

  selectRow = (selectedDataId) => {
    this.setState({
      campaign: this.state.records.find((c) => c.id === selectedDataId),
      campaingMessagesModalOpen: true,
    });
  };

  copy = (selectedDataId) => {
    const { records } = this.state;
    const dataIndex = records.findIndex((c) => c.id === selectedDataId);

    const copyData = { ...records[dataIndex] };

    copyData.id = null;
    copyData.copy = true;

    this.setState(
      {
        records: [...records].concat(copyData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  toggleCampaingMessagesModal = () => {
    this.setState({
      campaingMessagesModalOpen: !this.state.campaingMessagesModalOpen,
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      groups,
      loadingGroups,
      value,
      openColumnModal,
      rowSelected,
      campaingMessagesModalOpen,
    } = this.state;

    const campaign = records[selectedDataIndex]
      ? records[selectedDataIndex]
      : this.state.campaign;

    return (
      <div className="pageHolder">
        <Tour
          steps={this.props.tour.campaignSteps}
          isOpen={this.props.tour.isOpenCampaign}
          onRequestClose={this.props.closeTourCampaign}
        />
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Campanhas</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                hide_actions={!!rowSelected}
                rowSelected={rowSelected}
                onAddClick={this.newDataClick}
                columns={columns}
                tableCampaing
                copyCampaign={this.copy}
                onRowClick={(d) => this.selectRow(d.id)}
                campaignActions
                data={records.map((r) => {
                  return {
                    ...r,
                    message: (
                      <span className="campaign_message">{r.message}</span>
                    ),
                    start_date: (
                      <Label size="tiny">
                        <span className="textCenter">
                          <Icon name="calendar alternate" />
                          {r.start_date}
                        </span>
                      </Label>
                    ),
                    read_messages: (
                      <span className="textCenter">
                        <Label color="green" size="tiny">
                          {r.read_messages}
                        </Label>
                      </span>
                    ),
                    delivered_messages: (
                      <span className="textCenter">
                        <Label color="yellow" size="tiny">
                          {r.delivered_messages}
                        </Label>
                        /
                        <Label color="yellow" size="tiny">
                          {r.total_messages}
                        </Label>
                      </span>
                    ),
                    read_messages: (
                      <span className="textCenter">
                        <Label color="green" size="tiny">
                          {r.read_messages}
                        </Label>
                        /
                        <Label color="green" size="tiny">
                          {r.total_messages}
                        </Label>
                      </span>
                    ),
                    total_messages: (
                      <span className="enviadas_total">
                        <div className="align_center">
                          <Label color="blue" size="tiny">
                            {r.sent_messages}
                          </Label>
                          /
                          <Label color="blue" size="tiny">
                            {r.total_messages}
                          </Label>
                        </div>
                      </span>
                    ),
                    user: r.user ? (
                      <Label as="a" title={r.user} image>
                        <img src={`${r.avatar}`} alt="" />
                        <span>{r.user.split(" ")[0]}</span>
                      </Label>
                    ) : null,
                    progress: (
                      <div className="progresso_tabela">
                        <Progress
                          percent={r.progress}
                          title={`Progresso ${r.progress}%`}
                          indicating
                          size="tiny"
                        />
                      </div>
                    ),
                    status: r.paused ? (
                      <Label className="label_status" color="orange">
                        Campanha em Pausa
                      </Label>
                    ) : r.status === 0 ? (
                      <Label className="label_status" color="orange">
                        Aguardando Inicio
                      </Label>
                    ) : r.status === -1 ? (
                      <Label className="label_status" color="red">
                        Campanha Interrompida
                      </Label>
                    ) : r.status === 1 ? (
                      <Label className="label_status" color="orange">
                        Campanha Iniciada
                      </Label>
                    ) : r.paused === 1 ? (
                      <Label className="label_status" color="orange">
                        Campanha em Pausa
                      </Label>
                    ) : r.status === 2 ? (
                      <Label className="label_status" color="green">
                        Campanha Finalizada
                      </Label>
                    ) : (
                      ""
                    ),
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
                fetchMessages={() => {}}
              />

              {campaign ? (
                <CampaingMessagesModal
                  campaingMessagesModalOpen={campaingMessagesModalOpen}
                  toggleCampaingMessagesModal={this.toggleCampaingMessagesModal}
                  campaing_id={campaign.id}
                />
              ) : null}

              {selectedDataIndex !== -1 ? (
                campaign.id ? (
                  <CampaignModal
                    handleClose={this.handleCloseEditModal}
                    closeModal={this.closeImport}
                    columns={this.state.contactColumns}
                    submit={this.submitImport}
                    openColumnModal={openColumnModal}
                    UploadList={this.UploadList}
                    selectedColumns={this.state.selectedColumns}
                    showMessage={this.state.showMessage}
                    handleDismiss={this.handleDismiss}
                    onSelectName={this.onSelectName}
                    onChange={this.handleChange}
                    handleNext={this.nextRecord}
                    handlePrevious={this.previousRecord}
                    onClickSave={this.update}
                    onClickAdd={this.submit}
                    documentOnloadHandler={this.documentOnloadHandler}
                    clearTmpFile={this.clearTmpFile}
                    campaign={campaign}
                    handleGroupChange={this.handleGroupChange}
                    handleContactChange={this.handleContactChange}
                    handleMediaChange={this.handleMediaChange}
                    modalHeader={`Edição do ${campaign.title}`}
                    open={editModalOpen}
                    handleChangeSlide={this.handleChangeSlide}
                    value={value}
                    onDrop={this.onDrop}
                    handleUserChange={this.handleUserChange}
                    BasicUpload={this.BasicUpload}
                    generalError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.message
                        : ""
                    }
                    messageError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.errors
                        : ""
                    }
                    cleanErrors={this.cleanErrors}
                    previousButtonEnabled={selectedDataIndex === 0}
                    nextButtonEnabled={selectedDataIndex === records.length - 1}
                    loading={loading}
                    groups={groups}
                    loadingGroups={loadingGroups}
                    onSelectGroup={this.onSelectGroup}
                    handleGroupAddition={this.addGroup}
                  />
                ) : (
                  <NewCampaignModal
                    handleClose={this.handleCloseEditModal}
                    closeModal={this.closeImport}
                    columns={this.state.contactColumns}
                    submit={this.submitImport}
                    openColumnModal={openColumnModal}
                    UploadList={this.UploadList}
                    selectedColumns={this.state.selectedColumns}
                    showMessage={this.state.showMessage}
                    handleDismiss={this.handleDismiss}
                    onSelectName={this.onSelectName}
                    onChange={this.handleChange}
                    handleNext={this.nextRecord}
                    handlePrevious={this.previousRecord}
                    onClickSave={this.update}
                    onClickAdd={this.submit}
                    documentOnloadHandler={this.documentOnloadHandler}
                    clearTmpFile={this.clearTmpFile}
                    campaign={campaign}
                    handleGroupChange={this.handleGroupChange}
                    handleContactChange={this.handleContactChange}
                    handleMediaChange={this.handleMediaChange}
                    modalHeader={
                      campaign.copy ? "Cópia de Campanha" : "Nova Campanha"
                    }
                    open={editModalOpen}
                    handleChangeSlide={this.handleChangeSlide}
                    value={value}
                    onDrop={this.onDrop}
                    handleUserChange={this.handleUserChange}
                    BasicUpload={this.BasicUpload}
                    generalError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.message
                        : ""
                    }
                    messageError={
                      this.state.errors.status > 0
                        ? this.state.errors.data.errors
                        : ""
                    }
                    cleanErrors={this.cleanErrors}
                    previousButtonEnabled={selectedDataIndex === 0}
                    nextButtonEnabled={selectedDataIndex === records.length - 1}
                    loading={loading}
                    groups={groups}
                    loadingGroups={loadingGroups}
                    onSelectGroup={this.onSelectGroup}
                    handleGroupAddition={this.addGroup}
                  />
                )
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tour: state.tour,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Campaign);
