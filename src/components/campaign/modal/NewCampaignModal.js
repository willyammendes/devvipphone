import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormNewCampaign from "../forms/FormNewCampaign";

class CampaignModal extends React.Component {
  state = {
    part: 0,
    titlePart: "",
  };

  componentDidMount() {
    const { campaign } = this.props;
    if (campaign.id) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.campaign.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };
  back = () => {
    this.setState({
      part: this.state.part - 1,
    });
  };
  advance = () => {
    const { part } = this.state;
    this.setState({
      part: part + 1,
    });
  };

  render() {
    const {
      campaign,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      handleGroupChange,
      handleContactChange,
      handleMediaChange,
      groups,
      loading,
      documentOnloadHandler,
      clearTmpFile,
      generalError,
      messageError,
      cleanErrors,
      showNavigation,
      handleChangeSlide,
      value,
      handleUserChange,
      onDrop,
      BasicUpload,
      UploadList,
      openColumnModal,
      selectedColumns,
      closeModal,
      columns,
      handleDismiss,
      showMessage,
      onSelectName,
    } = this.props;

    const { part } = this.state;
    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>

        <Modal.Content style={{ minHeight: "400px" }}>
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                <p>{generalError}</p>
              </div>
            )}
            <div className="holder_campaign">
              <div className="form-campaign">
                <FormNewCampaign
                  documentOnloadHandler={documentOnloadHandler}
                  clearTmpFile={clearTmpFile}
                  campaign={campaign}
                  handleClose={closeModal}
                  onChange={onChange}
                  onChecked={onChecked}
                  columns={columns}
                  handleDismiss={handleDismiss}
                  showMessage={showMessage}
                  groups={groups}
                  onDrop={onDrop}
                  openColumnModal={openColumnModal}
                  value={value}
                  submit={this.props.submit}
                  BasicUpload={BasicUpload}
                  UploadList={UploadList}
                  onSelectName={onSelectName}
                  selectedColumns={selectedColumns}
                  messageError={messageError}
                  handleGroupChange={handleGroupChange}
                  handleContactChange={handleContactChange}
                  handleMediaChange={handleMediaChange}
                  handleChangeSlide={handleChangeSlide}
                  part={part}
                  handleUserChange={handleUserChange}
                  loading={loading}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {showNavigation && campaign.id ? (
            <Button.Group>
              <Button
                icon
                onClick={handlePrevious}
                disabled={previousButtonEnabled}
              >
                <Icon name="left arrow" />
              </Button>
              <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
                <Icon name="right arrow" />
              </Button>
            </Button.Group>
          ) : (
            ""
          )}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              onClick={this.back}
              loading={loading}
              disabled={part === 0 ? true : loading}
              color="grey"
            >
              Voltar
            </Button>
            <Button.Or />
            {part === 4 ? (
              <Button
                positive
                onClick={this.save}
                loading={loading}
                disabled={loading}
              >
                Salvar
              </Button>
            ) : (
              <Button
                positive
                onClick={this.advance}
                loading={loading}
                disabled={loading}
                color="blue"
              >
                Avançar
              </Button>
            )}
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default CampaignModal;
