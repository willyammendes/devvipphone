import React, { Component } from "react";
import _ from "lodash";
import { Table, Label, Icon } from "semantic-ui-react";
import apimedia from "../../../api/social-media";
import "jspdf-autotable";
import api from "../../../api/campaign";
import LoaderComponent from "../../semantic/Loading";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../../store/ducks/conversation";
import { connect } from "react-redux";
import socket from "../../../services/socket";

const columns = {
  id: "Id",
  contact_name: "Contato",
  phone_number: "Telefone",
  account_number: "Conta de Saída",
  status: "Status",
  created_at: "Data",
};

class CampaingMessagesTable extends Component {
  state = {
    activePage: 1,
    loading: false,
    recordsPerPage: 10,
    selectedId: 0,
    data: [],
    orderDirection: "desc",
    accounts: [],
    order: "id",
  };

  resendMessage = (m) => {
    this.updateMessageStatus(m, true);
    this.props.resendMessage(m);
  };

  updateMessageStatus = (m, resending) => {
    const messageIndex = this.state.data.findIndex((c) => c.id === m.id);
    let data = [...this.state.data];
    let message = this.state.data[messageIndex];
    message.status = m.status;
    message.resending = resending;
    data[messageIndex] = message;

    this.setState({
      data,
    });
  };

  messageUpdated = (e) => {
    this.updateMessageStatus(e.message);
  };

  componentWillUnmount() {
    this.state.accounts.forEach((element) => {
      socket.leave(
        `message-updated-${this.props.user.company_id}-${element.phone_number}`
      );
    });
  }

  componentWillMount() {
    apimedia.media.fetchData().then((media) => {
      this.setState({ accounts: media });
      media.map((d) =>
        socket
          .private(
            `message-updated-${this.props.user.company_id}-${d.phone_number}`
          )
          .listen("MessageUpdated", this.messageUpdated)
      );
    });

    this.setState({ loading: true });
    api.campaign
      .fetchMessage(this.props.campaing_id)
      .then((res) => {
        this.setState({
          loading: false,
          data: res.data,
        });
      })
      .catch((err) => {
        this.setState({ loading: false });
      });
  }

  handleSort = (clickedColumn) => () => {
    const { order, data, orderDirection } = this.state;

    if (order !== clickedColumn) {
      this.setState({
        order: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        orderDirection: "ascending",
      });

      return;
    }

    this.setState({
      data: data.reverse(),
      orderDirection:
        orderDirection === "ascending" ? "descending" : "ascending",
    });
  };

  render() {
    const { order, orderDirection, data, loading } = this.state;
    const columNames = Object.keys(columns);

    return (
      <div className="DataTableActions">
        {loading && (
          <div className="loading-datatable">
            <LoaderComponent />
          </div>
        )}

        <Table sortable unstackable celled id={"data-table"}>
          <Table.Header>
            <Table.Row>
              {Object.keys(columns).map((c, i) => (
                <Table.HeaderCell
                  sorted={order === c ? orderDirection : "ascsending"}
                  onClick={this.handleSort(c)}
                  key={`column-${i * 1}`}
                >
                  <span>
                    {columns[c]}
                    <span
                      className={
                        c === order
                          ? orderDirection === "asc"
                            ? "ascended"
                            : "descended"
                          : ""
                      }
                    />
                  </span>
                </Table.HeaderCell>
              ))}
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {data
              .map((r) => {
                return {
                  ...r,
                  status: (
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Label size="tiny">
                        <span className="textCenter">
                          {r.status === -3 ? (
                            <Icon
                              name="whatsapp"
                              color="red"
                              title="O Contato pode não ter conta no Whatsapp. Tente Reenviar"
                            />
                          ) : r.status === -1 ? (
                            <Icon
                              name="clock"
                              color="red"
                              title="Aguardando Envio"
                            />
                          ) : r.status === -2 ? (
                            <Icon
                              name="user delete"
                              color="red"
                              title="Contato bloqueado para campanhas"
                            />
                          ) : r.status === 0 ? (
                            <Icon
                              name="clock"
                              color="green"
                              title="Aguardando entrega"
                            />
                          ) : r.status === 1 ? (
                            <Icon.Group size="small" className="status_cinza">
                              <Icon name="envelope outline" color="grey" />
                              <Icon
                                corner="bottom right"
                                className="seta"
                                name="mail forward"
                                title={`Mensagem Enviada por ${
                                  r.account_number
                                }`}
                              />
                            </Icon.Group>
                          ) : r.status === 2 ? (
                            <Icon
                              name="envelope outline"
                              size="small"
                              color="green"
                              title="Mensagem Entregue"
                            />
                          ) : r.status === 3 ? (
                            <Icon
                              name="envelope open outline"
                              size="small"
                              color="blue"
                              title="Mensagem Lida"
                            />
                          ) : r.status === 4 ? (
                            <Icon
                              name="microphone"
                              size="small"
                              className="status_cinza azul"
                              title="Áudio Reproduzido"
                            />
                          ) : (
                            <Icon name="close" color="red" />
                          )}

                          {r.status === -3
                            ? "Não Entregue"
                            : r.status === 0
                            ? "Aguardando Entrega"
                            : r.status === 1
                            ? "Enviada"
                            : r.status === 2
                            ? "Entregue"
                            : r.status === 3
                            ? "Lida"
                            : r.status === 4
                            ? "Reproduzida"
                            : r.status === -1
                            ? "Aguardando Envio"
                            : r.status === -2
                            ? "Envio Bloqueado"
                            : "Indefinido"}
                        </span>
                      </Label>
                      {r.status === -3 ? (
                        <Icon
                          name="refresh"
                          size="small"
                          color="red"
                          title="Reenviar"
                          style={{ cursor: "pointer" }}
                          onClick={() => this.resendMessage(r)}
                        />
                      ) : null}
                    </div>
                  ),
                };
              })
              .map((d, i) => (
                <Table.Row className="table-list" key={`contact-${i * 1}`}>
                  {columNames.map((c, i) => (
                    <Table.Cell key={`row-column-${d.id}-${i}`}>
                      {d[c]}
                    </Table.Cell>
                  ))}
                </Table.Row>
              ))}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CampaingMessagesTable);
