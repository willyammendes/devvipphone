import React from "react";
import { Modal, Grid } from "semantic-ui-react";
import CampaingMessagesTable from "./CampaingMessagesTable";

const CampaingMessagesModal = ({
  campaingMessagesModalOpen,
  toggleCampaingMessagesModal,
  campaing_id,
}) => {
  return (
    <Modal
      open={campaingMessagesModalOpen}
      onClose={toggleCampaingMessagesModal}
      closeOnEscape
      closeOnDocumentClick
      closeIcon
      size="large"
    >
      <Modal.Header>Mensagens</Modal.Header>
      <Modal.Content>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <CampaingMessagesTable campaing_id={campaing_id} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

export default CampaingMessagesModal;
