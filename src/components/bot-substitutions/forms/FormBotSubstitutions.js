import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormBotSubstitutions = ({ substitution, onChange }) => {
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Usuário digitou:"
          name="word"
          onChange={onChange}
          value={substitution.word}
        />
        <Form.Field
          autoComplete="off"
          control={Input}
          label="Substitua por:"
          name="substitution"
          onChange={onChange}
          value={substitution.substitution}
        />
      </Form.Group>
    </Form>
  );
};

export default FormBotSubstitutions;
