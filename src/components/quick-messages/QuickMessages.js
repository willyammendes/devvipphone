import React, { Component } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../api/fast-message";
import { Icon } from "semantic-ui-react";
import QuickMessageModal from "../quick-message/modal/QuickMessageModal";
import AlertSuccess from "../alerts/AlertSuccess";

class QuickMessages extends Component {
  state = {
    fastmessage: [],
    quickmessages: {
      title: "",
      message: "",
      fast_menu: ""
    },
    editModalOpen: false,
    save_alert: false,
    loading: false
  };

  componentWillMount() {
    api.fastmessage
      .fetchDataTake()
      .then(fastmessage => this.setState({ fastmessage }));
  }
  newDataClick = () => {
    this.setState({
      editModalOpen: true
    });
  };

  handleChange = e =>
    this.setState({
      quickmessages: {
        ...this.state.quickmessages,
        [e.target.name]: e.target.value
      }
    });

  handleChecked = (e, { name, checked }) =>
    this.setState({
      quickmessages: {
        ...this.state.quickmessages,
        [name]: checked
      }
    });

  handleCloseEditModal = () => {
    this.setState({
      editModalOpen: false,
      quickmessages: {
        title: "",
        message: "",
        fast_menu: ""
      }
    });
  };

  submit = () => {
    const { fastmessage, quickmessages } = this.state;
    this.setState({
      loading: true
    });

    const fastIndex = Object.keys(fastmessage).length;
    return api.fastmessage
      .submit(quickmessages)
      .then(data => {
        this.setState({
          fastmessage: [
            ...fastmessage.slice(0, fastIndex),
            quickmessages,
            ...fastmessage.slice(fastIndex + 1)
          ],
          quickmessages: {
            title: "",
            message: "",
            fast_menu: ""
          },
          loading: false,
          save_alert: true,
          editModalOpen: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          fastmessage: [...fastmessage].filter(c => c.id > 0)
        });
      });
  };
  render() {
    const { fastmessage, quickmessages, editModalOpen, loading } = this.state;

    return (
      <PerfectScrollbar className="scroll_horizontal">
        <div className="QuickMessages" style={{ position: "absolute" }}>
          {this.state.save_alert && <AlertSuccess />}
          <ul>
            <Icon
              style={{ marginTop: 10, marginRight: 8 }}
              name="plus"
              color="black"
              className="add_fast_message"
              onClick={this.newDataClick}
            />
            {fastmessage
              .filter(m => !!m.fast_menu)
              .map((d, i) => (
                <li key={`fastmessage-${i * 1}`}>
                  <span onClick={() => this.props.addText(d.message)}>
                    {d.title}
                  </span>
                </li>
              ))}

            <QuickMessageModal
              handleClose={this.handleCloseEditModal}
              onChange={this.handleChange}
              onClickSave={this.submit}
              onClickAdd={this.submit}
              fastmessage={quickmessages}
              onChecked={this.handleChecked}
              modalHeader={"Nova Mensagem Rápida"}
              open={editModalOpen}
              loading={loading}
            />
          </ul>
        </div>
      </PerfectScrollbar>
    );
  }
}
export default QuickMessages;
