import React from "react";

const BoxTicket = ({ children }) => <div className="BoxTicket">{children}</div>;

export default BoxTicket;
