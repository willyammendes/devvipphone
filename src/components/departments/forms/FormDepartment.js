import React from "react";
import { Form, Input, Checkbox, TextArea, Dropdown } from "semantic-ui-react";
import InlineError from "../../messages/InlineError";

const FormDepartment = ({
  department,
  onChange,
  onChecked,
  messageError,
  userOptions,
}) => {
  return (
    <Form autoComplete="off">
      <Form.Group widths="equal">
        <Form.Field error={!!messageError.name}>
          <label htmlFor="name">Nome</label>
          <Input
            autoComplete="off"
            control={Input}
            name="name"
            onChange={onChange}
            value={department.name}
            placeholder="Nome"
          />
          {messageError.name && <InlineError text={messageError.name} />}
        </Form.Field>
        <Form.Field error={!!messageError.menu}>
          <label htmlFor="menu">Código (Robô)</label>
          <Input
            autoComplete="off"
            control={Input}
            name="menu"
            onChange={onChange}
            value={department.menu}
          />
          {messageError.menu && <InlineError text={messageError.menu} />}
        </Form.Field>
      </Form.Group>

      <Form.Group>
        <Form.Field error={!!messageError.users}>
          <label htmlFor="menu">Usuários</label>
          <Dropdown
            name="users"
            multiple
            placeholder="Selecione os usuários do Departamento"
            selection
            clearable
            onChange={onChange}
            options={userOptions}
            value={department.users}
          />
          {messageError.menu && <InlineError text={messageError.menu} />}
        </Form.Field>
      </Form.Group>

      <Form.Field
        control={TextArea}
        label="Descrição"
        name="description"
        onChange={onChange}
        value={department.description}
        placeholder="Usuários do setor financeiro"
      />

      <Checkbox
        label="Departamento ativo"
        name="active"
        onChange={onChecked}
        checked={department.active}
      />

      <Form.Field
        control={Checkbox}
        label="Permitir Distribuir em Filas"
        name="is_queueable"
        onChange={onChecked}
        checked={department.is_queueable}
      />
    </Form>
  );
};

export default FormDepartment;
