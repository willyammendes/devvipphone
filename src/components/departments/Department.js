import React, { Component } from "react";
import api from "../../api/department";
import apiUser from "../../api/users";
import DataTable from "../table/DataTable";
import DepartmentModal from "./modal/DepartmentModal";
import AlertSuccess from "../alerts/AlertSuccess";

class Department extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    userOptions: [],
  };

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "" }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
        };

    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.department.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  fetchUsers = async (params) => {
    this.setState({ loading: true });

    return await apiUser.user.fetchAll({ take: 100 }).then((res) => {
      this.setState({
        userOptions: res.data.map((u) => ({
          key: u.id,
          value: u.id,
          text: u.name,
          image: { avatar: true, src: u.avatar },
        })),
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
    this.fetchUsers();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.department
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.department
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...this.state.records.slice(0, selectedDataIndex),
            { ...data.data },
            ...this.state.records.slice(selectedDataIndex + 1),
          ],
          editModalOpen: false,
          selectedDataIndex: -1,
          save_alert: true,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  delete = (id) => api.department.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      userOptions,
    } = this.state;

    const department = this.state.records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Departamentos</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
              />
              {selectedDataIndex !== -1 ? (
                <DepartmentModal
                  userOptions={userOptions}
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  handleGroupAddition={this.addGroup}
                  department={department}
                  modalHeader={
                    department.id
                      ? `Edição do ${department.name}`
                      : "Novo Departamento"
                  }
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.errors
                      : ""
                  }
                  generalError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  cleanErrors={this.cleanErrors}
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Department;
