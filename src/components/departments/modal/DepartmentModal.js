import React from "react";

import { Modal, Button, Icon } from "semantic-ui-react";
import FormDepartment from "../forms/FormDepartment";

class DepartmentModal extends React.Component {
  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", (e) => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.department.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      department,
      handleClose,
      userOptions,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      messageError,
      generalError,
      cleanErrors,
    } = this.props;

    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                <p>{generalError}</p>
              </div>
            )}
            <div className="holder_department">
              <div className="form-department">
                <FormDepartment
                  userOptions={userOptions}
                  department={department}
                  onChange={onChange}
                  onChecked={onChecked}
                  messageError={messageError}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default DepartmentModal;
