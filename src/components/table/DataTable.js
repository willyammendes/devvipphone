import React, { Component } from "react";
import {
  Table,
  Menu,
  Icon,
  Pagination,
  Dropdown,
  Checkbox,
  Button,
  Input,
  Modal,
  Header,
  Popup,
} from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import PropTypes from "prop-types";
import DropdownUsers from "../users/DropdownUsers";
import api from "../../api/social-media";
import { CSVLink } from "react-csv";
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import DownloadExcel from "./DownloadExcel";

const availableLengths = [
  { key: 5, text: 5, value: 5 },
  { key: 10, text: 10, value: 10 },
  { key: 15, text: 15, value: 15 },
  { key: 20, text: 20, value: 20 },
  { key: 40, text: 40, value: 40 },
  { key: 50, text: 50, value: 50 },
  { key: 100, text: 100, value: 100 },
  { key: 200, text: 200, value: 200 },
  { key: 300, text: 300, value: 300 },
  { key: 500, text: 500, value: 500 },
];

const defaultLength = 10;

const iconStyle = {
  position: "absolute",
  top: 0,
  bottom: 0,
  margin: "auto",
  right: "1em",
  lineHeight: 1,
  zIndex: 1,
};

const containerStyle = {
  position: "relative",
};

class DataTable extends Component {
  state = {
    activePage: 1,
    boundaryRange: 1,
    siblingRange: 1,
    loading: false,
    showEllipsis: true,
    showFirstAndLastNav: true,
    showPreviousAndNextNav: true,
    recordsPerPage: 10,
    modalDeleteOpen: false,
    modalDeleteAllOpen: false,
    selectedId: 0,
    allChecked: false,
    checkedItems: [],
    search: "",
    orderDirection: "desc",
    order: "id",
    hide_actions: true,
    groupId: "",
    errors: "",
    desconnectModal: false,
    reconnectModal: false,
  };

  handlePaginationChange = (e, { activePage }) => {
    const { recordsPerPage: take, search, order, orderDirection } = this.state;
    const skip = activePage === 1 ? 0 : take * activePage - take;
    const except = this.props.data.map((c) => c.id);

    const params = {
      skip,
      search,
      take,
      except,
      order,
      order_direction: orderDirection,
    };

    this.props.fetchData(params).then(() => {
      this.setState({ activePage, loading: false });
    });
  };

  componentDidMount() {
    this.setState({
      order: this.props.order ? this.props.order : this.state.order,
    });
  }
  createPDF = () => {
    const doc = new jsPDF({ orientation: "landscape" });
    const href = window.location.href.substring(
      window.location.href.lastIndexOf("/") + 1
    );
    const pdftable = this.props.csv.map((d, i) =>
      Object.keys(this.props.columns).map((c, i) => d[c])
    );
    doc.autoTable({
      head: [this.props.columns],
      body: pdftable,
      styles: { minCellWidth: 20, overflow: "linebreak", cellWidth: "auto" },
    });
    doc.save(`${href}.pdf`);
  };

  onRecordsPerPageChanged = (e, { value }) => {
    this.setState({
      recordsPerPage: value,
      activePage: 1,
      loading: true,
    });

    const skip = 0;
    const { search, order, orderDirection } = this.state;

    const except = this.props.data.map((c) => c.id);

    const params = {
      skip,
      search,
      take: value,
      except,
      order,
      order_direction: orderDirection,
    };

    this.props.fetchData(params).then(() => {
      this.setState({ loading: false });
    });
  };

  reloadData = () => {
    const {
      recordsPerPage,
      activePage,
      search,
      order,
      orderDirection,
    } = this.state;
    const skip =
      activePage === 1 ? 0 : recordsPerPage * activePage - recordsPerPage;
    this.setState({ loading: true });

    const except = this.props.data.map((c) => c.id);

    const params = {
      skip,
      search,
      take: recordsPerPage,
      except,
      order: this.props.oldticketBt ? "start_date" : order,
      order_direction: this.props.oldticketBt ? "desc" : orderDirection,
    };

    this.props.fetchData(params).then((res) => {
      this.setState({ loading: false });
    });
  };

  resetSearch = () => {
    this.setState({ search: "", loading: true }, this.onSearch);
  };

  setUser = (checkedItems) => {
    this.props.changeUser(checkedItems.join(",")).then((res) => {
      this.setState({ checkedItems: [] });
    });
  };
  setStatus = (checkedItems) => {
    this.props.changeStatus(checkedItems.join(",")).then((res) => {
      this.setState({ checkedItems: [] });
    });
  };
  delete = () => {
    const { contactsOpen } = this.props;

    this.setState({ loading: true });
    const { selectedId } = this.state;
    if (contactsOpen) {
      this.props
        .onDeleteContact(selectedId)
        .then((res) => {
          this.setState({
            selectedId: 0,
            allChecked: false,
            modalDeleteAllOpen: false,
            modalDeleteOpen: false,
            loading: false,
            checkedItems: [],
          });
          this.reloadData();
        })
        .catch((err) => {
          this.setState({
            loading: false,
            errors: err.response.data.errors.global.message,
            modalDeleteAllOpen: false,
            modalDeleteOpen: false,
          });
        });
      setTimeout(
        function() {
          this.setState({ errors: "" });
        }.bind(this),
        10000
      );
    }
    if (!contactsOpen) {
      this.props.onDelete(selectedId);
      this.setState({
        modalDeleteOpen: false,
        modalDeleteAllOpen: false,
      });
      setTimeout(
        function() {
          this.reloadData();
        }.bind(this),
        2000
      );
    }
  };

  cancelDisconect = (data) => {
    this.setState({
      desconnectModal: !this.state.desconnectModal,
      selectedId: data,
    });
  };

  disconnectUser = async () => {
    this.setState({ loading: true });
    await api.media.disconnect(this.state.selectedId).then((res) => {
      this.setState({ desconnectModal: false, loading: false });

      this.reloadData();
    });
  };

  cancelReconnect = (data) => {
    this.setState({
      reconnectModal: !this.state.reconnectModal,
      selectedId: data,
    });
  };

  reconnect = async () => {
    this.setState({ loading: true });
    await api.media.reconnect(this.state.selectedId).then((res) => {
      this.setState({ reconnectModal: false, loading: false });

      this.reloadData();
    });
  };

  select = (data) => {
    this.setState({
      selectedId: data.id,
    });
    this.props.onEditClick(data);
  };

  selectRow = (data) => {
    this.setState({
      selectedId: data.id,
    });
    this.props.onRowClick(data);
  };

  campaignBack = () => {
    this.props.fetchData();
  };

  selectContact = (data) => {
    this.setState({
      selectedId: data.id,
    });
    this.props.onEditContact(data);
  };

  selectGroup = (data) => {
    this.setState({
      selectedId: data.id,
      groupId: data.id,
    });

    this.props.onGroupClick(data);
  };

  backGroup = (data) => {
    this.setState({
      selectedId: data.id,
      groupId: data.id,
    });

    this.props.onGroupClick();
  };

  handleCloseDeleteModal = () => {
    this.setState({
      modalDeleteOpen: false,
      selectedId: 0,
    });
  };
  toggleWallet = (data) => {
    this.props.addWallet(data);
  };
  onOrderChange = (column) => {
    const { search, recordsPerPage, orderDirection } = this.state;
    this.setState({
      loading: true,
    });
    const except = this.props.data.map((c) => c.id);

    const params = {
      skip: 0,
      search,
      take: recordsPerPage,
      except,
      order: column,
      order_direction: orderDirection === "asc" ? "desc" : "asc",
    };

    this.props.fetchData(params).then(() => {
      this.setState({
        orderDirection: orderDirection === "asc" ? "desc" : "asc",
        order: column,
        loading: false,
      });
    });
  };

  handleOpenDeleteModal = (id) => {
    this.setState({
      modalDeleteOpen: true,
      selectedId: id,
    });
  };

  handleOpenDeleteAllModal = () => {
    const { checkedItems } = this.state;

    if (checkedItems.length > 0) {
      this.setState({
        modalDeleteAllOpen: true,
        selectedId: checkedItems.join(","),
      });
    } else {
      // TODO: show alert to select items to delete
    }
  };

  handleCloseDeleteAllModal = () => {
    this.setState({
      modalDeleteAllOpen: false,
      selectedId: -1,
    });
  };

  handleCheckBoxChanged = (checked, id) => {
    const { checkedItems } = this.state;

    if (checked) {
      if (checkedItems.indexOf(id) === -1) {
        this.setState({
          checkedItems: [...checkedItems].concat(id),
        });
      }
    } else {
      this.setState({
        checkedItems: [...checkedItems].filter((c) => c !== id),
        allChecked: false,
      });
    }
  };

  checkAll = (e, { checked }) => {
    this.setState({
      allChecked: checked,
      checkedItems: checked ? this.props.data.map((d) => d.id) : [],
    });
  };

  onSearchChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ search: e.target.value });
    this.timer = setTimeout(this.onSearch, 300);
  };

  onSearch = () => {
    const { search, recordsPerPage, order, orderDirection } = this.state;

    const except = this.props.data.map((c) => c.id);

    const params = {
      skip: 0,
      search,
      take: recordsPerPage,
      except,
      order: order || "start_date",
      order_direction: orderDirection || "desc",
    };

    if (search.length > 2 || search.length === 0) {
      this.setState({ loading: true });
      this.props.fetchData(params).then((res) => {
        this.setState({ loading: false });
      });
    }
  };
  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  render() {
    const {
      checkedItems,
      loading,
      allChecked,
      selectedId,
      activePage,
      boundaryRange,
      siblingRange,
      showEllipsis,
      showFirstAndLastNav,
      showPreviousAndNextNav,
      search,
      recordsPerPage,
      order,
      orderDirection,
      errors,
    } = this.state;

    const {
      data,
      columns,
      onAddClick,
      hide_actions,
      action_disconnect,
      action_reconnect,
      phone_icon,
      group_action,
      contactsOpen,
      tableTickets,
      messageError,
      cleanErrors,
      user_id,
      selectUserModal,
      handleUserChange,
      oldticketBt,
      toggleClosedTickets,
      openUserModal,
      changeStatus,
      campaignActions,
      csv,
      tableCampaing,
      extra_fields,
      fetchUserRecords,
    } = this.props;

    const columNames = Object.keys(this.props.columns);
    const totalPages = Math.ceil(this.props.totalRecords / recordsPerPage);
    const datacsv =
      csv &&
      csv.map((r) => {
        return {
          ...r,
        };
      });
    const dataColumn =
      datacsv &&
      datacsv.map((d, i) =>
        Object.assign({}, ...columNames.map((c, i) => ({ [c]: d[c] })))
      );

    return (
      <div className="DataTableActions">
        {errors && (
          <div className="errors-table">
            <Button
              circular
              basic
              color="black"
              icon="close"
              className="button-close"
              onClick={this.cleanErrors}
            />
            <p>{errors}</p>
          </div>
        )}
        {messageError && (
          <div className="errors-table">
            <Button
              circular
              basic
              color="black"
              icon="close"
              className="button-close"
              onClick={cleanErrors}
            />
            <p>{messageError}</p>
          </div>
        )}

        {data.map((d, i) => (
          <div className="show_phone" key={`contact-${i * 1}`}>
            {hide_actions ? (
              ""
            ) : (
              <div className="icon-list">
                {phone_icon === true ? (
                  <Popup
                    content={
                      d.connected !== "Conectado"
                        ? `${d.phone_number} ${
                            d.connected
                          }. Tente abrir o Whatsapp no Celular`
                        : d.phone_number
                    }
                    position="top left"
                    open={d.connected !== "Conectado"}
                    style={{
                      backgroundColor: "#f36262",
                    }}
                    size="mini"
                    trigger={
                      <Icon
                        className={(() => {
                          if (d.connected === "Desconectado")
                            return "celular desconectado";
                          if (d.connected === "Conectado")
                            return "celular conectado";
                          if (d.connected === "Não Autenticado")
                            return "celular notautenticado";
                        })()}
                        onClick={() => this.select(d)}
                        name="mobile alternate"
                      />
                    }
                  />
                ) : (
                  ""
                )}
              </div>
            )}
          </div>
        ))}
        {this.props.loading && (
          <div className="loading-datatable">
            <LoaderComponent />
          </div>
        )}

        {this.props.showActions ? (
          <div className="actions">
            <div className="options-list">
              <Button
                icon
                color="blue"
                onClick={() => {
                  window.history.back();
                }}
              >
                <Icon name="backward" />
              </Button>
              <Button icon color="blue" onClick={this.reloadData}>
                <Icon name="refresh" />
              </Button>
              {oldticketBt ? (
                <Button icon color="blue" onClick={toggleClosedTickets}>
                  <Icon name="file archive" />
                </Button>
              ) : (
                ""
              )}
              {oldticketBt && (
                <Button icon color="blue" onClick={fetchUserRecords}>
                  <Icon name="eye" />
                </Button>
              )}
              {this.props.wallet ? (
                <Button
                  icon
                  color="blue"
                  onClick={this.props.fetchWallet}
                  title="Minha carteira"
                >
                  <Icon name="money bill alternate" />
                </Button>
              ) : (
                ""
              )}
              {(contactsOpen || this.props.contactsImport) && (
                <Button
                  icon
                  color="blue"
                  onClick={this.props.importContacts}
                  title="Importar lista de contatos para o grupo"
                >
                  <Icon name="download" />
                </Button>
              )}
            </div>
            <Dropdown
              compact
              selection
              options={availableLengths}
              defaultValue={defaultLength}
              onChange={this.onRecordsPerPageChanged}
            />
            <div className="busca-list" style={containerStyle}>
              <Icon
                link
                name={"close"}
                style={{
                  ...iconStyle,
                  display: search !== "" ? "block" : "none",
                }}
                onClick={this.resetSearch}
              />
              <Icon
                link
                name={"search"}
                style={{
                  ...iconStyle,
                  display: search !== "" ? "none" : "block",
                }}
                onClick={this.onSearch}
              />
              <Input
                placeholder="Busca"
                fluid
                value={search}
                onChange={this.onSearchChange}
              />
            </div>
            <div className="select-all">
              <div className="checkbox-bloco">
                <Checkbox onChange={this.checkAll} checked={allChecked} />
              </div>
              <div className="dropdown-all">
                <Dropdown direction="right">
                  <Dropdown.Menu position="right">
                    <Dropdown.Item
                      onClick={this.handleOpenDeleteAllModal}
                      icon="trash"
                      text="Deletar"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
            {oldticketBt ? (
              <div className="adicionar-list">
                <Button
                  icon="user"
                  color="blue"
                  onClick={() => openUserModal(checkedItems)}
                  title="Alterar responsável"
                />
              </div>
            ) : (
              ""
            )}
            {oldticketBt ? (
              <div className="adicionar-list">
                <Button
                  icon="check"
                  color="blue"
                  onClick={() => changeStatus(checkedItems)}
                  title="Alterar Status"
                />
              </div>
            ) : (
              ""
            )}
            <div className="adicionar-list">
              <Button
                icon="add"
                color="blue"
                onClick={onAddClick}
                title="Novo Registro"
              />
            </div>
            <div className="adicionar-list">
              <Button
                color="blue"
                onClick={this.createPDF}
                icon="file pdf outline"
                title="Gerar PDF"
              />
            </div>
            <DownloadExcel columNames={columNames} dataColumn={dataColumn} />
            {datacsv ? (
              <div className="adicionar-list">
                <CSVLink data={csv}>
                  <Button color="blue">CSV</Button>
                </CSVLink>
              </div>
            ) : (
              ""
            )}
          </div>
        ) : null}

        <Table
          sortable
          unstackable
          celled
          id={this.props.phone_table ? "" : "data-table"}
        >
          <Table.Header>
            <Table.Row>
              {Object.keys(columns).map((c, i) => (
                <Table.HeaderCell
                  key={`column-${i * 1}`}
                  onClick={() => {
                    extra_fields.find((e) => e === c)
                      ? alert(
                          "Por se tratar de um Campo Adicional, não é possível fazer ordenação nesta coluna!"
                        )
                      : this.onOrderChange(c);
                  }}
                >
                  <span>
                    {columns[c]}
                    <span
                      className={
                        c === order
                          ? orderDirection === "asc"
                            ? "ascended"
                            : "descended"
                          : ""
                      }
                    />
                  </span>
                </Table.HeaderCell>
              ))}
              {!hide_actions || this.props.rowSelected ? (
                <Table.HeaderCell className="actions_header">
                  #
                </Table.HeaderCell>
              ) : null}
            </Table.Row>
          </Table.Header>

          {(tableTickets === true || tableCampaing) &&
          !this.props.rowSelected ? (
            <Table.Body className="tickets-row">
              {data.map((d, i) => (
                <Table.Row className="table-list" key={`contact-${i * 1}`}>
                  {columNames.map((c, i) => (
                    <Table.Cell
                      key={`row-column-${d.id}-${i}`}
                      onClick={() => this.selectRow(d)}
                    >
                      {d[c]}
                    </Table.Cell>
                  ))}

                  {hide_actions ? null : (
                    <Table.Cell>
                      <div className="icon-list">
                        {action_disconnect === true ? (
                          <Modal
                            closeIcon
                            open={this.state.desconnectModal}
                            onClose={this.cancelDisconect}
                            size="mini"
                            trigger={
                              <Icon
                                name="lock"
                                onClick={() => this.cancelDisconect(d.id)}
                              />
                            }
                          >
                            <Modal.Header>
                              <Icon name="unlock" /> Deseja desconectar
                            </Modal.Header>
                            <Modal.Description>
                              <p>
                                Ao desconectar perderá o acesso de envio de
                                mensagem por este número
                              </p>
                              <br />
                              <p>
                                <strong>continuar</strong>
                              </p>
                            </Modal.Description>
                            <Modal.Actions>
                              <Button
                                color="red"
                                onClick={this.cancelDisconect}
                              >
                                <Icon name="remove" /> não
                              </Button>
                              <Button
                                color="green"
                                loading={this.state.loading}
                                disabled={this.state.loading}
                                onClick={this.disconnectUser}
                              >
                                <Icon name="checkmark" /> Sim
                              </Button>
                            </Modal.Actions>
                          </Modal>
                        ) : (
                          ""
                        )}

                        {action_reconnect === true ? (
                          <Modal
                            closeIcon
                            open={this.state.reconnectModal}
                            onClose={this.cancelReconnect}
                            size="mini"
                            trigger={
                              <Icon
                                name="refresh"
                                title="Reconectar"
                                onClick={() => this.cancelReconnect(d.id)}
                              />
                            }
                          >
                            <Modal.Header>
                              <Icon name="refresh" title="Reconectar" />{" "}
                              Tentaremos reconectar a sua conta
                            </Modal.Header>
                            <Modal.Description>
                              <p>
                                Tentaremos reconectar sua conta caso ela ainda
                                esteja ativa em nosso servidor
                              </p>
                              <br />
                              <p>
                                <strong>Continuar ?</strong>
                              </p>
                            </Modal.Description>
                            <Modal.Actions>
                              <Button
                                color="red"
                                onClick={this.cancelReconnect}
                              >
                                <Icon name="remove" /> não
                              </Button>
                              <Button
                                color="green"
                                loading={this.state.loading}
                                disabled={this.state.loading}
                                onClick={this.reconnect}
                              >
                                <Icon name="checkmark" /> Sim
                              </Button>
                            </Modal.Actions>
                          </Modal>
                        ) : (
                          ""
                        )}

                        {phone_icon === true ? (
                          <Icon
                            className="celular"
                            onClick={() => this.select(d)}
                            name="mobile alternate"
                          />
                        ) : (
                          ""
                        )}
                        {contactsOpen ? (
                          <Icon
                            className="teste"
                            onClick={() => this.backGroup(d)}
                            name="bars"
                          />
                        ) : (
                          <span>
                            {group_action === true ? (
                              <Icon
                                className="group"
                                onClick={() => this.selectGroup(d)}
                                name="users"
                              />
                            ) : (
                              ""
                            )}
                          </span>
                        )}

                        {campaignActions && (
                          <Icon
                            onClick={() => this.props.onRowClick(d)}
                            name="comment"
                            style={{ float: "none" }}
                            title="Detalhes das mensagens"
                          />
                        )}

                        {campaignActions && (
                          <Icon
                            onClick={() => this.props.copyCampaign(d.id)}
                            name="copy"
                            title="Copiar Campanha"
                          />
                        )}
                        {contactsOpen ? (
                          <Icon
                            onClick={() => this.selectContact(d)}
                            name="edit"
                            color="green"
                          />
                        ) : (
                          <Icon onClick={() => this.select(d)} name="edit" />
                        )}
                        {contactsOpen ? (
                          <Icon
                            onClick={() => this.handleOpenDeleteModal(d.id)}
                            name="trash"
                            color="red"
                          />
                        ) : (
                          <Icon
                            onClick={() => this.handleOpenDeleteModal(d.id)}
                            name="trash"
                            color="red"
                          />
                        )}

                        <Checkbox
                          checked={checkedItems.indexOf(d.id) !== -1}
                          onChange={(e, data) =>
                            this.handleCheckBoxChanged(data.checked, d.id)
                          }
                        />
                      </div>
                    </Table.Cell>
                  )}
                </Table.Row>
              ))}
            </Table.Body>
          ) : (
            <Table.Body>
              {data.map((d, i) => (
                <Table.Row className="table-list" key={`contact-${i * 1}`}>
                  {columNames.map((c, i) => (
                    <Table.Cell key={`row-column-${d.id}-${i}`}>
                      {d[c]}
                    </Table.Cell>
                  ))}

                  {this.props.rowSelected && (
                    <Table.Cell>
                      <div className="icon-list">
                        <Icon
                          onClick={() => this.campaignBack(d)}
                          name="backward"
                        />
                      </div>
                    </Table.Cell>
                  )}
                  {!hide_actions && (
                    <Table.Cell>
                      <div className="icon-list">
                        {campaignActions && (
                          <span
                            className={
                              d.status.props.children === "Campanha Iniciada"
                                ? ""
                                : "hide"
                            }
                          >
                            <Icon name={d.paused === 0 ? "pause" : "play"} />
                          </span>
                        )}
                        {campaignActions && (
                          <span
                            className={
                              d.status.props.children ===
                                "Campanha Interrompida" ||
                              (d.status.props.children ===
                                "Campanha Finalizada" &&
                                d.sent_messages < d.total_messages)
                                ? ""
                                : "hide"
                            }
                          >
                            <Icon name="refresh" />
                          </span>
                        )}
                        {this.props.addToWallet && (
                          <span
                            title="Adicionar na carteira"
                            onClick={() => this.toggleWallet(d)}
                          >
                            <Icon name="money bill alternate" />
                          </span>
                        )}
                        {action_disconnect === true ? (
                          <Modal
                            closeIcon
                            open={this.state.desconnectModal}
                            onClose={this.cancelDisconect}
                            size="mini"
                            trigger={
                              <Icon
                                name="lock"
                                onClick={() => this.cancelDisconect(d.id)}
                              />
                            }
                          >
                            <Modal.Header>
                              <Icon name="unlock" /> Deseja desconectar
                            </Modal.Header>
                            <Modal.Description>
                              <p>
                                Ao desconectar perderá o acesso de envio de
                                mensagem por este número
                              </p>
                              <br />
                              <p>
                                <strong>continuar</strong>
                              </p>
                            </Modal.Description>
                            <Modal.Actions>
                              <Button
                                color="red"
                                onClick={this.cancelDisconect}
                              >
                                <Icon name="remove" /> não
                              </Button>
                              <Button
                                color="green"
                                onClick={this.disconnectUser}
                              >
                                <Icon name="checkmark" /> Sim
                              </Button>
                            </Modal.Actions>
                          </Modal>
                        ) : (
                          ""
                        )}

                        {action_reconnect === true ? (
                          <Modal
                            closeIcon
                            open={this.state.reconnectModal}
                            onClose={this.cancelReconnect}
                            size="mini"
                            trigger={
                              <Icon
                                name="refresh"
                                onClick={() => this.cancelReconnect(d.id)}
                              />
                            }
                          >
                            <Modal.Header>
                              <Icon name="refresh" /> Tentaremos reconectar a
                              sua conta
                            </Modal.Header>
                            <Modal.Description>
                              <p>
                                Tentaremos reconectar sua conta caso ela ainda
                                esteja ativa em nosso servidor
                              </p>
                              <br />
                              <p>
                                <strong>Continuar?</strong>
                              </p>
                            </Modal.Description>
                            <Modal.Actions>
                              <Button
                                color="red"
                                onClick={this.cancelReconnect}
                              >
                                <Icon name="remove" /> não
                              </Button>
                              <Button
                                color="green"
                                loading={this.state.loading}
                                disabled={this.state.loading}
                                onClick={this.reconnect}
                              >
                                <Icon name="checkmark" /> Sim
                              </Button>
                            </Modal.Actions>
                          </Modal>
                        ) : (
                          ""
                        )}
                        {phone_icon === true ? (
                          <Icon
                            className="celular"
                            onClick={() => this.select(d)}
                            name="mobile alternate"
                          />
                        ) : (
                          ""
                        )}
                        {contactsOpen ? (
                          <Icon
                            className="teste"
                            onClick={() => this.backGroup(d)}
                            name="bars"
                          />
                        ) : (
                          <span>
                            {group_action === true ? (
                              <Icon
                                className="group"
                                onClick={() => this.selectGroup(d)}
                                name="users"
                              />
                            ) : (
                              ""
                            )}
                          </span>
                        )}

                        {contactsOpen ? (
                          <Icon
                            onClick={() => this.selectContact(d)}
                            name="edit"
                          />
                        ) : (
                          <Icon onClick={() => this.select(d)} name="edit" />
                        )}
                        {contactsOpen ? (
                          <Icon
                            onClick={() => this.handleOpenDeleteModal(d.id)}
                            name="trash"
                            color="red"
                          />
                        ) : (
                          <span>
                            {d.is_system_status === 1 ||
                            d.is_system_status === true ? (
                              ""
                            ) : (
                              <Icon
                                onClick={() => this.handleOpenDeleteModal(d.id)}
                                name="trash"
                                color="red"
                              />
                            )}
                          </span>
                        )}
                        {d.is_system_status === 1 ||
                        d.is_system_status === true ? (
                          ""
                        ) : (
                          <Checkbox
                            checked={checkedItems.indexOf(d.id) !== -1}
                            onChange={(e, data) =>
                              this.handleCheckBoxChanged(data.checked, d.id)
                            }
                          />
                        )}
                      </div>
                    </Table.Cell>
                  )}
                </Table.Row>
              ))}
            </Table.Body>
          )}
          {this.props.showPagination ? (
            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan="15">
                  <Menu fluid pagination>
                    {oldticketBt && (
                      <span
                        style={{
                          textalign: "center",
                          fontSize: 12,
                          display: "flex",
                          alignItems: "center",
                          justifyItems: "start",
                        }}
                      >
                        Total de tickets:&nbsp;
                        <strong> {this.props.totalRecords}</strong>
                      </span>
                    )}
                    <Pagination
                      floated="right"
                      activePage={activePage}
                      boundaryRange={boundaryRange}
                      onPageChange={this.handlePaginationChange}
                      size="mini"
                      siblingRange={siblingRange}
                      totalPages={totalPages}
                      ellipsisItem={showEllipsis ? undefined : null}
                      firstItem={showFirstAndLastNav ? undefined : null}
                      lastItem={showFirstAndLastNav ? undefined : null}
                      prevItem={showPreviousAndNextNav ? undefined : null}
                      nextItem={showPreviousAndNextNav ? undefined : null}
                    />
                  </Menu>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          ) : null}
        </Table>

        <Modal
          open={this.state.modalDeleteOpen}
          onClose={this.handleCloseDeleteModal}
          closeIcon
          size="mini"
        >
          <Header
            icon="trash"
            content={`Exclusão do Registro ID: ${selectedId}`}
          />
          <Modal.Content>
            <p>Não será possível recuperar o registro após sua exclusão.</p>
            {selectUserModal ? (
              <div className="selectDrop">
                Selecionar Usuário para assumir Tickets:
                <div className="field">
                  <label>Responsável</label>
                  <DropdownUsers
                    onSelectUser={handleUserChange}
                    user_id={user_id.user_id}
                    allowAdditions={false}
                  />
                </div>
              </div>
            ) : (
              ""
            )}

            <p>
              <strong>Continuar ?</strong>
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button color="red" onClick={this.handleCloseDeleteModal}>
              <Icon name="remove" /> Não
            </Button>
            <Button
              color="green"
              onClick={this.delete}
              disabled={loading}
              loading={loading}
            >
              <Icon name="checkmark" /> Sim
            </Button>
          </Modal.Actions>
        </Modal>

        <Modal
          open={this.state.modalDeleteAllOpen}
          onClose={this.handleCloseDeleteAllModal}
          closeIcon
          size="mini"
        >
          <Header icon="trash" content="Exclusão de Multiplos Registros!" />
          <Modal.Content>
            <p>
              {`Você selecionou ${
                checkedItems.length
              } registros para exclusão! Esses registros não poderão ser recuperados!`}
            </p>
            <p>
              <strong>Continuar ?</strong>
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button color="red" onClick={this.handleCloseDeleteAllModal}>
              <Icon name="remove" /> Não
            </Button>
            <Button
              color="green"
              onClick={this.delete}
              disabled={loading}
              loading={loading}
            >
              <Icon name="checkmark" /> Sim
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

DataTable.defaultProps = {
  extra_fields: [],
  showActions: true,
  showPagination: true,
};

DataTable.propTypes = {
  columns: PropTypes.shape({}).isRequired,
  totalRecords: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default DataTable;
