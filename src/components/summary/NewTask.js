import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import api from "../../api/tasks";
import moment from "moment";
import "moment/locale/pt-br";
import AlertSuccess from "../alerts/AlertSuccess";
import { Button, Icon } from "semantic-ui-react";
import ButtonAdd from "./ButtonAdd";
import TasksModal from "../events/modal/TasksModal";

class NewTask extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    departments: [],
    taskcategorys: [],
    taskStatuss: [],
    contacts: [],
    loadingContacts: false,
    loadingStats: false,
    users: [],
    loadingUsers: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingDepartments: false,
    loadingCategorys: false,
    loadingClients: false,
    save_alert: false
  };

  select = selectedDataId => {
    const {
      departments,
      taskcategorys,
      clients,
      contacts,
      taskStatuss,
      users
    } = this.state;
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (departments.length === 0) this.fetchDepartments();
    if (taskcategorys.length === 0) this.fetchCategorys();
    if (contacts.length === 0) this.fetchCategorys();
    if (users.length === 0) this.fetchUsers();
    if (taskStatuss.length === 0) this.fetchStats();

    if (
      !!data.client_id &&
      clients.filter(c => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };
  newDataClick = () => {
    const { records } = this.state;
    const { id, activeConversationId, conversation } = this.props;

    const today = new Date();

    const todayDate =
      today.getDate().length == 1 ? `0${today.getDate()}` : today.getDate();

    const monthZero =
      today.getMonth() + 1 > 9
        ? today.getMonth() + 1
        : `0${today.getMonth() + 1}`;

    const date = `${today.getFullYear()}-${monthZero}-${todayDate}`;

    const timeZero =
      today.getHours() > 9 ? today.getHours() : `0${today.getHours()}`;

    const minutesZero =
      today.getMinutes() > 9 ? today.getMinutes() : `0${today.getMinutes()}`;

    const time = `${timeZero}:${minutesZero}`;
    const nextMonth = new Date(new Date(today).setMonth(today.getMonth() + 1));

    const nextMonthDay = `${nextMonth.getFullYear()}-${nextMonth.getMonth() +
      1}-${nextMonth.getDate().length === 1}`
      ? nextMonth.getDate()
      : nextMonth.getDate();

    const nextMonthDayZero =
      nextMonthDay > 9 ? nextMonthDay : `0${nextMonthDay}`;

    const nextMonthZero =
      nextMonth.getMonth() + 1 > 9
        ? nextMonth.getMonth() + 1
        : `0${nextMonth.getMonth() + 1}`;

    const nextMonthDate = `${nextMonth.getFullYear()}-${nextMonthZero}-${nextMonthDayZero}`;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) =>
            Object.assign(o, {
              [key]: "",
              active: false,
              start_at: date,
              end_date: nextMonthDate,
              created_by: id,
              send_automatically: 0,
              status: 0,
              users: id
            }),
          {}
        )
      : {
          active: false,
          start_at: date,
          end_at: nextMonthDate,
          created_by: id,
          send_automatically: 0,
          status: 0,

          users: id
        };
    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.task.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleUserChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          users: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  handleContactChange = value => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          client_id: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };
  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.task
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      save_alert
    } = this.state;

    const task = records[selectedDataIndex];

    return (
      <div>
        <Button
          icon
          className="button_plus"
          color="blue"
          size="mini"
          onClick={this.newDataClick}
        >
          <Icon name="plus" />
        </Button>

        {selectedDataIndex !== -1 ? (
          <TasksModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onClickSave={this.update}
            onClickAdd={this.submit}
            task={task}
            onChecked={this.handleChecked}
            handleUserChange={this.handleUserChange}
            handleContactChange={this.handleContactChange}
            modalHeader={task.id ? `Edição do ${task.title}` : "Nova Tarefa"}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            loading={loading}
          />
        ) : null}
      </div>
    );
  }
}

NewTask.propTypes = {
  name: PropTypes.any.isRequired,
  id: PropTypes.any.isRequired
};

const mapStateToProps = state => ({
  name: state.user.user.name,
  id: state.user.user.id
});

export default connect(mapStateToProps)(NewTask);
