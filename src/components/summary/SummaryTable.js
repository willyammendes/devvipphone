import React, { Component } from "react";
import api from "../../api/dashboard-tickets";
import DataTable from "../table/DataTable";
import SummaryTableModal from "./modal/SummaryTableModal";

class SummaryTable extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: true,
    hide_actions: true
  };

  select = selectedDataId => {
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = Object.keys(records[0]).reduce(
      (o, key) => Object.assign(o, { [key]: "" }),
      {}
    );

    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.ticketsDashboardTable.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.ticketsDashboardTable
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          loading: false,
          records: [
            ...records.slice(0, dataIndex),
            data.ticketsDashboardTable,
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.ticketsDashboardTable.update(data.id, data).then(data => {
      this.setState({
        records: [
          ...this.state.records.slice(0, selectedDataIndex),
          { ...data.data },
          ...this.state.records.slice(selectedDataIndex + 1)
        ],
        loading: false,
        save_alert: true
      });
    });
  };

  delete = id => api.ticketsDashboardTable.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      save_alert,
      hide_actions
    } = this.state;

    const ticketsDashboardTable = this.state.records[selectedDataIndex];

    const error = this.state.errors > 0 && this.state.errors.data.message;
    return (
      <div className="tabela-padrao">
        <DataTable
          loading={loading}
          onAddClick={this.newDataClick}
          columns={columns}
          data={records}
          totalRecords={total_records}
          messageError={
            this.state.errors.status > 0 ? this.state.errors.data.message : ""
          }
          hide_actions={hide_actions}
          onDelete={id => this.delete(id)}
          onEditClick={d => this.select(d.id)}
          fetchData={this.fetchRecords}
        />

        {selectedDataIndex !== -1 ? (
          <SummaryTableModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={this.update}
            onClickAdd={this.submit}
            handleGroupAddition={this.addGroup}
            ticketsDashboardTable={ticketsDashboardTable}
            modalHeader={`Edição do ${ticketsDashboardTable.title}`}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            loading={loading}
          />
        ) : null}
      </div>
    );
  }
}

export default SummaryTable;
