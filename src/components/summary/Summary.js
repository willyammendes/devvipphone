import React, { Component } from "react";
import {
  Card,
  Dropdown,
  Label,
  Icon,
  Divider,
  Button,
} from "semantic-ui-react";
import PerfectScrollbar from "react-perfect-scrollbar";
import Socket from "../../services/socket";
import { connect } from "react-redux";
import moment from "moment";
import { bindActionCreators } from "redux";
import { Creators as TaskActions } from "../../store/ducks/task";
import { Creators as UserActions } from "../../store/ducks/auth";
import "moment/locale/pt-br";
import { Bar } from "react-chartjs-2";
import apiproductivity from "../../api/user-productivity-table";
import apiuser from "../../api/users";
import Activities from "./Activities";
import jwtDecode from "jwt-decode";
import Heading from "./Heading";
import OpenTicketCard from "./OpenTicketCard";
import ClosedTicketsCard from "./ClosedTicketsCard";
import AssignedTicketsCard from "./AssignedTicketsCard";
import WaitingTicketCard from "./WaitingTicketCard";
import TasksModal from "../events/modal/TasksModal.js";

const dateFilter = [
  {
    key: "Hoje",
    text: "Hoje",
    value: "Hoje",
  },
  {
    key: "Ontem",
    text: "Ontem",
    value: "Ontem",
  },
  {
    key: "Esta semana",
    text: "Esta semana",
    value: "Esta semana",
  },
  {
    key: "Últimos 7 dias",
    text: "Últimos 7 dias",
    value: "Últimos 7 dias",
  },
  {
    key: "Semana passada",
    text: "Semana passada",
    value: "Semana passada",
  },
  {
    key: "Este mês",
    text: "Este mês",
    value: "Este mês",
  },
  {
    key: "Últimos 30 dias",
    text: "Últimos 30 dias",
    value: "Últimos 30 dias",
  },
  {
    key: "Mês passado",
    text: "Mês passado",
    value: "Mês passado",
  },
  {
    key: "Este ano",
    text: "Este ano",
    value: "Este ano",
  },
  {
    key: "Ano passado",
    text: "Ano passado",
    value: "Ano passado",
  },
];

class Summary extends Component {
  state = {
    tasks: [],
    dataBar: "",
    records: [],
    users: [],
    loading: true,
    height: "",
    isOpen: false,
  };

  listenerTask = (list) => {
    this.props.addEventTask({ list });
  };

  newDataClick = () => {
    this.props.editModal({ payload: true, selectedDataIndex: 1 });
  };

  componentWillMount() {
    this.fetchRecords();
    this.fetchUsers();
    this.fetchProductivity();

    const token = localStorage.token;
    let tokenExpired = true;

    if (token) {
      const tokenDecoded = jwtDecode(localStorage.token);
      tokenExpired = Date.now() >= tokenDecoded.exp * 1000;

      if (tokenExpired) {
        this.props.logout();
      }
    } else {
      this.fetchRecords();
      this.fetchUsers();
    }
  }

  componentDidMount() {
    Socket.connector.pusher.config.auth.headers.Authorization = `Bearer ${
      localStorage.token
    }`;

    Socket.private(`new-task-user-${this.props.user.id}`).listen(
      "NewTaskFound",
      this.listenerTask
    );

    Socket.private(`log-out-user-${this.props.user.id}`).listen(
      "LogOutUser",
      () => this.props.logout()
    );

    Socket.private(`front-end-uploaded`).listen(
      "FrontEndUploaded",
      this.reloadPage
    );

    this.setState({ height: window.screen.height * 0.25185 });
  }

  reloadPage = () => {
    window.location.reload();
  };

  fetchProductivity = async (params) => {
    this.setState({ loading: true });

    return await apiproductivity.userProductivityTable
      .fetchAll(params)
      .then((res) => {
        this.setState({
          dataBar: res.data,
          dataBarData: res,
          loading: false,
        });
      });
  };

  fetchRecords = () => {
    this.props.isEditing({
      loading: true,
    });
    this.props.fetchRecords();
    this.props.isEditing({
      loading: false,
    });
  };

  deleteTask = (id) => {
    const joinTask = {
      ...id,
      status: id.status ? 0 : 1,
      start_at: moment.unix(id.start_at).format("YYYY-MM-DD HH:mm"),
      updated_at: moment.unix(id.updated_at).format("YYYY-MM-DD HH:mm"),
      created_at: moment.unix(id.created_at).format("YYYY-MM-DD HH:mm"),
      end_at: moment.unix(id.end_at).format("YYYY-MM-DD HH:mm"),
    };
    this.props.updatedTaskEvent({
      payload: joinTask,
    });

    this.props.deleteTask(id.id);

    setTimeout(() => {
      this.props.fetchRecords();
      this.props.removeEventTask(id);
    }, 1000);
  };

  fetchUsers = async (params) => {
    this.setState({ loading: true });
    return await apiuser.user.fetchUsers(params).then((res) => {
      this.setState({
        users: res.data,
      });
    });
  };
  changeStatus = (id) => {
    this.props.isEditing({
      loading: false,
    });

    const joinTask = {
      ...id,
      status: id.status ? 0 : 1,
      start_at: moment.unix(id.start_at).format("YYYY-MM-DD HH:mm"),
      updated_at: moment.unix(id.updated_at).format("YYYY-MM-DD HH:mm"),
      created_at: moment.unix(id.created_at).format("YYYY-MM-DD HH:mm"),
      end_at: moment.unix(id.end_at).format("YYYY-MM-DD HH:mm"),
    };

    this.props.alterEventTask({
      payload: joinTask,
    });
    this.props.alterTask({
      id: joinTask.id,
      payload: joinTask,
    });
    setTimeout(() => {
      this.props.fetchRecords();
      this.props.removeEventTask(id);
    }, 1000);
  };
  render() {
    const { users, dataBar, height } = this.state;
    const { user, permissions = [] } = this.props;

    if (!user.companies || user.companies.length < 1) {
      this.props.logout();
      window.location.reload();
    }
    const dataBarFilter = [...dataBar].filter(
      (c) => c.openTickets > 0 || c.waitingTicket > 0
    );

    const userId = user.id;
    const userLastSeen = users.length > 0 && users.find((c) => c.id === userId);
    const companyName = user.companies.find((c) => c.id === user.company_id)
      .name;

    const dataLabels = dataBarFilter ? dataBarFilter.map((c) => c.user) : "";
    const dataOpen = dataBarFilter
      ? dataBarFilter.map((c) => c.openTickets)
      : "";
    const dataWaiting = dataBarFilter
      ? dataBarFilter.map((c) => c.waitingTicket)
      : "";

    const style = {
      height,
    };

    const dataUsers = {
      labels: dataLabels,
      backgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      hoverBackgroundColor: [
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
        "rgba(54, 162, 235, 0.7)",
      ],
      datasets: [
        {
          label: "Aguardando",
          data: dataWaiting,
          backgroundColor: "rgba(54, 162, 235, 0.7)",
        },
        {
          label: "Abertos",
          data: dataOpen,
          backgroundColor: "blue",
        },
      ],
    };

    return (
      <div className="page_stats CofigNav">
        <Heading companyName={companyName} userLastSeen={userLastSeen} />
        <div className="holderPage">
          <div className="holder_cards">
            <PerfectScrollbar>
              <div className="wrap_cards">
                {permissions.find((c) => c === "list-statistic") ? (
                  <OpenTicketCard />
                ) : null}

                {permissions.find((c) => c === "list-statistic") ? (
                  <AssignedTicketsCard />
                ) : null}

                {permissions.find((c) => c === "list-statistic") ? (
                  <ClosedTicketsCard />
                ) : null}

                {permissions.find((c) => c === "list-statistic") ? (
                  <WaitingTicketCard />
                ) : null}

                {permissions.find((c) => c === "list-task") && (
                  <div className="card_tarefas card_meio mb0">
                    <Card fluid>
                      <Card.Header>
                        <h3>Tarefas</h3>
                        <Button
                          onClick={this.newDataClick}
                          color="blue"
                          size="mini"
                          floated="right"
                          className="button_plus"
                          icon="plus"
                        />
                        {this.props.selectedDataIndex !== -1 ? (
                          <TasksModal />
                        ) : null}
                      </Card.Header>
                      <Card.Content className="tarefas" style={style}>
                        <PerfectScrollbar>
                          {this.props.task.records &&
                            this.props.task.records
                              .sort((a, b) => a.start_hour - b.start_hour)
                              .map((d, i) =>
                                this.props.user.id === parseInt(d.user_id) &&
                                !d.status ? (
                                  <div
                                    className="tarefa"
                                    key={`tasks-${i * 1}`}
                                  >
                                    <label className="toggleButtonTrash">
                                      <Icon
                                        name="trash alternate"
                                        size="large"
                                        color="red"
                                        onClick={() => this.deleteTask(d)}
                                      />
                                    </label>
                                    <label className="toggleButton">
                                      <input
                                        type="checkbox"
                                        checked={!d.status}
                                      />
                                      <div onClick={() => this.changeStatus(d)}>
                                        <svg viewBox="0 0 44 44">
                                          <path
                                            d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758"
                                            transform="translate(-2.000000, -2.000000)"
                                          />
                                        </svg>
                                      </div>
                                    </label>

                                    <div className="holder_tarefa">
                                      <h4> {d.title}</h4>
                                      <span>{d.description}</span>
                                      <Label
                                        as="a"
                                        color={d.status ? "green" : "orange"}
                                      >
                                        <Icon name="clock" />
                                        {moment
                                          .unix(d.start_at)
                                          .locale("pt-br")
                                          .format("L")}{" "}
                                        -{" "}
                                        {moment
                                          .unix(d.start_at)
                                          .locale("pt-br")
                                          .format("LT")}
                                      </Label>
                                    </div>
                                    <Divider />
                                  </div>
                                ) : (
                                  ""
                                )
                              )}
                        </PerfectScrollbar>
                      </Card.Content>
                    </Card>
                  </div>
                )}
                {/* card_meio */}

                {permissions.find((c) => c === "list-statistic") && (
                  <div className="card_meio mb0 chart_tickets">
                    <Card fluid>
                      <Card.Header>
                        <h3>Tickets</h3>
                        <Dropdown
                          placeholder="Hoje"
                          className="dateFilter"
                          header="Filtro de data"
                          value="Hoje"
                          selection
                          options={dateFilter}
                        />
                      </Card.Header>
                      <Card.Content className="barras">
                        <PerfectScrollbar>
                          <Bar
                            data={dataUsers}
                            options={{
                              scales: {
                                xAxes: [
                                  {
                                    stacked: true,
                                  },
                                ],
                                yAxes: [
                                  {
                                    stacked: true,
                                    ticks: {
                                      suggestedMin: 0,
                                      suggestedMax: 10,
                                      beginAtZero: true,
                                    },
                                  },
                                ],
                              },
                            }}
                          />
                        </PerfectScrollbar>
                      </Card.Content>
                    </Card>
                  </div>
                )}
              </div>
            </PerfectScrollbar>
          </div>
          {permissions.find((c) => c === "list-activity-log") && <Activities />}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...TaskActions, ...UserActions }, dispatch);

const mapStateToProps = ({ user, task }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
    selectedDataIndex: task.selectedDataIndex,
    editModal: task.editModalOpen,
    task,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Summary);
