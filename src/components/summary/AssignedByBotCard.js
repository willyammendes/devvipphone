import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Dropdown } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import { userFilter, dateFilter } from "./FilterTypes";
import { Creators as StatsAction } from "../../store/ducks/stats";

class AssignedTicketCard extends Component {
	state = {
		assignedTicketsByBot: 0,
		loading: true,
		showInfo: false,
		filter: {
			created: "today",
			by: "company"
		},
		by: "company",
		created: "today"
	};

	componentWillMount() {
		this.fetchData({ filter: this.state.filter });
	}

	fetchData = (params = {}) => {
		this.props.getBotAssignedTicketsCount(params);
	};

	filterChanged = (e, { name, value }) => {
		this.setState(
			state => {
				return {
					loading: true,
					filter: {
						...state.filter,
						[name]: value
					},
					[name]: value
				};
			},
			() => this.fetchData({ filter: this.state.filter })
		);
	};

	render() {
		const { by, created } = this.state;
		const {
			assignedTicketsByBotCount,
			loadingAssignedTicktsByBotCount
		} = this.props.stats;

		return (
			<div className="card_meio">
				<Card fluid>
					<Card.Header>
						<Dropdown
							className="userFilter"
							header="Filtro de usuários"
							value={by}
							name="by"
							onChange={this.filterChanged}
							clearable
							selection
							options={userFilter}
						/>
						<Dropdown
							className="dateFilter"
							header="Filtro de data"
							value={created}
							name="created"
							onChange={this.filterChanged}
							clearable
							selection
							options={dateFilter}
						/>
					</Card.Header>
					<Card.Content>
						<div>
							<span className="info">
								Ticket atendidos pelo robô
							</span>
							<div className="numero">
								<span>
									{loadingAssignedTicktsByBotCount ? (
										<div className="loading-datatable">
											<LoaderComponent />
										</div>
									) : (
										assignedTicketsByBotCount
									)}
								</span>{" "}
								tickets atendidos pelo robô
							</div>
						</div>
					</Card.Content>
				</Card>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch =>
	bindActionCreators(StatsAction, dispatch);

const mapStateToProps = state => ({
	stats: state.stats
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AssignedTicketCard);
