import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Dropdown } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import { userFilter, dateFilter } from "./FilterTypes";
import { Creators as StatsAction } from "../../store/ducks/stats";

class ClosedTicketsCard extends Component {
	state = {
		closedTickets: 0,
		loading: true,
		showInfo: false,
		filter: {
			created: "last_thirty_days",
			by: "me"
		},
		by: "me",
		created: "last_thirty_days"
	};

	componentWillMount() {
		this.fetchData({ filter: this.state.filter });
	}

	fetchData = (params = {}) => {
		this.props.getClosedTicketsCount(params);
	};

	filterChanged = (e, { name, value }) => {
		this.setState(
			state => {
				return {
					loading: true,
					filter: {
						...state.filter,
						[name]: value
					},
					[name]: value
				};
			},
			() => this.fetchData({ filter: this.state.filter })
		);
	};

	render() {
		const { by, created } = this.state;
		const {
			closedTicketsCount,
			loadingClosedTicketsCount
		} = this.props.stats;

		return (
			<div className="card_meio">
				<Card fluid>
					<Card.Header>
						<Dropdown
							className="userFilter"
							header="Filtro de usuários"
							value={by}
							name="by"
							onChange={this.filterChanged}
							clearable
							selection
							options={userFilter}
						/>
						<Dropdown
							className="dateFilter"
							header="Filtro de data"
							value={created}
							name="created"
							onChange={this.filterChanged}
							clearable
							selection
							options={dateFilter}
						/>
					</Card.Header>
					<Card.Content>
						<div>
							<span className="info">
								Tickets fechados com sucesso
							</span>
							<div className="numero">
								<span>
									{loadingClosedTicketsCount ? (
										<div className="loading-datatable">
											<LoaderComponent />
										</div>
									) : (
										closedTicketsCount
									)}
								</span>{" "}
								tickets fechados
							</div>
						</div>
					</Card.Content>
				</Card>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch =>
	bindActionCreators(StatsAction, dispatch);

const mapStateToProps = state => ({
	stats: state.stats
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ClosedTicketsCard);
