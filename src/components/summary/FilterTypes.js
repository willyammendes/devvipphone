
export const userFilter = [
	{ key: "me", text: "Eu", value: "me", icon: "user outline" },
	{ key: "company", text: "Empresa", value: "company", icon: "globe" },
	{ key: "bot", text: "Robô", value: "bot", icon: "microchip" }
];

export const dateFilter = [
	{ key: "today", text: "Hoje", value: "today" },
	{ key: "yesterday", text: "Ontem", value: "yesterday" },
	{ key: "week", text: "Esta semana", value: "week" },
	{ key: "last_seven_days", text: "Últimos 7 dias", value: "last_seven_days" },
	{ key: "last_week", text: "Semana passada", value: "last_week" },
	{ key: "month", text: "Este mês", value: "month" },
	{ key: "last_thirty_days", text: "Últimos 30 dias", value: "last_thirty_days" },
	{ key: "last_month", text: "Mês passado", value: "last_month" },
	{ key: "year", text: "Este ano", value: "year" },
	{ key: "last_year", text: "Ano passado", value: "last_year" }
];