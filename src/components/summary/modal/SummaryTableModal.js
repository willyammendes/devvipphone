import React from "react";
import PropTypes from "prop-types";
import { Modal, Button, Icon, Image } from "semantic-ui-react";
import FormSummaryTable from "../forms/FormSummaryTable";

class SummaryTableModal extends React.Component {
    componentDidMount() {
        document.removeEventListener("keydown", () => {});
        document.addEventListener("keydown", e => {
            if (e.keyCode === 39) this.props.handleNext();
            if (e.keyCode === 37) this.props.handlePrevious();
        });
    }

    save = () => {
        if (this.props.ticketsDashboardTable.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    render() {
        const {
            ticketsDashboardTable,
            handleClose,
            onChange,
            onChecked,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading
        } = this.props;

        return (
            <Modal
                size="large"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <div className="holder_ticketsDashboardTable">
                            <div className="form-ticketsDashboardTable">
                                <FormSummaryTable
                                    ticketsDashboardTable={
                                        ticketsDashboardTable
                                    }
                                    onChange={onChange}
                                    onChecked={onChecked}
                                />
                            </div>
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button
                            icon
                            onClick={handlePrevious}
                            disabled={previousButtonEnabled}
                        >
                            <Icon name="left arrow" />
                        </Button>
                        <Button
                            icon
                            onClick={handleNext}
                            disabled={nextButtonEnabled}
                        >
                            <Icon name="right arrow" />
                        </Button>
                    </Button.Group>{" "}
                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default SummaryTableModal;
