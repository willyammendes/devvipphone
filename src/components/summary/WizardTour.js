import React from "react";
import { NavLink } from "react-router-dom";

const WizardTour = (props) => {
    
	
	return(
        <div style={props.style}>
            <h1>{props.title}</h1>
            <p>{props.body}</p>
            {props.link && <NavLink to={props.link}>Vamos Conhecer? {props.title}</NavLink>}
        </div>
    )
	
}

export default WizardTour;
