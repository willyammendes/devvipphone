import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Label, Icon } from "semantic-ui-react";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../api/activities";
import LoaderComponent from "../semantic/Loading";

class Activities extends Component {
  state = {
    activities: [],
    loading: true,
    showInfo: false,
  };
  componentWillMount() {
    const { filter } = this.props;
    api.activities
      .fetchAll(
        filter ? { filter: JSON.stringify([["user_id", "=", filter]]) } : ""
      )
      .then((activities) =>
        this.setState({
          activities: activities.data,
          loading: false,
          showInfo: true,
        })
      );
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      const { filter } = this.props;
      api.activities
        .fetchAll(
          filter ? { filter: JSON.stringify([["user_id", "=", filter]]) } : ""
        )
        .then((activities) =>
          this.setState({
            activities: activities.data,
            loading: false,
            showInfo: true,
          })
        );
    }
  }

  render() {
    const { activities, loading } = this.state;
    const { user } = this.props;
    return (
      <div className="holder_atividades">
        <Card fluid>
          <Card.Header>
            <h3>Atividades</h3>
          </Card.Header>
          <Card.Content>
            <div className="atividades_bloco">
              <PerfectScrollbar>
                {loading && (
                  <div className="loading-datatable">
                    <LoaderComponent />
                  </div>
                )}
                {activities.map((activity) => (
                  <div className="atividade" key={`activity-${activity.id}`}>
                    <div className="icone_atividade">
                      <Icon
                        circular
                        inverted
                        color="green"
                        name={
                          activity.forwardedTo.id ? "mail forward" : "users"
                        }
                      />
                    </div>

                    <div className="card_atividade">
                      <div className="foto_pessoa">
                        <img
                          src={activity.user.avatar}
                          alt={activity.user.name}
                        />
                      </div>
                      <span className="pessoa">
                        {activity.user.name}
                        {activity.user.company_id !== user.company_id
                          ? activity.user.monitchat_support_user
                            ? " (Suporte Monitchat)"
                            : ""
                          : ""}
                      </span>
                      <Label as="a" color="blue" basic>
                        <Icon name="clock" />
                        {activity.date}
                      </Label>
                      <span>
                        {activity.forwardedTo.id ? (
                          <span>
                            Encaminhado Ticket {activity.ticket_id} para{" "}
                            <b>{activity.forwardedTo.name}</b>
                          </span>
                        ) : (
                          ""
                        )}
                      </span>
                      <span>{activity.comment}</span>
                    </div>
                  </div>
                ))}
              </PerfectScrollbar>
            </div>
          </Card.Content>
        </Card>
      </div>
    );
  }
}

const mapStatetoProps = ({ user }) => {
  return {
    user: user.user,
  };
};

export default connect(mapStatetoProps)(Activities);
