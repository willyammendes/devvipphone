import React from "react";
import PropTypes from "prop-types";
import { Form, Input, Checkbox, TextArea, Dropdown } from "semantic-ui-react";

const FormSummaryTable = ({
    ticketsDashboardTable,
    onChange,
    onChecked,
    handleGroupAddition
}) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Usuário"
                    name="user"
                    onChange={onChange}
                    value={ticketsDashboardTable.user}
                />
            </Form.Group>
        </Form>
    );
};

export default FormSummaryTable;
