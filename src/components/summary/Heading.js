import React from "react";
import moment from "moment";
import "moment/locale/pt-br";

const Heading = ({ companyName, userLastSeen }) => {
	
	
	return (
		
		<div className="headingPage">
		
			<h1>Resumo</h1>
			{userLastSeen ? 
			<span className="last_login">
				<b>Último login: </b>
				{moment
					.unix(userLastSeen.last_seen)
					.locale("pt-br")
					.format("lll")}
			</span>
			: null }
			<div className="subHeadingPage">
				<h2>{companyName}</h2>
			</div>
			
		</div>
	);
}

export default Heading;
