import React, { Component } from "react";
import api from "../../api/conversation";

import apimedia from "../../api/social-media";
import NewConversationModal from "./NewConversationModal";
import { Icon } from "semantic-ui-react";

class NewConversation extends Component {
	state = {
		newConversation: [],
		media: [],
		errors: { message: "", phone_number: "" },
		loading: false,
		conversationModalOpen: false,
		save_alert: false,
		account_number: ""
	};

	componentWillMount() {
		this.fetchRecords();
		apimedia.media.fetchAll().then(media => {
			this.setState({
				media,
				account_number:
					media.data.find(
						c => c.connected === 1 && c.can_reply === 1
					) &&
					media.data.find(c => c.connected === 1 && c.can_reply === 1)
						.phone_number
			});
		});
	}

	fetchRecords = async params => {
		this.setState({ loading: true });

		return await apimedia.media.fetchAll(params).then(res => {
			this.setState({
				media: res.data,
				loading: false
			});
		});
	};

	openModal = () => {
		this.setState({
			conversationModalOpen: true
		});
	};

	handleChange = e =>
		this.setState({
			newConversation: {
				...this.state.newConversation,
				[e.target.name]: e.target.value
			}
		});

	handleChecked = (e, { name, checked }) => {
		this.setState({
			newConversation: { ...this.state.newConversation, [name]: checked }
		});
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	handleMediaChange = (value, text) => {
		this.setState({
			newConversation: {
				...this.state.newConversation,
				account_number: value
			}
		});
	};

	handleContactChange = value => {
		this.setState({
			newConversation: { ...this.state.newConversation, contact: value }
		});
	};

	handleCloseEditModal = () => {
		this.setState({
			conversationModalOpen: false,
			newConversation: []
		});
	};
	submit = () => {
		const { errors, newConversation } = this.state;

		if (!newConversation.message) {
			errors.message = "Não pode enviar sem mensagem";
		}
		if (!newConversation.phone_number) {
			errors.phone_number = "Can't be blank";
		}

		this.setState({
			loading: true
		});

		const data = {
			message_type: 0,
			sender: 1,
			phone_number: newConversation.phone_number.replace(/\D/g, ""),
			account_number: this.state.account_number
		};

		const dataJoin = { ...newConversation, ...data };

		return api.conversations
			.sendNewConversation(dataJoin)
			.then(data => {
				this.setState({
					save_alert: true,
					loading: false,
					conversationModalOpen: false,
					newConversation: []
				});
				setTimeout(
					function() {
						this.setState({ save_alert: false });
					}.bind(this),
					5000
				);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response,
					contactModalOpen: false
				});
			});
	};
	render() {
		const {
			loading,

			newConversation,
			conversationModalOpen
		} = this.state;

		return (
			<div className="icone-add">
				<Icon.Group
					size="large"
					className="add-comment"
					title="Nova Conversa"
					onClick={this.openModal}
				>
					<Icon name="user" />
					<Icon corner name="add" />
				</Icon.Group>

				<NewConversationModal
					handleClose={this.handleCloseEditModal}
					onChange={this.handleChange}
					onChecked={this.handleChecked}
					handleMediaChange={this.handleMediaChange}
					handleContactChange={this.handleContactChange}
					modalHeader={`Nova Conversa`}
					messageError={
						this.state.errors.status > 0
							? this.state.errors.data.errors
							: ""
					}
					generalError={
						this.state.errors.status > 0
							? this.state.errors.data.message
							: ""
					}
					cleanErrors={this.cleanErrors}
					newConversation={newConversation}
					open={conversationModalOpen}
					onClickAdd={this.submit}
					loading={loading}
				/>
			</div>
		);
	}
}
export default NewConversation;
