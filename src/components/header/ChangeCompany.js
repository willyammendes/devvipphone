import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Modal, Dropdown, Button, Icon, Grid } from "semantic-ui-react";
import { Creators as AuthActions } from "../../store/ducks/auth";
import API from "../../services/api";
import Socket from "../../services/socket";

class ChangeCompany extends Component {
  state = {
    companies: [],
    loadingCompanies: true,
    company_id: -1,
    token: "",
  };

  componentWillMount() {
    API.get("/company?take=2000").then((c) => {
      this.setState({
        companies: c.data.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
      });
    });
  }

  componentDidMount() {
    this.setState({
      company_id: this.props.company_id,
    });
  }

  requestAccess = () => {
    if (this.state.company_id !== this.props.user.default_company_id) {
      Socket.leave(`access-request-created-${this.state.token}`);

      API.post(`/access-request`, { company_id: this.state.company_id }).then(
        (res) => {
          this.setState({
            token: res.data.request.authorization_token,
          });
          Socket.private(
            `access-request-authorized-${res.data.request.authorization_token}`
          ).listen("AccessRequestAuthorized", (e) => {
            this.props.logout();
          });
        }
      );
    }
  };

  handleChange = (e, { name, value }) => {
    if (value === this.props.user.default_company_id) {
      this.setState(
        {
          company_id: value,
        },
        () => {
          API.post(`/user/changeCompany/${value}`).then((c) => {
            this.props.logout();
          });
        }
      );
    } else {
      this.setState(
        {
          company_id: value,
        },
        () => {
          API.post(`/access-request`, { company_id: value }).then((res) => {
            this.setState({
              token: res.data.request.authorization_token,
            });

            Socket.private(
              `access-request-authorized-${
                res.data.request.authorization_token
              }`
            ).listen("AccessRequestAuthorized", (e) => {
              this.props.logout();
            });
          });
        }
      );
    }
  };

  render() {
    return (
      <Modal trigger={<a href="/#">Selecionar Empresa</a>} size="small">
        <Modal.Header>Selecionar Empresa</Modal.Header>
        <Modal.Content>
          <Grid>
            <Grid.Row>
              <Grid.Column width={10}>
                <Dropdown
                  fluid
                  placeholder="Selecione a Empresa"
                  search
                  value={this.state.company_id}
                  name="company_id"
                  selection
                  onChange={this.handleChange}
                  options={this.state.companies}
                />
              </Grid.Column>
              <Grid.Column width={2}>
                <Button
                  icon
                  onClick={this.requestAccess}
                  title="Solicitar Novo Codigo"
                >
                  <Icon name="refresh" />
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(AuthActions, dispatch);

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangeCompany);
