import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../../api/contact";
import apigroup from "../../api/group";
import apiclient from "../../api/client";
import DataTable from "../table/DataTable";
import ContactModal from "../contact/modal/ContactModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { Button, Icon } from "semantic-ui-react";

class NewContact extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    groups: [],
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingGroups: false,
    loadingClients: false,
    save_alert: false
  };

  select = selectedDataId => {
    const { groups, clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (groups.length === 0) this.fetchGroups();
    if (
      !!data.client_id &&
      clients.filter(c => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  newDataClick = () => {
    this.setState({
      editModalOpen: true
    });
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.contact
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = contact => {
    const { selectedDataIndex, records } = this.state;
    this.setState({ loading: true });

    api.contact
      .update(contact.id, contact)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.contact.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };
  render() {
    const { loading, editModalOpen } = this.state;
    const { data: conversations } = this.props.conversation;
    const { activeConversationId } = this.props.conversation;
    const activeConversation = conversations.find(
      c => c.id === activeConversationId
    );
    const {
      onSelectGroup,
      onSelectClient,
      onSearchClientChange,
      handleGroupAddition,
      loadingGroups,
      loadingClients,
      clients,
      groups
    } = this.props;

    return (
      <div className="icone-add">
        <Icon.Group
          size="large"
          className="add-comment"
          title="Novo Contato"
          onClick={this.newDataClick}
        >
          <Icon name="user" />
          <Icon corner name="add" />
        </Icon.Group>
        {activeConversation ? (
          <ContactModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={() => this.update(activeConversation.contact)}
            onClickAdd={this.submit}
            contact={activeConversation.contact}
            modalHeader={`Edição do contato ${activeConversation.contact.name}`}
            open={editModalOpen}
            previousButtonEnabled={false}
            nextButtonEnabled={false}
            onSelectGroup={onSelectGroup}
            onSelectClient={onSelectClient}
            onSearchClientChange={onSearchClientChange}
            handleGroupAddition={handleGroupAddition}
            loadingGroups={loadingGroups}
            loadingClients={loadingClients}
            groups={groups}
            clients={clients}
            loading={loading}
            showNavigation={false}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ conversation }) => ({ conversation });

export default connect(
  mapStateToProps,
  null
)(NewContact);
