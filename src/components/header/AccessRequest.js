import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Modal, Loader, Button, Icon } from "semantic-ui-react";
import { Creators as AuthActions } from "../../store/ducks/auth";
import API from "../../services/api";
import Socket from "../../services/socket";
import DataTable from "../table/DataTable";

class AccessRequest extends Component {
  state = {
    requests: [],
    loading: true,
    loadingAuthorization: false,
    unlock: false,
  };

  componentDidMount() {
    Socket.leave(`access-request-created-${this.props.user.company_id}`);
    Socket.private(
      `access-request-created-${this.props.user.company_id}`
    ).listen("AccessRequestCreated", this.requestReceived);
  }

  requestReceived = (e) => {
    this.setState({
      requests: [e.request],
      loading: false,
      unlock: false,
      loadingAuthorization: false,
    });
  };

  authorizeRequest = () => {
    const request = this.state.requests[0];
    this.setState({ loadingAuthorization: true });
    API.put(`/access-request/${request.id}`, {}).then((res) => {
      this.setState({
        loadingAuthorization: false,
        unlock: !this.state.unlock,
        requests: [res.data.data],
      });
    });
  };

  render() {
    return (
      <Modal trigger={<a href="/#">Autorização de Acesso</a>}>
        <Modal.Header>Autorizar Acesso ao Suporte Monitchat</Modal.Header>
        <Modal.Content>
          <div style={{ minHeight: "50px" }}>
            {this.state.loading ? (
              <Loader />
            ) : (
              <DataTable
                loading={this.state.loading}
                hide_actions={true}
                columns={{
                  id: "ID",
                  requested_by: "Solicitado Por",
                  authorized_by: "Autorizado por",
                  authorization_token: "Token de Acesso",
                  created_at: "Solicitado Em",
                  authorized_on: "Autorizado Em",
                  revoked_on: "Acesso Removido Em",
                  action: "#",
                }}
                data={this.state.requests.map((r) => ({
                  ...r,
                  action: (
                    <Button
                      color={this.state.unlock ? "red" : "green"}
                      icon
                      title={
                        this.state.unlock
                          ? "Revogar Acesso"
                          : "Autorizar Acesso"
                      }
                      loading={this.state.loadingAuthorization}
                      onClick={this.authorizeRequest}
                    >
                      <Icon name={this.state.unlocked ? "unlock" : "lock"} />
                    </Button>
                  ),
                }))}
                totalRecords={this.state.requests.length}
                showActions={false}
                showPagination={false}
              />
            )}
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(AuthActions, dispatch);

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccessRequest);
