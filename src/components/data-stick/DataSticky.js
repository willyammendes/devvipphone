import React from "react";
import { Popup, Icon } from "semantic-ui-react";

const DataSticky = ({ ticket, contact }) => {
  return (
    <div className="sticky-topo" id={`ticket-${ticket.id}`}>
      <div className="origem_ticket">
        <div className="origem">
          <Icon
            name={
              ticket.source === "ticket"
                ? "ticket alternate"
                : ticket.source === "monitchat"
                ? "ticket alternate"
                : ticket.source === "phone"
                ? "phone"
                : ticket.source === "whatsapp"
                ? "whatsapp"
                : ticket.source === "email"
                ? "mail"
                : ticket.source === "campaing"
                ? "bullhorn"
                : "comments"
            }
            circular
            inverted
            color="grey"
          />
        </div>
      </div>
      <span>
        Número do Ticket: <b>{ticket.ticket_number}</b>
      </span>
      <br />
      <span>
        Criado em:
        <time> {ticket.created_at}</time>
      </span>
      <span>
        Contato: <b>{contact.name}</b>
      </span>
      <div className="atendimento">
        {ticket.user ? (
          <span>
            Encaminhado para o antendente:
            <Popup
              content={ticket.user}
              position="top left"
              size="mini"
              trigger={
                <img
                  src={ticket.user_avatar || ticket.avatar}
                  alt="avatar do usuário"
                />
              }
            />
          </span>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
export default DataSticky;
