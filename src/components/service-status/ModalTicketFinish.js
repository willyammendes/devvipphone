import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import { Modal, Form, Input, TextArea, Button } from "semantic-ui-react";
import api from "../../api/ticket";
import DropdownDepartments from "../departments/DropdownDepartments";
import DropdownCategories from "../ticket-category/DropdownCategories";
import "moment/locale/pt-br";

class ModalTicketFinish extends Component {
  state = {
    ticket: {},
    loading: false,
    error: false,
  };

  handleChange = (e, { name, value }) => {
    this.setState({
      ticket: { ...this.state.ticket, [name]: value },
    });
  };

  handleDepartmentChange = (value) => {
    this.setState({
      ticket: { ...this.state.ticket, department_id: value },
    });
  };

  handleCategoryChange = (value) => {
    this.setState({
      ticket: { ...this.state.ticket, ticket_category_id: value },
    });
  };
  componentWillMount() {
    this.setState({ loading: true });
    if (this.props.activeTicket) {
      api.ticket.fetchTicket(this.props.activeTicket).then((ticket) => {
        this.setState(
          {
            ticket: ticket.data,
            loading: false,
          },
          () => {}
        );
      });
    }
  }

  update = () => {
    const { ticket } = this.state;
    const { ticketFinish } = this.props;
    setTimeout(() => {
      const comment = {
        message: ticket.comment,
        comment: ticket.comment,
        ticket_number: ticket.ticket_number,
        user_id: this.props.user.id,
        ticket_id: ticket.id,
        path: 1,
        source: "comment",
        comment_type: 0,
        ticket_category_id: ticket.ticket_category_id,
        progress: 100,
        current_progress: 100,
      };
      const commentStatus = {
        status_id: ticketFinish.current_status,
        user_id: this.props.user.id,
        ticket_id: ticket.id,
        path: 1,
        source: "comment",
        comment_type: 0,
      };
      const ticketFinished = {
        ...ticket,
        progress: 100,
        current_progress: 100,
        current_status: ticketFinish.current_status,
      };
      this.setState({ loading: true });
      if (
        ticket.title &&
        ticket.title.length > 0 &&
        (ticket.comment && ticket.comment.length > 9) &&
        ticket.department_id > 0 &&
        ticket.ticket_category_id > 0
      ) {
        api.ticket
          .update(ticket.id, ticketFinished)
          .then((data) => {
            this.setState({
              loading: false,
            });
            this.props.sendComment(comment, ticket.id, this.props.user.id);
            this.props.sendComment(
              commentStatus,
              ticket.id,
              this.props.user.id
            );
            this.props.unselectConversation();
            this.props.successModal();
            const archive = {
              id: this.props.activeConversationId,
              archived: ticketFinished.current_progress > 99,
            };
            this.props.archiveConversation({ conversation: archive });
            setTimeout(
              function() {
                this.setState({ save_alert: false });
              }.bind(this),
              5000
            );
          })
          .catch((err) => {
            this.setState({
              loading: false,
            });
          });
      } else {
        this.setState({
          error: true,
          loading: false,
        });
      }
    }, 500);
  };

  render() {
    const { open, handleClose } = this.props;
    const { ticket, loading, error } = this.state;

    return (
      <Modal
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          Preencher informações para finalizar atendimento
        </Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {error && (
              <div className="errors-table">
                <p>Ainda existem informações para serem preenchidas</p>
              </div>
            )}
            <Form>
              <div className="form-row">
                <div className="field-row">
                  <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Título"
                    name="title"
                    onChange={this.handleChange}
                    value={ticket.title}
                    className={
                      ticket.title && ticket.title.length > 0
                        ? "input-success"
                        : "input-error"
                    }
                  />
                  <div
                    className={
                      ticket.department_id && ticket.department_id > 0
                        ? "field input-success"
                        : "field input-error"
                    }
                  >
                    <label>Departamento</label>
                    <DropdownDepartments
                      onSelectDepartment={this.handleDepartmentChange}
                      department_id={ticket.department_id}
                    />
                  </div>
                  <div
                    className={
                      ticket.ticket_category_id && ticket.ticket_category_id > 0
                        ? "field input-success"
                        : "field input-error"
                    }
                  >
                    <label>Categoria</label>
                    <DropdownCategories
                      handleCategoryChange={this.handleCategoryChange}
                      onSelectCategory={this.handleCategoryChange}
                      ticket_category_id={ticket.ticket_category_id}
                      allowAdditions
                    />
                  </div>
                </div>

                <div className="field-row">
                  <label>Comentário</label>
                  <TextArea
                    placeholder="Interagir no chamado"
                    name="comment"
                    onChange={this.handleChange}
                    value={ticket.comment}
                    className={
                      ticket.comment && ticket.comment.length > 9
                        ? "input-success"
                        : "input-error"
                    }
                  />
                  {ticket.comment && ticket.comment.length > 9 ? (
                    ""
                  ) : (
                    <span>
                      Faltam {ticket.comment ? 10 - ticket.comment.length : 10}{" "}
                      carácteres para que você possa comentar.
                    </span>
                  )}
                </div>
              </div>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button
            positive
            onClick={this.update}
            loading={loading}
            disabled={
              loading ||
              (ticket.comment &&
                (isNaN(ticket.comment.length - 10) ||
                  ticket.comment.length < 10)) ||
              !(ticket.ticket_category_id && ticket.ticket_category_id > 0) ||
              !(ticket.department_id && ticket.department_id > 0) ||
              !(ticket.title && ticket.title.length > 0)
            }
          >
            Salvar
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => ({
  conversation: state.conversation,
  user: state.user.user,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalTicketFinish);
