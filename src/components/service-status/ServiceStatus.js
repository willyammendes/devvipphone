import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import Icon from "@mdi/react";
import { mdiChevronRight } from "@mdi/js";
import api from "../../api/ticket-status";
import apiticket from "../../api/ticket";
import { Button, Popup } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";

class ServiceStatus extends Component {
  state = {
    ticketStatus: [],
    ticket: {},
    message: "",
    open: false,
    loading: false,
    finished: false,
    openModal: false,
  };

  createClass = () => {
    this.setState({
      open: true,
    });
  };
  closeClass = () => {
    this.setState({
      open: false,
    });
  };

  fetchTicketStatus = async () => {
    await api.ticketStatus.fetchStatusMain().then((ticketStatus) => {
      this.setState({
        ticketStatus,
      });
    });
  };

  componentWillMount() {
    this.setState({ loading: true });
    this.fetchTicketStatus();
  }

  fetchTicket = async () => {
    const { conversation, activeConversationId } = this.props;
    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );
    this.setState({
      loading: false,
    });
    await apiticket.ticket
      .fetchTicket(activeConversation.active_ticket_id)
      .then((ticket) => {
        this.setState(
          {
            ticket: ticket.data,
          },
          () => {}
        );
      });
  };

  componentDidMount() {
    this.fetchTicket();
  }

  handleClose = () => {
    this.setState({
      openModal: false,
    });
  };

  setStatus = async (ticket, comment) => {
    apiticket.ticket.update(ticket.id, ticket).then((data) => {
      this.props.sendComment(comment, ticket.id, comment.user_id).then(() => {
        this.setState({ message: "", loading: false });
      });
    });
  };

  sendForward = async (e, { value }) => {
    e.preventDefault();
    const {
      conversation,
      activeConversationId,
      openTicketModal,
      parameters,
    } = this.props;

    const activeConversation = conversation.data.find(
      (c) => c.id === activeConversationId
    );

    const ticket_id = activeConversation.active_ticket_id;
    const progress = value.progress_percentage;
    const ticket = activeConversation.tickets.find((c) => c.id === ticket_id);
    const user_id = this.props.user.id;

    const ticketProgress = {
      ...ticket,
      current_status: value.id,
      ticket_id,
      progress,
      current_progress: progress,
    };

    const comment = {
      status_id: value.id,
      user_id,
      ticket_id,
      path: 1,
      source: "comment",
      comment_type: 0,
    };

    this.setState({
      ticket,
      loading: true,
    });

    /**
     * Verifica Se o ticket está sendo finalizado
     */
    if (value.progress_percentage >= 100) {
      /**
       * Verifica se o usuário é obrigado
       * a preencher as informações do ticket ao finalizar
       */
      if (parameters.fill_out_ticket_before_finalizing) {
        openTicketModal(ticketProgress);
      } else {
        this.setState({ loading: true });
        await this.setStatus(ticketProgress, comment);

        const id = this.props.activeConversationId;
        const openTicket = activeConversation.tickets.find(
          (t) => t.current_progress < 100 && t.id !== ticketProgress.id
        );

        const archived = !openTicket;

        if (archived) {
          this.props.archiveConversation({ conversation: { id, archived } });
        } else {
          this.props.setActiveTicket(openTicket, null);
        }

        this.setState({ loading: false });
      }
    } else {
      this.setState({ loading: true });
      await this.setStatus(ticketProgress, comment);
      this.setState({ loading: false });
    }
  };

  render() {
    const { ticketStatus, loading } = this.state;

    return (
      <div
        name="service"
        className={this.state.open ? "ServiceStatus open" : "ServiceStatus"}
        onMouseOver={this.createClass}
        onMouseOut={this.closeClass}
      >
        {this.props.children}
        <div className="openStatus">
          <div className="iconSeta">
            <Icon path={mdiChevronRight} size={0.8} />
          </div>
        </div>
        {loading && (
          <div className="loading-message body">
            <LoaderComponent />
          </div>
        )}

        <div className="status">
          <nav>
            <ul>
              {ticketStatus
                .filter((s) => !!s.fast_menu_icon)
                .map((d, i) => (
                  <li key={`ticketStatus-${i * 1}`}>
                    <Popup
                      content={d.description}
                      position="top left"
                      size="mini"
                      trigger={
                        <Button
                          icon={d.fast_menu_icon}
                          className={
                            this.state.finished && d.progress_percentage > 99
                              ? "hidden"
                              : ""
                          }
                          value={d}
                          onClick={this.sendForward}
                        />
                      }
                    />
                  </li>
                ))}
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => ({
  conversation: state.conversation,
  user: state.user.user,
  parameters: state.user.user.companies[0].parameters,
  loadingTickets: state.conversation.loadingTickets,
  activeConversationId: state.conversation.activeConversationId,
  enterAsSend: state.conversation.enterAsSend,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceStatus);
