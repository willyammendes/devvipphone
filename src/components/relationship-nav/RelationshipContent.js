import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Icon } from "semantic-ui-react";
import ButtonFluid from "../semantic/Button";

const RelationshipContent = ({ navLinks }) => (
  <nav>
    <ul>
      {navLinks.map((navLink, i) => (
        <li key={`config-link-${i * 1}`}>
          <Link to={navLink.url}>
            <ButtonFluid>
              <Icon name={navLink.icon} />
              {navLink.title}
            </ButtonFluid>
          </Link>
        </li>
      ))}
    </ul>
  </nav>
);

RelationshipContent.propTypes = {
  navLinks: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired
    })
  ).isRequired
};

export default RelationshipContent;
