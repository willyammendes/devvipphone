import React from "react";
import { Card } from "semantic-ui-react";
import RelationshipContent from "./RelationshipContent";
import Tour from "reactour";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Creators as TourActions } from "../../store/ducks/tour";

const RelationshipNav = ({
  permissions = [],
  tour,
  closeTourRelationship,
  closeTourRelationshipAgente,
}) => {
  const client = permissions.find((c) => c === "list-client");
  const contact = permissions.find((c) => c === "list-contact");
  const professional = permissions.find((c) => c === "list-professional");
  const event = permissions.find((c) => c === "list-event");
  const place = permissions.find((c) => c === "list-place");
  const speciality = permissions.find((c) => c === "list-speciality");
  const schedulesList = permissions.find((c) => c === "list-schedule");
  const intent = permissions.find((c) => c === "list-bot-trigger");
  const botContext = permissions.find((c) => c === "list-bot-context");
  const botVar = permissions.find((c) => c === "list-bot-var");
  const botSubstitution = permissions.find(
    (c) => c === "list-bot-substitution"
  );
  const contactGroup = permissions.find((c) => c === "list-contact-group");

  const navLinks = [
    {
      name: "Compromissos",
      links: [
        {
          id: event ? 1 : "",
          title: event ? "Eventos e Tarefas" : "",
          url: event ? "/events" : "",
          icon: event ? "calendar alternate outline" : "",
        },
        {
          id: professional ? 2 : "",
          title: professional ? "Profissionais" : "",
          url: professional ? "/professionals" : "",
          icon: professional ? "address book outline" : "",
        },
        {
          id: place ? 3 : "",
          title: place ? "Endereços" : "",
          url: place ? "/places" : "",
          icon: place ? "address card outline" : "",
        },
        {
          id: speciality ? 4 : "",
          title: speciality ? "Especialidades" : "",
          url: speciality ? "specialities" : "",
          icon: speciality ? "calendar alternate outline" : "",
        },
        {
          id: schedulesList ? 5 : "",
          title: schedulesList ? "Horários" : "",
          url: schedulesList ? "/schedules" : "",
          icon: schedulesList ? "clock outline" : "",
        },
      ],
    },
    /* {
      name: "MonitBot",
      links: [
        {
          id: intent ? 1 : "",
          title: intent ? "Intenções" : "",
          url: intent ? "/triggers" : "",
          icon: intent ? "chat" : ""
        },
        {
          id: botContext ? 2 : "",
          title: botContext ? "Contextos" : "",
          url: botContext ? "/bot-context" : "",
          icon: botContext ? "folder open outline" : ""
        },
        {
          id: botVar ? 3 : "",
          title: botVar ? "Váriaveis" : "",
          url: botVar ? "/bot-vars" : "",
          icon: botVar ? "exclamation" : ""
        },
        {
          id: botSubstitution ? 4 : "",
          title: botSubstitution ? "Substituições" : "",
          url: botSubstitution ? "/bot-substitutions" : "",
          icon: botSubstitution ? "exchange" : ""
        }
      ]
    }, */
    {
      name: "Relacionamento",
      links: [
        {
          id: contact ? 1 : "",
          title: contact ? "Contatos" : "",
          url: contact ? "/contacts" : "",
          icon: contact ? "user" : "",
        },
        {
          id: client ? 2 : "",
          title: client ? "Clientes(Empresas)" : "",
          url: client ? "/client" : "",
          icon: client ? "building" : "",
        },
        {
          id: contactGroup ? 3 : "",
          title: contactGroup ? "Grupo de Contatos" : "",
          url: contactGroup ? "/contacts/groups" : "",
          icon: contactGroup ? "users" : "",
        },
        {
          id: 4,
          title: "Contatos Restritos",
          url: "/contacts/restricted-contacts",
          icon: "hand paper",
        },
      ],
    },
  ];
  return (
    <div className="CofigNav">
      <Tour
        steps={tour.relationshipSteps}
        isOpen={tour.isOpenRelationship}
        onRequestClose={closeTourRelationship}
      />
      <Tour
        steps={tour.relationshipStepsAgente}
        isOpen={tour.isOpenRelationshipAgente}
        onRequestClose={closeTourRelationshipAgente}
      />
      <div className="headingPage">
        <h1>Relacionamento</h1>
      </div>
      <div className="holderPage">
        {navLinks.map((navLink, i) => (
          <div
            className="navRelationship navConfig"
            key={`config-nav-${i * 1}`}
          >
            <Card fluid>
              <Card.Content header={navLink.name} />
              <Card.Content>
                <RelationshipContent
                  navLinks={navLink.links.filter((c) => c.id !== "")}
                />
              </Card.Content>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    tour: state.tour,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RelationshipNav);
