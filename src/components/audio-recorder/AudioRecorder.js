import React, { Component } from "react";
import { Modal, Button, Icon, Header } from "semantic-ui-react";

class AudioRecorder extends Component {
  state = {
    modalOpen: false,
    audios: [],
    interval: 0,
    recordTime: 0,
    recording: false,
    data: null,
  };

  sendAudio = () => {
    const { data } = this.state;
    this.props.sendVoice(data);

    this.setState({
      audios: [],
      modalOpen: false,
      recording: false,
      interval: null,
      data: null,
    });
  };

  recordAudio = () => {
    this.setState({
      recording: true,
      audios: [],
    });
  };

  componentDidMount() {
    const MicRecorder = require("mic-recorder-to-mp3");

    const button = document.querySelector(".button-record");
    let btnIcon = document.querySelector(".button-record > i");

    const recorder = new MicRecorder({
      bitRate: 128,
    });

    button.addEventListener("click", startRecording);
    let self = this;
    function startRecording() {
      recorder
        .start()
        .then(() => {
          btnIcon.classList.remove("fa-microphone");
          btnIcon.classList.add("fa-spinner");
          btnIcon.classList.add("fa-spin");
          btnIcon.title = "Parar Gravação";
          btnIcon.style.color = "red";
          self.recordAudio();
          button.removeEventListener("click", startRecording);
          button.addEventListener("click", stopRecording);
        })
        .catch((e) => {
          console.error(e);
        });
    }
    function stopRecording() {
      recorder
        .stop()
        .getMp3()
        .then(([buffer, blob]) => {
          const file = new File(buffer, "file.mp3", {
            type: blob.type,
            lastModified: Date.now(),
          });

          const player = new Audio(URL.createObjectURL(file));

          self.blobToDataURL(blob, (e) => {
            self.setState({
              data: e,
            });
          });

          player.controls = true;

          btnIcon.classList.add("fa-microphone");
          btnIcon.classList.remove("fa-spinner");
          btnIcon.classList.remove("fa-spin");
          btnIcon.style.color = "green";
          btnIcon.title = "Gravar";
          button.removeEventListener("click", stopRecording);

          button.addEventListener("click", startRecording);

          self.setState({
            modalOpen: true,
            audios: [URL.createObjectURL(file)],
            recording: false,
          });
        })
        .catch((e) => {
          console.error(e);
        });
    }
  }

  blobToDataURL = (blob, callback) => {
    var a = new FileReader();
    a.onload = function(e) {
      callback(e.target.result);
    };
    a.readAsDataURL(blob);
  };

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    return (
      <div className="dib">
        <div className="reply-icons">
          <a
            className="button-record"
            id="audio-file"
            rel="tooltip"
            title="Gravar"
          >
            <i className="fa fa-microphone fa-2x" style={{ color: "green" }} />
          </a>
        </div>
        <Modal
          onClose={this.handleClose}
          open={this.state.modalOpen}
          closeOnEscape={true}
          closeOnDocumentClick={true}
        >
          <Header icon="archive" content="Enviar Gravação" />
          <Modal.Content>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              {this.state.audios.map((a) => {
                return (
                  <div>
                    <audio src={a} controls />
                  </div>
                );
              })}
              <div>
                <Button
                  icon
                  labelPosition="right"
                  color="green"
                  onClick={this.sendAudio}
                >
                  Enviar
                  <Icon name="right arrow" />
                </Button>
                <Button
                  icon
                  labelPosition="right"
                  color="red"
                  onClick={this.handleClose}
                >
                  Cancelar
                  <Icon name="stop" inverted />
                </Button>
              </div>
            </div>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

export default AudioRecorder;
