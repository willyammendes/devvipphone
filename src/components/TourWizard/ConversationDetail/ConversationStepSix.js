import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <h2>Última mensagem</h2>
    <p>Aqui aparecerá a última mensagem que o agente recebeu ou enviou!</p>
  </Grid>
);
