import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <h2>Última atualização</h2>
    <p>Aqui aparece o tempo da última atualização de conversa </p>
  </Grid>
);
