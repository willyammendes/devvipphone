import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Clientes</h2>
      <p>
        <strong>
          Neste Menu ficará todos relatório de clientes, a quantidade de
          clientes pessoa física, Pessoa Jurídica e seus gráficos
        </strong>
      </p>
    </Grid.Row>
  </Grid>
);
