import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Progresso</h2>
      <p>Nesta coluna você verá o progresso da campanha disparada</p>
    </Grid.Row>
  </Grid>
);
