import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Mensagem</h2>
      <p>Nesta coluna você verá a mensagem que será enviada na campanha</p>
    </Grid.Row>
  </Grid>
);
