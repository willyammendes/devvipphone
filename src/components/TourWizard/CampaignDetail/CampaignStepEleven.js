import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Resposável</h2>
      <p>Nesta coluna você verá o reponsável da campanha.</p>
    </Grid.Row>
  </Grid>
);
