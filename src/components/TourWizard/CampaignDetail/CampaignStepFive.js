import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Data Inicio</h2>
      <p>Nesta coluna você verá a data e a hora que iniciará a campanha</p>
    </Grid.Row>
  </Grid>
);
