import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Responsável</h2>
      <p>Nesta coluna você verá o responsável pelo ticket</p>
    </Grid.Row>
  </Grid>
);
