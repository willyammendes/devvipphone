import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Edição</h2>
      <p>
        Nesta coluna você poderá editar o ticket, deletar o ticket ou selecionar
        o ticket
      </p>
    </Grid.Row>
  </Grid>
);
