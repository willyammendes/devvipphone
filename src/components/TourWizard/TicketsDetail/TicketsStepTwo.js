import React from "react";
import { Grid, Dropdown, Input, Button, Checkbox } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Ações</h2>
      <p>Aqui é onde fica as ações que podemos realizar</p>
      <p>
        <Button icon="backward" color="blue" /> Voltar a página anterior, por
        exemplo você estava em conversas e veio pra tickets, este botão voltará
        pra conversas
      </p>
      <p>
        <Button icon="refresh" color="blue" /> Atualiza os dados da tabela
      </p>
      <p>
        <Button icon="file archive" color="blue" /> Exibe os tickets fechados
      </p>
      <p>
        <Button icon="eye" color="blue" /> Exibe os seus tickets
      </p>
      <p style={{ marginBottom: 2 }}>
        <Dropdown text="10" selection />
      </p>
      <p>seleciona a quantidade de dados que vai ser exibido na tabela</p>
      <p style={{ marginBottom: 2 }}>
        <Input icon="search" placeholder="Busca" />
      </p>
      <p>Buscar por um ticket</p>
      <p>
        <Button color="blue">CSV</Button> Exporta os dados para arquivo CSV
      </p>
      <p>
        <Button color="blue">XLS</Button> Exporta os dados para arquivo XLS
      </p>
      <p>
        <Button icon="file pdf outline" color="blue" /> Exporta os dados para
        arquivo PDF
      </p>
      <p>
        <Button icon="plus" color="blue" /> cria um novo ticket
      </p>
      <p>
        <Button icon="check" color="blue" /> Alterar o status do ticket
        selecionado
      </p>
      <p>
        <Button icon="user" color="blue" /> Altera o responsável do ticket
        selecionado
      </p>
      <br />
      <div style={{ marginTop: 10 }}>
        <div>
          <Checkbox />
          <Dropdown>
            <Dropdown.Menu>
              <Dropdown.Item icon="trash" text="Deletar" />
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <p>Seleciona todas as tickets para deletar</p>
      </div>
    </Grid.Row>
  </Grid>
);
