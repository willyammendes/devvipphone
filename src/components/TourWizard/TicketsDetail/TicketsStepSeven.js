import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Telefone</h2>
      <p>Nesta coluna você verá o número do telefone do contato</p>
    </Grid.Row>
  </Grid>
);
