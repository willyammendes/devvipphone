import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Categoria</h2>
      <p>
        Nesta coluna você verá categorias no qual o ticket foi atribuído, de
        acordo com o que você cadastrou em{" "}
        <strong>configurações -> cadegorias de atendimento </strong>
      </p>
    </Grid.Row>
  </Grid>
);
