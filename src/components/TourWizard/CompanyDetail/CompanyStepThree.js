import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Dados de Faturamento</h2>
      <p>
        Aqui irá ficar seus dados de faturamento, Tipo de faturamento: pessoa
        física ou Jurídica, os dados da empresa e o email de combrança da empesa
        e método de pagamento.
      </p>
    </Grid.Row>
  </Grid>
);
