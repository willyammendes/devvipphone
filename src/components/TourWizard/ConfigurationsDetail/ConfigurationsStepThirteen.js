import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Palavras Impróprias</h2>
      <p>
        Aqui é onde se cadastra as palavras impróprias quando se envia uma
        palavra imprópria não envia palavras imprópria e se receber uma palavra
        imprópria ela não exibe essa palavra.
      </p>
    </Grid.Row>
  </Grid>
);
