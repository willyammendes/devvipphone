import React from "react";
import { Grid } from "semantic-ui-react";

export default () => (
  <Grid>
    <Grid.Row>
      <h2>Motivos de Pausa</h2>
      <p>
        Aqui é onde se cadastra os motivos para pausa dos agentes, é onde se
        cadastra o tempo que o usuário irá almoçar, tomar café ir ao banheiro ou
        se ele está em treinamento por exemplo.
      </p>
    </Grid.Row>
  </Grid>
);
