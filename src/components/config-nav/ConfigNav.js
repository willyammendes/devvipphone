import React from "react";
import { Card } from "semantic-ui-react";
import Tour from "reactour";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Creators as TourActions } from "../../store/ducks/tour";
import ConfigContent from "../config-nav/ConfigContent";

const ConfigNav = ({
  permissions,
  closeTourConfigurations,
  closeTourConfigurationsAgente,
  closeTourConfigurationsSupervisor,
  tour,
  user: loggedUser,
}) => {
  const user = permissions.find((c) => c === "edit-user");
  const role = permissions.find((c) => c === "list-role");
  const rolePermission = permissions.find((c) => c === "delete-permission");
  const manager = permissions.find((c) => c === "delete-permission");
  const department = permissions.find((c) => c === "list-department");
  const ticketStatus = permissions.find((c) => c === "list-ticket-status");
  const ticketCategory = permissions.find((c) => c === "list-ticket-category");
  const interruptionType = permissions.find(
    (c) => c === "edit-interruption-type"
  );
  const socialMedia = permissions.find((c) => c === "list-social-account");
  const quickMessage = permissions.find((c) => c === "list-fast-message");
  const badWord = permissions.find((c) => c === "list-bad-word");
  const alertWord = permissions.find((c) => c === "list-alert-word");
  const parameter = permissions.find((c) => c === "list-parameter");
  const webhook = permissions.find((c) => c === "list-webhook");
  const server = permissions.find((c) => c === "list-server");

  const navLinks = [
    {
      name: "Geral",
      links: [
        {
          id: user ? 1 : "",
          title: user ? "Usuários" : "",
          url: user ? "users" : "",
          icon: user ? "user" : "",
        },
        {
          id: role ? 2 : "",
          title: role ? "Perfis de acesso" : "",
          url: role ? "roles" : "",
          icon: role ? "chess" : "",
        },
        {
          id: rolePermission ? 3 : "",
          title: rolePermission ? "Permissões" : "",
          url: rolePermission ? "permissions" : "",
          icon: rolePermission ? "unlock alternate" : "",
        },
        {
          id: department ? 4 : "",
          title: department ? "Departamentos" : "",
          url: department ? "department" : "",
          icon: department ? "users" : "",
        },
        {
          id: ticketStatus ? 5 : "",
          title: ticketStatus ? "Status de Atendimento" : "",
          url: ticketStatus ? "ticket-status" : "",
          icon: ticketStatus ? "clock" : "",
        },
        {
          id: ticketCategory ? 6 : "",
          title: ticketCategory ? "Categorias de Atendimento" : "",
          url: ticketCategory ? "ticket-category" : "",
          icon: ticketCategory ? "folder" : "",
        },
        {
          id: interruptionType ? 7 : "",
          title: interruptionType ? "Motivos de Pausa" : "",
          url: interruptionType ? "/interruption-type" : "",
          icon: interruptionType ? "clock outline" : "",
        },
        {
          id: manager ? 7 : "",
          title: manager ? "Whatsapp Monitor" : "",
          url: manager ? "/whatsapp-monitor" : "",
          icon: manager ? "whatsapp" : "",
        },
      ],
    },
    {
      name: "Integrações e Mensagens",
      links: [
        {
          id: socialMedia ? 1 : "",
          title: socialMedia ? "Whatsapp" : "",
          url: socialMedia ? "social-media" : "",
          icon: socialMedia ? "whatsapp" : "",
        },
        {
          id: quickMessage ? 2 : "",
          title: quickMessage ? "Mensagem Rápida" : "",
          url: quickMessage ? "quick-message" : "",
          icon: quickMessage ? "mail outline" : "",
        },
        {
          id: badWord ? 3 : "",
          title: badWord ? "Palavras Impróprias" : "",
          url: badWord ? "bad-word" : "",
          icon: badWord ? "ban" : "",
        },
        {
          id: alertWord ? 4 : "",
          title: alertWord ? "Palavras de Alerta" : "",
          url: alertWord ? "alert-word" : "",
          icon: alertWord ? "ban" : "",
        },
      ],
    },
    {
      name: "Integrações",
      links: [
        {
          id: parameter ? 1 : "",
          title: parameter ? "Parâmetros" : "",
          url: parameter ? "parameters" : "",
          icon: parameter ? "settings" : "",
        },
        {
          id: webhook ? 2 : "",
          title: webhook ? "Webhooks" : "",
          url: webhook ? "webhook" : "",
          icon: webhook ? "lightning" : "",
        },
        {
          id: server ? 3 : "",
          title: server ? "Servidores" : "",
          url: server ? "server" : "",
          icon: server ? "server" : "",
        },
      ],
    },

    /* {
      name: "Financeiro",
      links: [
        {
          id: pricingPlan ? 1 : "",
          title: pricingPlan ? "Planos de Assinatura" : "",
          url: pricingPlan ? "plans" : "",
          icon: pricingPlan ? "usd" : "",
        },
      ],
    }, */
  ];
  return (
    <div className="CofigNav">
      <Tour
        steps={tour.configurationsSteps}
        isOpen={tour.isOpenConfigurations}
        onRequestClose={closeTourConfigurations}
      />
      <Tour
        steps={tour.configurationsStepsAgente}
        isOpen={tour.isOpenConfigurationsAgente}
        onRequestClose={closeTourConfigurationsAgente}
      />
      <Tour
        steps={tour.configurationsStepsSupervisor}
        isOpen={tour.isOpenConfigurationsSupervisor}
        onRequestClose={closeTourConfigurationsSupervisor}
      />
      <div className="headingPage">
        <h1>Configurações</h1>
      </div>
      <div className="holderPage">
        {navLinks.map((navLink, i) => (
          <div className="navConfig" key={`config-nav-${i * 1}`}>
            <Card fluid>
              <Card.Content header={navLink.name} />
              <Card.Content>
                <ConfigContent
                  navLinks={navLink.links.filter((c) => c.id !== "")}
                />
              </Card.Content>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = ({ tour, user }) => {
  return {
    tour,
    user,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigNav);
