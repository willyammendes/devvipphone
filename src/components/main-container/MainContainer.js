import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Header from "../header/Header";
import { Creators as AuthActions } from "../../store/ducks/auth";
import { Creators as TourActions } from "../../store/ducks/tour";
import MenuOffside from "../MenuOffside/MenuOffside";
import InternalChat from "../internal-chat/InternalChat";
import TaskEvent from "../events/TaskEvent";
import Tour from "reactour";

const MainContainer = ({
  children,
  getCurrentPause,
  getPermissions,
  user,
  tour,
  closeTour
}) => {
  useEffect(() => {
    getCurrentPause();
    getPermissions(user.user);
  }, [getCurrentPause, getPermissions]);

  return (
    <div>
      <MenuOffside />
      <Tour
        steps={tour.mainSteps}
        isOpen={tour.isOpen}
        onRequestClose={closeTour}
      />
      <div className="wrap_chat" name="MenuOffside">
        <Header /> {children}
      </div>{" "}
      <TaskEvent />
      <InternalChat />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.user.isAuthenticated,
    currentPause: state.user.currentPause,
    user: state.user,
    tour: state.tour
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...AuthActions, ...TourActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainContainer);
