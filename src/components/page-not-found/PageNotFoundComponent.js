import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import LoaderComponent from "../semantic/Loading";

const PageNotFoundComponent = ({ loading }) => {
  return (
    <div className="pageHolder">
      <div className="holderPage">
        <div className="full">
          {!loading ? (
            <div
              className="headingPage"
              style={{ textAlign: "center", width: "100%" }}
            >
              <span>SEM PERMISSÃO PARA ACESSAR ESTA PÁGINA</span>
            </div>
          ) : (
            <div>
              <LoaderComponent />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
    companies: user.user.companies
  };
};

export default withRouter(connect(mapStateToProps)(PageNotFoundComponent));
