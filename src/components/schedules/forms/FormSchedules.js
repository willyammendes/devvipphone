import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormSchedules = ({ schedule, onChange }) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Nome:"
                    name="name"
                    onChange={onChange}
                    value={schedule.name}
                />
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Código:"
                    name="menu"
                    onChange={onChange}
                    value={schedule.menu}
                />
            </Form.Group>
        </Form>
    );
};

export default FormSchedules;
