import React, { Component } from "react";
import api from "../../api/trigger";
import { Dropdown } from "semantic-ui-react";

class DropdownDepartments extends Component {
	state = {
		trigger: {
			name: "",
			description: ""
		},
		options: [],
		loading: false,
		search: "",
		openModal: false
	};

	handleDepartmentAddition = e => {};

	onSearchChange = (e, { searchQuery }) => {
		clearTimeout(this.timer);
		this.setState({ search: searchQuery });
		this.timer = setTimeout(this.onSearchDepartment, 300);
	};

	componentWillMount() {
		api.intent.fetchActions().then(intents => {
			this.setState({
				options: intents.map((c, index) => ({
					key: c,
					value: index,
					text: c
				})),
				loading: false
			});
		});
	}

	componentDidMount() {}

	onSearchDepartment = async () => {
		this.setState({ loading: true });
		const { search } = this.state;

		await api.intent.fetchActions({ search }).then(intents => {
			this.setState({
				options: intents.map(c => ({
					key: c,
					value: c,
					text: c
				})),
				loading: false
			});
		});
	};

	onChange = (e, { name, value }) => {
		this.setState(state => ({
			intent: { ...state.intent, [name]: value }
		}));
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { intent } = this.state;

		this.setState({
			loading: true
		});

		return api.intent
			.submit(intent)
			.then(data => {
				this.setState(state => ({
					options: [...state.options].concat({
						key: data.data.id,
						value: data.data.id,
						text: data.data.name
					}),
					loading: false,
					intent: {
						name: "",
						description: ""
					},
					openModal: !state.openModal
				}));

				this.props.onSelectDepartment(data.data.id);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
				
			});
	};

	toggle = (e, { name, value }) => {
		this.setState(state => ({
			openModal: !state.openModal,
			intent: {
				name: value,
				description: ""
			}
		}));
	};

	render() {
		const { options, loading } = this.state;

		return (
			<div>
				<Dropdown
					placeholder="Escolha uma ação"
					fluid
					search
					selection
					scrolling
					allowAdditions
					additionLabel="Adicionar: "
					name="action_type"
					value={this.props.action_type}
					onSearchChange={this.onSearchChange}
					onChange={(e, { value }) =>
						this.props.onSelectAction(value)
					}
					options={options}
					onAddItem={this.toggle}
					closeOnChange
					loading={loading}
					clearable
				/>
			</div>
		);
	}
}

export default DropdownDepartments;
