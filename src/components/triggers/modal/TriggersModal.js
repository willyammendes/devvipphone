import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormTriggers from "../forms/FormTriggers";

class TriggersModal extends React.Component {
  state = {
    actionTrue: false
  };

  componentDidMount() {
    document.removeEventListener("keydown", () => {});
    document.addEventListener("keydown", e => {
      if (e.keyCode === 39) this.props.handleNext();
      if (e.keyCode === 37) this.props.handlePrevious();
    });
  }

  save = () => {
    if (this.props.intent.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      intent,
      handleClose,
      onChange,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      intents,
      actionTypes,
      onAddIntent,
      clickAddIntent,
      trigger,
      handleKeyDown,
      onAddReplies,
      replies,
      replyInput,
      rateAdd,
      deleteReply,
      deleteTrigger,
      handleActionChange,
      handleDepartmentChange,
      handleUserChange,
      handleMediaChange,
      BasicUpload,
      changeAction,
      actionTrue
    } = this.props;

    return (
      <Modal
        size="small"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="form-intent">
              <FormTriggers
                intent={intent}
                BasicUpload={BasicUpload}
                actionTypes={actionTypes}
                actionTrue={actionTrue}
                intents={intents}
                changeAction={changeAction}
                onAddIntent={onAddIntent}
                handleActionChange={handleActionChange}
                handleDepartmentChange={handleDepartmentChange}
                handleUserChange={handleUserChange}
                handleMediaChange={handleMediaChange}
                onChange={onChange}
                replyInput={replyInput}
                clickAddIntent={clickAddIntent}
                trigger={trigger}
                replies={replies}
                deleteReply={deleteReply}
                deleteTrigger={deleteTrigger}
                handleKeyDown={handleKeyDown}
                onAddReplies={onAddReplies}
                rateAdd={rateAdd}
              />
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>{" "}
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default TriggersModal;
