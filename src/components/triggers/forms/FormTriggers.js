import React from "react";
import {
    Form,
    Input,
    Checkbox,
    TextArea,
    Button,
    Divider,
    Icon,
    Rating
} from "semantic-ui-react";
import DropdownTriggers from "../DropdownTriggers";
import DropdownDepartments from "../../departments/DropdownDepartments";
import DropdownUsers from "../../users/DropdownUsers";
import DropdownMedia from "../../social-media/DropdownMedia";

const FormTriggers = ({
    intent,
    onChange,
    onAddIntent,
    clickAddIntent,
    trigger,
    handleKeyDown,
    onAddReplies,
    replies,
    replyInput,
    intents,
    rateAdd,
    deleteReply,
    deleteTrigger,
    actionTrue,
    changeAction,
    handleActionChange,
    handleDepartmentChange,
    handleUserChange,
    handleMediaChange,
    BasicUpload
}) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                icon={
                    intent.id ? (
                        false
                    ) : (
                        <Icon
                            name="plus"
                            onClick={clickAddIntent}
                            inverted
                            circular
                            link
                        />
                    )
                }
                label="Adicione uma expressão do usuário:"
                placeholder="Bom dia"
                name={intent.id ? "trigger" : "description"}
                onChange={intent.id ? onChange : onAddIntent}
                onKeyDown={e =>
                    e.key === "Enter" && !intent.id ? clickAddIntent : ""
                }
                value={intent.id ? intent.trigger : intents.description}
            />
            <div className="intent_triggers">
                {trigger.map((d, index) => (
                    <div key={`intent-triggers-${d.id}`}>
                        <Icon name="quote left" />
                        {d.description}
                        <Icon
                            name="trash"
                            className="fr"
                            onClick={() => deleteTrigger(index)}
                        />
                    </div>
                ))}
            </div>
            <Divider />
            <Form.Field
                control={TextArea}
                label="Digite um texto para resposta:"
                placeholder="Bom dia"
                name="reply"
                onChange={onAddReplies}
                value={replyInput.reply}
            />
            <div className="field">
                <Button color="green" onClick={clickAddIntent}>
                    Adicionar Resposta
                </Button>
            </div>
            <div className="replies_triggers">
                {replies.map((d, index) => (
                    <div>
                        <Icon name="quote left" />
                        {d.description || d.reply}
                        <Icon
                            name="trash"
                            className="fr"
                            onClick={() => deleteReply(index)}
                        />

                        <Rating
                            icon="star"
                            maxRating={5}
                            defaultRating={d.weight}
                            value={d.weight}
                            clearable
                            className="fr"
                            name="weight"
                            onRate={(e, { rating }) =>
                                rateAdd(index, rating, d)
                            }
                        />
                    </div>
                ))}
            </div>
            <div className="field">
                <Checkbox
                    toggle
                    label="Executar Uma Ação"
                    value={actionTrue}
                    checked={actionTrue}
                    onChange={changeAction}
                />
            </div>
            {actionTrue ? (
                <div className="field">
                    <label>Ação a ser executada</label>
                    <DropdownTriggers
                        onSelectAction={handleActionChange}
                        action_type={intent.action.action_type}
                    />
                </div>
            ) : (
                ""
            )}
            {actionTrue &&
            (intent.action.action_type === 0 ||
                intent.action.action_type === 2) ? (
                <div className="field">
                    <DropdownDepartments
                        onSelectDepartment={handleDepartmentChange}
                        departments={intent.action.department_id}
                    />
                </div>
            ) : (
                ""
            )}
            {actionTrue &&
            (intent.action.action_type === 1 ||
                intent.action.action_type === 5) ? (
                <div className="field">
                    <DropdownUsers
                        onSelectUser={handleUserChange}
                        user_id={intent.action.user_id}
                        allowAdditions={false}
                    />
                </div>
            ) : (
                ""
            )}
            {actionTrue &&
            (intent.action.action_type === 3 ||
                intent.action.action_type === 4) ? (
                <div className="holder_requisition">
                    <h3>Informações da Requisição</h3>
                    {intent.id ? (
                        <Form.Field
                            control={Input}
                            label="URL"
                            placeholder="Digite a url ex: https://api.monitchat.com"
                            name={
                                intent.action.action_type === 3
                                    ? "get_url"
                                    : "post_url"
                            }
                            onChange={onChange}
                            value={
                                intent.action.action_type === 3
                                    ? intent.action.get_url
                                    : intent.action.post_url
                            }
                        />
                    ) : (
                        <Form.Field
                            control={Input}
                            label="URL"
                            placeholder="Digite a url ex: https://api.monitchat.com"
                            name={
                                intent.action.action_type === 3
                                    ? "get_url"
                                    : "post_url"
                            }
                            onChange={onChange}
                            value={
                                intent.action.action_type === 3
                                    ? intent.get_url
                                    : intent.post_url
                            }
                        />
                    )}

                    {intent.id ? (
                        <Form.Field
                            control={Input}
                            label="Campos de Retorno"
                            placeholder="Digite a url ex: https://api.monitchat.com"
                            name={
                                intent.action.action_type === 3
                                    ? "get_fields"
                                    : "post_fields"
                            }
                            onChange={onChange}
                            value={
                                intent.action.action_type === 3
                                    ? intent.action.get_fields
                                    : intent.action.post_fields
                            }
                        />
                    ) : (
                        <Form.Field
                            control={Input}
                            label="Campos de Retorno"
                            placeholder="Digite a url ex: https://api.monitchat.com"
                            name={
                                intent.action.action_type === 3
                                    ? "get_fields"
                                    : "post_fields"
                            }
                            onChange={onChange}
                            value={
                                intent.action.action_type === 3
                                    ? intent.get_fields
                                    : intent.post_fields
                            }
                        />
                    )}

                    <Form.Group widths="equal">
                        <Form.Field
                            control={Input}
                            label="Cabeçalhos da Requisição"
                            placeholder="Cabeçalhos. Ex: Authorization;Content-type;etc..."
                            name="headers"
                            onChange={onChange}
                            value={
                                intent.id
                                    ? intent.action.headers
                                    : intent.headers
                            }
                        />
                        <Form.Field
                            control={Input}
                            label="Valores dos Cabeçalhos"
                            placeholder="Valores. Ex: Basic:asdf=s-fsd;application/json;etc..."
                            name="headers_value"
                            onChange={onChange}
                            value={
                                intent.id
                                    ? intent.action.headers_value
                                    : intent.headers_value
                            }
                        />
                    </Form.Group>
                </div>
            ) : (
                ""
            )}
            {intent.action.action_type === 10 && <BasicUpload />}

            {actionTrue ? (
                <Form.Field
                    control={TextArea}
                    label="Mensagem de Retorno:"
                    placeholder="Obrigado! Em breve um de nossos analistas irá lhe atender"
                    name={intent.id ? "message" : "reply"}
                    onChange={onChange}
                    value={intent.id ? intent.action.message : intent.reply}
                />
            ) : (
                ""
            )}

            <div className="field">
                <label>Contas que o Monitbot irá Atender (Vazio = Todas)</label>
                <DropdownMedia
                    onSelectMedia={handleMediaChange}
                    accounts={intent.accounts}
                    allowAdditions={false}
                />
            </div>
        </Form>
    );
};

export default FormTriggers;
