import React from "react";
import { Popup, Button, Input } from "semantic-ui-react";
import { mdiPencilBox } from "@mdi/js";
import Icon from "@mdi/react";

const Popover = ({ children, value }) => {
  const POPOVER_CONTENT = (
    <div className="PopoverContent">
      <div className="popOverHeader">
        <h3>Editar</h3>
      </div>
      <div className="input">
        <Input fluid value={children} />
      </div>
      <div className="popOverFooter">
        <Button content="Salvar" primary />
        <Button content="Cancelar" basic right />
      </div>
    </div>
  );

  return (
    <Popup
      trigger={
        <div className="iconEditar">
          <Icon path={mdiPencilBox} size={1} />
        </div>
      }
      content={POPOVER_CONTENT}
      on="click"
      position="right center"
    />
  );
};

export default Popover;
