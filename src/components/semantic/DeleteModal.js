import React from "react";
import PropTypes from "prop-types";
import { Modal, Button, Icon } from "semantic-ui-react";

const DeleteModal = ({
    handleClick = () => {},
    handleClose = () => {},
    onChange,
    onChecked,
    trigger,
    modalHeader = ""
}) => {
    return (
        <Modal
            size="tiny"
            closeIcon
            trigger={trigger}
            onClose={handleClose}
            className="deleteModal"
        >
            <Modal.Content>
                <Modal.Description>
                    <div className="form-ticket">
                        <Icon name="warning circle" />
                        <h1>Tem certeza?</h1>
                        <p>Você não poderá recuperar este registro!</p>
                    </div>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button primary onClick={handleClick}>
                    Sim, excluir!
                </Button>
                <Button basic onClick={handleClose}>
                    Não, voltar
                </Button>
            </Modal.Actions>
        </Modal>
    );
};

DeleteModal.propTypes = {
    ticket: PropTypes.shape().isRequired,
    handleClick: PropTypes.func.isRequired,
    trigger: PropTypes.shape().isRequired,
    handleClose: PropTypes.func.isRequired,
    modalHeader: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onChecked: PropTypes.func.isRequired
};

export default DeleteModal;
