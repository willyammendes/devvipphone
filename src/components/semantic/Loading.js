import React from "react";
import { Loader } from "semantic-ui-react";

const LoaderComponent = () => <Loader active inline="centered" size="small" />;

export default LoaderComponent;
