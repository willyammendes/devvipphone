import React from "react";
import { Button } from "semantic-ui-react";

const ButtonFluid = ({ children }) => (
	<Button fluid icon labelPosition="left">
		{children}
	</Button>
);

export default ButtonFluid;
