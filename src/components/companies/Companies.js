import React, { useState } from "react";
import { connect } from "react-redux";
import { Card } from "semantic-ui-react";
import Tour from "reactour";
import { bindActionCreators } from "redux";

import { Creators as TourActions } from "../../store/ducks/tour";
import CompaniesContent from "./CompaniesContent";
import PaymentInformationModal from "./PaymentInformationModal";
import InvoiceModal from "./InvoiceModal";
import SelectPlanModal from "./SelectPlanModal";

const Companies = ({
  user: { pages, permissions },
  closeTourCompany,
  tour,
}) => {
  const [
    paymentInformationModalOpen,
    setPaymentInformationModalOpen,
  ] = useState(false);

  const [invoiceModalOpen, setInvoiceModalOpen] = useState(false);

  const [plansModalOpen, setPlansModalOpen] = useState(false);

  const togglePaymentModal = () => {
    setPaymentInformationModalOpen(!paymentInformationModalOpen);
  };

  const toggleInvoiceModal = () => {
    setInvoiceModalOpen(!invoiceModalOpen);
  };

  const togglePlansModal = () => {
    setPlansModalOpen(!plansModalOpen);
  };

  const page = permissions.find((c) => c === "list-page");
  const extraInput = permissions.find((c) => c === "list-extra-input");

  const navLinks = [
    {
      name: "Financeiro",
      id: 2,
      links: [
        {
          id: 1,
          title: "Dados de Faturamento",
          url: "/companies",
          icon: "user",
          modalLink: true,
          openModal: togglePaymentModal,
        },
        {
          id: 2,
          title: "Faturas",
          url: "/invoices",
          icon: "credit card",
          modalLink: true,
          openModal: toggleInvoiceModal,
        },
        {
          id: 3,
          title: "Planos",
          url: "/companies",
          icon: "building",
          modalLink: true,
          openModal: togglePlansModal,
        },
      ],
    },
    {
      name: "Customização",
      links: [
        {
          id: page ? 1 : "",
          title: page ? "Cadastros Customizados" : "",
          url: page ? "page" : "",
          icon: page ? "settings" : "",
        },
        {
          id: extraInput ? 2 : "",
          title: extraInput ? "Campos Adicionais" : "",
          url: extraInput ? "extra-input" : "",
          icon: extraInput ? "settings" : "",
        },
      ],
    },
  ];

  const menuIndex = 100;

  const groupBy = function(xs, key) {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  const group = groupBy(pages, "category");

  for (const i in group) {
    navLinks.push({
      name: i,
      links: group[i].map((e) => {
        return {
          id: menuIndex,
          title: e.title,
          url: `/custom-page/${e.id}`,
          icon: e.menu_icon,
        };
      }),
    });
  }
  return (
    <div className="CofigNav">
      <Tour
        steps={tour.companySteps}
        isOpen={tour.isOpenCompany}
        onRequestClose={closeTourCompany}
      />
      <div className="headingPage">
        <h1>Administração</h1>
      </div>
      <div className="holderPage">
        {navLinks.map((navLink, i) => (
          <div className="navCompanies navConfig" key={`config-nav-${i * 1}`}>
            <Card fluid>
              <Card.Content header={navLink.name} />
              <Card.Content>
                <CompaniesContent navLinks={navLink.links} />
              </Card.Content>
            </Card>
          </div>
        ))}
      </div>
      <PaymentInformationModal
        togglePaymentModal={togglePaymentModal}
        paymentInformationModalOpen={paymentInformationModalOpen}
      />
      <SelectPlanModal
        togglePlansModal={togglePlansModal}
        plansModalOpen={plansModalOpen}
      />
      <InvoiceModal
        toggleInvoiceModal={toggleInvoiceModal}
        invoiceModalOpen={invoiceModalOpen}
      />
    </div>
  );
};

const mapStateToProps = ({ user, tour }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
    tour,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(TourActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Companies);
