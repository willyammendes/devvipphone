import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Modal, Grid, Label, Icon } from "semantic-ui-react";
import api from "../../api/company";
import InvoiceTable from "./InvoiceTable";

const InvoiceModal = ({
  invoiceModalOpen,
  toggleInvoiceModal,
  user: { company_id },
}) => {
  return (
    <Modal
      open={invoiceModalOpen}
      onClose={toggleInvoiceModal}
      closeOnEscape
      closeOnDocumentClick
      closeIcon
      size="large"
    >
      <Modal.Header>Faturas</Modal.Header>
      <Modal.Content>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <InvoiceTable company_id={company_id} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
  };
};

export default connect(
  mapStateToProps,
  null
)(InvoiceModal);
