import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Card,
  Modal,
  Grid,
  Form,
  Input,
  Button,
  Tab,
  Divider,
  Dropdown,
} from "semantic-ui-react";
import InputMask from "react-input-mask";
import api from "../../api/company";

const panes = [
  {
    menuItem: {
      key: 0,
      icon: "barcode",
      content: "Boleto",
      fluid: true,
      style: { minWidth: "100%", display: "flex", justifyContent: "center" },
    },
    render: () => null,
  },
];

const PaymentInformationModal = ({
  paymentInformationModalOpen,
  togglePaymentModal,
  user: { company_id },
}) => {
  const [personsType, setPersonsType] = useState([]);
  const [company, setCompany] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setPersonsType([
      { key: 1, value: 1, text: "Jurídica" },
      { key: 2, value: 2, text: "Física" },
    ]);

    api.company
      .fetchCompany(company_id)
      .then((res) => {
        setCompany(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, []);

  const updateCompany = () => {
    setLoading(true);
    api.company
      .paymentInformation(company_id, company)
      .then((res) => {
        setCompany(res.data);
        setLoading(false);
      })
      .catch((err) => setLoading(false));
  };

  const onChange = (e, { name, value }) => {
    setCompany({
      ...company,
      [name]: value,
    });
  };

  const handleTabChange = (e, { activeIndex }) => {
    setCompany({
      ...company,
      payment_type: activeIndex,
    });
  };

  return (
    <Modal
      open={paymentInformationModalOpen}
      onClose={togglePaymentModal}
      closeOnEscape
      closeOnDocumentClick
      closeIcon
    >
      <Modal.Header>Dados de Pagamento</Modal.Header>
      <Modal.Content>
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column>
              <Card fluid>
                <Card.Content header="Seus Dados" />
                <Card.Content>
                  <Form autoComplete="off">
                    <Form.Group>
                      <Form.Dropdown
                        loading={loading}
                        name="person_type"
                        fluid
                        placeholder="Selecione uma Opção"
                        selection
                        size="small"
                        clearable
                        label="Tipo de Faturamento"
                        onChange={onChange}
                        options={personsType}
                        value={company.person_type}
                      />
                    </Form.Group>

                    <Form.Group widths="equal">
                      <Form.Field>
                        <label htmlFor="name">Nome</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          control={Input}
                          size="small"
                          name="name"
                          onChange={onChange}
                          value={company.name}
                          placeholder="Monitchat"
                        />
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="cpf_cnpj">
                          {company.person_type === 1 ? "CNPJ" : "CPF"}
                        </label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          size="small"
                          control={Input}
                          children={
                            <InputMask
                              mask={
                                company.person_type === 1
                                  ? "99.999.999/9999-99"
                                  : "999.999.999-99"
                              }
                              name="cpf_cnpj"
                              onChange={(e) =>
                                onChange(e, {
                                  name: e.target.name,
                                  value: e.target.value,
                                })
                              }
                              value={company.cpf_cnpj}
                              placeholder={
                                company.person_type === 1 ? "CNPJ" : "CPF"
                              }
                            />
                          }
                        />
                      </Form.Field>
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Form.Field>
                        <label htmlFor="name">Emails de Cobrança</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          control={Input}
                          size="small"
                          name="invoice_emails"
                          onChange={onChange}
                          value={company.invoice_emails}
                          placeholder="financeiro@vipphone.com.br;cobranca@monitchat.com.br"
                        />
                      </Form.Field>
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Form.Field>
                        <label htmlFor="address">CEP</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          size="small"
                          control={Input}
                          children={
                            <InputMask
                              mask={"99999-999"}
                              name="zip_code"
                              onChange={(e) =>
                                onChange(e, {
                                  name: e.target.name,
                                  value: e.target.value,
                                })
                              }
                              value={company.zip_code}
                              placeholder={"29900-000"}
                            />
                          }
                        />
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="address">Endereço</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          control={Input}
                          size="small"
                          name="address"
                          onChange={onChange}
                          value={company.address}
                          placeholder="Informe o Endereço"
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group widths="equal">
                      <Form.Field>
                        <label htmlFor="complement">Complemento</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          control={Input}
                          size="small"
                          name="complement"
                          onChange={onChange}
                          value={company.complement}
                          placeholder="Monitchat"
                        />
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="city">Cidade</label>
                        <Input
                          loading={loading}
                          autoComplete="off"
                          control={Input}
                          size="small"
                          name="city"
                          onChange={onChange}
                          value={company.city}
                          placeholder="Monitchat"
                        />
                      </Form.Field>
                    </Form.Group>
                  </Form>
                </Card.Content>
                <Card.Content extra>
                  <div className="ui two buttons">
                    <Button basic color="green" onClick={updateCompany}>
                      Salvar Alterações
                    </Button>
                  </div>
                </Card.Content>
              </Card>
            </Grid.Column>
            <Grid.Column>
              <Grid.Row>
                <Grid.Column>
                  <Card fluid>
                    <Card.Content header="Método de Pagamento" />
                    <Tab
                      menu={{
                        attached: "top",
                      }}
                      panes={panes}
                      activeIndex={company.payment_type}
                      onTabChange={handleTabChange}
                    />
                  </Card>
                </Grid.Column>
                <Divider />
                <Grid.Column>
                  <Form>
                    <Form.Group widths="2">
                      <Form.Field>
                        <label htmlFor="due_day">Dia de Vencimento:</label>
                        <Dropdown
                          name="due_day"
                          size="mini"
                          selection
                          style={{ width: "100px" }}
                          search
                          value={company.due_day}
                          onChange={onChange}
                          options={[
                            { key: 5, value: 5, text: "05" },
                            { key: 10, value: 10, text: "10" },
                            { key: 15, value: 15, text: "15" },
                            { key: 20, value: 20, text: "20" },
                          ]}
                        />
                        <p
                          style={{
                            fontSize: "10px",
                            color: "rgb(55, 51, 228)",
                          }}
                        >
                          Só pode ser alterado a cada 6 meses
                        </p>
                      </Form.Field>
                    </Form.Group>
                  </Form>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
  };
};

export default connect(
  mapStateToProps,
  null
)(PaymentInformationModal);
