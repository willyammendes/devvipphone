import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Card, Modal, Grid, Form, Button, Message } from "semantic-ui-react";
import api from "../../api/plans";
import apiCompany from "../../api/company";

const SelectPlanModal = ({
  plansModalOpen,
  togglePlansModal,
  user: { company_id },
}) => {
  const [plans, setPlans] = useState([]);
  const [error, setError] = useState();
  const [company, setCompany] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    apiCompany.company
      .fetchCompany(company_id)
      .then((res) => {
        setCompany(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });

    const filter = { filter: JSON.stringify([["active", "=", 1]]) };

    api.plan
      .fetchAll({ take: 500, filter })
      .then((res) => {
        setPlans(res);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, []);

  const updateCompany = () => {
    setLoading(true);
    apiCompany.company
      .changePlan(company_id, company)
      .then((res) => {
        setLoading(false);
        setCompany(res.data);
      })
      .catch((err) => {
        setLoading(false);

        if (err.response.data.global) {
          setError({
            title: err.response.data.title,
            message: err.response.data.global,
          });
        } else {
          setError({
            title: err.response.data.title,
            message: "Verifique seus Dados de Pagamento",
            errors: err.response.data.errors.map((c) => c.id + ": " + c.erro),
          });
        }
      });
  };

  const onChange = (e, { name, value }) => {
    setCompany({
      ...company,
      [name]: value,
    });
  };

  return (
    <Modal
      open={plansModalOpen}
      onClose={togglePlansModal}
      closeOnEscape
      closeOnDocumentClick
      closeIcon
    >
      <Modal.Header>Plano Contratado</Modal.Header>
      <Modal.Content>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              {error && (
                <Message
                  negative
                  onDismiss={() => setError(null)}
                  header={error.title}
                  content={error.message}
                  list={error.errors}
                  color="red"
                />
              )}
              <Card fluid>
                <Card.Content header="Escolha um plano para utilização da Plataforma" />

                <Card.Content>
                  <Form autoComplete="off">
                    <Form.Group>
                      <Form.Dropdown
                        loading={loading}
                        name="pricing_plan_id"
                        fluid
                        placeholder="Selecione uma Opção"
                        selection
                        size="small"
                        clearable
                        disabled={
                          company.invoices && company.invoices.length > 0
                        }
                        label="Plano Selecionado"
                        onChange={onChange}
                        options={plans.map((p) => ({
                          key: p.id,
                          value: p.id,
                          text: `${p.name} - ${
                            p.description
                          } / R$ ${p.price.replace(".", ",")}`,
                        }))}
                        value={company.pricing_plan_id}
                      />
                    </Form.Group>
                    {company.invoices && company.invoices.length > 0 && (
                      <label>
                        Para alterar seu plano, entre em contato conosco!
                        <br />
                        <strong>Telefone:</strong> +55 (27) 3010 - 9500 <br />
                        <strong>E-mail:</strong> contato@vipphone.com.br
                      </label>
                    )}
                  </Form>
                </Card.Content>
                <Card.Content extra>
                  <div className="ui two buttons">
                    <Button
                      basic
                      disabled={company.invoices && company.invoices.length > 0}
                      color="green"
                      onClick={updateCompany}
                    >
                      Salvar Alterações
                    </Button>
                  </div>
                </Card.Content>
              </Card>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    user: user.user,
    permissions: user.user.permissions,
  };
};

export default connect(
  mapStateToProps,
  null
)(SelectPlanModal);
