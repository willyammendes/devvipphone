import React from "react";
import Popover from "../semantic/Popover";

const BadWordContent = ({ badword }) => (
  <ul>
    <li className="w-20">
      <div className="dataEditar" key={badword.id}>
        <Popover value={badword.name}>Nome:</Popover>
        <span>{badword.name}</span>
      </div>
    </li>
  </ul>
);

export default BadWordContent;
