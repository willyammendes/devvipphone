import React from "react";
import { Form, Input } from "semantic-ui-react";

const FormBadWord = ({ badword, onChange }) => {
    return (
        <Form>
            <Form.Field
                autoComplete="off"
                control={Input}
                label="Palavra:"
                name="word"
                onChange={onChange}
                value={badword.word}
            />
        </Form>
    );
};

export default FormBadWord;
