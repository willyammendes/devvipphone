import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormBadWord from "../forms/FormBadWord";

class BadWordModal extends React.Component {
    componentDidMount() {
        document.removeEventListener("keydown", () => {});
        document.addEventListener("keydown", e => {
            if (e.keyCode === 39) this.props.handleNext();
            if (e.keyCode === 37) this.props.handlePrevious();
        });
    }

    save = () => {
        if (this.props.badword.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    render() {
        const {
            badword,
            handleClose,
            onChange,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading
        } = this.props;

        return (
            <Modal
                size="small"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <div className="form-badword">
                            <FormBadWord
                                badword={badword}
                                onChange={onChange}
                            />
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button
                            icon
                            onClick={handlePrevious}
                            disabled={previousButtonEnabled}
                        >
                            <Icon name="left arrow" />
                        </Button>
                        <Button
                            icon
                            onClick={handleNext}
                            disabled={nextButtonEnabled}
                        >
                            <Icon name="right arrow" />
                        </Button>
                    </Button.Group>{" "}
                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default BadWordModal;
