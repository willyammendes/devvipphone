import React, { Component } from "react";
import { Card, Form, Input, Button } from "semantic-ui-react";
import LoaderComponent from "../semantic/Loading";
import api from "../../api/parameters";

class Email extends Component {
	state = {
		parameter: [],
		loading: false,
		hasMore: true
	};
	handleChange = (e, { value }) => this.setState({ value });

	componentWillMount() {
		this.setState({ loading: true });
		api.parameter.fetchAll().then(parameter => {
			this.setState({
				parameter,
				loading: false
			});
		});
	}

	render() {
		const { parameter, loading } = this.state;
		return (
			<div className="pageHolder">
				<div className="headingPage">
					<h1>Email</h1>
				</div>
				{loading && (
					<div className="loading-conversa">
						<LoaderComponent />
					</div>
				)}
				<div className="holderPage">
					<div className="full">
						<Card fluid>
							<Card.Content>
								<Card.Header>Integração de Email</Card.Header>
							</Card.Content>
							<Card.Content>
								<Form>
									<Form.Field
										control={Input}
										label="Servidor de Envio:"
										placeholder="smtp.gmail.com"
										name="server"
										value={parameter.server}
									/>
									<Form.Field
										control={Input}
										label="Usuário (nome):"
										placeholder="crm@vipphone.com.br"
										name="user"
										value={parameter.user}
									/>
									<Form.Field
										control={Input}
										label="Endereço de envio:"
										placeholder="crm@vipphone.com.br"
										type="email"
										name="address"
										value={parameter.address}
									/>
									<Form.Field
										control={Input}
										label="Porta de saída:"
										placeholder="465"
										name="port"
										value={parameter.port}
									/>
									<Form.Field
										control={Input}
										label="Senha de envio:"
										type="password"
										name="password"
										value={parameter.password}
									/>
									<Form.Field
										control={Input}
										label="Tipo de segurança:"
										placeholder="SSL"
										type="text"
										name="type"
										value={parameter.type}
									/>
									<Button color="blue">Salvar</Button>
								</Form>
							</Card.Content>
						</Card>
					</div>
				</div>
			</div>
		);
	}
}

export default Email;
