import React from "react";
import { connect } from "react-redux";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormSocialMedia from "../forms/FormSocialMedia";
import Socket from "./../../../services/socket";
import api from "../../../api/social-media";
import { Creators as ConversationActions } from "../../../store/ducks/conversation";
import { bindActionCreators } from "redux";
import AlertError from "../../alerts/AlertError";
import { Link } from "react-router-dom";

class SocialMediaModal extends React.Component {
  state = {
    qrCodeCreated: false,
    qrCodeImage: "",
    loadingQrCode: false,
  };

  componentDidMount() {
    if (this.props.editModalOpen) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }

    const { company_id } = this.props;

    Socket.private(`whatsapp-login-${company_id}`).listen(
      "WhatsAppLoginSuccess",
      (e) => {
        window.location.reload();
      }
    );

    Socket.private(`whatsapp-qrcode-${company_id}`).listen(
      "WhatsAppQRCodeCreated",
      (e) => {
        this.setState({
          qrCodeCreated: true,
          qrCodeImage: e.qrCode.file_name,
          loadingQrCode: false,
        });
      }
    );
  }

  componentWillUnmount() {
    const { company_id } = this.props;

    Socket.leave(`whatsapp-login-${company_id}`);
    Socket.leave(`whatsapp-qrcode-${company_id}`);
  }

  save = () => {
    if (this.props.media.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  sendTextMessage = () => {};

  createQrCode = () => {
    this.setState({
      loadingQrCode: true,
    });

    api.media
      .createQrCode()
      .then((res) => {})
      .catch((err) => {
        this.setState({
          error: err.response.data,
          loadingQrCode: false,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);
      });
  };

  render() {
    const {
      media,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      loginSuccess,
      phone_number,
      connected,
      showNavigation = true,
    } = this.props;

    const { qrCodeCreated, qrCodeImage, loadingQrCode } = this.state;

    return (
      <Modal
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          {this.state.error ? (
            this.state.error.no_plan ? (
              <AlertError
                style={{
                  zIndex: 10000,
                  padding: "5px 10px",
                  marginTop: "-25px",
                  width: 300,
                }}
                message={this.state.error.message}
              />
            ) : (
              <AlertError
                style={{
                  zIndex: 10000,
                  padding: "5px 10px",
                  marginTop: "-25px",
                  width: 300,
                }}
                message={this.state.error.message}
              >
                <Link to="/company" style={{ color: "blue" }}>
                  Planos
                </Link>
              </AlertError>
            )
          ) : null}
          <Modal.Description>
            <div className="holder_media">
              <div className="form-media">
                <FormSocialMedia
                  media={media}
                  phone_number={phone_number}
                  qrCodeCreated={qrCodeCreated}
                  loginSuccess={loginSuccess}
                  qrCodeImage={qrCodeImage}
                  loadingQrCode={loadingQrCode}
                  createQrCode={this.createQrCode}
                  onChange={onChange}
                  onChecked={onChecked}
                  connected={connected}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        {media.id ? (
          <Modal.Actions>
            {showNavigation && (
              <Button.Group>
                <Button
                  icon
                  onClick={handlePrevious}
                  disabled={previousButtonEnabled}
                >
                  <Icon name="left arrow" />
                </Button>
                <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
                  <Icon name="right arrow" />
                </Button>
              </Button.Group>
            )}
            <Button.Group>
              <Button onClick={handleClose}>Cancelar</Button>
              <Button.Or />
              <Button
                positive
                onClick={this.save}
                loading={loading}
                disabled={loading}
              >
                Salvar
              </Button>
            </Button.Group>
          </Modal.Actions>
        ) : (
          ""
        )}
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

const mapStateToProps = (state) => {
  return {
    company_id: state.user.user.company_id,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialMediaModal);
