import React from "react";
import {
  Form,
  Input,
  Checkbox,
  TextArea,
  Icon,
  Loader
} from "semantic-ui-react";
import qrcode from "../../../assets/img/qrcode.jpg";

const FormSocialMedia = ({
  media,
  onChange,
  onChecked,
  handleGroupAddition,
  createQrCode,
  loadingQrCode,
  qrCodeCreated,
  qrCodeImage,
  loginSuccess,
  phone_number,
  connected
}) => {
  return (
    <Form>
      {media.id ? (
        <div className="holder_social">
          <span className="numero_da_conta">{media.phone_number}</span>
          <span
            className="numero_da_conta"
            name="phone_number"
            onChange={onChange}
          >
            {phone_number}
          </span>
          <Form.Field>
            <Checkbox
              label="Resposta Automática"
              name="auto_reply"
              checked={media.auto_reply ===1 || media.auto_reply === true? true : false}
              onChange={onChecked}
            />
          </Form.Field>
          <Form.Field>
            <Checkbox
              label="Envio em Massa?"
              name="can_spam"
              checked={media.can_spam ===1 ||media.can_spam === true? true : false }
              onChange={onChecked}
            />
          </Form.Field>
          <Form.Field>
            <Checkbox
              label="Pode Responder?"
              name="can_reply"
              checked={media.can_reply === 1||media.can_reply === true ? true : false}
              onChange={onChecked}
            />
          </Form.Field>
          <Form.Field>
            <Checkbox
              label="Atendimento Automatizado?"
              name="intelliway_integration"
              checked={media.intelliway_integration === 1 || media.intelliway_integration === true ? true :false}
              onChange={onChecked}
            />
          </Form.Field>
          <Form.Field
            autoComplete="off"
            control={Input}
            label="Nome do Assistente Virtual:"
            name="virtual_assistant"
            onChange={onChange}
            placeholder="Assistente Virtual"
            value={media.virtual_assistant}
          />

          <Form.Field>
            <Checkbox
              label="Enviar email quando a conta perder autenticação?"
              name="send_email_on_deauthentication"
              checked={media.send_email_on_deauthentication ===1 || media.send_email_on_deauthentication === true? true : false}
              onChange={onChecked}
            />
          </Form.Field>

          <Form.Field
            control={TextArea}
            label="Legenda:"
            name="auto_reply_message"
            onChange={onChange}
            value={media.auto_reply_message}
          />
        </div>
      ) : (
        ""
      )}
      {media.id ? (
        <div className="holder_social">
          <div className="bloco_qrcode">
            {loadingQrCode && (
              <div className="loading_qrcode">
                <Loader active inline="centered" size="huge" />
              </div>
            )}
            <img
              src={qrCodeCreated ? qrCodeImage : qrcode}
              alt=""
              className={
                !qrCodeCreated || loginSuccess
                  ? loadingQrCode
                    ? "apagado"
                    : "inativo"
                  : ""
              }
            />
            {media.connected === 1 ? (
              ""
            ) : (
              <div>
                {!qrCodeCreated && !loadingQrCode && (
                  <button onClick={createQrCode}>
                    <Icon name="redo alternate" color="white" />
                    Clique para carregar o código QR Code
                  </button>
                )}
              </div>
            )}

            {media.connected === 1 ? (
              <div className="conectado">
                <svg
                  className="checkmark"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 52 52"
                >
                  <circle
                    className="checkmark__circle"
                    cx="26"
                    cy="26"
                    r="25"
                    fill="none"
                  />
                  <path
                    className="checkmark__check"
                    fill="none"
                    d="M14.1 27.2l7.1 7.2 16.7-16.8"
                  />
                </svg>
              </div>
            ) : (
              ""
            )}
            {loginSuccess && (
              <div className="conectado">
                <svg
                  className="checkmark"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 52 52"
                >
                  <circle
                    className="checkmark__circle"
                    cx="26"
                    cy="26"
                    r="25"
                    fill="none"
                  />
                  <path
                    className="checkmark__check"
                    fill="none"
                    d="M14.1 27.2l7.1 7.2 16.7-16.8"
                  />
                </svg>
              </div>
            )}
          </div>
        </div>
      ) : (
        <div className="holder_social full">
          <div className="bloco_qrcode">
            {loadingQrCode && (
              <div className="loading_qrcode">
                <Loader active inline="centered" size="huge" />
              </div>
            )}
            <img
              src={qrCodeCreated ? qrCodeImage : qrcode}
              alt=""
              className={
                !qrCodeCreated || loginSuccess
                  ? loadingQrCode
                    ? "apagado"
                    : "inativo"
                  : ""
              }
            />
            {!qrCodeCreated && !loadingQrCode && (
              <button onClick={createQrCode}>
                <Icon name="redo alternate" color="white" />
                Clique para carregar o código QR Code
              </button>
            )}
            {loginSuccess && (
              <div className="conectado">
                <svg
                  className="checkmark"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 52 52"
                >
                  <circle
                    className="checkmark__circle"
                    cx="26"
                    cy="26"
                    r="25"
                    fill="none"
                  />
                  <path
                    className="checkmark__check"
                    fill="none"
                    d="M14.1 27.2l7.1 7.2 16.7-16.8"
                  />
                </svg>
              </div>
            )}
          </div>
          {loadingQrCode ? (
            <div className="instrucoes">
              <p>Gerando QrCode</p>
              <ul>
                <li>Aguarde vinte segundos para a conexão se estabelecer.</li>
              </ul>
            </div>
          ) : (
            <div className="instrucoes">
              <p>Autenticação Whatsapp</p>
              <ul>
                <li>Aguarde o código aparecer na tela.</li>
                <li>Leia o código com o Whatsapp no seu celular.</li>
                <li>Aguarde vinte segundos para a conexão se estabelecer.</li>
              </ul>
            </div>
          )}
        </div>
      )}
    </Form>
  );
};

export default FormSocialMedia;
