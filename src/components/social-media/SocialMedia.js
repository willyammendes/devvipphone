import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../../api/social-media";
import DataTable from "../table/DataTable";
import SocialMediaModal from "./modal/SocialMediaModal";

import AlertSuccess from "../alerts/AlertSuccess";
import AlertError from "../alerts/AlertError";
import { Link } from "react-router-dom";

class SocialMedia extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingQrCode: false,
    qrCodeCreated: false,
    qrCodeImage: null,
    loginSuccess: false,
    save_alert: false,
    action_disconnect: true,
    action_reconnect: true,
    phone_icon: false,
    phoneNumber: "",
    connected: "",
  };

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );
    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "" }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
        };

    newData.webhook_active = true;
    newData.send_campaing = true;
    newData.auto_reply = false;
    newData.auto_reply_image = false;
    newData.auto_reply_image_path = false;
    newData.media_type = 1;
    newData.active = 1;
    newData.connected = 0;
    newData.can_reply = 0;
    newData.can_spam = 1;
    newData.intelliway_integration = 0;
    newData.send_email_on_deauthentication = 0;
    newData.virtual_assistant = "Assistente Automático";

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex =
      dataIndex < records.length - 1 ? dataIndex + 1 : records.length - 1;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const { records, selectedDataIndex: dataIndex } = this.state;
    const selectedDataIndex = dataIndex - 1 > 0 ? dataIndex - 1 : 0;
    const selectedDataId = records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    return await api.media.fetchAll(params).then((res) => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false,
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.media
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,

            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          error: err.response.data,
        });

        setTimeout(() => {
          this.setState({
            error: null,
          });
        }, 10000);

        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          loadingQrCode: false,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });
    api.media
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            {
              ...data.data,
            },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  delete = (id) => api.media.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      loadingQrCode: false,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      action_reconnect,
      action_disconnect,
      connected,
      phone_icon,
    } = this.state;

    const media = records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Whatsapp</h1>
                {this.state.save_alert && <AlertSuccess />}
              </div>
              <div className="SocialMediaTable">
                <DataTable
                  loading={loading}
                  onAddClick={this.newDataClick}
                  csv={records}
                  columns={columns}
                  phone_icon={phone_icon}
                  action_disconnect={action_disconnect}
                  action_reconnect={action_reconnect}
                  data={records.map((r) => {
                    return {
                      ...r,
                      connected:
                        r.connected === 1
                          ? "Conectado"
                          : r.connected === 0
                          ? "Desconectado"
                          : r.connected === 2
                          ? "Não Autenticado"
                          : "",
                      can_spam: r.can_spam ? "Sim" : "Não",
                      can_reply: r.can_reply ? "Sim" : "Não",
                      auto_reply: r.auto_reply ? "Sim" : "Não",
                      battery: <span>{r.battery}%</span>,
                    };
                  })}
                  totalRecords={total_records}
                  messageError={
                    this.state.errors.status > 0
                      ? this.state.errors.data.message
                      : ""
                  }
                  onDelete={(id) => this.delete(id)}
                  onEditClick={(d) => this.select(d.id)}
                  fetchData={this.fetchRecords}
                />
              </div>
              {this.state.error ? (
                this.state.error.no_plan ? (
                  <AlertError
                    style={{
                      zIndex: 10000,
                      padding: "5px 10px",
                      marginTop: "-25px",
                      width: 300,
                    }}
                    message={this.state.error.message}
                  />
                ) : (
                  <AlertError
                    style={{
                      zIndex: 10000,
                      padding: "5px 10px",
                      marginTop: "-25px",
                      width: 300,
                    }}
                    message={this.state.error.message}
                  >
                    <Link to="/company" style={{ color: "blue" }}>
                      Planos
                    </Link>
                  </AlertError>
                )
              ) : null}

              {selectedDataIndex !== -1 ? (
                <SocialMediaModal
                  handleClose={this.handleCloseEditModal}
                  loginSuccess={this.state.loginSuccess}
                  phone_number={this.state.phoneNumber}
                  connected={connected}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  handleGroupAddition={this.addGroup}
                  media={media}
                  modalHeader={
                    media.id
                      ? `Edição do ${media.virtual_assistant}`
                      : `Novo Número`
                  }
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    company_id: state.user.user.company_id,
  };
};

export default connect(mapStateToProps)(SocialMedia);
