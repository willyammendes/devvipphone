import React, { Component } from "react";
import api from "../../api/social-media";
import { Dropdown } from "semantic-ui-react";

class DropdownMedia extends Component {
  state = {
    media: {
      phone_number: "",
    },
    options: [],
    loading: false,
    search: "",
    openModal: false,
  };

  handleMediaAddition = (e) => {};

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({ search: searchQuery });
    this.timer = setTimeout(this.onSearchMedia, 300);
  };
  componentWillMount() {
    api.media.fetchAll().then((media) => {
      const { spamOnly } = this.props;

      const data = spamOnly ? media.data.filter((c) => c.can_spam) : media.data;

      this.setState({
        options: data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.phone_number,
        })),
        loading: false,
      });
    });
  }
  componentDidMount() {}

  onSearchMedia = async () => {
    this.setState({ loading: true });
    const { search } = this.state;

    await api.media.fetchAll({ search }).then((media) => {
      this.setState({
        options: media.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.phone_number,
        })),
        loading: false,
      });
    });
  };

  onChange = (e, { name, value }) => {
    this.setState((state) => ({
      media: { ...state.media, [name]: value },
    }));
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { media } = this.state;

    this.setState({
      loading: true,
    });

    return api.media
      .submit(media)
      .then((data) => {
        this.setState((state) => ({
          options: [...state.options].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.phone_number,
          }),
          loading: false,
          media: {
            phone_number: "",
          },
          openModal: !state.openModal,
        }));

        this.props.onSelectMedia(data.data.id);
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  toggle = (e, { name, value }) => {
    this.setState((state) => ({
      openModal: !state.openModal,
      media: {
        phone_number: value,
      },
    }));
  };

  render() {
    const { options, loading } = this.state;

    return (
      <div>
        <Dropdown
          placeholder="Pesquise uma Conta do Whatsapp"
          fluid
          search
          selection
          scrolling
          multiple
          additionLabel="Adicionar: "
          name="accounts"
          value={this.props.accounts}
          onSearchChange={this.onSearchChange}
          onChange={(e, { value, text }) =>
            this.props.onSelectMedia(value, text)
          }
          options={options}
          onAddItem={this.toggle}
          closeOnChange
          loading={loading}
          clearable
        />
      </div>
    );
  }
}

export default DropdownMedia;
