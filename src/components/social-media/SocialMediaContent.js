import React from "react";
import PopoverForm from "../semantic/Popover";
import { Table } from "semantic-ui-react";

const SocialMediaContent = ({ socialmedia }) => (
  <Table.Body>
    {socialmedia.map((socialmedia, i) => (
      <Table.Row key={`socialmedia-menu-${i * 1}`}>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.id}</span>
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.accountnumber}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.activationdate}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.automaticanswer}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.automaticmessage}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.status}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.mass}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.cananswer}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.automaticattendance}</span>
          <PopoverForm />
        </Table.Cell>
        <Table.Cell className="dataEditar" key={socialmedia.id}>
          <span>{socialmedia.virtualassistant}</span>
          <PopoverForm />
        </Table.Cell>
      </Table.Row>
    ))}
  </Table.Body>
);

export default SocialMediaContent;
