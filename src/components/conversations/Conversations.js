import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import PerfectScrollbar from "react-perfect-scrollbar";
import apiConversation from "../../api/conversation";
import apimedia from "../../api/social-media";
import apigroup from "../../api/group";
import apiclient from "../../api/client";
import apiticket from "../../api/ticket";
import ConversationCard from "../conversation-card/ConversationCard";
import LoaderComponent from "../semantic/Loading";
import SearchConversation from "../search-conversation/SearchConversation";
import Socket from "../../services/socket";
import { withRouter } from "react-router";
import audio from "../../assets/sound/notification.mp3";

const source = new Audio(audio);

function toggleClass() {
  const element = document.getElementsByName("chat");
  for (let i = 0; i < element.length; i += 1) {
    element[i].classList.toggle("open_chat");
  }
}

class Conversations extends React.Component {
  state = {
    contacts: [],
    media: [],
    contact: {},
    conversas: [],
    loading: false,
    hasMore: true,
    open: false,
    modalOpen: false,
    selectedDataIndex: -1,
    loadingGroups: false,
    loadingClients: false,
    groups: [],
    clients: [],
    parameters: [],
    conversationsLoaded: [],
    animateCard: "",
    executed: false,
    search: "",
    animateClose: "",
    ticket: [],
    conversationOpened: {},
    scrollEnd: true,
    allLoaded: false,
    conversationLoading: false,
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };
  onSearchConversation = (e) => {
    clearTimeout(this.timer);
    this.setState({ search: e.target.value, allLoaded: false, loading: true });
    this.timer = setTimeout(this.searchConversation, 1000);
  };

  searchConversation = (e) => {
    const { search } = this.state;
    this.props.fetchSearch(search).then(() => {
      this.setState({
        loading: false,
      });
    });
  };
  cleanSearch = () => {
    this.setState(
      {
        search: "",
      },
      () => {
        this.searchConversation();
      }
    );
  };
  onSearchClientChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then((clients) => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map((c) => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false,
        });
      });
    }
  };

  onSelectGroup = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  addGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingGroups: true });

    apigroup.group
      .submit({ name: value })
      .then((data) => {
        this.setState({
          groups: [...this.state.groups].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name,
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              groups: [...records[dataIndex].groups].concat(data.data.id),
            },
            ...records.slice(dataIndex + 1),
          ],
        });
      })
      .then(() => {
        this.setState({ loadingGroups: false });
      });
  };

  fetchClients = async () => {
    this.setState({ loadingClients: true });
    await apiclient.client.fetchAll().then((clients) => {
      this.setState({
        clients: clients.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingClients: false,
      });
    });
  };

  fetchGroups = async () => {
    this.setState({ loadingGroups: true });
    await apigroup.group.fetchAll().then((groups) => {
      this.setState({
        groups: groups.data.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
        loadingGroups: false,
      });
    });
  };

  componentWillMount() {
    this.fetchGroups();
    this.fetchClients();

    if (window.webkitNotifications) {
    } else {
    }

    //Após o carregamento da página
    document.addEventListener("DOMContentLoaded", function() {
      //Se não tiver suporte a Notification manda um alert para o usuário
      if (!Notification) {
        alert(
          "Desktop notifications not available in your browser. Try Chromium."
        );
        return;
      }

      //Se não tem permissão, solicita a autorização do usuário
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });

    //Após o carregamento da página
    document.addEventListener("DOMContentLoaded", function() {
      //Se não tiver suporte a Notification manda um alert para o usuário
      if (!Notification) {
        alert(
          "Desktop notifications not available in your browser. Try Chromium."
        );
        return;
      }

      //Se não tem permissão, solicita a autorização do usuário
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });
    Socket.private(
      `conversation-auto-reply-changed-${this.props.user.company_id}`
    ).listen("ConversationAutoReplyChanged", this.enableAutoReply);

    Socket.private(
      `conversation-archived-${this.props.user.company_id}`
    ).listen("ConversationArchived", this.archiveConversation);

    Socket.private(`ticket-changed-owner-${this.props.user.company_id}`).listen(
      "TicketChangedOwner",
      this.ticketOwnerChanged
    );

    Socket.private(`conversation-read-${this.props.user.company_id}`).listen(
      "ConversationRead",
      this.conversationRead
    );

    Socket.private(`new-ticket-${this.props.user.company_id}`).listen(
      "NewTicketCreated",
      this.newTicketCreated
    );

    apimedia.media.fetchData().then((media) => {
      media.map((d) =>
        Socket.private(
          `messages-${this.props.user.company_id}-${d.phone_number}`
        ).listen("MessageReceived", this.newMessageReceived)
      );

      media.map((d) =>
        Socket.private(
          `message-updated-${this.props.user.company_id}-${d.phone_number}`
        ).listen("MessageUpdated", this.messageUpdated)
      );
    });
  }

  componentDidMount() {
    this.setState({
      conversationsLoaded: this.props.conversation.data.map((c) => c.id),
    });

    const { conversationId, ticketId } = this.props.match.params;

    if (
      !!!this.props.conversation.data.find(
        (c) => c.id === Number(conversationId)
      )
    ) {
      this.props.getConversation(conversationId).then((data) => {
        this.props.conversationSelected(
          this.props.conversation.data.find(
            (c) => c.id === Number(conversationId)
          )
        );

        if (ticketId) {
          setTimeout(() => {
            const elmnt = document.getElementById(`ticket-${ticketId}`);
            if (elmnt) {
              //elmnt.scrollIntoView();
            }
          }, 5000);
          this.props.setActiveTicket(ticketId, null);
        }
      });
    } else {
      if (!this.state.conversationLoading) {
        this.props.conversationSelected(
          this.props.conversation.data.find(
            (c) => c.id === Number(conversationId)
          )
        );

        if (ticketId) {
          setTimeout(() => {
            const elmnt = document.getElementById(`ticket-${ticketId}`);
            if (elmnt) {
              //elmnt.scrollIntoView();
            }
          }, 5000);

          this.props.setActiveTicket(ticketId, null);
        }
      }
    }
  }
  newTicketCreated = (e) => {
    const conversation = this.props.conversations.find(
      (c) => c === e.ticket.conversation_id
    );

    if (conversation) {
      const filter = JSON.stringify([["tickets.id", "=", e.ticket.id]]);

      apiticket.ticket.fetchAll({ filter }).then((res) => {
        if (!!res.length) {
          this.props.addTicket(e.ticket);
        }
      });
    } else {
      const filter = JSON.stringify([
        ["conversations.id", "=", e.ticket.conversation_id],
      ]);

      apiConversation.conversations.fetchAll({ filter }).then((res) => {
        if (!!res.length) {
          this.props.getConversation(e.ticket.conversation_id);

          this.setState({
            conversationsLoaded: [
              ...this.props.conversations.map((c) => c.id),
              e.ticket.conversation_id,
            ],
          });
        }

        const filter = JSON.stringify([["tickets.id", "=", e.ticket.id]]);

        apiticket.ticket.fetchAll({ filter }).then((res) => {
          if (!!res.length) {
            this.props.addTicket(e.ticket);
          }
        });
      });
    }
  };

  enableAutoReply = (e) => {
    const { conversation_id, auto_reply } = e.conversation;
    const activeConversation = this.props.conversation.data.find(
      (c) => c.id === conversation_id
    );

    if (activeConversation) {
      const conversationId = activeConversation.id;

      const autoReply = {
        auto_reply,
        conversation_id: activeConversation.id,
      };

      this.props.changeAutoReply(conversationId, autoReply);
    }
  };

  ticketOwnerChanged = (e) => {
    let ticket = e.ticket;

    ticket.user_id = e.user_id;
    if (this.props.user.id !== e.user_id) {
      this.props.ticketUserChanged(ticket);
    }
  };

  archiveConversation = (e) => {
    const { id: conversationId } = e.conversation;

    const conversation = this.props.conversation.data.find(
      (c) => c.id === conversationId
    );

    if (conversation) {
      this.props.archiveConversation({ conversation: e.conversation });
    }
  };

  conversationRead = (e) => {
    const {
      conversation_id: conversationId,
      unread_messages: unreadMessages,
    } = e.conversation;
    const conversation = this.props.conversation.data.find(
      (c) => c.id === conversationId
    );

    if (conversation) {
      const data = {
        unreadMessages,
        conversationId,
      };

      this.props.changeUnreadMessages(data);
    }
  };

  conversationUnreadUpdated = (e) => {
    const {
      conversation_id: conversationId,
      unread_messages: unreadMessages,
    } = e.conversation;

    const conversation = this.props.conversation.data.find(
      (c) => c.id === conversationId
    );

    if (conversation) {
      const data = {
        unreadMessages,
        conversationId,
      };

      this.props.changeUnreadMessages(data);
    }
  };
  scrollBottom = () => {
    const body = document.querySelector(".chat .scrollbar-container");
    const boxes = document.querySelector(".wrap_boxes");
    if (body) {
      body.scrollTo(0, boxes.scrollHeight);
    }
  };

  notify = (e) => {
    if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      new Notification(e.message.contact_name + ":", {
        body: e.message.message,
      });
      source.play();
    } else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function(permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          new Notification(e.message.contact_name + ":", {
            body: e.message.message,
          });
          source.play();
        }
      });
    }
  };

  newMessageReceived = (e) => {
    const conversation = this.props.conversations.find(
      (c) => c.id === e.message.conversation_id
    );

    if (conversation) {
      this.props.addTextMessage(e.message);

      this.notify(e);
    } else {
      const filter = JSON.stringify([
        ["conversations.id", "=", e.message.conversation_id],
      ]);

      apiConversation.conversations.fetchAll({ filter }).then((res) => {
        if (!!res.length) {
          this.props.getConversation(e.message.conversation_id);

          this.setState({
            conversationsLoaded: [
              ...this.props.conversations.map((c) => c.id),
              e.message.conversation_id,
            ],
          });
        }
      });
    }
  };

  messageUpdated = (e) => {
    this.props.updateMessageStatus(e.message);
    this.scrollBottom();
  };

  selectContact = (contact, modalOpen) => {
    this.setState({
      contact,
      modalOpen,
    });
    this.setState({ modalOpen: true });
  };

  onSelectClient = (e, { name, value }) => {
    this.setState({ contact: { ...this.state.contact, [name]: value } });
  };

  onSearchClientChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  onSearchGroupChange = (e) => {
    clearTimeout(this.timer);
    this.setState({ searchGroup: e.target.value });
    this.timer = setTimeout(this.onSearchGroup, 300);
  };

  onSearchGroup = () => {
    const { searchGroup } = this.state;

    if (searchGroup.length > 2) {
      this.setState({ fetchingGroups: true });

      apigroup.group.search(searchGroup).then((groups) => {
        this.setState({
          groups: [...this.state.groups].concat(
            groups.map((c) => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingGroups: false,
        });
      });
    }
  };

  onSelectGroup = (e, { name, value }) => {
    this.setState({
      contact: {
        ...this.state.contact,
        [name]: value,
      },
    });
  };

  onScrollEnd = (conversation) => {
    if (this.state.scrollEnd && !this.state.loading) {
      const { conversationsLoaded, allLoaded } = this.state;
      if (conversation.data.length > 9 && !allLoaded) {
        const filter = {
          search: this.state.search,
          filter: this.state.search
            ? ""
            : JSON.stringify([["archived", "=", 0]]),
          skip: conversation.data.length,
          take: 10,
        };
        this.setState({
          loading: true,
          scrollEnd: false,
        });
        this.props.fetchAll(filter).then((res) => {
          const LoadedConversations = this.props.conversation.data.map(
            (c) => c.id
          );
          if (res.data.length === 0) {
            this.setState({
              allLoaded: true,
            });
          }
          this.setState(
            {
              conversationsLoaded: {
                ...conversationsLoaded,
                ...LoadedConversations,
              },
            },
            () => {
              this.setState({
                scrollEnd: true,
                loading: false,
              });
            }
          );
        });
      }
    }
  };

  onConversationSelected = (id) => {
    if (!this.state.conversationLoading) {
      this.props.conversationSelected(
        this.props.conversation.data.find((c) => c.id === id)
      );
      this.setState({
        conversationLoading: true,
        loading: true,
      });
      setTimeout(
        function() {
          this.setState({ conversationLoading: false, loading: false });
        }.bind(this),
        3500
      );
    }
  };

  render() {
    const {
      conversation,
      parameter,
      activeConversationId,
      conversations,
    } = this.props;

    const {
      animateCard,
      contact,
      loadingGroups,
      loadingClients,
      search,
      loading,
    } = this.state;

    return (
      <div className="conversasBusca">
        <SearchConversation
          contact={contact}
          clients={this.state.clients}
          groups={this.state.groups}
          changeclassName={this.changeClass}
          searchQuery={search}
          onSearchConversation={this.onSearchConversation}
          cleanSearch={this.cleanSearch}
          placeholder="Pesquisar Contatos"
        />

        <div className="Conversas">
          {loading ? (
            <div className="loading-conversa">
              <LoaderComponent />
            </div>
          ) : (
            ""
          )}
          {conversation.loading ? (
            <div className="loading-conversa">
              <LoaderComponent />
            </div>
          ) : (
            <PerfectScrollbar
              onYReachEnd={() => this.onScrollEnd(this.props.conversation)}
            >
              {conversations.filter((c) =>
                search.length < 1
                  ? c.archived === 0 || c.archived === false
                  : c.archived === 1 ||
                    c.archived === true ||
                    (c.archived === 0 || c.archived === false)
              ).length > 0 ? (
                <div className="wrap-Conversas">
                  {conversations
                    .filter((c) =>
                      search.length < 1
                        ? c.archived === 0 || c.archived === false
                        : c.archived === 1 ||
                          c.archived === true ||
                          (c.archived === 0 || c.archived === false)
                    )
                    .map((c, i) => {
                      return (
                        <div onClick={toggleClass} key={`conversation-${c.id}`}>
                          <ConversationCard
                            active={activeConversationId === c.id}
                            conversation={c}
                            animationTrue={
                              animateCard.activeConversationId === c.id
                            }
                            animateClose={
                              this.state.animateClose.activeConversationId ===
                              c.id
                            }
                            user={this.props.user}
                            idCard={c.contact.id}
                            idConversation={c.id}
                            animateCard={animateCard}
                            contact={contact}
                            parameter={parameter}
                            clients={this.state.clients}
                            groups={this.state.groups}
                            changeclassName={this.changeClass}
                            onSelectGroup={this.onSelectGroup}
                            onSelectClient={this.onSelectClient}
                            onSearchClientChange={this.onSearchClientChange}
                            handleGroupAddition={this.addGroup}
                            loadingGroups={loadingGroups}
                            loadingClients={loadingClients}
                            handleConversationClick={() =>
                              this.onConversationSelected(c.id)
                            }
                          />
                        </div>
                      );
                    })}
                </div>
              ) : (
                <div className="wrap-Conversas">
                  <span className="noTickets">Nenhum ticket em aberto</span>
                </div>
              )}
            </PerfectScrollbar>
          )}
        </div>
      </div>
    );
  }
}

Conversations.propTypes = {
  conversation: PropTypes.shape({
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        tickets: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            user: PropTypes.any.isRequired,
          })
        ).isRequired,
      }).isRequired
    ),
  }).isRequired,
  activeConversationId: PropTypes.number.isRequired,
  conversationSelected: PropTypes.func.isRequired,
  fetchSearch: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

const mapStateToProps = ({ conversation, user, media }) => {
  return {
    user: user.user,
    companies: user.user.companies,
    conversation,
    media,
    parameter: user.user.companies[0].parameters,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter ? conversation.filter : "",
    conversations: !conversation.data ? [] : Object.values(conversation.data),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Conversations));
