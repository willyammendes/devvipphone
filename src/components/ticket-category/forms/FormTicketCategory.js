import React from "react";
import { Form, Input } from "semantic-ui-react";
import DropdownCategories from "../DropdownCategories";

const FormTicketCategory = ({
    ticketcategory,
    onChange,
    onChecked,
    handleCategoryChange,
    handleGroupAddition
}) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Descrição"
                    name="description"
                    onChange={onChange}
                    value={ticketcategory.description}
                />
                <div className="field">
                    <label>Categoria Pai</label>
                    <DropdownCategories
                        handleCategoryChange={handleCategoryChange}
                        onSelectCategory={handleCategoryChange}
                        ticket_category_id={ticketcategory.ticket_category_id}
                        allowAdditions
                    />
                </div>
            </Form.Group>
        </Form>
    );
};

export default FormTicketCategory;
