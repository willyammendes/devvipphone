import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Icon, Button, Modal } from "semantic-ui-react";
import Activities from "../summary/Activities";
import apiUser from "../../api/users";
import api from "../../services/api";
import auth from "../../store/store";
import Validator from "validator";
import InlineError from "../messages/InlineError";
import InputMask from "react-input-mask";
import DropdownContacts from "../contact/DropdownContacts";

function Profile({ name, email, phone_number, avatar, id }) {
  // state component
  const [editName, setEditName] = useState(true);
  const [editEmail, setEditEmail] = useState(true);
  const [editPhone, setEditPhone] = useState(true);
  const [valueName, setValueName] = useState(name);
  const [valueEmail, setValueEmail] = useState(email);
  const [valuePhone, setValuePhone] = useState(phone_number);
  const [maskNumber, setMaskNumer] = useState("");
  const [numberTest, setNumberTest] = useState(valuePhone);
  const [phoneIsValid, setPhoneIsValid] = useState(true);
  const [userContactModalOpen, toggleUserContactModal] = useState(false);
  const [contacts, setContacts] = useState([]);
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  // capitalize for set name in camecase
  const capitalize = (str) => {
    str = str.split(" ");

    for (let i = 0, x = str.length; i < x; i++) {
      if (str[i] !== "") str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }

    return str.join(" ");
  };

  useEffect(() => {
    if (/\+55 \([1-9]{2}\) \d{5}-\d{4}/.test(valuePhone)) {
      setMaskNumer("+55 (99) 99999-9999");
    } else {
      setMaskNumer("+55 (99) 9999-9999");
    }
    setNumberTest(valuePhone.replace(/_/, ""));
  }, [valuePhone]);

  useEffect(() => {
    setTimeout(() => {
      setPhoneIsValid(true);
    }, 700);
  }, [phoneIsValid]);

  useEffect(() => {
    apiUser.user.getContacts(id).then((data) => {
      setContacts(data.map((c) => c.id));
      setOptions(data);
    });
  }, []);
  // handle change input tag
  const handleChange = (e) => {
    if (!editName) {
      setEditEmail(true);
      setEditPhone(true);
      setValueName(e.target.value);
    }
    if (!editEmail) {
      setEditPhone(true);
      setEditName(true);
      setValueEmail(e.target.value);
    }
    if (!editPhone) {
      setEditName(true);
      setEditEmail(true);
      setValuePhone(e.target.value);
    }
  };

  const handleContactChange = (value, opt) => {
    setLoading(true);
    api
      .post("/contact-user", {
        contacts: value,
        user_id: id,
      })
      .then((res) => {
        setContacts(res.data.contacts);
        setLoading(false);
      })
      .catch((err) => setLoading(false));
  };

  // format Phone Number 8 digt
  const formatPhoneNumber = (number) => {
    const formatNumber = number.split("");
    formatNumber.pop();
    const t = formatNumber[13];
    const r = formatNumber[14];
    formatNumber[14] = t;
    formatNumber[13] = r;
    return formatNumber.join("");
  };

  // save data in API and alter state redux
  const saveData = async () => {
    const get = await apiUser.user.fetchUser(id);
    const NameOld = get.data.name;
    const emailOld = get.data.email;
    const numberOld = get.data.phone_number;

    if (editName === false) {
      setValueName(capitalize(valueName));
      get.data.name = capitalize(valueName);
      setEditName(!editName);
    }
    if (editEmail === false) {
      if (Validator.isEmail(valueEmail)) {
        get.data.email = valueEmail;
        setEditEmail(!editEmail);
      } else {
        setValueEmail(get.data.email);
      }
    }
    if (editPhone === false) {
      if (!/\+55 \([1-9]{2}\) \d{5}-\d{4}/.test(numberTest)) {
        if (/\+55 \([1-9]{2}\) \d{5}-\d{3}/.test(numberTest)) {
          setEditPhone(!editPhone);
          get.data.phone_number = formatPhoneNumber(valuePhone);
          setEditPhone(!editPhone);
        }
      }
      if (/\+55 \([1-9]{2}\) \d{5}-\d{4}/.test(numberTest)) {
        setEditPhone(!editPhone);
        get.data.phone_number = valuePhone;
        setEditPhone(!editPhone);
      }
      if (
        !/\+55 \([1-9]{2}\) \d{5}-\d{4}/.test(numberTest) &&
        !/\+55 \([1-9]{2}\) \d{5}-\d{3}/.test(numberTest) &&
        !/\+55 \([1-9]{2}\) \d{4}-\d{4}/.test(numberTest)
      ) {
        setPhoneIsValid(!phoneIsValid);
      }
    }
    if (
      get.data.name !== NameOld ||
      get.data.email !== emailOld ||
      (get.data.phone_number !== numberOld && phoneIsValid === true)
    ) {
      await apiUser.user.update(id, get.data);
      auth.dispatch({
        type: "user/SET_CURRENT_USER",
        isAuthenticated: true,
        user: get.data,
      });
    }
  };

  // buttons press ENTER
  const pressHandleButton = (e) => {
    if (e.which === 13) {
      saveData();
    }
    if (editPhone === false) {
      if (!/\+55 \([1-9]{2}\) \d{5}-\d{4}/.test(valuePhone)) {
        setMaskNumer("+55 (99) 99999-9999");
      }
    }
  };

  return (
    <div className="profile CofigNav">
      <div className="headingPage">
        <h2>
          <Icon name="user outline" size="large" />
          {name}
        </h2>
      </div>
      <div className="holderPage">
        <div className="wrap_atividades">
          <Activities filter={id} />
        </div>
        <div className="holder_profile">
          <div className="image_profile">
            <img src={avatar} alt="" />
            <Button
              content="Escolher foto"
              className="button_profile_picture"
              color="blue"
              size="tiny"
            >
              Escolher foto
              <input
                type="file"
                name="myImage"
                accept="image/x-png,image/gif,image/jpeg"
              />
            </Button>
          </div>
          <div className="dados_basicos">
            <h5 className="dados">Dados básicos</h5>
            <table id="vertical-1">
              <tbody>
                <tr>
                  <th>Nome</th>
                  <td>
                    {editName ? (
                      <span className="captalize_span">
                        {valueName}
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="name"
                          onClick={() => setEditName(!editName)}
                        />
                      </span>
                    ) : (
                      <div className="input-profile">
                        <input
                          name="name"
                          value={valueName}
                          onChange={handleChange}
                          onBlur={saveData}
                          autoFocus
                          onKeyPress={pressHandleButton}
                        />
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="name"
                          onClick={() => setEditName(!editName)}
                        />
                      </div>
                    )}
                  </td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>
                    {editEmail ? (
                      <span>
                        {email}
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="email"
                          onClick={() => setEditEmail(!editEmail)}
                        />
                      </span>
                    ) : (
                      <div className="input-profile">
                        <input
                          name="email"
                          value={valueEmail}
                          onChange={handleChange}
                          onBlur={saveData}
                          autoFocus
                          onKeyPress={pressHandleButton}
                        />
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="email"
                          onClick={() => setEditEmail(!editEmail)}
                        />
                      </div>
                    )}
                    {Validator.isEmail(valueEmail) ? (
                      ""
                    ) : (
                      <InlineError text="Email Inválido" />
                    )}
                  </td>
                </tr>
                <tr>
                  <th>Perfil</th>
                  <td>
                    <span>Administrador</span>
                  </td>
                </tr>
                <tr>
                  <th>Telefone</th>
                  <td>
                    {editPhone ? (
                      <span>
                        {phone_number}
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="phone_number"
                          onClick={() => setEditPhone(!editPhone)}
                        />
                      </span>
                    ) : (
                      <div className="input-profile">
                        <InputMask
                          mask={maskNumber}
                          name="phone_number"
                          value={valuePhone}
                          onChange={handleChange}
                          onBlur={saveData}
                          autoFocus
                          onKeyPress={pressHandleButton}
                        />
                        <Button
                          floated="right"
                          icon="edit"
                          color="green"
                          size="mini"
                          name="phone_number"
                          onClick={() => setEditPhone(!editPhone)}
                        />
                      </div>
                    )}
                    {phoneIsValid ? (
                      ""
                    ) : (
                      <InlineError text="Telefone Inválido" />
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <p>
            <Button content="Alterar senha" color="green" fluid />
          </p>
          <p>
            <Button
              content="Meus Contatos"
              color="orange"
              onClick={() => toggleUserContactModal(!userContactModalOpen)}
              fluid
            />
          </p>
        </div>
      </div>
      <Modal
        open={userContactModalOpen}
        onClose={() => toggleUserContactModal(!userContactModalOpen)}
        closeOnEscape
        closeIcon
        size="large"
      >
        <Modal.Header>Carteira de Contatos</Modal.Header>
        <Modal.Content>
          <DropdownContacts
            onSelectContact={handleContactChange}
            contact_id={contacts}
            allowAdditions={false}
            options={options}
            loading={loading}
            multiple
          />
        </Modal.Content>
      </Modal>
    </div>
  );
}

const mapStateToProps = (state) => ({
  contact: state.user,
  name: state.user.user.name,
  email: state.user.user.email,
  phone_number: state.user.user.phone_number,
  avatar: state.user.user.avatar,
  id: state.user.user.id,
});

export default connect(mapStateToProps)(Profile);
