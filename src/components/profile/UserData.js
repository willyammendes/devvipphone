import React, { Component } from "react";

class UserData extends Component {
	state = {
		user: [],
		loading: true
	};

	componentWillMount() {
		api.user.fetchAll().then(user =>
			this.setState({
				user,
				loading: false,
				showInfo: true
			})
		);
	}
	render() {
		const { loading, user } = this.state;
		
		return (
			<div className="dados_basicos">
					<h5 className="dados">Dados básicos</h5>
					<table id="vertical-1">
						<tbody>
							<tr>
								<th>Nome</th>
								<td>
									<span>
										{name}
										<Popover>{name}</Popover>
									</span>
								</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>
									<span>
										{email}
										<Popover>{email}</Popover>
									</span>
								</td>
							</tr>
							<tr>
								<th>Perfil</th>
								<td>
									<span>
										Administrador
										<Popover>Administrador</Popover>
									</span>
								</td>
							</tr>
							<tr>
								<th>Telefone</th>
								<td>
									<span>
										{phone_number}
										<Popover>{phone_number}</Popover>
									</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
		);
	}
}

export default UserData;
