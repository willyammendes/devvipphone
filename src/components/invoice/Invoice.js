import React, { Component } from "react";
import { Label, Icon, Input } from "semantic-ui-react";
import { DateInput } from "semantic-ui-calendar-react";
import api from "../../api/invoice";
import DataTable from "../table/DataTable";
import InvoiceModal from "./modal/InvoiceModal";
import AlertSuccess from "../alerts/AlertSuccess";
import AlertRed from "../alerts/AlertRed";

class Invoice extends Component {
  state = {
    records: [],
    columns: {},
    order: {},
    errors: "",
    loading: false,
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    save_alert: false,
    delete_alert: false,
  };

  select = (selectedDataId) => {
    const dataIndex = this.state.records.findIndex(
      (c) => c.id === selectedDataId
    );

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });
  };

  handleInlineInput = (e, { name, value }, recordId) => {
    let recordIndex = this.state.records.findIndex((r) => r.id === recordId);
    let record = this.state.records.find((r) => r.id === recordId);
    let records = this.state.records;

    record[name] = value;

    records[recordIndex] = record;

    this.setState({
      ...this.state,
      records,
    });
  };

  handleDateTimeChange = (e, { name, value }, recordId) => {
    this.setState({ loading: true });
    let recordIndex = this.state.records.findIndex((r) => r.id === recordId);
    let record = this.state.records.find((r) => r.id === recordId);
    let records = this.state.records;

    record[name] = value;

    records[recordIndex] = record;

    this.update(recordId);
  };

  newDataClick = () => {
    const { records } = this.state;

    const newData = records[0]
      ? Object.keys(records[0]).reduce(
          (o, key) => Object.assign(o, { [key]: "" }),
          {}
        )
      : {
          event_type: "",
          url: "",
          headers: "",
          headers_value: "",
        };

    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true,
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1,
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  fetchRecords = async (params) => {
    this.setState({ loading: true });

    const { company_id } = this.props;

    return await api.invoice
      .fetchAll({
        ...params,
        filter: company_id
          ? JSON.stringify([
              ["company_pricing_plans.company_id", "=", company_id],
            ])
          : JSON.stringify([]),
      })
      .then((res) => {
        this.setState({
          records: res.data,
          order: res.order,
          columns: res.columns,
          total_records: res.total_records,
          loading: false,
        });
      });
  };

  componentWillMount() {
    this.fetchRecords({ order: "company_pricing_plans.id" });
  }

  handleChange = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleFastMessageChange = (value) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          origin_fast_messages: value,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked,
        },
        ...records.slice(dataIndex + 1),
      ],
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true,
    });
    return api.invoice
      .submit(records[dataIndex])
      .then((data) => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1),
          ],
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
        });
      });
  };

  update = (id) => {
    const { records } = this.state;
    let selectedDataIndex = this.state.selectedDataIndex;

    if (id) {
      selectedDataIndex = records.findIndex((c) => c.id === id);
    }

    const data = records[selectedDataIndex];

    this.setState({ loading: true });

    api.invoice
      .update(data.id, data)
      .then((data) => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1),
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false,
          editingCell: null,
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter((c) => c.id > 0),
        });
      });
  };

  delete = (id) => api.invoice.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter((c) => c.id > 0),
    });
  };

  editCell = (editingCell, name) => {
    this.setState({ editingCell });

    setTimeout(() => {
      document.getElementsByName(name)[0].focus();
    }, 100);
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      editingCell,
    } = this.state;

    const invoice = this.state.records[selectedDataIndex];

    return (
      <div className="pageHolder">
        <div className="holderPage">
          <div className="full">
            <div className="tabela-padrao">
              <div className="headingPage">
                <h1>Faturas</h1>
                {this.state.save_alert && <AlertSuccess />}
                {this.state.delete_alert && <AlertRed />}
              </div>
              <DataTable
                loading={loading}
                csv={records}
                order={"company_pricing_plans.id"}
                hide_actions={true}
                onAddClick={this.newDataClick}
                columns={columns}
                data={records.map((r) => {
                  return {
                    ...r,
                    active: r.active ? "Sim" : "Não",
                    paid_at:
                      editingCell === `paidAt-${r.id}` ? (
                        <DateInput
                          name="paid_at"
                          size="mini"
                          style={{ width: "120px" }}
                          closable
                          clearable
                          iconPosition="left"
                          dateFormat="DD/MM/YYYY"
                          value={r.paid_at}
                          onChange={(a, d) =>
                            this.handleDateTimeChange(a, d, r.id)
                          }
                        />
                      ) : (
                        <div
                          onClick={() =>
                            this.editCell(`paidAt-${r.id}`, "paid_at")
                          }
                          style={{
                            width: "100%",
                            minHeight: "23px",
                          }}
                        >
                          {r.paid_at}
                        </div>
                      ),
                    activated_at:
                      editingCell === `activatedAt-${r.id}` ? (
                        <DateInput
                          name="activated_at"
                          style={{ width: "120px" }}
                          size="mini"
                          closable
                          clearable
                          iconPosition="left"
                          dateFormat="DD/MM/YYYY"
                          value={r.activated_at}
                          onChange={(a, d) =>
                            this.handleDateTimeChange(a, d, r.id)
                          }
                        />
                      ) : (
                        <div
                          onClick={() =>
                            this.editCell(`activatedAt-${r.id}`, "activated_at")
                          }
                          style={{
                            width: "100%",
                            minHeight: "23px",
                          }}
                        >
                          {r.activated_at}
                        </div>
                      ),
                    total_paid:
                      editingCell === `totalPaid-${r.id}` ? (
                        <Input
                          name="total_paid"
                          style={{ width: "120px" }}
                          size="mini"
                          closable
                          icon={{
                            name: "check",
                            link: true,
                            color: "green",
                            onClick: () => this.update(r.id),
                          }}
                          onChange={(a, d) =>
                            this.handleInlineInput(a, d, r.id)
                          }
                          value={r.total_paid}
                        />
                      ) : (
                        <div
                          onClick={() =>
                            this.editCell(`totalPaid-${r.id}`, "total_paid")
                          }
                          style={{
                            width: "100%",
                            minHeight: "23px",
                          }}
                        >
                          {r.total_paid}
                        </div>
                      ),
                    status: (
                      <Label size="tiny">
                        <span className="textCenter">
                          {r.status === "Pago" ? (
                            <Icon name="check" color="green" />
                          ) : (
                            <Icon
                              name="calendar alternate outline"
                              color="red"
                            />
                          )}
                          {r.status}
                        </span>
                      </Label>
                    ),
                    recurrent: r.recurrent ? "Sim" : "Não",
                  };
                })}
                totalRecords={total_records}
                messageError={
                  this.state.errors.status > 0
                    ? this.state.errors.data.message
                    : ""
                }
                cleanErrors={this.cleanErrors}
                onDelete={(id) => this.delete(id)}
                onEditClick={(d) => this.select(d.id)}
                fetchData={this.fetchRecords}
              />

              {selectedDataIndex !== -1 ? (
                <InvoiceModal
                  handleClose={this.handleCloseEditModal}
                  onChange={this.handleChange}
                  onChecked={this.handleChecked}
                  handleNext={this.nextRecord}
                  handlePrevious={this.previousRecord}
                  onClickSave={this.update}
                  onClickAdd={this.submit}
                  handleGroupAddition={this.addGroup}
                  handleFastMessageChange={this.handleFastMessageChange}
                  invoice={invoice}
                  modalHeader={
                    invoice.id
                      ? `Edição da Fatura ${invoice.id}`
                      : "Nova Fatura"
                  }
                  open={editModalOpen}
                  previousButtonEnabled={selectedDataIndex === 0}
                  nextButtonEnabled={selectedDataIndex === records.length - 1}
                  loading={loading}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Invoice;
