import React, { Component } from "react";
import { Form, Input } from "semantic-ui-react";
import ImageUpload from "../forms/ImageUpload";
import UploadDocumentForm from "../../form-message/UploadDocumentForm";
import $ from "jquery";

class FormStep2 extends Component {
	state = {};
	render() {
		const { documentOnloadHandler, clearTmpFile } = this.props;
		return (
			<Form>
				<div className="form-row">
					<div className="field-row">
						<h3>
							Passo 2 - Tipo de Mídia - Escolha Uma, das Duas
							Opções
						</h3>
						<div className="field">
							<label>Enviar imagem</label>
							<ImageUpload />
						</div>
						<div className="campaign_fileupload">
							<input
								type="file"
								accept=".xlsx,.xls,.doc,.docx,.pdf,.txt"
								id="document"
								className="document"
								name="document"
							/>
							{(function() {
								$("#document").change(function(e) {
									if (e.target.files.length > 0) {
										$("#document_name").val(
											e.target.files[0].name
										);

										const file = document.getElementById(
											"document"
										).files[0];
										const reader = new FileReader();
										const ext = file.name.split(".").pop();
										reader.readAsDataURL(file);

										reader.onload = e => {
											documentOnloadHandler(e, file, ext);
										};
									}
								});
							})()}
							<UploadDocumentForm
								onClickUpload={() => {
									$("#document").click();
								}}
								clearTmpFile={clearTmpFile}
							/>
						</div>
						<Form.Field
							control={Input}
							label="Enviar Documento"
							name="file_name"
							type="file"
							icon="upload"
						/>
					</div>
				</div>
			</Form>
		);
	}
}
export default FormStep2;
