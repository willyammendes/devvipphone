import React, { Component } from "react";
import { Form } from "semantic-ui-react";
import DropdownGroups from "../../contacts-groups/DropdownGroups";
import DropdownContacts from "../../contact/DropdownContacts";

class FormStep3 extends Component {
  state = {};
  render() {
    const { campaign, handleGroupChange, handleContactChange } = this.props;
    return (
      <Form>
        <div className="form-row">
          <div className="field-row">
            <h3>Passo 3 - Para quem vai enviar?</h3>
            <div className="field">
              <label>
                <h4>Escolha os Contatos</h4>
                <span>Por Grupo</span>
              </label>
              <DropdownGroups
                onSelectGroup={handleGroupChange}
                groups={campaign.groups.map((c) =>
                  typeof c === "object" ? c.id : c
                )}
                options={campaign.groups}
                allowAdditions={false}
                name="contacts"
              />
            </div>
            <div className="field">
              <label>Por Contato</label>
              <DropdownContacts
                onSelectContact={handleContactChange}
                contact_id={campaign.contacts.map((c) =>
                  typeof c === "object" ? c.id : c
                )}
                allowAdditions={false}
                options={campaign.contacts}
                multiple
              />
            </div>
          </div>
        </div>
      </Form>
    );
  }
}
export default FormStep3;
