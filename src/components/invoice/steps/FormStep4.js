import React, { Component } from "react";
import { Form, Input } from "semantic-ui-react";
import TimeRange from "../forms/TimeRange";

class FormStep4 extends Component {
	state = {};
	render() {
		const { onChange, campaign } = this.props;
		return (
			<Form>
				<div className="form-row">
					<div className="field-row">
						<h3>Passo 4 - Data e Hora do Envio</h3>
						<Form.Group widths="equal">
							<Form.Field
								control={Input}
								label="Data de Inicio:"
								name="start"
								value={campaign.start}
								onChange={onChange}
								type="date"
								icon="calendar alternate"
							/>
							<Form.Field
								control={Input}
								label="Hora"
								name="time"
								value={campaign.time}
								onChange={onChange}
								type="time"
								icon="clock outline"
							/>
						</Form.Group>

						<Form.Group widths="equal">
							<Form.Field
								control={Input}
								label="Hora de Inicio"
								name="min_time"
								onChange={onChange}
								value={campaign.min_time}
								type="time"
							/>
							<Form.Field
								control={Input}
								label="Hora do Fim"
								name="max_time"
								type="time"
								onChange={onChange}
								value={campaign.max_time}
							/>
						</Form.Group>
					</div>
				</div>
			</Form>
		);
	}
}
export default FormStep4;
