import React, { Component } from "react";
import { Form, Input, TextArea } from "semantic-ui-react";

class FormStep1 extends Component {
	state = {};
	render() {
		const { onChange, campaign } = this.props;
		return (
			<Form>
				<div className="form-row">
					<div className="field-row">
						<h3>Passo 1 - Informações Básicas</h3>
						<Form.Field
							control={Input}
							label="Título da Campanha"
							onChange={onChange}
							name="title"
							value={campaign.title}
							placeholder="Título da Campanha"
						/>
						<Form.Field
							control={TextArea}
							label="Mensagem a Ser Enviada"
							name="message"
							onChange={onChange}
							value={campaign.message}
							placeholder="Mensagem"
							icon="pencil"
							rows={6}
						/>
					</div>
				</div>
			</Form>
		);
	}
}
export default FormStep1;
