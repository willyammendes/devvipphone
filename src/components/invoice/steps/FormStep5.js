import React, { Component } from "react";
import { Form, Button } from "semantic-ui-react";
import whatsapp from "../../../assets/img/whatsapp.jpg";
import DropdownMedia from "../../social-media/DropdownMedia";

class FormStep5 extends Component {
	save = () => {
		if (this.props.campaign.id) {
			this.props.onClickSave();
		} else {
			this.props.onClickAdd();
		}
	};

	render() {
		const { campaign, handleMediaChange, loading } = this.props;
		return (
			<Form>
				<div className="form-row">
					<div className="field-row">
						<h3>Passo 5 - Finalizar!</h3>
						<div className="field">
							<label>
								Escolha as contas que podem disparar a campanha:
							</label>
							<DropdownMedia
								onSelectMedia={handleMediaChange}
								accounts={campaign.accounts}
								allowAdditions={false}
							/>
						</div>
						<Button basic color="blue">
							Gravar
						</Button>
					</div>
					<div className="field-row">
						<div className="field">
							<img
								src={whatsapp}
								alt=""
								className="whatsapp-image"
							/>
						</div>
					</div>
				</div>
				<Button
					positive
					onClick={this.save}
					loading={loading}
					disabled={loading}
					className="save_right"
				>
					Salvar
				</Button>
			</Form>
		);
	}
}
export default FormStep5;
