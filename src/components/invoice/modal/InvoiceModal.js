import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormInvoice from "../forms/FormInvoice";
import apiCompany from "../../../api/company";

class InvoiceModal extends React.Component {
  state = {
    show_var: false,
    companies: [],
  };

  componentDidMount() {
    const { open } = this.props;

    if (open) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", (e) => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  componentWillMount() {
    apiCompany.company.fetchAll().then((res) => {
      this.setState({
        companies: res.data,
      });
    });
  }

  save = () => {
    if (this.props.invoice.id) {
      this.props.onClickSave();
    } else {
      this.props.onClickAdd();
    }
  };

  render() {
    const {
      invoice,
      handleClose,
      onChange,
      onChecked,
      open,
      modalHeader = "",
      handleNext,
      handlePrevious,
      previousButtonEnabled,
      nextButtonEnabled,
      loading,
      handleFastMessageChange,
    } = this.props;

    const { companies } = this.state;
    return (
      <Modal
        size="large"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <div className="holder_fastmessage">
              <div className="form-invoice">
                <FormInvoice
                  invoice={invoice}
                  companies={companies}
                  onChange={onChange}
                  onChecked={onChecked}
                  handleFastMessageChange={handleFastMessageChange}
                />
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button
              icon
              onClick={handlePrevious}
              disabled={previousButtonEnabled}
            >
              <Icon name="left arrow" />
            </Button>
            <Button icon onClick={handleNext} disabled={nextButtonEnabled}>
              <Icon name="right arrow" />
            </Button>
          </Button.Group>
          <b> </b>
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default InvoiceModal;
