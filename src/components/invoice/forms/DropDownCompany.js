import React, { Component } from "react";
import api from "../../../api/company";
import { Dropdown } from "semantic-ui-react";

class DropDownCompany extends Component {
  state = {
    options: [],
    loading: false,
    search: "",
  };

  onSearchChange = (e, { searchQuery }) => {
    clearTimeout(this.timer);
    this.setState({ search: searchQuery });
    this.timer = setTimeout(this.onSearchContact, 800);
  };

  componentDidMount() {
    const { options } = this.props;

    if (options) {
      this.setState({
        options: options.map((c) => ({
          key: c.id,
          value: c.id,
          text: c.name,
        })),
      });
    }
  }

  onSearchContact = async () => {
    this.setState({ loading: true });
    const { search } = this.state;

    await api.company.fetchAll({ search }).then((contacts) => {
      this.setState({
        options: this.state.options.concat(
          contacts.data
            .filter((el) => {
              return !this.state.options.some((a) => a.key === el.id);
            })
            .map((c) => ({
              key: c.id,
              value: c.id,
              text: c.name,
            }))
        ),
        loading: false,
      });
    });
  };

  render() {
    const { options, loading } = this.state;
    const { invoice, onChange } = this.props;
    return (
      <Dropdown
        placeholder="Pesquise uma empresa"
        search
        selection
        scrolling
        name="company_id"
        value={invoice.company_id}
        onSearchChange={this.onSearchChange}
        onChange={onChange}
        options={options}
        closeOnChange
        loading={loading}
        clearable
      />
    );
  }
}

export default DropDownCompany;
