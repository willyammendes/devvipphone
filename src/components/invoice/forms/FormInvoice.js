import React from "react";
import { Form, Input, Dropdown } from "semantic-ui-react";
import DropDownCompany from "./DropDownCompany";
import DropDownPricingPlans from "./DropDownPricingPlans";
import { DateInput } from "semantic-ui-calendar-react";

const FormInvoice = ({ invoice, onChange, onChecked }) => {
  return (
    <Form>
      <Form.Group widths="equal">
        <Form.Field>
          <label>Empresa</label>
          <DropDownCompany invoice={invoice} onChange={onChange} />
        </Form.Field>
        <Form.Field>
          <label>Plano</label>
          <DropDownPricingPlans invoice={invoice} onChange={onChange} />
        </Form.Field>
      </Form.Group>
      <Form.Group widths="4">
        <DateInput
          name="due_date"
          closable
          label="Vencimento"
          clearable
          iconPosition="left"
          dateFormat="DD/MM/YYYY"
          value={invoice.due_date}
          onChange={onChange}
        />
        <Form.Field>
          <label>Meio de Pagamento</label>
          <Dropdown
            placeholder="Escolha uma opção"
            search
            selection
            name="payment_type"
            value={0}
            onChange={onChange}
            options={[
              { key: 0, value: 0, text: "Boleto", icon: { name: "barcode" } },
            ]}
            closeOnChange
          />
        </Form.Field>
      </Form.Group>
    </Form>
  );
};

export default FormInvoice;
