import React, { Component } from "react";
import api from "../../../api/plans";
import { Dropdown } from "semantic-ui-react";

class DropDownPricingPlans extends Component {
  state = {
    options: [],
    loading: false,
    search: "",
  };

  async componentDidMount() {
    const filter = { filter: JSON.stringify([["active", "=", 1]]) };
    await api.plan.fetchAll({ filter }).then((plans) => {
      this.setState({
        options: this.state.options.concat(
          plans
            .filter((el) => {
              return !this.state.options.some((a) => a.key === el.id);
            })
            .map((c) => ({
              key: c.id,
              value: c.id,
              text: `${c.name} - ${c.description}/ R$ ${c.price}`,
            }))
        ),
        loading: false,
      });
    });
  }

  render() {
    const { options, loading } = this.state;
    const { invoice, onChange } = this.props;
    return (
      <Dropdown
        placeholder="Pesquise um plano"
        search
        selection
        scrolling
        name="pricing_plan_id"
        value={invoice.pricing_plan_id}
        onChange={onChange}
        options={options}
        closeOnChange
        loading={loading}
        clearable
      />
    );
  }
}

export default DropDownPricingPlans;
