import React, { Component } from "react";
import api from "../../api/contact";
import apigroup from "../../api/group";
import apiclient from "../../api/client";
import DataTable from "../table/DataTable";
import ContactModal from "../contact/modal/ContactModal";
import AlertSuccess from "../alerts/AlertSuccess";
import { Icon } from "semantic-ui-react";
import Trigger from "./Trigger";

class ContactModalCard extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    order: {},
    loading: false,
    groups: [],
    total_records: 0,
    selectedDataIndex: -1,
    editModalOpen: false,
    loadingGroups: false,
    loadingClients: false,
    save_alert: false
  };

  select = selectedDataId => {
    const { groups, clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, editModalOpen: true });

    if (groups.length === 0) this.fetchGroups();
    if (
      !!data.client_id &&
      clients.filter(c => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  newDataClick = () => {
    const { records, groups } = this.state;

    if (groups.length === 0) this.fetchGroups();

    const newData = Object.keys(records[0]).reduce(
      (o, key) => Object.assign(o, { [key]: "" }),
      {}
    );

    newData.groups = [];
    newData.webhook_active = true;
    newData.send_campaing = true;

    this.setState(
      {
        records: [...records].concat(newData),
        editModalOpen: true
      },
      () => {
        this.setState({
          selectedDataIndex: this.state.records.length - 1
        });
      }
    );
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  onSearchClientChange = e => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then(clients => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map(c => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false
        });
      });
    }
  };

  onSelectGroup = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  addGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingGroups: true });

    apigroup.group
      .submit({ name: value })
      .then(data => {
        this.setState({
          groups: [...this.state.groups].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              groups: [...records[dataIndex].groups].concat(data.data.id)
            },
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .then(() => {
        this.setState({ loadingGroups: false });
      });
  };

  fetchRecords = async params => {
    this.setState({ loading: true });

    return await api.contact.fetchAll(params).then(res => {
      this.setState({
        records: res.data,
        order: res.order,
        columns: res.columns,
        total_records: res.total_records,
        loading: false
      });
    });
  };

  fetchGroups = async () => {
    this.setState({ loadingGroups: true });
    await apigroup.group.fetchAll().then(groups => {
      this.setState({
        groups: groups.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingGroups: false
      });
    });
  };

  getClient = async id => {
    this.setState({ loadingClients: true });
    await apiclient.client.get(id).then(client => {
      this.setState({
        clients: [...this.state.clients].concat({
          key: client.id,
          value: client.id,
          text: client.name
        }),
        loadingClients: false
      });
    });
  };

  componentWillMount() {
    this.fetchRecords();
  }

  handleChange = e => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [e.target.name]: e.target.value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  handleChecked = (e, { name, checked }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: checked
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };
  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.contact
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          editModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  update = () => {
    const { selectedDataIndex, records } = this.state;
    const data = records[selectedDataIndex];
    this.setState({ loading: true });

    api.contact
      .update(data.id, data)
      .then(data => {
        this.setState({
          records: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          editModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          editModalOpen: false,
          selectedId: -1,
          selectedDataIndex: -1,
          records: [...records].filter(c => c.id > 0)
        });
      });
  };

  delete = id => api.contact.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      editModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const {
      records,
      loading,
      columns,
      total_records,
      selectedDataIndex,
      editModalOpen,
      groups,
      clients,
      loadingGroups,
      loadingClients,
      save_alert
    } = this.state;

    const contact = this.state.records[selectedDataIndex];

    return (
      <div className="modal-edit">
        {this.state.save_alert && <AlertSuccess />}
        <Trigger
          loading={loading}
          onAddClick={this.newDataClick}
          columns={columns}
          data={records}
          totalRecords={total_records}
          messageError={error}
          onDelete={id => this.delete(id)}
          onEditClick={d => this.select(d.id)}
          fetchData={this.fetchRecords}
        />

        {selectedDataIndex !== -1 ? (
          <ContactModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={this.update}
            onClickAdd={this.submit}
            onSelectGroup={this.onSelectGroup}
            onSelectClient={this.onSelectClient}
            onSearchClientChange={this.onSearchClientChange}
            handleGroupAddition={this.addGroup}
            contact={contact}
            modalHeader={`Edição do contato ${contact.name}`}
            open={editModalOpen}
            previousButtonEnabled={selectedDataIndex === 0}
            nextButtonEnabled={selectedDataIndex === records.length - 1}
            groups={groups}
            clients={clients}
            loading={loading}
            loadingGroups={loadingGroups}
            loadingClients={loadingClients}
          />
        ) : null}
      </div>
    );
  }
}

export default ContactModalCard;
