import React, { Component } from "react";
import api from "../../api/contact";
import apiuser from "../../api/users";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as UserActions } from "../../store/ducks/auth";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import { Icon, Modal, Button, Form } from "semantic-ui-react";
import DropdownUsers from "../users/DropdownUsers";

class AddtoGroupAdmin extends Component {
  state = {
    contact: [],
    media: [],
    user_contacts: [],
    loading: false,
    conversationModalOpen: false,
    save_alert: false,
    user_id: ""
  };

  handleUserChange = value => {
    this.setState({
      user_id: value
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.user_id !== prevState.user_id) {
      apiuser.user.fetchConection(this.state.user_id).then(res => {
        this.setState({
          user_contacts: res.data.data
        });
      });
    }
  }
  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { user_id, user_contacts } = this.state;
    const { contact, findContact } = this.props;

    this.setState({
      loading: true
    });

    if (findContact) {
      const deleteContact = user_contacts.contacts.filter(
        c => c !== findContact
      );
      var sendDelete = {
        user_id,
        contacts: deleteContact,
        contact
      };
    }

    user_contacts.contacts.push(contact);

    const sendContacts = {
      user_id,
      contacts: user_contacts.contacts,
      contact
    };

    return api.contact
      .submitContact(findContact ? sendDelete : sendContacts)
      .then(() => {
        this.props.WalletToConversation(
          findContact ? sendDelete : sendContacts
        );
        this.props.AddToWallet(findContact ? sendDelete : sendContacts);

        this.setState({
          save_alert: true,
          loading: false,
          conversationModalOpen: false,
          contact: []
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          contactModalOpen: false
        });
      });
  };
  render() {
    const { user_id } = this.props;

    return (
      <div className="addToGroup">
        <Modal
          trigger={
            <Icon
              name="money bill alternate"
              className="users"
              title="Adicionar na sua carteira"
            />
          }
        >
          <Modal.Header>Selecionar a carteira pertencente</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Form.Field>
                <label>Carteiras</label>
                <DropdownUsers
                  onSelectUser={this.handleUserChange}
                  user_id={user_id}
                  allowAdditions={false}
                />{" "}
              </Form.Field>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <div className="field">
              <Button positive onClick={this.submit}>
                Salvar
              </Button>
            </div>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...UserActions, ...ConversationActions }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AddtoGroupAdmin);
