import React, { Component } from "react";
import { Table, Icon } from "semantic-ui-react";
import PropTypes from "prop-types";

class DataTable extends Component {
    state = {
        activePage: 1,
        boundaryRange: 1,
        siblingRange: 1,
        loading: false,
        showEllipsis: true,
        showFirstAndLastNav: true,
        showPreviousAndNextNav: true,
        recordsPerPage: 10,
        modalDeleteOpen: false,
        modalDeleteAllOpen: false,
        selectedId: 0,
        allChecked: false,
        checkedItems: [],
        search: "",
        orderDirection: "asc",
        order: "id",
        hide_actions: true
    };

    handlePaginationChange = (e, { activePage }) => {
        const {
            recordsPerPage: take,
            search,
            order,
            orderDirection
        } = this.state;
        const skip = activePage === 1 ? 0 : take * activePage - take;
        const except = this.props.data.map(c => c.id);

        const params = {
            skip,
            search,
            take,
            except,
            order,
            order_direction: orderDirection
        };

        this.props.fetchData(params).then(() => {
            this.setState({ activePage, loading: false });
        });
    };

    onRecordsPerPageChanged = (e, { value }) => {
        this.setState({
            recordsPerPage: value,
            activePage: 1,
            loading: true
        });

        const skip = 0;
        const { search, order, orderDirection } = this.state;

        const except = this.props.data.map(c => c.id);

        const params = {
            skip,
            search,
            take: value,
            except,
            order,
            order_direction: orderDirection
        };

        this.props.fetchData(params).then(() => {
            this.setState({ loading: false });
        });
    };

    reloadData = () => {
        const {
            recordsPerPage,
            activePage,
            search,
            order,
            orderDirection
        } = this.state;
        const skip =
            activePage === 1 ? 0 : recordsPerPage * activePage - recordsPerPage;
        this.setState({ loading: true });

        const except = this.props.data.map(c => c.id);

        const params = {
            skip,
            search,
            take: recordsPerPage,
            except,
            order,
            order_direction: orderDirection
        };

        this.props.fetchData(params).then(res => {
            this.setState({ loading: false });
        });
    };

    resetSearch = () =>
        this.setState({ search: "", loading: true }, this.onSearch);

    delete = () => {
        this.setState({ loading: true });
        const { selectedId } = this.state;

        this.props.onDelete(selectedId).then(res => {
            this.reloadData();
            this.setState({
                selectedId: 0,
                allChecked: false,
                modalDeleteAllOpen: false,
                modalDeleteOpen: false,
                loading: false,
                checkedItems: []
            });
        });
    };

    select = data => {
        this.setState({
            selectedId: data.id
        });
        this.props.onEditClick(data);
    };

    handleCloseDeleteModal = () => {
        this.setState({
            modalDeleteOpen: false,
            selectedId: 0
        });
    };

    onOrderChange = column => {
        const { search, recordsPerPage, orderDirection } = this.state;
        this.setState({
            loading: true
        });
        const except = this.props.data.map(c => c.id);

        const params = {
            skip: 0,
            search,
            take: recordsPerPage,
            except,
            order: column,
            order_direction: orderDirection === "asc" ? "desc" : "asc"
        };

        this.props.fetchData(params).then(() => {
            this.setState({
                orderDirection: orderDirection === "asc" ? "desc" : "asc",
                order: column,
                loading: false
            });
        });
    };

    handleOpenDeleteModal = id => {
        this.setState({
            modalDeleteOpen: true,
            selectedId: id
        });
    };

    handleOpenDeleteAllModal = () => {
        const { checkedItems } = this.state;

        if (checkedItems.length > 0) {
            this.setState({
                modalDeleteAllOpen: true,
                selectedId: checkedItems.join(",")
            });
        } else {
            // TODO: show alert to select items to delete
        }
    };

    handleCloseDeleteAllModal = () => {
        this.setState({
            modalDeleteAllOpen: false,
            selectedId: -1
        });
    };

    handleCheckBoxChanged = (checked, id) => {
        const { checkedItems } = this.state;

        if (checked) {
            if (checkedItems.indexOf(id) === -1) {
                this.setState({
                    checkedItems: [...checkedItems].concat(id)
                });
            }
        } else {
            this.setState({
                checkedItems: [...checkedItems].filter(c => c !== id),
                allChecked: false
            });
        }
    };

    checkAll = (e, { checked }) => {
        this.setState({
            allChecked: checked,
            checkedItems: checked ? this.props.data.map(d => d.id) : []
        });
    };

    onSearchChange = e => {
        clearTimeout(this.timer);
        this.setState({ search: e.target.value });
        this.timer = setTimeout(this.onSearch, 300);
    };

    onSearch = () => {
        const { search, recordsPerPage, order, orderDirection } = this.state;

        const except = this.props.data.map(c => c.id);

        const params = {
            skip: 0,
            search,
            take: recordsPerPage,
            except,
            order,
            order_direction: orderDirection
        };

        if (search.length > 2 || search.length === 0) {
            this.setState({ loading: true });
            this.props.fetchData(params).then(res => {
                this.setState({ loading: false });
            });
        }
    };

    render() {
        const { data, hide_actions } = this.props;

        return (
            <div>
                {data.map((d, i) => (
                    <Table.Row className="table-list" key={`contact-${i * 1}`}>
                        {hide_actions === true ? (
                            ""
                        ) : (
                            <Table.Cell>
                                <div className="icon-list">
                                    <Icon
                                        name="edit outline"
                                        className="editar-card"
                                        onClick={() => this.select(d)}
                                    />
                                </div>
                            </Table.Cell>
                        )}
                    </Table.Row>
                ))}
            </div>
        );
    }
}

DataTable.propTypes = {
    columns: PropTypes.shape({}).isRequired,
    totalRecords: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired
};

export default DataTable;
