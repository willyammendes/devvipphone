import React from "react";
import placeholder from "../../assets/img/placeholder.png";

const LastTicketUser = ({ ticket }) => {
  return ticket ? (
    !!ticket.user_avatar && !!ticket.user ? (
      <div className="atendente">
        <img src={ticket.user_avatar} alt={ticket.user} />
        <span>{ticket.user.split(" ")[0]}</span>
      </div>
    ) : !!!ticket.user_avatar && !!ticket.user ? (
      <div className="atendente">
        <img src={placeholder} alt={ticket.user} />
        <span>{ticket.user}</span>
      </div>
    ) : (
      ""
    )
  ) : null;
};

export default LastTicketUser;
