import React, { Component } from "react";
import { Icon } from "semantic-ui-react";

class ButtonAdd extends Component {
  render() {
    const { onAddClick, icon, title } = this.props;
    return (
      <Icon
        name={icon}
        title={title}
        onClick={onAddClick}
        className="editar-card"
      />
    );
  }
}

export default ButtonAdd;
