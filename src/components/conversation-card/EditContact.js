import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../../api/contact";
import apirestricted from "../../api/restricted-contacts";
import apigroup from "../../api/group";
import apiclient from "../../api/client";
import ContactModal from "../contact/modal/ContactModal";
import ButtonAdd from "./ButtonAdd.js";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";

class EditContact extends Component {
  state = {
    records: [],
    columns: {},
    clients: [],
    contact: {},
    order: {},
    loading: false,
    groups: [],
    total_records: 0,
    selectedDataIndex: -1,
    contactModalOpen: false,
    loadingGroups: false,
    loadingClients: false,
    save_alert: false,
    send_campaign: ""
  };

  select = selectedDataId => {
    const { groups, clients } = this.state;
    const dataIndex = this.state.records.findIndex(
      c => c.id === selectedDataId
    );
    const data = this.state.records[dataIndex];

    this.setState({ selectedDataIndex: dataIndex, contactModalOpen: true });

    if (groups.length === 0) this.fetchGroups();
    if (
      !!data.client_id &&
      clients.filter(c => c.key === data.client_id).length === 0
    )
      this.getClient(data.client_id);
  };

  newDataClick = () => {
    this.setState({ loading: true });
    const { contactId } = this.props;

    api.contact.fetchId(contactId).then(res => {
      this.setState({
        contact: res.data,
        send_campaing: res.data.send_campaing,
        contactModalOpen: true,
        loading: false
      });
    });
  };

  nextRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex < this.state.records.length - 1
        ? this.state.selectedDataIndex + 1
        : this.state.records.length - 1;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  previousRecord = () => {
    const selectedDataIndex =
      this.state.selectedDataIndex - 1 > 0
        ? this.state.selectedDataIndex - 1
        : 0;
    const selectedDataId = this.state.records[selectedDataIndex].id;

    this.select(selectedDataId);
  };

  onSelectClient = (e, { name, value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          [name]: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  onSearchClientChange = e => {
    clearTimeout(this.timer);
    this.setState({ searchClient: e.target.value });
    this.timer = setTimeout(this.onSearchClient, 300);
  };

  onSearchClient = () => {
    const { searchClient } = this.state;

    if (searchClient.length > 2) {
      this.setState({ fetchingClients: true });

      apiclient.client.search(searchClient).then(clients => {
        this.setState({
          clients: [...this.state.clients].concat(
            clients.map(c => ({ key: c.id, value: c.id, text: c.name }))
          ),
          fetchingClients: false
        });
      });
    }
  };

  onSelectGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;

    this.setState({
      records: [
        ...records.slice(0, dataIndex),
        {
          ...records[dataIndex],
          groups: value
        },
        ...records.slice(dataIndex + 1)
      ]
    });
  };

  submitRestrictedContact = contact => {
    const restrictedContacts = { contacts: [contact.id] };
    this.setState({
      loading: true
    });
    return apirestricted.restrictedcontact
      .submit(restrictedContacts)
      .then(data => {
        const send_campaing = data.contact.send_campaing;

        this.setState({
          loading: false,
          send_campaign: send_campaing,
          contact: {
            ...this.state.contact,
            send_campaing: !this.state.contact.send_campaing
          }
        });

        setTimeout(
          function() {
            this.setState({ save_alert: false, send_campaign: send_campaing });
          }.bind(this),
          500
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response
        });
      });
  };

  blockContact = e => {
    const { contact } = this.state;
    this.setState({
      loading: true
    });
    if (contact.whatsapp_blocked) {
      api.contact.unblockContact(contact.id).then(data => {
        this.setState({
          contact: {
            ...this.state.contact,
            whatsapp_blocked: !contact.whatsapp_blocked
          },
          loading: false
        });
      });
    } else {
      api.contact.blockContact(contact.id).then(data => {
        this.setState({
          contact: {
            ...this.state.contact,
            whatsapp_blocked: !contact.whatsapp_blocked
          },
          loading: false
        });
      });
    }
  };

  addGroup = (e, { value }) => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({ loadingGroups: true });

    apigroup.group
      .submit({ name: value })
      .then(data => {
        this.setState({
          groups: [...this.state.groups].concat({
            key: data.data.id,
            value: data.data.id,
            text: data.data.name
          }),
          records: [
            ...records.slice(0, dataIndex),
            {
              ...records[dataIndex],
              groups: [...records[dataIndex].groups].concat(data.data.id)
            },
            ...records.slice(dataIndex + 1)
          ]
        });
      })
      .then(() => {
        this.setState({ loadingGroups: false });
      });
  };

  fetchClients = async () => {
    this.setState({ loadingClients: true });
    await apiclient.client.fetchAll().then(clients => {
      this.setState({
        clients: clients.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingClients: false
      });
    });
  };

  fetchGroups = async () => {
    this.setState({ loadingGroups: true });
    await apigroup.group.fetchAll().then(groups => {
      this.setState({
        groups: groups.data.map(c => ({
          key: c.id,
          value: c.id,
          text: c.name
        })),
        loadingGroups: false
      });
    });
  };

  getClient = async id => {
    this.setState({ loadingClients: true });
    await apiclient.client.get(id).then(client => {
      this.setState({
        clients: [...this.state.clients].concat({
          key: client.id,
          value: client.id,
          text: client.name
        }),
        loadingClients: false
      });
    });
  };

  handleChange = e =>
    this.setState({
      contact: { ...this.state.contact, [e.target.name]: e.target.value }
    });

  handleChecked = (e, { name, checked }) => {
    this.setState({
      contact: { ...this.state.contact, [name]: checked }
    });
  };

  handleGroupChange = value => {
    this.setState({
      contact: { ...this.state.contact, groups: value }
    });
  };
  handleClientChange = value => {
    this.setState({
      contact: { ...this.state.contact, client_id: value }
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  submit = () => {
    const { selectedDataIndex: dataIndex, records } = this.state;
    this.setState({
      loading: true
    });
    return api.contact
      .submit(records[dataIndex])
      .then(data => {
        this.setState({
          save_alert: true,
          loading: false,
          selectedDataIndex: -1,
          contactModalOpen: false,
          records: [
            ...records.slice(0, dataIndex),
            data.data,
            ...records.slice(dataIndex + 1)
          ]
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          contactModalOpen: false
        });
      });
  };

  update = contact => {
    const { selectedDataIndex, records } = this.state;

    this.setState({ loading: true });
    api.contact
      .update(contact.id, contact)
      .then(data => {
        this.setState({
          contact: [
            ...records.slice(0, selectedDataIndex),
            { ...data.data },
            ...records.slice(selectedDataIndex + 1)
          ],
          save_alert: true,
          contactModalOpen: false,
          selectedDataIndex: -1,
          loading: false
        });
        const changeClient = {
          id: this.props.conversationId,
          contact: data.data
        };
        this.props.changeClient({ conversation: changeClient });
        this.props.setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          contactModalOpen: false
        });
      });
  };

  delete = id => api.contact.delete(id);

  handleCloseEditModal = () => {
    const { records } = this.state;
    this.setState({
      contactModalOpen: false,
      selectedId: -1,
      selectedDataIndex: -1,
      records: [...records].filter(c => c.id > 0)
    });
  };

  render() {
    const { loading, contactModalOpen, contact, send_campaign } = this.state;

    const {
      onSelectGroup,
      onSelectClient,
      onSearchClientChange,
      handleGroupAddition,
      loadingGroups,
      loadingClients,
      clients
    } = this.props;

    return (
      <div className="icone-add">
        <ButtonAdd
          onAddClick={this.newDataClick}
          icon="edit outline"
          className="editar-card"
          title="Editar Contato"
        />
        {contact ? (
          <ContactModal
            handleClose={this.handleCloseEditModal}
            onChange={this.handleChange}
            onChecked={this.handleChecked}
            handleNext={this.nextRecord}
            handlePrevious={this.previousRecord}
            onClickSave={() => this.update(contact)}
            onClickAdd={this.submit}
            contact={contact}
            modalHeader={`Edição do contato ${contact.name}`}
            open={contactModalOpen}
            previousButtonEnabled={false}
            nextButtonEnabled={false}
            onSelectGroup={onSelectGroup}
            handleGroupChange={this.handleGroupChange}
            handleClientChange={this.handleClientChange}
            onSelectClient={onSelectClient}
            onSearchClientChange={onSearchClientChange}
            handleGroupAddition={handleGroupAddition}
            loadingGroups={loadingGroups}
            loadingClients={loadingClients}
            clients={clients}
            send_campaign={send_campaign}
            loading={loading}
            showNavigation={false}
            blockContact={this.blockContact}
            submitRestrictedContact={() =>
              this.submitRestrictedContact(contact)
            }
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ conversation, user, media }) => {
  return {
    user: user.user,
    conversation,
    media,
    activeConversationId: conversation.activeConversationId,
    filter: conversation.filter,
    conversations: !conversation.data ? [] : Object.values(conversation.data)
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditContact);
