import React from "react";

import { Modal, Button } from "semantic-ui-react";
import FormContact from "../forms/FormContact";

const ContactModal = ({
  contact,
  handleClick = () => {},
  handleClose = () => {},
  onChange,
  onChecked,
  trigger,
  modalHeader = "",
  onSearchGroupChange,
  onSearchGroup,
  onSelectGroup,
  groups,
  clients
}) => {
  return (
    <Modal size="large" closeIcon trigger={trigger} onClose={handleClose}>
      <Modal.Header>{modalHeader}</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <div className="holder_contact">
            <div className="form-contact">
              <FormContact
                contact={contact}
                groups={groups}
                clients={clients}
                onChange={onChange}
                onChecked={onChecked}
                onSearchGroupChange={onSearchGroupChange}
                onSelectGroup={onSelectGroup}
              />
            </div>
            <div className="contact-image">
              <img src={contact.contact_picture} alt="" />
              <div className="block">
                <Button color="red" className="block-button" size="small">
                  BLOQUEADO
                </Button>
                <span>
                  Bloqueado por Cosme Perovano em{" "}
                  <time>25 de Junho de 2019</time>
                </span>
              </div>
            </div>
          </div>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button primary onClick={handleClose}>
          Cancelar
        </Button>
        <Button primary onClick={handleClick}>
          Salvar
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default ContactModal;
