import React, { Component } from "react";
import api from "../../api/contact";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as UserActions } from "../../store/ducks/auth";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import { Icon } from "semantic-ui-react";

class AddtoGroup extends Component {
  state = {
    contact: [],
    media: [],
    errors: { message: "", phone_number: "" },
    loading: false,
    conversationModalOpen: false,
    save_alert: false
  };

  openModal = () => {
    const { idCard } = this.props;
    this.setState({
      conversationModalOpen: true
    });
    api.contact.fetchId(idCard).then(res => {
      this.setState({
        contact: res.data,
        loading: false
      });
    });
  };

  handleChange = e =>
    this.setState({
      contact: {
        ...this.state.contact,
        [e.target.name]: e.target.value
      }
    });

  handleChecked = (e, { name, checked }) => {
    this.setState({
      contact: { ...this.state.contact, [name]: checked }
    });
  };

  handleGroupChange = value => {
    const { contact } = this.state;

    this.setState({
      contact: { ...contact, groups: value }
    });
  };

  cleanErrors = () => {
    this.setState({ errors: "" });
  };

  handleMediaChange = (value, text) => {
    this.setState({
      contact: {
        ...this.state.contact,
        account_number: value
      }
    });
  };

  handleContactChange = value => {
    this.setState({
      contact: { ...this.state.contact, contact: value }
    });
  };

  handleCloseEditModal = () => {
    this.setState({
      conversationModalOpen: false,
      contact: []
    });
  };

  submit = () => {
    const { user, contact, findContact } = this.props;
    this.setState({
      loading: true
    });

    if (findContact) {
      const deleteContact = user.contacts.filter(c => c !== findContact);
      var sendDelete = {
        user_id: user.id,
        contacts: deleteContact,
        contact
      };
    }

    user.contacts.push(contact);

    const sendContacts = {
      user_id: user.id,
      contacts: user.contacts,
      contact
    };

    return api.contact
      .submitContact(findContact ? sendDelete : sendContacts)
      .then(() => {
        this.props.WalletToConversation(
          findContact ? sendDelete : sendContacts
        );
        this.props.AddToWallet(findContact ? sendDelete : sendContacts);

        this.setState({
          save_alert: true,
          loading: false,
          conversationModalOpen: false,
          contact: []
        });
        setTimeout(
          function() {
            this.setState({ save_alert: false });
          }.bind(this),
          5000
        );
      })
      .catch(err => {
        this.setState({
          loading: false,
          errors: err.response,
          contactModalOpen: false
        });
      });
  };
  render() {
    const { findContact, wallet } = this.props;

    return (
      <div className="addToGroup">
        {!wallet ? (
          <Icon
            name="money bill alternate"
            className="users"
            title="Adicionar na sua carteira"
            onClick={this.submit}
          />
        ) : (
          <div
            className="users_wallet"
            title={
              findContact || wallet
                ? "Remover da sua carteira"
                : "Adicionar na sua carteira"
            }
            onClick={this.submit}
          >
            {this.props.children}
          </div>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...UserActions, ...ConversationActions }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AddtoGroup);
