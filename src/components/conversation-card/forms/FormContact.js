import React from "react";
import { Form, Input, Checkbox, TextArea, Dropdown } from "semantic-ui-react";

const FormContact = ({
    contact,
    onChange,
    onChecked,
    clients,
    groups,
    onSelectGroup,
    onSelectClient,
    onSearchGroupChange,
    onSearchClientChange
}) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Nome"
                    name="name"
                    onChange={onChange}
                    value={contact.name}
                />
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Email"
                    name="email"
                    onChange={onChange}
                    value={contact.email}
                />
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Celular"
                    name="phone_number"
                    onChange={onChange}
                    value={contact.phone_number}
                />
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Cidade"
                    name="city"
                    onChange={onChange}
                    value={contact.city}
                />
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Bairro"
                    name="neighborhood"
                    onChange={onChange}
                    value={contact.neighborhood}
                />
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="CEP"
                    name="code"
                    onChange={onChange}
                    value={contact.code}
                />
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Endereço"
                    name="address"
                    onChange={onChange}
                    value={contact.address}
                />
                <div className="field">
                    <label>Webhook</label>
                    <Checkbox
                        label="Habilitado"
                        onChange={onChecked}
                        name="webhook_active"
                        checked={contact.webhook_active}
                    />
                </div>
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    control={TextArea}
                    label="Informações Adicionais"
                    name="additional_info"
                    onChange={onChange}
                    value={contact.additional_info}
                />
                <div className="field" />
            </Form.Group>
            <Form.Group widths="equal">
                <div className="field">
                    <label>Empresa</label>
                    <Dropdown
                        placeholder="Selecione o cliente do contato"
                        fluid
                        search
                        selection
                        name="client_id"
                        value={contact.client_id}
                        onSearchChange={onSearchClientChange}
                        onChange={onSelectClient}
                        options={clients}
                    />
                </div>

                <div className="field">
                    <label>Grupos de Relacionamento</label>
                    <Dropdown
                        placeholder="Grupos de Relacionamento"
                        fluid
                        search
                        multiple
                        selection
                        onSearchChange={onSearchGroupChange}
                        name="groups"
                        value={contact.groups}
                        onChange={onSelectGroup}
                        options={groups}
                    />
                </div>
            </Form.Group>
        </Form>
    );
};

export default FormContact;
