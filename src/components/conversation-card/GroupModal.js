import React from "react";
import { Modal, Button, Form } from "semantic-ui-react";
import DropdownContacts from "../contact/DropdownContacts";
import InlineError from "../messages/InlineError";

class GroupModal extends React.Component {
  componentDidMount() {
    if (this.props.showNavigation) {
      document.removeEventListener("keydown", () => {});
      document.addEventListener("keydown", e => {
        if (e.keyCode === 39) this.props.handleNext();
        if (e.keyCode === 37) this.props.handlePrevious();
      });
    }
  }

  save = () => {
    if (this.props.contact.id) {
      this.props.onClickAdd();
    } else {
      this.props.onClickAdd();
    }
  };
  render() {
    const {
      handleClose,
      modalHeader,
      contact,
      open,
      loading,
      messageError,
      generalError,
      cleanErrors,
      handleGroupChange
    } = this.props;

    return (
      <Modal
        size="small"
        closeIcon
        open={open}
        onClose={handleClose}
        dimmer="blurring"
        closeOnDimmerClick={false}
      >
        <Modal.Header>{modalHeader}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {generalError && (
              <div className="errors-table">
                <Button
                  circular
                  basic
                  color="black"
                  icon="close"
                  className="button-close"
                  onClick={cleanErrors}
                />
                <p>{generalError}</p>
              </div>
            )}
            <div className="holder_conversation">
              <div className="form-conversation">
                <Form>
                  <Form.Field
                    error={messageError ? !!messageError.groups : ""}
                  >
                    <label>Contatos</label>
                    <DropdownContacts
                      onSelectGroup={handleGroupChange}
                      contact_id={contact.contact_id}
                      allowAdditions={false}
                      multiple={false}
                      closeOnChange
                    />
                    {messageError ? (
                      <InlineError text={messageError.groups} />
                    ) : (
                      ""
                    )}
                  </Form.Field>
                </Form>
              </div>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button.Group>
            <Button onClick={handleClose}>Cancelar</Button>
            <Button.Or />
            <Button
              positive
              onClick={this.save}
              loading={loading}
              disabled={loading}
            >
              Salvar
            </Button>
          </Button.Group>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default GroupModal;
