import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ConversationActions } from "../../store/ducks/conversation";
import { Icon, Loader } from "semantic-ui-react";
import api from "../../api/conversation";

class ArchivedConversation extends Component {
  state = {
    errors: { message: "", phone_number: "" },
    loading: false
  };

  submit = () => {
    const { conversation } = this.props;
    this.setState({
      loading: true
    });
    const archivedConversation = {
      archived: conversation.archived === null ? true : !conversation.archived
    };

    api.conversations
      .archiveConversation(conversation.id, archivedConversation)
      .then(data => {
        this.setState({
          loading: false
        });
        if (data.status !== "error") {
          this.props.archiveConversation({ conversation: data.data });
        }
      });
  };
  render() {
    const { loading } = this.state;
    const { conversation, className } = this.props;

    return (
      <div className={className}>
        <Loader active={loading} inline="centered" />
        <Icon
          name={
            conversation.archived === 0 || conversation.archived === false
              ? "folder outline"
              : "folder open outline"
          }
          className="archive"
          title={
            conversation.archived === 0 || conversation.archived === false
              ? "Arquivar Conversa"
              : "Desarquivar Conversa"
          }
          onClick={this.submit}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(ConversationActions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ArchivedConversation);
