/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import api from "../../api/users";
import apiticket from "../../api/ticket";
import renderHTML from "react-render-html";
import { Icon, Popup } from "semantic-ui-react";
import { Line } from "react-chartjs-2";
import EditContact from "./EditContact";
import AddtoGroup from "./AddtoGroup";
import AddtoGroupAdmin from "./AddtoGroupAdmin";
import ArchivedConversation from "./ArchivedConversation";
import LastTicketUser from "./LastTicketUser";

const customTooltips = function(tooltip) {
  // Tooltip Element
  let tooltipEl = document.getElementById("chartjs-tooltip");
  if (!tooltipEl) {
    tooltipEl = document.createElement("div");
    tooltipEl.id = "chartjs-tooltip";
    tooltipEl.innerHTML = "<table></table>";
    this._chart.canvas.parentNode.appendChild(tooltipEl);
  }
  // Hide if no tooltip
  if (tooltip.opacity === 0) {
    tooltipEl.style.opacity = 0;
    return;
  }
  // Set caret Position
  tooltipEl.classList.remove("above", "below", "no-transform");
  if (tooltip.yAlign) {
    tooltipEl.classList.add(tooltip.yAlign);
  } else {
    tooltipEl.classList.add("no-transform");
  }
  function getBody(bodyItem) {
    return bodyItem.lines;
  }
  // Set Text
  if (tooltip.body) {
    const titleLines = tooltip.title || [];
    const bodyLines = tooltip.body.map(getBody);
    let innerHtml = "<thead>";
    titleLines.forEach(function(title) {
      innerHtml += `<tr><th>${title}</th></tr>`;
    });
    innerHtml += "</thead><tbody>";
    bodyLines.forEach(function(body, i) {
      const colors = tooltip.labelColors[i];
      let style = `background:${colors.backgroundColor}`;
      style += `; border-color:${colors.borderColor}`;
      style += "; border-width: 2px";
      const span = `<span className="chartjs-tooltip-key" style="${style}"></span>`;
      innerHtml += `<tr><td>${span}${body}</td></tr>`;
    });
    innerHtml += "</tbody>";
    const tableRoot = tooltipEl.querySelector("table");
    tableRoot.innerHTML = innerHtml;
  }
  const positionY = this._chart.canvas.offsetTop;
  const positionX = this._chart.canvas.offsetLeft;
  // Display, position, and set styles for font
  tooltipEl.style.opacity = 1;
  tooltipEl.style.left = `${positionX + tooltip.caretX}px`;
  tooltipEl.style.top = `${positionY + tooltip.caretY}px`;
  tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
  tooltipEl.style.fontSize = `${tooltip.bodyFontSize}px`;
  tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
  tooltipEl.style.padding = `${tooltip.yPadding}px ${tooltip.xPadding}px`;
};

const ConversationCard = ({
  contact,
  conversation,
  active,
  handleConversationClick,
  onSelectGroup,
  onSelectClient,
  onSearchClientChange,
  handleGroupAddition,
  loadingGroups,
  loadingClients,
  clients,
  groups,
  parameter,
  animationTrue,
  animateCard,
  animateClose,
  idCard,
  user,
  idConversation,
}) => {
  const [users, setUsers] = useState([]);

  const { activeTicket: ticket } = conversation;

  useEffect(() => {
    if (conversation.contact.users.length > 0) {
      const params = {
        filter: JSON.stringify([["id", "=", conversation.contact.users]]),
      };
      api.user.fetchAll(params).then((res) => {
        setUsers(res.data);
      });
    }
  }, [conversation.contact.users]);

  const dataLine = {
    labels: [
      "Janeiro",
      "Fevereiro",
      "Março",
      "Abril",
      "Maio",
      "Junho",
      "Julho",
    ],
    datasets: [
      {
        data: [conversation.stats],
        fill: false,
        lineTension: 0.4,
        backgroundColor: "rgba(0,0,0,0.4)",
        borderColor: "rgba(153, 202, 60, 0.5)",
        borderWidth: 1,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(0,0,0,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointHoverBackgroundColor: "rgba(0,0,0,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 0,
        pointRadius: 0,
        pointHitRadius: 0,

        scales: {
          xAxes: [
            {
              gridLines: {
                drawBorder: false,
                display: false,
                color: "rgba(0, 0, 0, 0)",
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: false,
                drawBorder: false,
                color: "rgba(0, 0, 0, 0)",
              },
            },
          ],
        },
      },
    ],
  };
  return (
    <div
      className={
        animateClose
          ? "card-conversa holder_card_not_click true"
          : conversation.last_ticket_source === "phone"
          ? "card-conversa holder_card_not_click false phone"
          : "card-conversa holder_card_not_click false"
      }
    >
      <div
        className={animationTrue ? "animate_closed slide-up" : "animate_closed"}
      >
        <span>
          Esse ticket foi finalizado. <Icon name={animateCard.icon} />
        </span>
      </div>
      {conversation.unread_messages > 0 ? (
        <span className="mensagens-novas" title="Mensagens novas">
          {conversation.unread_messages}
        </span>
      ) : (
        ""
      )}

      <LastTicketUser ticket={ticket} />

      <div className="holder_icons">
        <EditContact
          onSelectGroup={onSelectGroup}
          onSelectClient={onSelectClient}
          onSearchClientChange={onSearchClientChange}
          handleGroupAddition={handleGroupAddition}
          loadingGroups={loadingGroups}
          loadingClients={loadingClients}
          clients={clients}
          groups={groups}
          conversationId={conversation.id}
          contactId={conversation.contact.id}
        />
        {user.roles.find((c) => c === "") ? (
          <div className="admin_wallet">
            {parameter.restrict_contacts_by_contact_user === 1 &&
            users.length === 0 ? (
              <AddtoGroupAdmin
                idCard={idCard}
                user={user}
                findContact={user.contacts.find(
                  (c) => c === conversation.contact.id
                )}
                contact={conversation.contact.id}
              />
            ) : (
              ""
            )}
          </div>
        ) : (
          <div className="user_wallet">
            {parameter.restrict_contacts_by_contact_user === 1 &&
            users.length === 0 ? (
              <AddtoGroup
                idCard={idCard}
                user={user}
                findContact={user.contacts.find(
                  (c) => c === conversation.contact.id
                )}
                contact={conversation.contact.id}
              />
            ) : (
              ""
            )}
          </div>
        )}

        <ArchivedConversation
          idConversation={idConversation}
          conversation={conversation}
          className={
            parameter.restrict_contacts_by_contact_user === 1 &&
            !user.contacts.find((c) => c === conversation.contact.id)
              ? "addToGroup"
              : "addToGroup carteira_desativada"
          }
        />
      </div>

      <div
        className={`card-conversa ${active ? "active-card" : ""}`}
        onClick={handleConversationClick}
      >
        <div className="holder_card">
          <div className="holder_nome">
            {conversation.contact.whatsapp_blocked ? (
              <Icon
                name={"ban"}
                title={"Contato Bloqueado"}
                size="large"
                color={"red"}
                className="icone_origem"
              />
            ) : (
              <Icon
                name={
                  conversation.last_ticket_source === "whatsapp"
                    ? "whatsapp"
                    : conversation.last_ticket_source === "ticket"
                    ? "ticket"
                    : conversation.last_ticket_source === "phone"
                    ? "phone"
                    : conversation.last_ticket_source === "email"
                    ? "email"
                    : conversation.last_ticket_source === "monitchat"
                    ? "chat"
                    : conversation.last_ticket_source === "campaing"
                    ? "bullhorn"
                    : "chat"
                }
                title={
                  conversation.last_ticket_source === "whatsapp"
                    ? "Ticket Aberto por Whatsapp"
                    : conversation.last_ticket_source === "ticket"
                    ? "Ticket Aberto por Monitchat"
                    : conversation.last_ticket_source === "phone"
                    ? "Ticket Aberto por Telefone"
                    : conversation.last_ticket_source === "email"
                    ? "Ticket Aberto por Email"
                    : conversation.last_ticket_source === "monitchat"
                    ? "Ticket Aberto pelo Monitchat"
                    : conversation.last_ticket_source === "campaing"
                    ? "Ticket Aberto por Campanha"
                    : "Ticket Aberto pelo monitchat"
                }
                size="large"
                color={
                  conversation.last_ticket_source === "phone"
                    ? "black"
                    : "green"
                }
                className="icone_origem"
              />
            )}

            <p className="nome">
              {conversation.contact.name}
              <span>{conversation.contact.client.name} </span>
            </p>
          </div>

          <div className="icones_status">
            {conversation.my_tickets > 0 ? (
              <div className="icone encaminhada">
                <Popup
                  content="Meus tickets"
                  trigger={
                    <Icon name="comments outline">
                      <span>{conversation.my_tickets}</span>
                    </Icon>
                  }
                />
              </div>
            ) : (
              ""
            )}

            {conversation.auto_reply < 1 ? (
              ""
            ) : (
              <div className="icone mensagem-bot">
                <Popup
                  content="Enviado pelo MonitBot"
                  trigger={<Icon name="android" />}
                />
              </div>
            )}

            {conversation.last_message.last_message_sender < 1 &&
            conversation.last_message.minutes_sice_last_message >
              parameter.service_timeout ? (
              <div className="icone tempo-expirado">
                <Popup
                  content={`Ultrapassou o tempo de resposta de ${
                    parameter.service_timeout
                  } minutos`}
                  trigger={
                    <Icon.Group>
                      <Icon name="comment outline" />
                      <Icon corner name="clock" />
                    </Icon.Group>
                  }
                />
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="wrap-conversa">
            <a href="/" className="perfil">
              <img
                src={
                  conversation.contact.avatar.indexOf("whatsapp") !== -1
                    ? conversation.contact.avatar
                    : `${conversation.contact.avatar}`
                }
                alt=""
                className="fotoPerfil"
              />
            </a>

            <p className="resumo-conversa">
              {conversation.last_message.message &&
              (conversation.last_message.message.includes("<p>") ||
                conversation.last_message.message.includes("<b>"))
                ? renderHTML(conversation.last_message.message)
                : conversation.last_message.message}
            </p>
            <div className="info-conversa">
              <time dateTime={conversation.last_message.created_at}>
                {conversation.last_message.created_at}
              </time>
            </div>
          </div>
        </div>
        {parameter.restrict_contacts_by_contact_user === 1 &&
        users.length > 0 ? (
          <div className="carteira">
            <AddtoGroup
              onSelectGroup={onSelectGroup}
              idCard={idCard}
              user={user}
              wallet
              contact={conversation.contact.id}
              findContact={user.contacts.find(
                (c) => c === conversation.contact.id
              )}
            >
              {users.map((c) => (
                <span>{c.name}</span>
              ))}
            </AddtoGroup>
          </div>
        ) : (
          ""
        )}

        <div className="chart_holder">
          <Line
            data={dataLine}
            height={20}
            className="lineCard"
            options={{
              legend: {
                display: false,
              },
              tooltips: {
                enabled: false,
                mode: "index",
                position: "nearest",
                custom: customTooltips,
                callbacks: {
                  label(tooltipItem) {
                    return tooltipItem.yLabel;
                  },
                },
              },
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      display: false,
                      drawBorder: false,
                      drawOnChartArea: false,
                    },
                    ticks: {
                      display: false,
                    },
                  },
                ],
                yAxes: [
                  {
                    gridLines: {
                      display: false,
                      drawBorder: false,
                      drawOnChartArea: false,
                    },
                    ticks: {
                      display: false,
                      suggestedMin: 0,
                      suggestedMax: 100,
                      beginAtZero: true,
                    },
                  },
                ],
              },
            }}
          />
        </div>
      </div>
    </div>
  );
};

ConversationCard.propTypes = {
  conversation: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,

  active: PropTypes.bool.isRequired,
  handleConversationClick: PropTypes.func.isRequired,
};

export default ConversationCard;
