import React from "react";
import { Form, Input, Checkbox, TextArea } from "semantic-ui-react";
import DropdownFastMessage from "../DropdownFastMessage";

const FormQuickMessage = ({
    fastmessage,
    onChange,
    onChecked,
    handleGroupAddition,
    handleFastMessageChange
}) => {
    return (
        <Form>
            <Form.Group widths="equal">
                <Form.Field
                    autoComplete="off"
                    control={Input}
                    label="Título"
                    name="title"
                    onChange={onChange}
                    value={fastmessage.title}
                />
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    control={TextArea}
                    label="Mensagem"
                    name="message"
                    onChange={onChange}
                    value={fastmessage.message}
                />
            </Form.Group>
            <Form.Group widths="equal">
                <Form.Field
                    control={Checkbox}
                    label="Acesso Rápido"
                    name="fast_menu"
                    onChange={onChecked}
                    checked={fastmessage.fast_menu}
                />
                <div className="field">
                    <label>Local de Mensagens Rápidas</label>
                    <DropdownFastMessage
                        onSelectFastMessage={handleFastMessageChange}
                        origin_fast_messages={fastmessage.origin_fast_messages}
                        allowAdditions
                    />
                </div>
            </Form.Group>
        </Form>
    );
};

export default FormQuickMessage;
