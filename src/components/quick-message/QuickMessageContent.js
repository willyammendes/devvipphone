import React from "react";
import PopoverForm from "../semantic/Popover";
import { Table } from "semantic-ui-react";

const QuickMessageContent = ({ quickmessage }) => (
  <Table.Body>
    {quickmessage.map((quickmessage, i) => (
      <Table.Row key={`quickmessage-menu-${i * 1}`}>
        <Table.Cell className="dataEditar" key={quickmessage.id}>
          <span>{quickmessage.id}</span>
        </Table.Cell>
        <Table.Cell className="dataEditar" key={quickmessage.id}>
          <span>{quickmessage.message}</span>
          <PopoverForm />
        </Table.Cell>
      </Table.Row>
    ))}
  </Table.Body>
);

export default QuickMessageContent;
