import React from "react";
import { Modal, Button, Icon } from "semantic-ui-react";
import FormQuickMessage from "../forms/FormQuickMessage";

class QuickMessageModal extends React.Component {
    state = {
        show_var: false
    };

    componentDidMount() {
        const { open } = this.props;

        if (open) {
            document.removeEventListener("keydown", () => {});
            document.addEventListener("keydown", e => {
                if (e.keyCode === 39) this.props.handleNext();
                if (e.keyCode === 37) this.props.handlePrevious();
            });
        }
    }

    save = () => {
        if (this.props.fastmessage.id) {
            this.props.onClickSave();
        } else {
            this.props.onClickAdd();
        }
    };

    showVariables = () => {
        this.setState({
            show_var: !this.state.show_var
        });
    };
    render() {
        const {
            fastmessage,
            handleClose,
            onChange,
            onChecked,
            open,
            modalHeader = "",
            handleNext,
            handlePrevious,
            previousButtonEnabled,
            nextButtonEnabled,
            loading,
            handleFastMessageChange
        } = this.props;

        return (
            <Modal
                size="large"
                closeIcon
                open={open}
                onClose={handleClose}
                dimmer="blurring"
                closeOnDimmerClick={false}
            >
                <Modal.Header>{modalHeader}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <div className="holder_fastmessage">
                            <div className="form-fastmessage">
                                <FormQuickMessage
                                    fastmessage={fastmessage}
                                    onChange={onChange}
                                    onChecked={onChecked}
                                    handleFastMessageChange={
                                        handleFastMessageChange
                                    }
                                />
                                <Button onClick={this.showVariables}>
                                    {this.state.show_var
                                        ? "Esconder Variáveis"
                                        : "Ver Variáveis"}
                                </Button>
                                {this.state.show_var ? (
                                    <ul className="show_variables">
                                        <li>
                                            <b>{"{ticket_number}"}</b> - número
                                            do ticket
                                        </li>
                                        <li>
                                            <b>{"{my_name}"}</b> - meu nome
                                        </li>
                                        <li>
                                            <b>{"{contact_name}"}</b> - nome do
                                            contato
                                        </li>
                                        <li>
                                            <b>{"{contact_cpf}"}</b> - contato
                                            CPF
                                        </li>
                                        <li>
                                            <b>{"{contact_email}"}</b> - E-mail
                                            do contato
                                        </li>
                                        <li>
                                            <b>{"{contact_phone_number}"}</b>-
                                            Número do contato
                                        </li>
                                        <li>
                                            <b>{"{contact_city}"}</b> - cidade
                                            do contato
                                        </li>
                                        <li>
                                            <b>{"{promotional_code}"}</b> -
                                            código promocional
                                        </li>
                                        <li>
                                            <b>{"{contact_cep}"}</b> - CEP do
                                            contato
                                        </li>
                                        <li>
                                            <b>{"{contact_address}"}</b> -
                                            Endereço do contato
                                        </li>
                                        <li>
                                            <b>{"{name}"}</b> - nome
                                        </li>
                                        <li>
                                            <b>{"{contact}"}</b> - contato
                                        </li>
                                        <li>
                                            <b>{"{week_day_time}"}</b> - dia da
                                            semana
                                        </li>
                                        <li>
                                            <b>{"{saturday_time}"}</b> - sábado
                                        </li>
                                        <li>
                                            <b>{"{sunday_time}"}</b> - domingo
                                        </li>
                                        <li>
                                            <b>{"{greeting}"}</b> - saudação
                                            (dia, tarde, noite)
                                        </li>
                                        <li>
                                            <b>{"{ticket_owner}"}</b> -
                                            responsável do ticket
                                        </li>
                                        <li>
                                            <b>{"{department_name}"}</b> - nome
                                            do departamento
                                        </li>
                                    </ul>
                                ) : (
                                    ""
                                )}
                            </div>
                        </div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button
                            icon
                            onClick={handlePrevious}
                            disabled={previousButtonEnabled}
                        >
                            <Icon name="left arrow" />
                        </Button>
                        <Button
                            icon
                            onClick={handleNext}
                            disabled={nextButtonEnabled}
                        >
                            <Icon name="right arrow" />
                        </Button>
                    </Button.Group>
                    <b> </b>
                    <Button.Group>
                        <Button onClick={handleClose}>Cancelar</Button>
                        <Button.Or />
                        <Button
                            positive
                            onClick={this.save}
                            loading={loading}
                            disabled={loading}
                        >
                            Salvar
                        </Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default QuickMessageModal;
