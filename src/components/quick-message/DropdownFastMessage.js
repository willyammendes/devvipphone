import React, { Component } from "react";
import api from "../../api/fast-message";
import { Dropdown } from "semantic-ui-react";

class DropdownFastMessage extends Component {
	state = {
		fastmessage: {
			title: "",
			category: ""
		},
		options: [],
		loading: false,
		search: "",
		openModal: false
	};

	handleFastMessageAddition = e => {};

	onSearchChange = (e, { searchQuery }) => {
		clearTimeout(this.timer);
		this.setState({ search: searchQuery });
		this.timer = setTimeout(this.onSearchFastMessage, 300);
	};
	componentWillMount() {
		api.fastmessage.fetchOrigin().then(fastmessages => {
			this.setState({
				options: fastmessages.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.title
				})),
				loading: false
			});
		});
	}

	componentDidMount() {}

	onSearchFastMessage = async () => {
		this.setState({ loading: true });
		const { search } = this.state;

		await api.fastmessage.fetchOrigin({ search }).then(fastmessages => {
			this.setState({
				options: fastmessages.data.map(c => ({
					key: c.id,
					value: c.id,
					text: c.title
				})),
				loading: false
			});
		});
	};

	onChange = (e, { name, value }) => {
		this.setState(state => ({
			fastmessage: { ...state.fastmessage, [name]: value }
		}));
	};

	cleanErrors = () => {
		this.setState({ errors: "" });
	};

	submit = () => {
		const { fastmessage } = this.state;

		this.setState({
			loading: true
		});

		return api.fastmessage
			.submit(fastmessage)
			.then(data => {
				this.setState(state => ({
					options: [...state.options].concat({
						key: data.data.id,
						value: data.data.id,
						text: data.data.title
					}),
					loading: false,
					fastmessage: {
						title: "",
						category: ""
					},
					openModal: !state.openModal
				}));

				this.props.onSelectFastMessage(data.data.id);
			})
			.catch(err => {
				this.setState({
					loading: false,
					errors: err.response
				});
			});
	};

	toggle = (e, { name, value }) => {
		this.setState(state => ({
			openModal: !state.openModal,
			fastmessage: {
				title: value,
				category: ""
			}
		}));
	};

	render() {
		const { options, loading } = this.state;
		const { allowAdditions } = this.props;

		return (
			<div>
				<Dropdown
					placeholder="Pesquise um Local de Mensagem Rápida"
					fluid
					search
					selection
					scrolling
					multiple
					allowAdditions={allowAdditions}
					additionLabel="Adicionar: "
					name="origin_fast_messages"
					value={this.props.origin_fast_messages}
					onSearchChange={this.onSearchChange}
					onChange={(e, { value }) =>
						this.props.onSelectFastMessage(value)
					}
					options={options}
					onAddItem={this.toggle}
					closeOnChange
					loading={loading}
					clearable
				/>
			</div>
		);
	}
}

export default DropdownFastMessage;
