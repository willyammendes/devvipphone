import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Button, Message, Dropdown } from "semantic-ui-react";
import { Link } from "react-router-dom";
import Validator from "validator";
import InlineError from "../messages/InlineError";
import api from "../../api/company";

class RegisterForm extends Component {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
      zip_code: "",
      number: "",
      cpf_cnpj: "",
      street: "",
      complement: "",
      corporate_name: "",
      segment: "",
      origin: "",
      email_secondary: "",
      phone_optional: "",
      address: "",
      neighborhood: "",
      city: "",
      state: "",
      country: "",
      contact: "",
      user: "",
      company_type: 1,
      active: false,
      intelliway_integration: false,
      paid_account: false,
      confirmPassword: ""
    },
    loading: false,
    errors: {}
  };
  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = () => {
    const { data } = this.state;
    const password = data.password;
    const confirmPassword = data.confirmPassword;

    if (password !== confirmPassword) {
      alert("As senhas não correspondem");
    } else {
      // make API call

      this.setState({
        loading: true
      });
      return api.company
        .submitAuth(data)
        .then(data => {
          this.setState({
            loading: false
          });
        })
        .catch(err => {
          this.setState({
            loading: false
          });
        });
    }
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email Invalído";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;
    const options = [
      {
        key: "Google",
        text: "Google",
        value: "Google"
      },
      {
        key: "Site Vipphone",
        text: "Site Vipphone",
        value: "Site Vipphone"
      },
      {
        key: "Indicação",
        text: "Indicação",
        value: "Indicação"
      },
      {
        key: "Contato Telefônico",
        text: "Contato Telefônico",
        value: "Contato Telefônico"
      }
    ];

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {errors.global && (
          <Message negative>
            <Message.Header>Alguma coisa deu errado</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        <Form.Field error={!!errors.name}>
          <label htmlFor="name">Nome</label>
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Nome"
            value={data.name}
            onChange={this.onChange}
          />
          {errors.name && <InlineError text={errors.name} />}
        </Form.Field>
        {/* Nome */}

        <Form.Field error={!!errors.corporate_name}>
          <label htmlFor="corporate_name">Empresa</label>
          <input
            type="text"
            id="corporate_name"
            name="corporate_name"
            placeholder="Empresa"
            value={data.corporate_name}
            onChange={this.onChange}
          />
          {errors.corporate_name && (
            <InlineError text={errors.corporate_name} />
          )}
        </Form.Field>
        {/* Empresa */}
        <Form.Field error={!!errors.segment}>
          <label htmlFor="segment">Segmento</label>
          <Dropdown
            placeholder="Segmentos?"
            fluid
            selection
            name="segment"
            value={data.segment}
            options={options}
            onChange={this.onChange}
          />
          {errors.segment && <InlineError text={errors.segment} />}
        </Form.Field>
        {/* Segmento */}

        <Form.Field error={!!errors.name}>
          <label htmlFor="origin">Origem</label>
          <Dropdown
            placeholder="Onde você descobriu o Monitchat?"
            fluid
            selection
            name="origin"
            value={data.origin}
            options={options}
            onChange={this.onChange}
          />
          {errors.origin && <InlineError text={errors.origin} />}
        </Form.Field>
        {/* Origem */}
        <Form.Field error={!!errors.cpf_cnpj}>
          <label htmlFor="cpf_cnpj">CNPJ</label>
          <input
            type="text"
            id="cpf_cnpj"
            name="cpf_cnpj"
            placeholder="CNPJ"
            value={data.cpf_cnpj}
            onChange={this.onChange}
          />
          {errors.cpf_cnpj && <InlineError text={errors.cpf_cnpj} />}
        </Form.Field>
        {/* CNPJ */}

        <Form.Field error={!!errors.phone_number}>
          <label htmlFor="phone_number">Telefone</label>
          <input
            type="text"
            id="phone_number"
            name="phone_number"
            placeholder="Telefone"
            value={data.phone_number}
            onChange={this.onChange}
          />
          {errors.phone_number && <InlineError text={errors.phone_number} />}
        </Form.Field>
        {/* Telefone */}
        <Form.Field error={!!errors.email}>
          <label htmlFor="email">Email</label>
          <input
            type="text"
            id="email"
            name="email"
            placeholder="exemplo@exemplo.com"
            value={data.email}
            onChange={this.onChange}
          />
          {errors.email && <InlineError text={errors.email} />}
        </Form.Field>
        {/* Email */}

        <Button color="green" className="button_right" onClick="this.onSubmit">
          Criar Conta
        </Button>

        <div className="note">
          <Link to="/forgot"> Esqueceu a senha? </Link>
        </div>
      </Form>
    );
  }
}

RegisterForm.propTypes = {
  submit: PropTypes.func.isRequired
};
export default RegisterForm;
