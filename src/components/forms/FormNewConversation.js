import React from "react";
import { Form, Input, TextArea } from "semantic-ui-react";
import InlineError from "../messages/InlineError";
import InputMask from "react-input-mask";

const FormNewConversation = ({
	onChange,
	onChecked,
	newConversation,
	loading,
	generalError,
	messageError,
	handleContactChange,
	handleMediaChange
}) => {
	return (
		<Form autoComplete="off">
			<Form.Group widths="equal">
				<Form.Field
					error={messageError && !!messageError.phone_number}
				>
					<label htmlFor="message">Número do Contato</label>
					<Input
						autoComplete="off"
						control="Input"
						children={
							<InputMask
								mask="+55 (99) 9999-9999"
								name="phone_number"
								className="number_br"
								onChange={onChange}
								value={newConversation.phone_number}
							/>
						}
					/>
					{messageError && (
						<InlineError text={messageError.phone_number} />
					)}
				</Form.Field>
			</Form.Group>
			<Form.Field error={messageError && !!messageError.message}>
				<label htmlFor="message">Mensagem</label>
				<TextArea
					autoComplete="off"
					control="TextArea"
					name="message"
					onChange={onChange}
					value={newConversation.message}
					placeholder="Escreva sua mensagem"
				/>
				{messageError && (
					<InlineError text={messageError.message} />
				)}
			</Form.Field>
		</Form>
	);
};

export default FormNewConversation;
