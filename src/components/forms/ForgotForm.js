import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Button, Message } from "semantic-ui-react";

import Validator from "validator";
import InlineError from "../messages/InlineError";
import api from "../../api/user";

class ForgotForm extends Component {
  state = {
    data: {
      email: ""
    },
    loading: false,
    errors: {},
    message: ""
  };
  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  onSubmit = () => {
    const { data } = this.state;

    this.setState({
      loading: true
    });
    return api.user
      .forgotPassword(data)
      .then(res => {
        const status = res.status;
        this.setState({
          loading: false,
          message:
            status === 200
              ? "Se o usuário informado existir em nossa base de dados, enviaremos um email com um link para recuperação de senha"
              : "Se o usuário informado existir em nossa base de dados, enviaremos um email com um link para recuperação de senha"
        });
      })
      .catch(err => {
        this.setState({
          loading: false
        });
      });
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email Invalído";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  render() {
    const { data, errors, loading, message } = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {errors.global && (
          <Message negative>
            <Message.Header>Alguma coisa deu errado</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        {message ? (
          <h4>{message}</h4>
        ) : (
          <div>
            <Form.Field error={!!errors.email}>
              <label htmlFor="email">
                Preencha seu e-mail para solicitar nova senha
              </label>
              <input
                type="text"
                id="email"
                name="email"
                placeholder="exemplo@exemplo.com"
                value={data.email}
                onChange={this.onChange}
              />
              {errors.email && <InlineError text={errors.email} />}
            </Form.Field>
            {/* Email */}

            <Button
              color="green"
              className="button_right"
              onClick="this.onSubmit"
            >
              Recuperar Senha
            </Button>
          </div>
        )}
      </Form>
    );
  }
}

ForgotForm.propTypes = {
  submit: PropTypes.func.isRequired
};
export default ForgotForm;
