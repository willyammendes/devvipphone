import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Button, Message } from "semantic-ui-react";

import Validator from "validator";
import InlineError from "../messages/InlineError";
import api from "../../api/user";

class ForgotForm extends Component {
  state = {
    data: {
      password: "",
      passwordConfirmation: ""
    },
    loading: false,
    errors: {},
    message: "",
    token: "",
    status: ""
  };
  componentWillMount() {
    const token = window.location.href.split(/[\s,/]+/);

    this.setState(
      {
        token: token[token.length - 1]
      },
      () => {}
    );
  }

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  onSubmit = () => {
    const { data, token } = this.state;

    this.setState({
      loading: true
    });

    const password = data.password;
    const confirmPassword = data.passwordConfirmation;
    if (password.length < 6) {
      alert("A senha precisa ter mais de 6 caracteres");
      this.setState({
        loading: false
      });
    } else if (password !== confirmPassword) {
      alert("As senhas não correspondem");
      this.setState({
        loading: false
      });
    } else {
      return api.user
        .resetPassword(token, data)
        .then(res => {
          const status = res.data.isAmdin;
          this.setState({
            loading: false,
            message: status
              ? "Sua nova senha foi cadastrada."
              : "As senhas não correspondem.",
            status: "success"
          });
          this.props.submit(res.data.user);
        })
        .catch(err => {
          this.setState({
            loading: false,
            message: "Token Inválido."
          });
        });
    }
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email Invalído";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  render() {
    const { data, errors, loading, message, status } = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {errors.global && (
          <Message negative>
            <Message.Header>Alguma coisa deu errado</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        {message ? (
          <h4>
            {message}
            {status ? (
              <a href="/" className="button_anchor">
                Entrar
              </a>
            ) : (
              ""
            )}
          </h4>
        ) : (
          <div>
            <Form.Field error={!!errors.password}>
              <label htmlFor="password">Informe sua nova senha</label>
              <input
                type="password"
                id="password"
                name="password"
                placeholder="Senha"
                value={data.password}
                onChange={this.onChange}
              />
              {errors.password && <InlineError text={errors.password} />}
            </Form.Field>
            {/* Senha */}
            <Form.Field error={!!errors.passwordConfirmation}>
              <label htmlFor="passwordConfirmation">
                Confirme sua nova senha
              </label>
              <input
                type="password"
                id="passwordConfirmation"
                name="passwordConfirmation"
                placeholder="Senha"
                value={data.passwordConfirmation}
                onChange={this.onChange}
              />
              {errors.passwordConfirmation && (
                <InlineError text={errors.passwordConfirmation} />
              )}
            </Form.Field>
            {/* Senha */}

            <Button
              color="green"
              className="button_right"
              onClick="this.onSubmit"
            >
              Solicitar mudança de senha
            </Button>
          </div>
        )}
      </Form>
    );
  }
}

ForgotForm.propTypes = {
  submit: PropTypes.func.isRequired
};
export default ForgotForm;
