import React from "react";
import PropTypes from "prop-types";
import { Form, Input, Dropdown, TextArea, Checkbox } from "semantic-ui-react";
import DropdownMedia from "../../social-media/DropdownMedia";
import InlineError from "../../messages/InlineError";

const FormUsers = ({
	onChange,
	onChecked,
	newConversation,
	loading,
	generalError,
	messageError
}) => {
	return (
		<Form autoComplete="off">
			<Form.Group widths="equal">
				<Form.Field error={messageError ? !!messageError.name : ""}>
					<label htmlFor="name">Nome</label>
					<Input
						autoComplete="off"
						control={Input}
						name="name"
						onChange={onChange}
						value={user.name}
						placeholder="Nome"
					/>
					{messageError ? (
						<InlineError text={messageError.name} />
					) : (
						""
					)}
				</Form.Field>
			</Form.Group>
		</Form>
	);
};

export default FormUsers;
