import api from "../../api/stats";

// Types declaration
export const Types = {
  IS_FETCHING_OPEN_TICKETS_COUNT: "stats/IS_FETCHING_OPEN_TICKETS_COUNT",
  IS_FETCHING_CLOSED_TICKETS_COUNT: "stats/IS_FETCHING_CLOSED_TICKETS_COUNT",
  IS_FETCHING_ASSIGNED_TICKETS_COUNT: "stats/IS_FETCHING_ASSIGNED_TICKETS_COUNT",
  IS_FETCHING_BOT_ASSIGNED_TICKETS_COUNT: "stats/IS_FETCHING_BOT_ASSIGNED_TICKETS_COUNT",
  IS_FETCHING_WAITING_TICKETS_COUNT: "stats/IS_FETCHING_WAITING_TICKETS_COUNT",
  IS_FETCHING_BOT_CLOSED_TICKETS_COUNT: "stats/IS_FETCHING_BOT_CLOSED_TICKETS_COUNT",
  OPEN_TICKETS_COUNT_FETCHED: "stats/OPEN_TICKETS_COUNT_FETCHED",
  CLOSED_TICKETS_COUNT_FETCHED: "stats/CLOSED_TICKETS_COUNT_FETCHED",
  WAITING_TICKETS_COUNT_FETCHED: "stats/WAITING_TICKETS_COUNT_FETCHED",
  ASSIGNED_TICKETS_COUNT_FETCHED: "stats/ASSIGNED_TICKETS_COUNT_FETCHED",
  ASSIGNED_BOT_TICKETS_COUNT_FETCHED: "stats/ASSIGNED_BOT_TICKETS_COUNT_FETCHED",
  CLOSED_BOT_TICKETS_COUNT_FETCHED: "stats/CLOSED_BOT_TICKETS_COUNT_FETCHED",
};

// Reducer
const INITIAL_STATE = {
  openTicketsCount: 0,
  assignedTicketsCount: 0,
  assignedTicketsByBotCount: 0,
  waitingTicketCount: 0,
  closedTicketsCount: 0,
  closedTicketsByBotCount: 0,
  loadingOpenTicketsCount: true,
  loadingClosedTicketsCount: false,
  loadingAssignedTicketsCount: false,
  loadingWaitingTicketsCount: false,
  loadingClosedTicketsByBotCount: false,
  loadingAssignedTicktsByBotCount: false,
};

export default function conversation(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case Types.IS_FETCHING_OPEN_TICKETS_COUNT:
        return { ...state, loadingOpenTicketsCount: action.payload };
    case Types.IS_FETCHING_CLOSED_TICKETS_COUNT:
        return { ...state, loadingClosedTicketsCount: action.payload };
    case Types.IS_FETCHING_ASSIGNED_TICKETS_COUNT:
        return { ...state, loadingAssignedTicketsCount: action.payload };
    case Types.IS_FETCHING_BOT_ASSIGNED_TICKETS_COUNT:
        return { ...state, loadingAssignedTicktsByBotCount: action.payload };
    case Types.IS_FETCHING_WAITING_TICKETS_COUNT:
            return { ...state, loadingWaitingTicketsCount: action.payload };
    case Types.IS_FETCHING_BOT_CLOSED_TICKETS_COUNT:
        return { ...state, loadingClosedTicketsByBotCount: action.payload };
    case Types.OPEN_TICKETS_COUNT_FETCHED:
        return { ...state, openTicketsCount: action.payload };
    case Types.CLOSED_TICKETS_COUNT_FETCHED:
        return { ...state, closedTicketsCount: action.payload };
    case Types.WAITING_TICKETS_COUNT_FETCHED:
        return { ...state, waitingTicketCount: action.payload };
    case Types.ASSIGNED_TICKETS_COUNT_FETCHED:
        return { ...state, assignedTicketsCount: action.payload };
    case Types.ASSIGNED_BOT_TICKETS_COUNT_FETCHED:
        return { ...state, assignedTicketsByBotCount: action.payload };
    case Types.CLOSED_BOT_TICKETS_COUNT_FETCHED:
        return { ...state, closedTicketsByBotCount: action.payload };
    default:
      return state;
  }
}

// Actions
export const Creators = {
    
    getOpenTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_OPEN_TICKETS_COUNT, payload: true})
        api.openTickets.fetchAll(params)
            .then( count => dispatch({ type: Types.OPEN_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_OPEN_TICKETS_COUNT, payload: false}))
    },
    getClosedTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_CLOSED_TICKETS_COUNT, payload: true})
        api.closedTickets.fetchAll(params)
            .then( count => dispatch({ type: Types.CLOSED_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_CLOSED_TICKETS_COUNT, payload: false}))
    },
    getWaitingTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_WAITING_TICKETS_COUNT, payload: true})
        api.waitingTicket.fetchAll(params)
            .then( count => dispatch({ type: Types.WAITING_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_WAITING_TICKETS_COUNT, payload: false}))
    },
    getAssignedTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_ASSIGNED_TICKETS_COUNT, payload: true})
        api.openTickets.fetchAll(params)
            .then( count => dispatch({ type: Types.ASSIGNED_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_ASSIGNED_TICKETS_COUNT, payload: false}))
    },
    getBotClosedTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_BOT_CLOSED_TICKETS_COUNT, payload: true})
        api.closedTicketsByBot.fetchAll(params)
            .then( count => dispatch({ type: Types.CLOSED_BOT_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_BOT_CLOSED_TICKETS_COUNT, payload: false}))
    },
    getBotAssignedTicketsCount: params => dispatch => {
        dispatch({ type: Types.IS_FETCHING_BOT_ASSIGNED_TICKETS_COUNT, payload: true})
        api.closedTicketsByBot.fetchAll(params)
            .then( count => dispatch({ type: Types.ASSIGNED_BOT_TICKETS_COUNT_FETCHED, payload: count}))
            .then(() => dispatch({ type: Types.IS_FETCHING_BOT_ASSIGNED_TICKETS_COUNT, payload: false}))
    }
};
