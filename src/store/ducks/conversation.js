import api from "../../api/conversation";
import apicontact from "../../api/contact";
import apimedia from "../../api/social-media";
import moment from "moment";
import "moment/locale/pt-br";

// Types declaration
export const Types = {
  FETCHED: "conversation/FETCHED",
  FETCHED_SEARCH: "conversation/FETCHED_SEARCH",
  GET_CONVERSATION: "conversation/GET_CONVERSATION",
  UNREAD_MESSAGES_CHANGED: "UNREAD_MESSAGES_CHANGED",
  FETCHED_MEDIA: "conversation/FETCHED_MEDIA",
  ON_ERROR: "conversation/ON_ERROR",
  IS_FETCHING: "conversation/IS_FETCHING",
  IS_FETCHED: "conversation/IS_FETCHED",
  SELECTED: "conversation/SELECTED",
  UNSELECTED: "conversation/UNSELECTED",
  MESSAGES_FETCHED: "conversation/MESSAGES_FETCHED",
  IS_FETCHING_TICKETS: "conversation/IS_FETCHING_TICKETS",
  IS_FETCHING_MESSAGES: "conversation/IS_FETCHING_MESSAGES",
  TICKETS_FETCHED: "conversation/TICKETS_FETCHED",
  SET_ACTIVE_TICKET: "conversation/SET_ACTIVE_TICKET",
  SET_TICKET_USER: "conversation/SET_TICKET_USER",
  TICKET_USER_CHANGED: "conversation/TICKET_USER_CHANGED",
  ADD_MESSAGE: "conversation/ADD_MESSAGE",
  AUTO_REPLY: "conversation/AUTO_REPLY",
  UPDATE_MESSAGE_STATUS: "conversation/UPDATE_MESSAGE_STATUS",
  ADD_COMMENT: "conversation/ADD_COMMENT",
  ADD_TICKET: "conversation/ADD_TICKET",
  TICKET_ADDED: "conversation/TICKET_ADDED",
  USER_LOGGED_OUT: "user/USER_LOGGED_OUT",
  SET_ENTER_AS_SEND: "conversation/SET_ENTER_AS_SEND",
  SET_FILTER: "conversation/SET_FILTER",
  STATS_FETCHED: "conversation/STATS_FETCHED",
  ARCHIVE_CONVERSATION: "conversation/ARCHIVE_CONVERSATION",
  ADD_USER_WALLET: "conversation/ADD_USER_WALLET",
  CHANGE_CLIENT: "conversation/CHANGE_CLIENT",
  UPDATE_CLIENT: "conversation/UPDATE_CLIENT",
};

// Reducer
const INITIAL_STATE = {
  data: [],
  media: [],
  activeConversationId: -1,
  loading: true,
  error: {},
  loadingTickets: false,
  loadingMessages: false,
  enterAsSend: true,
  sendingMessage: false,
  filter: "",
  loadingStats: false,
};

export default function conversation(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case Types.FETCHED:
      return {
        ...state,
        data: state.data.concat(
          action.data.filter((el) => {
            return !state.data.some((a) => a.id === el.id);
          })
        ),
        loading: false,
      };
    case Types.FETCHED_SEARCH:
      return { ...state, data: action.data, loading: false };
    case Types.FETCHED_MEDIA:
      return { ...state, media: action.data, loading: false };
    case Types.SELECTED:
      return state.activeConversationId !== action.id
        ? { ...state, activeConversationId: action.id }
        : state;
    case Types.UNSELECTED:
      return { ...state, activeConversationId: action.payload };
    case Types.GET_CONVERSATION:
      const duplicateCheck = state.data.find((c) => c.id === action.payload.id);
      if (!duplicateCheck) {
        return {
          ...state,
          data: [action.payload, ...state.data],
        };
      }
    case Types.ON_ERROR:
      return {
        ...state,
        activeConversation: {},
        loading: false,
        error: action.error,
      };
    case Types.IS_FETCHING_TICKETS:
      return { ...state, loadingTickets: action.loadingTickets };
    case Types.IS_FETCHING_MESSAGES:
      return { ...state, loadingMessages: action.loadingMessages };
    case Types.USER_LOGGED_OUT:
      return INITIAL_STATE;
    case Types.SET_ENTER_AS_SEND:
      return { ...state, enterAsSend: !state.enterAsSend };

    case Types.TICKETS_FETCHED: {
      const { tickets } = action;
      const { activeConversationId } = state;
      const index = state.data.findIndex((c) => c.id === activeConversationId);

      const joincomments = tickets.map((t) =>
        t.comments.map((c) => ({
          id: c.id,
          ticket_number: t.ticket_number,
          forwardedTo: c.forwardedTo,
          user: c.user,
          avatar: c.avatar,
          progress_percentage: c.progress_percentage,
          status: c.status,
          icon: c.icon,
          created_at: c.created_at,
          timestamp: c.timestamp,
          message: c.message,
          source: "comment",
        }))
      );

      const newTicketsArray = tickets;

      const newState = {
        ...state,
        loadingTickets: false,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            tickets: state.data[index].tickets.find((c) =>
              newTicketsArray.map((d) => c.id === d.id)
            )
              ? state.data[index].tickets
              : [...state.data[index].tickets].concat(...newTicketsArray),

            messages:
              !!state.data[index].messages
                .filter(
                  (c) =>
                    c.source === "whatsapp" ||
                    c.source === "campaing" ||
                    c.source === "phone" ||
                    c.source === "monitchat" ||
                    c.source === "whatsapp" ||
                    c.source === "email"
                )
                .find((ticket) =>
                  newTicketsArray.map((d) => ticket.id === d.id)
                ) ||
              !!state.data[index].messages
                .filter(
                  (c) =>
                    c.source === "whatsapp" ||
                    c.source === "user_comment" ||
                    c.source === "campaing" ||
                    c.source === "phone" ||
                    c.source === "monitchat" ||
                    c.source === "whatsapp" ||
                    c.source === "email"
                )
                .find((ticket) => joincomments.map((d) => ticket.id === d.id))
                ? state.data[index].messages
                : [...state.data[index].messages]
                    .concat(newTicketsArray)
                    .concat(...joincomments),

            activeTicketIndex: tickets.length - 1,
          },
          ...state.data.slice(index + 1),
        ],
        activeConversationId,
      };

      return newState;
    }
    case Types.ADD_TICKET: {
      const ticket = action.payload;
      const conversation_id = ticket.conversation_id;
      const activeConversation = state.data.find(
        (c) => c.id === conversation_id
      );
      const index = state.data.findIndex((c) => c.id === conversation_id);

      const timestamp = Math.floor(new Date().getTime() / 1000);

      const NewTicket = {
        ...ticket,
        id: ticket.id,
        ticket_number: ticket.ticket_number,
        created_at: moment
          .unix(timestamp)
          .locale("pt-br")
          .format("lll"),
        current_progress: "0.00",
        timestamp,
        user: ticket.user_id,
        user_avatar: "",
        source: ticket.source,
      };

      if (activeConversation) {
        const newState = {
          ...state,
          loadingTickets: false,
          data: [
            {
              ...activeConversation,
              archived: 0,
              active_ticket_id: NewTicket.id,
              tickets: activeConversation.messagesLoaded
                ? [...activeConversation.tickets, { ...NewTicket }]
                : [],
              messages: activeConversation.messagesLoaded
                ? [...activeConversation.messages, { ...NewTicket }]
                : [],
            },
            ...state.data.slice(0, index),
            ...state.data.slice(index + 1),
          ],
        };
        return newState;
      }
      return state;
    }
    case Types.ADD_USER_WALLET: {
      const wallet = action.payload;
      const conversationId = state.data.find(
        (c) => c.contact.id === wallet.contact
      );
      const index = state.data.findIndex((c) => c.id === conversationId.id);

      const newState = {
        ...state,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            contact: {
              ...state.data[index].contact,
              users: [
                state.data[index].contact.users[0] === wallet.user_id
                  ? ""
                  : wallet.user_id,
              ],
            },
          },
          ...state.data.slice(index + 1),
        ],
      };
      return newState;
    }
    case Types.ARCHIVE_CONVERSATION: {
      const { conversation } = action.payload;

      const index = state.data.findIndex((c) => c.id === conversation.id);
      const newState = {
        ...state,
        activeConversationId:
          conversation.id === state.activeConversationId
            ? -1
            : state.activeConversationId,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            archived: conversation.archived,
          },
          ...state.data.slice(index + 1),
        ],
      };
      return newState;
    }
    case Types.CHANGE_CLIENT: {
      const { conversation } = action.payload;
      const index = state.data.findIndex((c) => c.id === conversation.id);
      const newState = {
        ...state,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            contact: {
              ...state.data[index].contact,
              client: conversation.contact.client
                ? conversation.contact.client
                : [],
              name: conversation.contact.name ? conversation.contact.name : [],
            },
          },
          ...state.data.slice(index + 1),
        ],
      };
      return newState;
    }

    case Types.SET_ACTIVE_TICKET: {
      const { ticket } = action;
      const { activeConversationId } = state;
      const index = state.data.findIndex((c) => c.id === activeConversationId);
      const activeConversation = state.data[index];
      if (activeConversation) {
        const activeTicketIndex = activeConversation.tickets.findIndex(
          (t) => t.id === ticket.id
        );

        const newState = {
          ...state,
          loadingTickets: false,
          data: [
            ...state.data.slice(0, index),
            {
              ...activeConversation,
              activeTicketIndex,
              active_ticket_id: ticket.id,
            },
            ...state.data.slice(index + 1),
          ],
        };

        return newState;
      }
      return state;
    }
    case Types.SET_TICKET_USER: {
      const { tickets } = action;
      const { activeConversationId } = state;
      const index = state.data.findIndex((c) => c.id === activeConversationId);
      const activeConversation = state.data[index];

      const newTicketsArray = tickets.data.map((t) => ({
        id: t.id,
        ticket_number: t.ticket_number,
        created_at: t.created_at,
        current_progress: t.current_progress,
        timestamp: t.timestamp,
        user: t.user,
        user_avatar: t.user_avatar,
        source: t.source,
      }));

      const newState = {
        ...state,
        loadingTickets: false,
        data: [
          ...state.data.slice(0, index),
          {
            ...activeConversation,

            tickets: newTicketsArray,
          },
          ...state.data.slice(index + 1),
        ],
      };

      return newState;
    }

    case Types.TICKET_USER_CHANGED: {
      const { payload: ticket } = action;

      const newState = {
        ...state,
        data: state.data.filter((c) => c.id !== ticket.conversation_id),
      };

      return newState;
    }

    case Types.MESSAGES_FETCHED: {
      const { messages } = action;
      const { activeConversationId } = state;
      const index = state.data.findIndex((c) => c.id === activeConversationId);

      const newState = {
        ...state,
        loadingMessages: false,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            messages: state.data[index].messages.find((c) =>
              messages.data.data.map((d) => c.id === d.id)
            )
              ? state.data[index].messages
              : [...state.data[index].messages].concat(
                  messages.data.data.filter(
                    (c) => c.id !== state.data[index].messages.id
                  )
                ),

            messagesLoaded: true,
          },
          ...state.data.slice(index + 1),
        ],
        activeConversationId,
      };

      return newState;
    }
    case Types.STATS_FETCHED: {
      const { stats, id } = action;
      const index = state.data.findIndex((c) => c.id === id);

      const newState = {
        ...state,
        loadingStats: false,
        data: [
          ...state.data.slice(0, index),
          {
            ...state.data[index],
            stats: stats.data,
          },
          ...state.data.slice(index + 1),
        ],
      };

      return newState;
    }
    case Types.ADD_MESSAGE: {
      const message = action.payload;
      const { conversation_id } = message;
      const conversation = state.data.find((c) => c.id === conversation_id);
      const index = state.data.findIndex((c) => c.id === conversation_id);

      if (conversation) {
        const ticket = conversation.tickets.find(
          (t) => t.id === message.ticket_id
        );

        if (ticket || !message.ticket_id) {
          const newState = {
            ...state,
            loadingTickets: false,
            data: [
              {
                ...conversation,
                last_message: {
                  message: message.message,
                  created_at: message.human_date,
                  type: message.message_type,
                },
                archived: false,
                unread_messages:
                  !message.sender || message.sender === 0
                    ? conversation.unread_messages + 1
                    : conversation.unread_messages,
                messages:
                  conversation.messages.find((c) => c.id === message.id) &&
                  conversation.messagesLoaded
                    ? conversation.messages
                    : [...conversation.messages, { ...message }],
              },
              ...state.data.slice(0, index),
              ...state.data.slice(index + 1),
            ],
          };
          return newState;
        }
      }
      return state;
    }

    case Types.ADD_COMMENT: {
      const message = action.payload.data;
      const { conversation_id } = message;
      const activeConversation = state.data.find(
        (c) => c.id === conversation_id
      );

      const index = state.data.findIndex((c) => c.id === conversation_id);
      const indexTicket = activeConversation.tickets.findIndex(
        (c) => c.id === message.ticket_id
      );

      if (message.status) {
        const newState = {
          ...state,
          loadingTickets: false,
          data: [
            ...state.data.slice(0, index),
            {
              ...activeConversation,

              tickets: [
                ...activeConversation.tickets.slice(0, indexTicket),
                {
                  ...activeConversation.tickets[indexTicket],
                  progress_percentage: message.status.progress_percentage,
                  current_progress: message.status.progress_percentage,
                },
                ...activeConversation.tickets.slice(indexTicket + 1),
              ],

              messages: [...activeConversation.messages].concat({
                ...message,
                message: message.comment,
                user: message.user.name,
                avatar: message.user.avatar,
              }),
            },
            ...state.data.slice(index + 1),
          ],
        };
        return newState;
      }
      const newState = {
        ...state,
        loadingTickets: false,
        data: [
          ...state.data.slice(0, index),
          {
            ...activeConversation,

            messages: [...activeConversation.messages].concat({
              ...message,
              message: message.comment,
              user: message.user.name,
              avatar: message.user.avatar,
            }),
          },
          ...state.data.slice(index + 1),
        ],
      };
      return newState;
    }

    case Types.AUTO_REPLY: {
      const autoReply = action.payload;
      const { conversation_id } = autoReply;
      const activeConversation = state.data.find(
        (c) => c.id === conversation_id
      );
      const index = state.data.findIndex((c) => c.id === conversation_id);

      const newState = {
        ...state,
        data: [
          ...state.data.slice(0, index),
          {
            ...activeConversation,
            auto_reply: autoReply.auto_reply,
          },
          ...state.data.slice(index + 1),
        ],
      };
      return newState;
    }
    case Types.UNREAD_MESSAGES_CHANGED: {
      const unreadMessages = action.payload;

      const { conversationId, unreadMessages: unread } = unreadMessages;
      const activeConversation = state.data.find(
        (c) => c.id === conversationId
      );
      const index = state.data.findIndex((c) => c.id === conversationId);

      if (activeConversation) {
        const newState = {
          ...state,
          data: [
            ...state.data.slice(0, index),
            {
              ...activeConversation,
              unread_messages: unread,
            },
            ...state.data.slice(index + 1),
          ],
        };
        return newState;
      }

      return state;
    }
    case Types.UPDATE_MESSAGE_STATUS: {
      const message = action.payload;
      const { conversation_id } = message;
      const activeConversation = state.data.find(
        (c) => c.id === conversation_id
      );
      const index = state.data.findIndex((c) => c.id === conversation_id);
      if (activeConversation) {
        const messageIndex = activeConversation.messages.findIndex((m) =>
          message.message_token && message.message_token.length > 0
            ? m.message_token === message.message_token
            : m.id === message.id
        );

        if (messageIndex > -1 || message.id) {
          const newState = {
            ...state,
            loadingTickets: false,
            filter: "",
            data: [
              ...state.data.slice(0, index),
              {
                ...activeConversation,
                last_message: {
                  message: message.message,
                  created_at: message.human_date,
                  type: message.message_type,
                },
                messages: activeConversation.messages.find((c) =>
                  message.message_token && message.message_token.length > 0
                    ? c.message_token === message.message_token
                    : c.id === message.id
                )
                  ? [
                      ...activeConversation.messages.slice(0, messageIndex),
                      {
                        ...activeConversation.messages[messageIndex],
                        ...message,
                      },
                      ...activeConversation.messages.slice(messageIndex + 1),
                    ]
                  : activeConversation.messages.find((c) =>
                      message.message_token && message.message_token.length > 0
                        ? c.message_token === message.message_token
                        : c.id === message.id
                    ) && activeConversation.messagesLoaded
                  ? activeConversation.messages
                  : [...activeConversation.messages, { ...message }],
              },
              ...state.data.slice(index + 1),
            ],
          };
          return newState;
        }
        return state;
      }
    }
    case Types.SET_FILTER:
      return {
        ...state,
        filter: action.filter,
      };
    case Types.UPDATE_CLIENT:
      return {
        ...state,
        data: action.conversation,
      };

    default:
      return state;
  }
}

// Actions
export const Creators = {
  getStats: (activeConversation) => (dispatch) => {
    api.conversations.getStats(activeConversation.id).then((stats) => {
      dispatch({
        type: Types.STATS_FETCHED,
        stats,
        id: activeConversation.id,
      });
    });
  },
  fetchAll: (filter) => (dispatch) =>
    api.conversations
      .fetchAll(filter)
      .then((conversations) =>
        dispatch({ type: Types.FETCHED, data: conversations })
      )
      .catch((err) => dispatch(Creators.error(err.response))),
  fetchMedia: () => (dispatch) =>
    apimedia.media
      .fetchData()
      .then((media) => dispatch({ type: Types.FETCHED_MEDIA, data: media }))
      .catch((err) => dispatch(Creators.error(err.response))),
  fetchMessagesTake: (id, take, length) => (dispatch) =>
    api.conversations
      .fetchMessages(id, take, length)
      .then((messages) => dispatch({ type: Types.MESSAGES_FETCHED, messages }))
      .catch((err) => dispatch(Creators.error(err.response))),
  addTicket: (ticket) => (dispatch) => {
    dispatch({ type: Types.ADD_TICKET, payload: ticket });
    Creators.scrollToMessage();
  },
  archiveConversation: (conversation) => (dispatch) => {
    dispatch({
      type: Types.ARCHIVE_CONVERSATION,
      payload: conversation,
    });
  },
  ticketUserChanged: (ticket) => (dispatch) => {
    dispatch({
      type: Types.TICKET_USER_CHANGED,
      payload: ticket,
    });
  },
  changeClient: (conversation) => (dispatch) => {
    dispatch({
      type: Types.CHANGE_CLIENT,
      payload: conversation,
    });
  },
  fetchTicketsTake: (id, take, length) => (dispatch) =>
    api.conversations
      .fetchTickets(id, take, length)
      .then((tickets) =>
        dispatch({ type: Types.TICKETS_FETCHED, tickets: tickets.data })
      )
      .catch((err) => dispatch(Creators.error(err.response))),
  unselectConversation: () => (dispatch) =>
    dispatch({ type: Types.UNSELECTED, payload: -1 }),
  conversationSelected: (activeConversation) => async (dispatch) => {
    if (activeConversation) {
      dispatch({ type: Types.SELECTED, id: activeConversation.id });
      dispatch({
        type: Types.IS_FETCHING_MESSAGES,
        loadingMessages: true,
      });

      await api.conversations
        .fetchMessages(activeConversation.id)
        .then((messages) => {
          dispatch({
            type: Types.MESSAGES_FETCHED,
            messages,
          });
          setTimeout(() => {
            const message = document.querySelector("textarea[name='message']");
            if (message) {
              message.focus();
            }

            const body = document.querySelector(".chat .scrollbar-container");
            const boxes = document.querySelector(".wrap_boxes");
            if (body && boxes) {
              body.scrollTo(0, boxes.scrollHeight);
            }
          }, 200);
        })
        .catch((err) => dispatch(Creators.error(err.response)));

      dispatch({
        type: Types.IS_FETCHING_TICKETS,
        loadingTickets: true,
      });

      await api.conversations
        .fetchTickets(activeConversation.id)
        .then((tickets) => {
          dispatch({
            type: Types.TICKETS_FETCHED,
            tickets: tickets.data,
          });

          const body = document.querySelector(".chat .scrollbar-container");
          const boxes = document.querySelector(".wrap_boxes");

          if (body && boxes) {
            body.scrollTo(0, boxes.scrollHeight);
          }
        })
        .catch((err) => dispatch(Creators.error(err.response)));
    } else {
      setTimeout(() => {
        const message = document.querySelector("textarea[name='message']");
        if (message) {
          message.focus();
        }

        const body = document.querySelector(".chat .scrollbar-container");
        const boxes = document.querySelector(".wrap_boxes");

        if (body && boxes) {
          body.scrollTo(0, boxes.scrollHeight);
        }
      }, 200);
    }
  },
  addTextMessage: (message) => (dispatch) => {
    dispatch({ type: Types.ADD_MESSAGE, payload: message });
    Creators.scrollToMessage();
  },
  updateMessageStatus: (message) => (dispatch) => {
    dispatch({
      type: Types.UPDATE_MESSAGE_STATUS,
      payload: message,
    });
  },
  setActiveTicket: (ticket, e) => (dispatch) => {
    if (e !== null) {
      e.preventDefault();
    }
    dispatch({ type: Types.SET_ACTIVE_TICKET, ticket });

    api.conversations.update({
      active_ticket_id: ticket.id,
      id: ticket.conversation_id,
    });
  },

  setTicketUser: (id) => (dispatch) =>
    api.conversations
      .fetchTickets(id)
      .then((tickets) => dispatch({ type: Types.SET_TICKET_USER, tickets }))
      .catch((err) => dispatch(Creators.error(err.response))),

  sendTextMessage: (conversationId, message) => (dispatch) => {
    dispatch({ type: Types.ADD_MESSAGE, payload: message });
    return api.conversations
      .sendTextMessage(conversationId, message)
      .then((data) => {
        dispatch({
          type: Types.UPDATE_MESSAGE_STATUS,
          payload: data,
        });
      });
  },
  resendMessage: (message) => (dispatch) => {
    message.status = -1;
    dispatch({
      type: Types.UPDATE_MESSAGE_STATUS,
      payload: message,
    });

    return api.conversations.resendMessage(message);
  },
  sendDocumentMessage: (conversationId, message) => (dispatch) => {
    dispatch({ type: Types.ADD_MESSAGE, payload: message });

    return api.conversations
      .sendDocumentMessage(conversationId, message)
      .then((data) => {
        dispatch({
          type: Types.UPDATE_MESSAGE_STATUS,
          payload: data,
        });
      });
  },
  sendVoiceMessage: (conversationId, message) => (dispatch) => {
    dispatch({ type: Types.ADD_MESSAGE, payload: message });

    return api.conversations
      .sendVoiceMessage(conversationId, message)
      .then((data) => {
        dispatch({
          type: Types.UPDATE_MESSAGE_STATUS,
          payload: data,
        });
      });
  },
  getConversation: (conversationId) => (dispatch) => {
    return api.conversations.get(conversationId).then((data) => {
      dispatch({
        type: Types.GET_CONVERSATION,
        payload: data.data.data,
      });
    });
  },
  sendImageMessage: (conversationId, message) => (dispatch) => {
    dispatch({ type: Types.ADD_MESSAGE, payload: message });

    return api.conversations
      .sendImageMessage(conversationId, message)
      .then((data) => {
        dispatch({
          type: Types.UPDATE_MESSAGE_STATUS,
          payload: data,
        });
      });
  },
  enableAutoReply: (conversationId, autoReply) => (dispatch) =>
    api.conversations
      .enableAutoReply(conversationId, autoReply)
      .then((data) => {
        dispatch({ type: Types.AUTO_REPLY, payload: autoReply });
      }),

  changeAutoReply: (conversationId, autoReply) => (dispatch) => {
    dispatch({ type: Types.AUTO_REPLY, payload: autoReply });
  },
  changeUnreadMessages: (data) => (dispatch) => {
    dispatch({
      type: Types.UNREAD_MESSAGES_CHANGED,
      payload: data,
    });
  },

  sendComment: (conversationId, ticketId) => (dispatch) =>
    api.conversations.sendComment(conversationId, ticketId).then((data) => {
      const datasend = {
        ...data.data,
        ticket_number: ticketId,
      };
      const joinData = { ...data, data: datasend };

      dispatch({ type: Types.ADD_COMMENT, payload: joinData });
      Creators.scrollToMessage();
    }),

  WalletToConversation: (data) => (dispatch) => {
    dispatch({ type: Types.ADD_USER_WALLET, payload: data });
  },

  error: (data) => ({
    type: Types.ON_ERROR,
    error: data,
  }),
  toggleEnterAsSend: () => ({
    type: Types.SET_ENTER_AS_SEND,
  }),
  foucsTextArea: () => {
    const message = document.querySelector("textarea[name='message']");
    if (message) {
      message.focus();
    }
  },
  scrollToMessage: () => {
    try {
      const body = document.querySelector(".chat .scrollbar-container");
      const boxes = document.querySelector(".wrap_boxes");
      if (body) {
        body.scrollTo(0, boxes.scrollHeight);
      }
    } catch (err) {}
  },
  setFilter: (filter) => ({
    type: Types.SET_FILTER,
    filter,
  }),
  updateContact: () => async (dispatch) => {
    const conversation = await api.conversations.fetchAll();
    dispatch({
      type: Types.UPDATE_CLIENT,
      conversation,
    });
  },
  fetchSearch: (search) => (dispatch) =>
    api.conversations
      .fetchAll(
        search.length > 0
          ? { search }
          : { filter: JSON.stringify([["archived", "=", 0]]) }
      )
      .then((conversations) =>
        dispatch({
          type: Types.FETCHED_SEARCH,
          data: conversations,
        })
      )
      .catch((err) => dispatch(Creators.error(err.response))),
};
