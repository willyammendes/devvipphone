import axios from "../services/api";

export default {
	restricted: {
		fetchAll: () =>
			axios.get("/contact?restricted").then(res => res.data.data)
	}
};
