import axios from "../services/api";

export default {
	schedule: {
		fetchAll: params =>
			axios.get(`/schedule`, { params }).then(res => res.data),
		delete: id => axios.delete(`/schedule/${id}`).then(res => res.data),
		update: (id, schedule) =>
			axios.put(`/schedule/${id}`, schedule).then(res => res.data),
		submit: schedule =>
			axios.post(`/schedule`, schedule).then(res => res.data)
	}
};
