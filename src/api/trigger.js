import axios from "../services/api";

export default {
  intent: {
    fetchAll: params => axios.get(`/trigger`, { params }).then(res => res.data),
    delete: id => axios.delete(`/trigger/${id}`).then(res => res.data),
    update: (id, intent) =>
      axios.put(`/trigger/${id}`, intent).then(res => res.data),
    submit: intent => axios.post(`/trigger`, intent).then(res => res.data),
    fetchActions: () =>
      axios.get(`/bot-action/botActionTypes`).then(res => res.data)
  }
};
