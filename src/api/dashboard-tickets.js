import axios from "../services/api";

export default {
	ticketsDashboardTable: {
		fetchAll: params =>
			axios
				.get(`/statistic/ticketsDashboardTable`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/ticketsDashboardTable/${id}`)
				.then(res => res.data),
		update: (id, ticketsDashboardTable) =>
			axios
				.put(
					`/statistic/ticketsDashboardTable/${id}`,
					ticketsDashboardTable
				)
				.then(res => res.data),
		submit: ticketsDashboardTable =>
			axios
				.post(`/statistic/ticketsDashboardTable`, ticketsDashboardTable)
				.then(res => res.data)
	}
};
