import axios from "../services/api";

export default {
	activities: {
		fetchAll: params =>
			axios
				.get(`/statistic/activities`, { params })
				.then(res => res.data),
		delete: id =>
			axios.delete(`/statistic/activities/${id}`).then(res => res.data),
		update: (id, activities) =>
			axios
				.put(`/statistic/activities/${id}`, activities)
				.then(res => res.data),
		submit: activities =>
			axios
				.post(`/statistic/activities`, activities)
				.then(res => res.data)
	}
};
