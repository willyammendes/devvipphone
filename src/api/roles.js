import axios from "../services/api";

export default {
	role: {
		fetchAll: params =>
			axios.get(`/role`, { params }).then(res => res.data),
		fetchPermission: params =>
			axios.get(`/permission`, { params }).then(res => res.data),
		delete: (id, role) =>
			axios.post(`/role/${id}`, role).then(res => res.data),
		update: (id, role) =>
			axios.put(`/role/${id}`, role).then(res => res.data),
		submit: role => axios.post(`/role`, role).then(res => res.data)
	}
};
