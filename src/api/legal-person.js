import axios from "../services/api";

export default {
	legalPerson: {
		fetchAll: params =>
			axios
				.get(`/statistic/legalPerson`, { params })
				.then(res => res.data),
		delete: id =>
			axios.delete(`/statistic/legalPerson/${id}`).then(res => res.data),
		update: (id, legalPerson) =>
			axios
				.put(`/statistic/legalPerson/${id}`, legalPerson)
				.then(res => res.data),
		submit: legalPerson =>
			axios
				.post(`/statistic/legalPerson`, legalPerson)
				.then(res => res.data)
	}
};
