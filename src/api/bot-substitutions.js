import axios from "../services/api";

export default {
	substitution: {
		fetchAll: params =>
			axios.get(`/bot-substitution`, { params }).then(res => res.data),
		delete: id =>
			axios.delete(`/bot-substitution/${id}`).then(res => res.data),
		update: (id, substitution) =>
			axios
				.put(`/bot-substitution/${id}`, substitution)
				.then(res => res.data),
		submit: substitution =>
			axios.post(`/bot-substitution`, substitution).then(res => res.data)
	}
};
