import axios from "../services/api";

export default {
	listWaitingTickets: {
		fetchAll: params =>
			axios
				.get(`/statistic/listWaitingTickets`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/listWaitingTickets/${id}`)
				.then(res => res.data),
		update: (id, listWaitingTickets) =>
			axios
				.put(`/statistic/listWaitingTickets/${id}`, listWaitingTickets)
				.then(res => res.data),
		submit: listWaitingTickets =>
			axios
				.post(`/statistic/listWaitingTickets`, listWaitingTickets)
				.then(res => res.data)
	}
};
