import axios from "../services/api";

export default {
	context: {
		fetchAll: params =>
			axios.get(`/bot-context`, { params }).then(res => res.data),
		delete: id => axios.delete(`/bot-context/${id}`).then(res => res.data),
		update: (id, context) =>
			axios.put(`/bot-context/${id}`, context).then(res => res.data),
		submit: context =>
			axios.post(`/bot-context`, context).then(res => res.data)
	}
};
