import axios from "../services/api";

export default {
	categorie: {
		fetchAll: () => axios.get("/ticket-category").then(res => res.data.data)
	}
};
