import axios from "../services/api";

export default {
	listOpenTickets: {
		fetchAll: params =>
			axios
				.get(`/statistic/listOpenTickets`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/listOpenTickets/${id}`)
				.then(res => res.data),
		update: (id, listOpenTickets) =>
			axios
				.put(`/statistic/listOpenTickets/${id}`, listOpenTickets)
				.then(res => res.data),
		submit: listOpenTickets =>
			axios
				.post(`/statistic/listOpenTickets`, listOpenTickets)
				.then(res => res.data)
	}
};
