import axios from "../services/api";

export default {
	professional: {
		fetchAll: params =>
			axios.get(`/professional`, { params }).then(res => res.data),
		delete: id => axios.delete(`/professional/${id}`).then(res => res.data),
		update: (id, professional) =>
			axios
				.put(`/professional/${id}`, professional)
				.then(res => res.data),
		submit: professional =>
			axios.post(`/professional`, professional).then(res => res.data)
	}
};
