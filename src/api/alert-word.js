import axios from "../services/api";

export default {
	alertword: {
		fetchAll: params =>
			axios.get(`/alert-word`, { params }).then(res => res.data),
		delete: id => axios.delete(`/alert-word/${id}`).then(res => res.data),
		update: (id, alertword) =>
			axios.put(`/alert-word/${id}`, alertword).then(res => res.data),
		submit: alertword =>
			axios.post(`/alert-word`, alertword).then(res => res.data)
	}
};
