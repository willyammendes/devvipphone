import axios from "../services/api";

export default {
  page: {
    fetchAll: params => axios.get(`/page`, { params }).then(res => res.data),
    delete: id => axios.delete(`/page/${id}`).then(res => res.data),
    update: (id, page) => axios.put(`/page/${id}`, page).then(res => res.data),
    submit: page => axios.post(`/page`, page).then(res => res.data)
  }
};
