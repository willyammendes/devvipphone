import axios from "../services/api";

export default {
  extraInput: {
    fetchAll: params =>
      axios.get(`/extra-input`, { params }).then(res => res.data),
    delete: id => axios.delete(`/extra-input/${id}`).then(res => res.data),
    update: (id, extraInput) =>
      axios.put(`/extra-input/${id}`, extraInput).then(res => res.data),
    submit: extraInput =>
      axios.post(`/extra-input`, extraInput).then(res => res.data)
  }
};
