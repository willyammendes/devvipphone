import axios from "../services/api";

export default {
  media: {
    fetchAll: (params) =>
      axios.get(`/social/whatsapp`, { params }).then((res) => res.data),
    monitor: (params) =>
      axios.get(`/social/whatsapp/monitor`, { params }).then((res) => res.data),
    fetchData: (params) =>
      axios.get(`/social/whatsapp`, { params }).then((res) => res.data.data),
    delete: (id) =>
      axios.delete(`/social/whatsapp/${id}`).then((res) => res.data),
    update: (id, media) =>
      axios.put(`/social/whatsapp/${id}`, media).then((res) => res.data),
    submit: (media) =>
      axios.post(`/social/whatsapp`, media).then((res) => res.data),
    createQrCode: () =>
      axios.post(`/social/whatsapp/createQrCode`, {}).then((res) => res.data),
    disconnect: (id) =>
      axios.post(`/social/disconnect/${id}`).then((res) => res.data),
    reconnect: (id) =>
      axios.post(`/social/reconnect/${id}`).then((res) => res.data),
  },
};
