import axios from "../services/api";

export default {
	parameter: {
		fetchAll: () => axios.get("/parameter").then(res => res.data),
		fetchData: params =>
			axios.get(`/parameter`, { params }).then(res => res.data),
		update: (id, parameter) =>
			axios.put(`/parameter/${id}`, parameter).then(res => res.data)
	}
};
