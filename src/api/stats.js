import axios from "../services/api";

export default {
	  openTickets: {
		  fetchAll: params =>	axios.get(`/statistic/openTickets`, { params }).then(res => res.data.count)
    },
    assignedTickets: {
      fetchAll: params => axios.get(`/statistic/assignedTickets`, { params }).then(res => res.data.count)
    },
    assignedTicketsByBot: {
		  fetchAll: params => axios.get(`/statistic/assignedTicketsByBot`, { params }).then(res => res.data.count)
    },
    waitingTicket: {
		  fetchAll: params => axios.get(`/statistic/waitingTicket`, { params }).then(res => res.data.count)
    },
    closedTickets: {
      fetchAll: params => axios.get(`/statistic/closedTickets`, { params }).then(res => res.data.count)
    },
    closedTicketsByBot: {
      fetchAll: params => axios.get(`/statistic/closedTicketsByBot`, { params }).then(res => res.data.count)
    },
    statistic: {
      fetchAll: params => axios.get(`/statistic/activities`, { params }).then(res => res)
    },
    total: {
      fetchAll: params => axios.get(`/ticket-report`, {params}).then(res => res)
    }
};
