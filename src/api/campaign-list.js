import axios from "../services/api";

export default {
	campaignList: {
		fetchAll: params =>
			axios
				.get(`/statistic/campaignList`, { params })
				.then(res => res.data),
		delete: id =>
			axios.delete(`/statistic/campaignList/${id}`).then(res => res.data),
		update: (id, campaignList) =>
			axios
				.put(`/statistic/campaignList/${id}`, campaignList)
				.then(res => res.data),
		submit: campaignList =>
			axios
				.post(`/statistic/campaignList`, campaignList)
				.then(res => res.data)
	}
};
