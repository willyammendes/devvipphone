import axios from "../services/api";

export default {
  conversations: {
    getStats: (conversationId) =>
      axios.get(`/conversation/${conversationId}/stats`),
    get: (conversationId) => axios.get(`/conversation/${conversationId}`),
    fetchAll: (params) =>
      axios.get(`/conversation`, { params }).then((res) => res.data.data),
    fetchTickets: (conversationId, take = 30, skip = 0) =>
      axios
        .get(
          `/conversation/${conversationId}/tickets?take=${take}&skip=${skip}`
        )
        .then((res) => res.data),

    fetchTicketsTake: (conversationId, take = 20, skip = 0) =>
      axios.get(
        `/conversation/${conversationId}/tickets?take=${take}&skip=${skip}`
      ),
    resendMessage: (message) =>
      axios
        .post(
          `/conversation/${message.conversation_id}/resendMessage/${message.id}`
        )
        .then((res) => res.data),
    fetchComments: (conversationId) =>
      axios
        .get(`/conversation/${conversationId}/tickets?take=50`)
        .then((res) => res.data),
    archiveConversation: (id, conversations) =>
      axios
        .put(`/conversation/${id}/archive`, conversations)
        .then((res) => res.data),
    fetchMessages: (conversationId, take = 50, skip = 0) =>
      axios.get(
        `/conversation/${conversationId}/messages?take=${take}&skip=${skip}`
      ),
    update: (conversation) =>
      axios
        .put(`/conversation/${conversation.id}`, conversation)
        .then((res) => res.data),
    fetchMessagesTake: (conversationId, take = 50, skip = 0) =>
      axios.get(
        `/conversation/${conversationId}/messages?take=${take}&skip=${skip}`
      ),
    sendTextMessage: (conversationId, message) =>
      axios
        .post(`/conversation/${conversationId}/message`, message)
        .then((res) => res.data.data),
    sendImageMessage: (conversationId, message) =>
      axios
        .post(`/conversation/${conversationId}/imageMessage`, message)
        .then((res) => res.data.data),
    sendDocumentMessage: (conversationId, message) =>
      axios
        .post(`/conversation/${conversationId}/documentMessage`, message)
        .then((res) => res.data.data),
    sendVoiceMessage: (conversationId, message) =>
      axios
        .post(`/conversation/${conversationId}/voiceMessage`, message)
        .then((res) => res.data.data),
    sendComment: (comment) =>
      axios.post(`/comment`, comment).then((res) => res.data),

    enableAutoReply: (conversationId, autoReply) =>
      axios
        .post(`chat/conversation/${conversationId}/autoReply`, autoReply)
        .then((res) => res.data.data),
    sendNewConversation: (message) =>
      axios.post(`/chat/message`, message).then((res) => res.data.data),
  },
};
