import axios from "../services/api";

export default {
	userProductivityTable: {
		fetchAll: params =>
			axios
				.get(`/statistic/userProductivityTable`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/userProductivityTable/${id}`)
				.then(res => res.data),
		update: (id, userProductivityTable) =>
			axios
				.put(
					`/statistic/userProductivityTable/${id}`,
					userProductivityTable
				)
				.then(res => res.data),
		submit: userProductivityTable =>
			axios
				.post(`/statistic/userProductivityTable`, userProductivityTable)
				.then(res => res.data)
	}
};
