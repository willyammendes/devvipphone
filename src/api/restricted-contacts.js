import axios from "../services/api";

export default {
	restrictedcontact: {
		fetchAll: params =>
			axios
				.get(`/contact/listRestrictContact`, { params })
				.then(res => res.data),
		delete: id =>
			axios.post(`/contact/restricted/${id}`).then(res => res.data),
		update: (id, restrictedcontact) =>
			axios
				.put(`/contact/restricted/${id}`, restrictedcontact)
				.then(res => res.data),
		submit: restrictedcontact =>
			axios
				.post(`/contact/restricted`, restrictedcontact)
				.then(res => res.data),
		deletesubmit: restrictedcontact =>
			axios
				.post(`/contact/restricted`, restrictedcontact)
				.then(res => res.data)
	}
};
