import axios from "../services/api";

export default {
  user: {
    login: (credentials) =>
      axios
        .post("/auth/login", {
          email: credentials.email,
          password: credentials.password,
        })
        .then((res) => res.data.user),
    logout: () => axios.post("/auth/logout"),
    get: () => axios.get("/user"),
    submitPause: (user) =>
      axios.post(`/user/pause/${user.id}`, user).then((res) => res.data),
    getPermissions: (user) =>
      axios.get(`/user/${user.id}/permissions`).then((res) => res.data),
    getCurrentPause: () =>
      axios.get(`/user/getCurrentPause`).then((res) => res.data),
    forgotPassword: (user) =>
      axios.post(`/auth/requestPasswordResetLink`, user).then((res) => res),
    resetPassword: (params, user) =>
      axios.post(`/auth/resetPassword/${params}`, user).then((res) => res),
  },
};
