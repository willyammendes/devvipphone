import axios from "../services/api";

export default {
	permission: {
		fetchAll: params =>
			axios.get(`/permission`, { params }).then(res => res.data),
		fetchPermission: params =>
			axios.get(`/permission`, { params }).then(res => res.data),
		delete: (id, permission) =>
			axios.delete(`/permission/${id}`, permission).then(res => res.data),
		update: (id, permission) =>
			axios.put(`/permission/${id}`, permission).then(res => res.data),
		submit: permission =>
			axios.post(`/permission`, permission).then(res => res.data)
	}
};
