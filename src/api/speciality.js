import axios from "../services/api";

export default {
	speciality: {
		fetchAll: params =>
			axios.get(`/speciality`, { params }).then(res => res.data),
		delete: id => axios.delete(`/speciality/${id}`).then(res => res.data),
		update: (id, speciality) =>
			axios.put(`/speciality/${id}`, speciality).then(res => res.data),
		submit: speciality =>
			axios.post(`/speciality`, speciality).then(res => res.data)
	}
};
