import axios from "../services/api";

export default {
	place: {
		fetchAll: params =>
			axios.get(`/place`, { params }).then(res => res.data),
		delete: id => axios.delete(`/place/${id}`).then(res => res.data),
		update: (id, place) =>
			axios.put(`/place/${id}`, place).then(res => res.data),
		submit: place => axios.post(`/place`, place).then(res => res.data)
	}
};
