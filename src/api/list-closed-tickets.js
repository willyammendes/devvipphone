import axios from "../services/api";

export default {
	listClosedTickets: {
		fetchAll: params =>
			axios
				.get(`/statistic/listClosedTickets`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/listClosedTickets/${id}`)
				.then(res => res.data),
		update: (id, listClosedTickets) =>
			axios
				.put(`/statistic/listClosedTickets/${id}`, listClosedTickets)
				.then(res => res.data),
		submit: listClosedTickets =>
			axios
				.post(`/statistic/listClosedTickets`, listClosedTickets)
				.then(res => res.data)
	}
};

