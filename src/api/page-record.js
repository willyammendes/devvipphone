import axios from "../services/api";

export default {
  pageRecord: {
    fetchAll: (page_id, params) =>
      axios.get(`/page/${page_id}/record`, { params }).then(res => res.data),
    delete: (page_id, id) =>
      axios.delete(`/page/${page_id}/record/${id}`).then(res => res.data),
    update: (page_id, id, record) =>
      axios.put(`/page/${page_id}/record/${id}`, record).then(res => res.data),
    submit: (page_id, record) =>
      axios.post(`/page/${page_id}/record`, record).then(res => res.data)
  }
};
