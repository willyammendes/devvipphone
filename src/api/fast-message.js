import axios from "../services/api";

export default {
	fastmessage: {
		fetchAll: params =>
			axios.get(`/fast-message`, { params }).then(res => res.data),
		fetchOrigin: params =>
			axios.get(`/origin-fast-message`, { params }).then(res => res.data),
		fetchData: params =>
			axios.get(`/fast-message`, { params }).then(res => res.data.data),
		fetchDataTake: (take = 100, skip = 0) =>
			axios
				.get(`/fast-message/?take=${take}&skip=${skip}`)
				.then(res => res.data.data),
		delete: id => axios.delete(`/fast-message/${id}`).then(res => res.data),
		update: (id, fastmessage) =>
			axios.put(`/fast-message/${id}`, fastmessage).then(res => res.data),
		submit: fastmessage =>
			axios.post(`/fast-message`, fastmessage).then(res => res.data)
	}
};
