import axios from "../services/api";

export default {
	listAssignedTickets: {
		fetchAll: params =>
			axios
				.get(`/statistic/listAssignedTickets`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/listAssignedTickets/${id}`)
				.then(res => res.data),
		update: (id, listAssignedTickets) =>
			axios
				.put(
					`/statistic/listAssignedTickets/${id}`,
					listAssignedTickets
				)
				.then(res => res.data),
		submit: listAssignedTickets =>
			axios
				.post(`/statistic/listAssignedTickets`, listAssignedTickets)
				.then(res => res.data)
	}
};
