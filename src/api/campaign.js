import axios from "../services/api";

export default {
	campaign: {
		fetchAll: params =>
			axios.get(`/campaing`, { params }).then(res => res.data),
		fetchMessage: (id, params) =>
			axios
				.get(`/campaing/${id}/messages`, { params })
				.then(res => res.data),
		delete: id => axios.delete(`/campaing/${id}`).then(res => res.data),
		update: (id, campaign) =>
			axios.put(`/campaing/${id}`, campaign).then(res => res.data),
		submit: campaign =>
			axios.post(`/campaing`, campaign).then(res => res.data)
	}
};
