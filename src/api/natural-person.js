import axios from "../services/api";

export default {
	naturalPerson: {
		fetchAll: params =>
			axios
				.get(`/statistic/naturalPerson`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/naturalPerson/${id}`)
				.then(res => res.data),
		update: (id, naturalPerson) =>
			axios
				.put(`/statistic/naturalPerson/${id}`, naturalPerson)
				.then(res => res.data),
		submit: naturalPerson =>
			axios
				.post(`/statistic/naturalPerson`, naturalPerson)
				.then(res => res.data)
	}
};
