import axios from "../services/api";

export default {
	badword: {
		fetchAll: params =>
			axios.get(`/bad-word`, { params }).then(res => res.data),
		delete: id => axios.delete(`/bad-word/${id}`).then(res => res.data),
		update: (id, badword) =>
			axios.put(`/bad-word/${id}`, badword).then(res => res.data),
		submit: badword =>
			axios.post(`/bad-word`, badword).then(res => res.data)
	}
};
