import axios from "../services/api";

export default {
	assignedByBotTable: {
		fetchAll: params =>
			axios
				.get(`/statistic/assignedByBotTable`, { params })
				.then(res => res.data),
		delete: id =>
			axios
				.delete(`/statistic/assignedByBotTable/${id}`)
				.then(res => res.data),
		update: (id, assignedByBotTable) =>
			axios
				.put(`/statistic/assignedByBotTable/${id}`, assignedByBotTable)
				.then(res => res.data),
		submit: assignedByBotTable =>
			axios
				.post(`/statistic/assignedByBotTable`, assignedByBotTable)
				.then(res => res.data)
	}
};
