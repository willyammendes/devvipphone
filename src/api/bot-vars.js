import axios from "../services/api";

export default {
	variable: {
		fetchAll: params =>
			axios.get(`/bot-var`, { params }).then(res => res.data),
		delete: id => axios.delete(`/bot-var/${id}`).then(res => res.data),
		update: (id, variable) =>
			axios.put(`/bot-var/${id}`, variable).then(res => res.data),
		submit: variable =>
			axios.post(`/bot-var`, variable).then(res => res.data)
	}
};
