import axios from "../services/api";

export default {
	server: {
		fetchAll: params =>
			axios.get(`/server`, { params }).then(res => res.data),
		delete: id => axios.delete(`/server/${id}`).then(res => res.data),
		update: (id, server) =>
			axios.put(`/server/${id}`, server).then(res => res.data),
		submit: server => axios.post(`/server`, server).then(res => res.data)
	}
};
