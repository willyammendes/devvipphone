import axios from "../services/api";

export default {
  plan: {
    fetchAll: (params) =>
      axios.get(`/pricing-plan`, { params }).then((res) => res.data.data),
  },
};
