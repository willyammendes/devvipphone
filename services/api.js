import axios from "axios";
import { config } from "../app/config/config";

axios.defaults.baseURL = config.apiRootUrl;

axios.interceptors.request.use(request => {
	request.headers["Authorization"] = "Bearer " + localStorage.token;
	return request;
});

export default axios;
